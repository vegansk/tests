#include <iostream>
#include <exception>
#include <boost/network/protocol/http/server.hpp>
#include <boost/network/protocol/http.hpp>

namespace http = boost::network::http;

struct handler;
typedef http::server<handler> http_server;

struct handler {
    void operator ()(const http_server::request &req, http_server::response &rsp) {
        rsp = http_server::response::stock_reply(http_server::response::ok, "Hello, world!");
    }

    void log(const http_server::string_type &info) {
        std::cerr << "ERROR: " << info << std::endl;
    }
};

int main(int argc, char *argv[]) {
    handler handler_;

    http_server::options options(handler_);

    try {
        http_server server(
                    options.address("0.0.0.0")
                    .port("8081")
                    );

        server.run();
    } catch (std::exception &e) {
        std::cerr << "ERROR: " << e.what() << std::endl;
    }

    return 0;
}
