#include "httpproxy.h"

#include <QCoreApplication>
#include <QTimer>

#include <Tufao/HttpServer>
#include <Tufao/HttpServerResponse>


int main(int argc, char *argv[]) {
    QCoreApplication app(argc, argv);

    Tufao::HttpServer server;

    QObject::connect(&server, &Tufao::HttpServer::requestReady, [](Tufao::HttpServerRequest &, Tufao::HttpServerResponse &rsp){
        rsp.writeHead(200, "OK");
        rsp.end("Hello, world!\n");
    });

    server.listen(QHostAddress::LocalHost, 8080);

    HttpProxy p(3000, 8080, "eldis.ru", 80);

    QObject::connect(&p, &HttpProxy::finished, &app, &QCoreApplication::quit);

    QTimer::singleShot(0, &p, SLOT(run()));

    return app.exec();
}
