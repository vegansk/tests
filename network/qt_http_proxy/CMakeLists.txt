set(APP_NAME qt_http_proxy)

find_package(Qt5Core)
find_package(Qt5Network)

aux_source_directory(. SRC)

include_directories("${TUFAO_INCLUDE_DIR}")
add_executable(${APP_NAME} ${SRC})
add_dependencies(${APP_NAME} ext_tufao)

target_link_libraries(${APP_NAME} Qt5::Core Qt5::Network ${TUFAO_LIBRARIES})
