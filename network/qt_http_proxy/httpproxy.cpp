#include "httpproxy.h"

#include <QTcpServer>
#include <QTcpSocket>

#include <string>
#include <algorithm>

namespace {
const char * const CLIENT_ID = "clientId";
const char * const LOCAL_SOCKET = "localSocket";
}

HttpProxy::HttpProxy(quint16 srcPort, quint16 localPort, const QString &dstHost, quint16 dstPort, QObject *parent) :
    QObject(parent), _srcPort(srcPort), _localPort(localPort), _dstHost(dstHost), _dstPort(dstPort), _server(nullptr) {
}


void HttpProxy::run() {
    Q_ASSERT(nullptr == _server);

    _server = new QTcpServer(this);

    if(!_server->listen(QHostAddress::LocalHost, _srcPort)) {
        qCritical() << tr("Error listening port %1, reason: %2").arg(_srcPort).arg(_server->errorString());
        emit finished();
        return;
    }

    qDebug() << tr("Start http proxy for %1:%2 on port %3").arg(_dstHost).arg(_dstPort).arg(_srcPort);

    connect(_server, &QTcpServer::newConnection, [=]() {
        newConnection(_server->nextPendingConnection());
    });
}

void HttpProxy::newConnection(QTcpSocket *s) {
    auto clientId = tr("Client %1:%2").arg(s->peerAddress().toString()).arg(s->peerPort());
    qDebug() << tr("%1. Got connection").arg(clientId);

    auto d = new QTcpSocket(s);
    d->setProperty(CLIENT_ID, clientId);

    // Setup error handlers
    connect(d, static_cast<void (QTcpSocket::*)(QAbstractSocket::SocketError)>(&QTcpSocket::error),
    [=](QAbstractSocket::SocketError err) {
        if(QAbstractSocket::RemoteHostClosedError != err) {
            qWarning() << tr("%1. Error in destination socket: %2 (%3)")
                       .arg(d->property(CLIENT_ID).toString())
                       .arg(d->errorString())
                       .arg(err);
        } else {
            qDebug() << tr("%1. Close connection").arg(d->property(CLIENT_ID).toString());
        }
        d->disconnectFromHost();
        s->disconnectFromHost();
    });
    connect(s, static_cast<void (QTcpSocket::*)(QAbstractSocket::SocketError)>(&QTcpSocket::error),
    [=](QAbstractSocket::SocketError err) {
        if(QAbstractSocket::RemoteHostClosedError != err) {
            qWarning() << tr("%1. Error in source socket: %2 (%3)")
                       .arg(d->property(CLIENT_ID).toString())
                       .arg(d->errorString())
                       .arg(err);
        } else {
            qDebug() << tr("%1. Close connection").arg(s->property(CLIENT_ID).toString());
        }
        d->disconnectFromHost();
        s->disconnectFromHost();
    });

    d->connectToHost(_dstHost, _dstPort);

    // Setup read handler for source socket
    connect(s, &QTcpSocket::readyRead, [=]() {
        auto data = s->readAll();
        if(!handleLocalRequest(data, s)) {
            qDebug() << "Remote:";
            qDebug() << QString(data);
            d->write(data);
        }
    });

    // Setup read handler for destination socket
    connect(d, &QTcpSocket::readyRead, [=]() {
        auto data = d->readAll();
        s->write(data);
    });

    // Setup disconnect handlers
    connect(s, &QTcpSocket::disconnected, [=]() {
        s->deleteLater();
    });
    connect(d, &QTcpSocket::disconnected, [=]() {
        s->disconnectFromHost();
        d->deleteLater();
    });
}

bool HttpProxy::handleLocalRequest(const QByteArray &data, QTcpSocket *s) {
    if(!isLocalRequest(data)) {
        return false;
    }

    qDebug() << "Local:";
    qDebug() << QString(data);

    auto d = s->property(LOCAL_SOCKET).value<QTcpSocket*>();
    if(!d || !d->isValid()) {
        d = new QTcpSocket(s);
        d->connectToHost(QHostAddress::LocalHost, _localPort);
        s->setProperty(LOCAL_SOCKET, QVariant::fromValue(d));
        connect(d, &QTcpSocket::disconnected, [=](){
            s->setProperty(LOCAL_SOCKET, QVariant::fromValue(static_cast<QTcpSocket*>(nullptr)));
            d->deleteLater();
        });
    }

    connect(d, &QTcpSocket::readyRead, [=]() {
        auto data = d->readAll();
        s->write(data);
    });

    d->write(data);

    return true;
}

bool HttpProxy::isLocalRequest(const QByteArray &data) {
    size_t maxSize = data.length() > 300 ? 300 : data.length();
    auto begin = std::find(data.constBegin(), data.constBegin() + maxSize, ' ');
    if(begin++ == data.constBegin() + maxSize) {
        return false;
    }
    auto end = std::find(begin, data.constBegin() + maxSize, ' ');
    if(end-- == data.constBegin() + maxSize) {
        return false;
    }

    std::string url(begin, end);
    return url.find("/local/") == 0;
}
