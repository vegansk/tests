#ifndef HTTPPROXY_H
#define HTTPPROXY_H

#include <QObject>

class QTcpServer;
class QTcpSocket;
class QByteArray;

class HttpProxy : public QObject
{
    Q_OBJECT
public:
    explicit HttpProxy(quint16 srcPort, quint16 localPort, const QString &dstHost, quint16 dstPort, QObject *parent = 0);

signals:
    void finished();

public slots:
    void run();

private slots:
    void newConnection(QTcpSocket *s);

private:
    quint16 _srcPort;
    quint16 _localPort;
    quint16 _dstPort;
    QString _dstHost;

    QTcpServer *_server;

    bool handleLocalRequest(const QByteArray &data, QTcpSocket *s);
    bool isLocalRequest(const QByteArray &data);
};

#endif // HTTPPROXY_H
