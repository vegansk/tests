#include <exception>
#include <iostream>
#include <sstream>
#include <vector>
#include <algorithm>
#include <Poco/Net/StreamSocket.h>

#ifdef __GNUG__
#include <cstdlib>
#include <memory>
#include <cxxabi.h>

std::string demangle(const char* name) {

  int status = -4; // some arbitrary value to eliminate the compiler warning

  // enable c++11 by passing the flag -std=c++11 to g++
  std::unique_ptr<char, void(*)(void*)> res {
    abi::__cxa_demangle(name, NULL, NULL, &status),
      std::free
  };

  return (status==0) ? res.get() : name ;
}

#else

// does nothing if not g++
std::string demangle(const char* name) {
  return name;
}
#endif

using namespace Poco::Net;

void sendBytes(StreamSocket& s, const void* bytes, int len) {
  auto data = reinterpret_cast<const char*>(bytes);
  while(len > 0) {
    auto n = s.sendBytes(bytes, len);
    if(n < 0) {
      std::cerr << "ERROR" << std::endl;
      return;
    }
    data += n;
    len -= n;
  }
}

class HttpLengthCalculator {
  private:
    size_t _contentLength = -1;
    size_t _currentLength = -1;
    bool _header = true;
    bool _chunked = false;
    std::vector<char> _buffer;

  public:
    HttpLengthCalculator() {
    }

    bool onReceive(const char* data, size_t len) {
      if(!_header) {
        _currentLength += len;
        if(_chunked)
          checkChunks(data, len);
        return !finished();
      }

      _buffer.insert(_buffer.end(), data, data + len);
      std::string hEnd("\r\n\r\n");
      auto it = std::search(_buffer.begin(), _buffer.end(), hEnd.begin(), hEnd.end());
      if(it == _buffer.end())
        return true;  // Did not find the header's end
      _header = false;

      _currentLength = std::distance(it + hEnd.length(), _buffer.end());

      std::string cLen = header("Content-Length");
      if(cLen.empty()) {
        if(header("Transfer-Encoding") == "chunked") {
          _chunked = true;
          checkChunks(data, len);
          return !finished();
        }
        return true;
      }

      std::istringstream ss(cLen);
      ss >> _contentLength;

      return !finished();
    }

    size_t contentLength() const {
      return _contentLength;
    }

    size_t currentLength() const {
      return _currentLength;
    }

    bool finished() const {
      return _contentLength != -1 && _currentLength >= _contentLength;
    }

    std::string header(const std::string& name) const {
      if(_header)
        return "";

      std::string h(name);
      h.append(":");

      auto it = std::search(_buffer.begin(), _buffer.end(), h.begin(), h.end());
      if(it == _buffer.end())
        return "";

      std::string v(it + h.length(), std::find(it, _buffer.end(), '\r'));
      v.erase(v.begin(), std::find_if(v.begin(), v.end(), std::not1(std::ptr_fun<int, int>(std::isspace))));

      return v;
    }

  private:
    void checkChunks(const char* data, size_t len) {
      std::string end("0\r\n\r\n");
      if(len >= end.size() && std::equal(end.begin(), end.end(), data + len - end.size()))
        _contentLength = _currentLength;
    }
};

int main() {
  try {
    SocketAddress sa("eldis.ru", 80);
    StreamSocket s;

    s.connect(sa);
    //s.setBlocking(false);
    s.setReceiveTimeout(Poco::Timespan(60, 0));
    std::string msg = "GET / HTTP/1.1\r\nHost: eldis.ru\r\nConnection:keep-alive\r\n\r\n";
    sendBytes(s, msg.c_str(), msg.length());

    char buff[1001];
    HttpLengthCalculator lc;
    while(true) {
      auto len = s.receiveBytes(buff, 1000);
      buff[len < 0 ? 0 : len] = 0;
      if(len >= 0)
        lc.onReceive(buff, len);
      std::cout << buff;
      if(len <= 0 || lc.finished())
        break;
    }

  } catch(Poco::TimeoutException&) {
    return 0;
  } catch(std::exception& ex) {
    std::cerr << demangle(typeid(ex).name()) << ": " << ex.what() << std::endl;
    return -1;
  }

  return 0;
}
