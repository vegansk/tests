#include "httpclient.h"

#include <QCoreApplication>
#include <QTimer>

int main(int argc, char *argv[]) {
    QCoreApplication app(argc, argv);

    HttpClient c;

    QObject::connect(&c, &HttpClient::finished, &app, &QCoreApplication::quit);

    QTimer::singleShot(0, &c, SLOT(run()));

    return app.exec();
}

