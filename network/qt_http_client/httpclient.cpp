#include "httpclient.h"
#include <QDebug>

#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QAuthenticator>

HttpClient::HttpClient(QObject *parent) :
    QObject(parent), mgr(new QNetworkAccessManager(this)) {
}

void HttpClient::run() {
    qDebug() << "HttpClient started";

    QNetworkReply *r = mgr->get(QNetworkRequest(QUrl("http://localhost:3000/basic_auth/test.txt")));

    connect(mgr, &QNetworkAccessManager::authenticationRequired, [=](QNetworkReply *, QAuthenticator *auth) {
        auth->setUser("testUser");
        auth->setPassword("testPassword");
    });

    connect(r, &QNetworkReply::readyRead, [=]() {
        qDebug() << "Data:";
        qDebug() << r->readAll();
    });

    connect(r, &QNetworkReply::finished, [=]() {
        if(r->error())
            qCritical() << "Error loading page: " << r->errorString();
        else
            qDebug() << "Data transferred!";
        r->deleteLater();
        setFinished();
    });

}

void HttpClient::setFinished() {
    qDebug() << "HttpClient stopped";
    emit finished();
}
