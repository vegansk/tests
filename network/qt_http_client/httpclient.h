#ifndef HTTPCLIENT_H
#define HTTPCLIENT_H

#include <QObject>

class QNetworkAccessManager;

class HttpClient : public QObject
{
    Q_OBJECT
public:
    explicit HttpClient(QObject *parent = 0);

signals:
    void finished();

public slots:
    void run();

private:
    QNetworkAccessManager *mgr;

    void setFinished();
};

#endif // HTTPCLIENT_H
