#ifndef TCPTUNNEL_H
#define TCPTUNNEL_H

#include <QObject>

class QTcpServer;

class TcpTunnel : public QObject
{
    Q_OBJECT
public:
    explicit TcpTunnel(quint16 srcPort, const QString &dstHost, quint16 dstPort, QObject *parent = 0);

signals:

public slots:
    void run();

private:
    quint16 _srcPort;
    quint16 _dstPort;
    QString _dstHost;

    QTcpServer *_server;
};

#endif // TCPTUNNEL_H
