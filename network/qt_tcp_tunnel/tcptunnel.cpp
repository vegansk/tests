#include "tcptunnel.h"
#include <QTcpServer>
#include <QTcpSocket>

TcpTunnel::TcpTunnel(quint16 srcPort, const QString &dstHost, quint16 dstPort, QObject *parent) :
    QObject(parent), _srcPort(srcPort), _dstPort(dstPort), _dstHost(dstHost), _server(nullptr) {
}

void TcpTunnel::run() {
    Q_ASSERT(nullptr == _server);

    _server = new QTcpServer(this);
    if(!_server->listen(QHostAddress::LocalHost, _srcPort)) {
        qCritical() << "Error creating server:" << _server->errorString();
    }

    connect(_server, &QTcpServer::newConnection, [=]() {
        auto src = _server->nextPendingConnection();
        auto dst = new QTcpSocket(src);

        dst->connectToHost(_dstHost, _dstPort);

        connect(src, &QTcpSocket::disconnected, [=]() {
            qDebug() << "Src disconnected";
            src->deleteLater();
            dst->disconnectFromHost();
        });

        connect(dst, &QTcpSocket::disconnected, [=]() {
            qDebug() << "Dst disconnected";
            src->disconnectFromHost();
        });

        connect(src, &QTcpSocket::readyRead, [=]() {
            auto data = src->readAll();
            qDebug() << "Src:";
            qDebug() << QString(data);
            dst->write(data);
        });

        connect(dst, &QTcpSocket::readyRead, [=]() {
            auto data = dst->readAll();
            qDebug() << "Dst:";
            qDebug() << QString(data);
            src->write(data);
        });
    });
}
