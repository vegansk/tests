#include "tcptunnel.h"

#include <QCoreApplication>
#include <QCommandLineParser>
#include <QCommandLineOption>
#include <QDebug>
#include <QTimer>

int main(int argc, char *argv[]) {
    QCoreApplication app(argc, argv);

    QCommandLineParser p;
    p.addHelpOption();
    QCommandLineOption src(QStringList() << "s" << "source", "Source [host:]port", "source", "3000");
    p.addOption(src);

    p.process(app);

    quint16 srcPort = p.value(src).toUShort();
    auto args = p.positionalArguments();
    if(args.length() < 1) {
        qCritical() << "Destination not set";
        return 1;
    }

    QString dstHost("localhost");
    quint16 dstPort(0);
    auto sp = args[0].split(":");
    if(sp.length() > 1) {
        dstHost = sp[0];
        dstPort = sp[1].toUShort();
    } else {
        dstPort = args[0].toUShort();
    }

    qDebug() << "Src port:" << srcPort << ", Dst host:" << dstHost << ", Dst port:" << dstPort;

    TcpTunnel t(srcPort, dstHost, dstPort);

    QTimer::singleShot(0, &t, SLOT(run()));

    return app.exec();
}
