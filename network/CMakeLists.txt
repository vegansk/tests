set(TUFAO_CMAKE_ARGS -DCMAKE_INSTALL_PREFIX:PATH=${DEPENDENCIES_DIR}
                     -DTUFAO_CMAKE_MODULE_DIR=${DEPENDENCIES_DIR}
                     -DTUFAO_MKSPECS_MODULE_DIR=${DEPENDENCIES_DIR}
                     -DCMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE})

ExternalProject_Add(ext_tufao
    PREFIX ${DEPENDENCIES_DIR}/tufao
    GIT_REPOSITORY https://github.com/vegansk/tufao
    CMAKE_ARGS ${TUFAO_CMAKE_ARGS}
    BUILD_IN_SOURCE 1
)

set(QJSON_CMAKE_ARGS -DCMAKE_INSTALL_PREFIX:PATH=${DEPENDENCIES_DIR}
                     -DCMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE})

ExternalProject_Add(ext_qjson
    PREFIX ${DEPENDENCIES_DIR}/qjson
    GIT_REPOSITORY https://github.com/flavio/qjson
    CMAKE_ARGS ${QJSON_CMAKE_ARGS}
    BUILD_IN_SOURCE 1
)

set(CPPNETLIB_CMAKE_ARGS -DCMAKE_INSTALL_PREFIX:PATH=${DEPENDENCIES_DIR}
                         -DCMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE}
                         -DCPP-NETLIB_BUILD_SHARED_LIBS:BOOL=ON)

ExternalProject_Add(ext_cpp-netlib
    PREFIX ${DEPENDENCIES_DIR}/cpp-netlib
    URL http://commondatastorage.googleapis.com/cpp-netlib-downloads/0.11.0/cpp-netlib-0.11.0.tar.bz2
    CMAKE_ARGS ${CPPNETLIB_CMAKE_ARGS}
    BUILD_IN_SOURCE 1
)

set(TUFAO_INCLUDE_DIR ${DEPENDENCIES_DIR}/include/tufao-1)
set(TUFAO_LIBRARIES tufao1)

set(QJSON_INCLUDE_DIR ${DEPENDENCIES_DIR}/include)
set(QJSON_LIBRARIES qjson)

set(CPPNETLIB_INCLUDE_DIR ${DEPENDENCIES_DIR}/include)
set(CPPNETLIB_LIBRARIES cppnetlib-client-connections cppnetlib-server-parsers cppnetlib-uri)

add_subdirectory(qt_http_client)
add_subdirectory(qt_echo_server)
add_subdirectory(qt_tufao_server)
add_subdirectory(qt_tcp_tunnel)
add_subdirectory(qt_http_proxy)
add_subdirectory(qt_tufao_proxy)
add_subdirectory(qt_poco_proxy)
add_subdirectory(boost_http_server)
add_subdirectory(qt_netmon)
add_subdirectory(poco_socket)
add_subdirectory(poco_send_multipart)

