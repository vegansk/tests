#ifndef ECHOSERVER_H
#define ECHOSERVER_H

#include <QObject>

class EchoServerPrivate;

class EchoServer : public QObject {
    Q_OBJECT
public:
    explicit EchoServer(const QString& host, quint16 port, QObject *parent = 0);
    ~EchoServer();
signals:
    void shutdown();

public slots:
    void start();

private:
    QScopedPointer<EchoServerPrivate> d_ptr;
    Q_DECLARE_PRIVATE(EchoServer)
};

#endif // ECHOSERVER_H
