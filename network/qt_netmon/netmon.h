#ifndef NETMON_H
#define NETMON_H

namespace funpp {

struct always_eq_t {};

template<typename T> bool operator == (const T &, always_eq_t) {
    return true;
}

template<typename T> bool operator == (always_eq_t, const T &) {
    return true;
}

constexpr always_eq_t _;

}

#define log qDebug

template<typename... Args> void logFatal(const QString& f, Args... v) {
    qWarning(f.toUtf8(), v...);
    std::exit(1);
}

#endif // NETMON_H
