#ifndef _QT_NETMON_STDAFX_H
#define _QT_NETMON_STDAFX_H

#include <iostream>
#include <tuple>
#include <limits>

#include <QObject>
#include <QString>
#include <QtDebug>
#include <QTimer>
#include <QHostInfo>
#include <QRegularExpression>
#include <QCommandLineOption>
#include <QCommandLineParser>
#include <QCoreApplication>
#include <QTcpServer>
#include <QTcpSocket>

#endif//_QT_NETMON_STDAFX_H
