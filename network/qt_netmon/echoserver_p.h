#ifndef ECHOSERVER_P_H
#define ECHOSERVER_P_H

#include "stdafx.h"
#include "netmon.h"

class EchoServerPrivate : public QObject {
private:
    Q_OBJECT

    EchoServer *q_ptr;
    Q_DECLARE_PUBLIC(EchoServer)

    QString host;
    quint16 port;

    QTcpServer *srv;

public:
    EchoServerPrivate(const QString &host_, quint16 port_, EchoServer *parent) :
        QObject(parent), host(host_), port(port_), q_ptr(parent) {}

    void start() {
        Q_ASSERT(srv == nullptr);
        srv = new QTcpServer(this);
        if(!srv->listen(QHostAddress(host), port)) {
            logFatal(tr("Got an error when trying to listen %s:%u: %s"),
                     host.toUtf8().data(),
                     (uint)port,
                     srv->errorString().toUtf8().data());
        }

        connect(srv, &QTcpServer::acceptError, [=](){
            log("QTcpServer::acceptError: %s", srv->errorString().toUtf8().data());
        });

        connect(srv, &QTcpServer::newConnection, [=](){
            clientConnect(srv->nextPendingConnection());
        });
    }

    void clientConnect(QTcpSocket* s) {

        connect(s, (void (QTcpSocket::*)(QAbstractSocket::SocketError))&QTcpSocket::error,[=](){
            log("QTcpSocket::error: %s", s->errorString().toUtf8().data());
        });

        connect(s, &QTcpSocket::readyRead, [=](){
            s->write(s->readAll());
        });
    }
};

#endif // ECHOSERVER_P_H
