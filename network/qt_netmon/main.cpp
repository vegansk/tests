﻿#include "stdafx.h"

#include "netmon.h"

#include "echoserver.h"

namespace {

struct Params {
    bool server;
    QString host;
    quint16 port;
};

inline quint16 toPort(const QString& s) {
    bool ok;
    auto r = s.toUInt(&ok);
    if(!r)
        logFatal(QObject::tr("Cannot convert \"%s\" to port number!"), s.toUtf8().data());
    if(r > std::numeric_limits<quint16>::max())
        logFatal(QObject::tr("Port number cannot be bigger than %d!"), static_cast<int>(std::numeric_limits<quint16>::max()));
    return r;
}

inline QString toIpV4(const QString& host) {
    const QRegularExpression reIpV4 {R"(^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\$)"};
    if(reIpV4.match(host).hasMatch())
        return host;
    auto l = QHostInfo::fromName(host);
    if(l.error()) {
        logFatal(QObject::tr("Error getting IP address for host %s: %s"), host.toUtf8().data(), l.errorString().toUtf8().data());
    }
    return l.addresses().at(0).toString();
}

}

void parseHostAndPort(const QString& v, bool allowEmptyHost, Params& p) {
    auto r = v.split(R"(:)", QString::SkipEmptyParts);
    if(r.size() == 1) {
        if(!allowEmptyHost)
            logFatal("%s", QObject::tr("Host must be specified").toUtf8().data());
        p.host = "0.0.0.0";
        p.port = toPort(r[0]);
    } else if(r.size() == 2) {
        p.host = toIpV4(r[0]);
        p.port = toPort(r[1]);
    } else {
        allowEmptyHost
            ? logFatal(QObject::tr("Cannot convert %s to [host:]port").toUtf8(), v.toUtf8().data())
            : logFatal(QObject::tr("Cannot convert %s to host:port").toUtf8(), v.toUtf8().data());
    }
}

void parseArguments(const QCoreApplication& app, Params& p) {
    QCommandLineParser parser;
    parser.setApplicationDescription(QObject::tr("Qt based network quality monitor"));
    parser.addHelpOption();
    parser.addVersionOption();

    QCommandLineOption serverOpt {
        QStringList{"server", "s"},
        QObject::tr("Run tool as server. server_addr is the [host:]port on which the server must listen the connections."),
        "server_addr"
    };
    parser.addOption(serverOpt);

    QCommandLineOption clientOpt {
        QStringList{"client", "c"},
        QObject::tr("Run tool as client. server_addr is the host:port value on which the server resides."),
        "server_addr"
    };
    parser.addOption(clientOpt);

    parser.process(app);
    if(parser.optionNames().isEmpty()) {
        qWarning("%s", QObject::tr("Wrong number of the arguments!").toUtf8().data());
        parser.showHelp(1);
    }

    using std::make_tuple;
    using funpp::_;

    auto par = make_tuple(parser.isSet(serverOpt), parser.isSet(clientOpt));
    if(par == make_tuple(true, true)) {
        logFatal("%s", QObject::tr("Choose the only one type: client or server!").toUtf8().data());
    } else if(par == make_tuple(true, _)) {
        parseHostAndPort(parser.value(serverOpt), true, p);
        p.server = true;
    } else {
        parseHostAndPort(parser.value(serverOpt), false, p);
        p.server = false;
    }
}

void startServer(const Params& p) {
    auto s = new EchoServer(p.host, p.port, QCoreApplication::instance());
    QObject::connect(s, &EchoServer::shutdown,
                     QCoreApplication::instance(), &QCoreApplication::quit);
    QTimer::singleShot(0, s, SLOT(start()));
}

void startClient(const Params& p) {

}

int main(int argc, char *argv[]) {
    QCoreApplication app(argc, argv);
    QCoreApplication::setApplicationName("qt_netmon");
    QCoreApplication::setApplicationVersion("0.1");

    Params p {};

    parseArguments(app, p);

    if(p.server) {
        log(QObject::tr("Starting server on %s:%u").toUtf8(), p.host.toUtf8().data(), static_cast<uint>(p.port));
        startServer(p);
    }
    else {
        log(QObject::tr("Starting client for server on %s:%u").toUtf8(), p.host.toUtf8().data(), static_cast<uint>(p.port));
        startClient(p);
    }

    return app.exec();
}
