#include "stdafx.h"
#include "echoserver.h"
#include "netmon.h"

#include "echoserver_p.h"

EchoServer::EchoServer(const QString &host, quint16 port, QObject *parent) :
    QObject(parent), d_ptr(new EchoServerPrivate {host, port, this}) {
}

EchoServer::~EchoServer() {
}

void EchoServer::start() {
    Q_D(EchoServer);

    d->start();
}
