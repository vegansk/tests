set(APP_NAME qt_netmon)

find_package(Qt5Core)
find_package(Qt5Network)

FILE(GLOB_RECURSE SRC  *.cpp *.h)

add_executable(${APP_NAME} ${SRC})

target_link_libraries(${APP_NAME} Qt5::Core Qt5::Network)

set_target_properties(${APP_NAME} PROPERTIES COTIRE_CXX_PREFIX_HEADER_INIT "stdafx.h")
add_definitions(-fPIC)
cotire(${APP_NAME})
