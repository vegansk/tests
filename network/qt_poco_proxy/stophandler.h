#ifndef STOPHANDLER_H
#define STOPHANDLER_H

#include <Poco/Net/HTTPRequestHandler.h>

class StopHandler : public Poco::Net::HTTPRequestHandler {
public:
    StopHandler();

    // HTTPRequestHandler interface
public:
    void handleRequest(Poco::Net::HTTPServerRequest &req, Poco::Net::HTTPServerResponse &rsp);
};

#endif // STOPHANDLER_H
