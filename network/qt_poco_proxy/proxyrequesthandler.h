#ifndef PROXYREQUESTHANDLER_H
#define PROXYREQUESTHANDLER_H

#include <Poco/Net/HTTPRequestHandler.h>

class ProxyRequestHandler : public Poco::Net::HTTPRequestHandler {
public:
    ProxyRequestHandler();

    // HTTPRequestHandler interface
public:
    void handleRequest(Poco::Net::HTTPServerRequest& req, Poco::Net::HTTPServerResponse& rsp);
};


#endif // PROXYREQUESTHANDLER_H
