#include "proxyrequesthandlerfactory.h"
#include "proxyserver.h"

#include <Poco/Net/HTTPServer.h>
#include <Poco/Net/HTTPServerRequest.h>
#include <Poco/Net/HTTPServerResponse.h>
#include <Poco/Net/HTTPRequestHandler.h>
#include <Poco/Net/ServerSocket.h>

using namespace Poco::Net;

ProxyServer::ProxyServer(QObject *parent) :
    QObject(parent) {
}

void ProxyServer::run() {

    ServerSocket socket(8000);

    _srv = std::unique_ptr<HTTPServer>(new HTTPServer(new ProxyRequestHandlerFactory{this}, socket, new HTTPServerParams));

    _srv->start();
}

void ProxyServer::stop() {
    _srv->stopAll(false);
    emit finished();
}
