#include "proxyrequesthandler.h"

#include <Poco/Net/HTTPServerRequest.h>
#include <Poco/Net/HTTPServerResponse.h>
#include <Poco/Net/HTTPClientSession.h>
#include <Poco/Net/HTTPResponse.h>
#include <Poco/StreamCopier.h>

#include <iostream>

using namespace Poco::Net;

ProxyRequestHandler::ProxyRequestHandler() {
}

void ProxyRequestHandler::handleRequest(HTTPServerRequest &req, HTTPServerResponse &rsp) {

    HTTPClientSession sess("svn.eldissoft.lan", 80);
    auto& rs = sess.sendRequest(req);
    Poco::StreamCopier::copyStream(req.stream(), rs);

    auto& rss = sess.receiveResponse(rsp);
    Poco::StreamCopier::copyStream(rss, rsp.send());
}
