#ifndef PROXYREQUESTHANDLERFACTORY_H
#define PROXYREQUESTHANDLERFACTORY_H

#include <Poco/Net/HTTPRequestHandlerFactory.h>

#include "proxyserver.h"

class ProxyRequestHandlerFactory : public Poco::Net::HTTPRequestHandlerFactory {
private:
    ProxyServer *_srv;
public:
    ProxyRequestHandlerFactory(ProxyServer *srv);

    // HTTPRequestHandlerFactory interface
public:
    Poco::Net::HTTPRequestHandler* createRequestHandler(const Poco::Net::HTTPServerRequest& req);
};

#endif // PROXYREQUESTHANDLERFACTORY_H
