#include <QCoreApplication>
#include <QTimer>

#include "proxyserver.h"

int main(int argc, char *argv[]) {
    QCoreApplication app(argc, argv);

    ProxyServer s;

    QObject::connect(&s, &ProxyServer::finished, &app, &QCoreApplication::quit);

    QTimer::singleShot(0, &s, SLOT(run()));

    return app.exec();
}
