#include "proxyrequesthandler.h"
#include "proxyrequesthandlerfactory.h"
#include "stophandler.h"

#include <QDebug>

#include <Poco/Net/HTTPServerRequest.h>

using namespace Poco::Net;

ProxyRequestHandlerFactory::ProxyRequestHandlerFactory(ProxyServer *srv)
    : _srv(srv) {
}

HTTPRequestHandler* ProxyRequestHandlerFactory::createRequestHandler(const HTTPServerRequest &req) {

    qDebug() << req.getURI().c_str();

    if(req.getURI() == "/quit") {
        QMetaObject::invokeMethod(_srv, "stop", Qt::QueuedConnection);
        return new StopHandler;
    }

    return new ProxyRequestHandler;
}
