#include "stophandler.h"

#include <Poco/Net/HTTPServerResponse.h>

using namespace Poco::Net;

StopHandler::StopHandler() {
}

void StopHandler::handleRequest(HTTPServerRequest &req, HTTPServerResponse &rsp) {
    rsp.setStatus(HTTPResponse::HTTP_OK);

    rsp.send() << "Proxy server stopped!";
}
