#ifndef PROXYSERVER_H
#define PROXYSERVER_H

#include <QObject>
#include <memory>

#include <Poco/Net/HTTPServer.h>

class ProxyServer : public QObject {
    Q_OBJECT
public:
    explicit ProxyServer(QObject *parent = 0);

signals:
    void finished();

public slots:
    void run();
    void stop();

private:
    std::unique_ptr<Poco::Net::HTTPServer> _srv;
};

#endif // PROXYSERVER_H
