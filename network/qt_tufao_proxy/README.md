qt_tufao_proxy
==============

Documentation on QHttpPart says:

    if device is sequential (e.g. sockets, but not files), QNetworkAccessManager::post()
    should be called after device has emitted finished().

So, we can't just redirect post request's data from Tufao to QNetworkAccessManager via QIODevice.
In simple case (as in this simple project), we read all the data from request to the internal
buffer, and then issue the post request. In real application, we must check the size of request's
body and make a deсision: to hold it in memory, or to write it in file.
