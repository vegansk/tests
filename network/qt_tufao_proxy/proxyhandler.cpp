#include "proxyhandler.h"

#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QByteArray>
#include <QBuffer>

#include <Tufao/Headers>
#include <Tufao/IByteArray>
#include <Tufao/HttpServerRequest>
#include <Tufao/HttpServerResponse>

using namespace Tufao;

ProxyHandler::ProxyHandler(const QUrl &source, const QUrl &destination, QObject *parent) :
    QObject(parent),
    AbstractHttpServerRequestHandler(),
    _source(source),
    _destination(destination) {
}

bool ProxyHandler::handleRequest(HttpServerRequest &req, HttpServerResponse &rsp) {

    qDebug("Got request for %s", req.url().toString().toStdString().c_str());

    connect(&req, &HttpServerRequest::end, [this,&req,&rsp](){
        doHandleRequest(&req, &rsp);
    });

    return true;
}

QUrl ProxyHandler::destination() const {
    return _destination;
}

void ProxyHandler::setDestination(const QUrl &destination) {
    _destination = destination;
}
QUrl ProxyHandler::source() const {
    return _source;
}

void ProxyHandler::setSource(const QUrl &source) {
    _source = source;
}


void ProxyHandler::doHandleRequest(HttpServerRequest *req, HttpServerResponse *rsp) {
    QNetworkAccessManager *mgr = new QNetworkAccessManager(this);

    auto dataBuff = new QByteArray(req->readBody());
    auto data = new QBuffer(dataBuff, req);

    QNetworkReply *proxyRsp;
    if(qstricmp(req->method(), "POST") == 0)
        proxyRsp = mgr->post(fromServerRequest(req), data);
    else
        proxyRsp = mgr->sendCustomRequest(fromServerRequest(req), req->method(), data);
    proxyRsp->setProperty("SAVED", false);

    connect(proxyRsp, static_cast<void (QNetworkReply::*)(QNetworkReply::NetworkError)>(&QNetworkReply::error), [req,rsp,proxyRsp](QNetworkReply::NetworkError err) {
        qWarning("Proxy error for url %s: %s (%d)", req->url().toString().toStdString().c_str(),
                 proxyRsp->errorString().toStdString().c_str(), proxyRsp->error());
        rsp->writeHead(HttpResponseStatus::INTERNAL_SERVER_ERROR, "Internal Server Error");
    });

    connect(proxyRsp, &QNetworkReply::readyRead, [this,proxyRsp,rsp]() {
        createResponseHeaders(proxyRsp, rsp);
        rsp->write(proxyRsp->readAll());
    });

    connect(proxyRsp, &QNetworkReply::finished, [this,dataBuff,rsp,proxyRsp,mgr]() {
        createResponseHeaders(proxyRsp, rsp);
        rsp->end();
        proxyRsp->deleteLater();
        mgr->deleteLater();
        delete dataBuff;
    });
}

QNetworkRequest ProxyHandler::fromServerRequest(const HttpServerRequest *req) {
    auto newUrl = destination();
    newUrl.setPath(newUrl.path() + req->url().path());
    if(req->url().hasQuery())
        newUrl.setQuery(req->url().query());
    QNetworkRequest proxyReq(newUrl.url());

    for(auto h: req->headers().keys()) {
        if(qstricmp(h.constData(), "host") == 0) {
            proxyReq.setRawHeader(h, destination().host().toLocal8Bit());
        } else if(qstricmp(h.constData(), "origin") == 0) {
            proxyReq.setRawHeader(h, (destination().scheme() + "://" + destination().host()).toLocal8Bit());
        } else if(qstricmp(h.constData(), "referer") == 0) {
            auto newUrl = destination();
            QUrl ref(QString(req->headers().value(h)));
            newUrl.setPath(newUrl.path() + ref.path());
            if(ref.hasQuery())
                newUrl.setQuery(ref.query());
            proxyReq.setRawHeader(h, newUrl.toEncoded());
        } else
            proxyReq.setRawHeader(h, req->headers().value(h));
    }

    return proxyReq;
}

void ProxyHandler::createResponseHeaders(QNetworkReply *proxyRsp, HttpServerResponse *rsp) {
    if(rsp->property("SAVED").toBool())
        return;

    auto statusCode = proxyRsp->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt();

    for(auto h: proxyRsp->rawHeaderPairs()) {
        if(statusCode == 302 && qstricmp(h.first.constData(), "location") == 0) {
            // URL redirection
            QUrl location(QString(h.second));
            if(location.host() == destination().host() && location.port(80) == destination().port(80) && location.path().startsWith(destination().path())) {
                QUrl newUrl(source());
                newUrl.setPath(location.path().mid(destination().path().length()));
                if(location.hasQuery())
                    newUrl.setQuery(location.query());
                rsp->headers().insert(h.first, QByteArray(newUrl.toEncoded()));
            } else
                rsp->headers().insert(h.first, h.second);
        } else
            rsp->headers().insert(h.first, h.second);
    }
    rsp->writeHead(statusCode, proxyRsp->attribute(QNetworkRequest::HttpReasonPhraseAttribute).toByteArray());
    rsp->setProperty("SAVED", true);
}
