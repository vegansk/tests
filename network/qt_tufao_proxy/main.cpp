#include "proxyhandler.h"

#include <QCoreApplication>

#include <Tufao/HttpServer>
#include <Tufao/HttpServerResponse>
#include <Tufao/HttpServerRequestRouter>
#include <Tufao/NotFoundHandler>

using namespace Tufao;

int main(int argc, char *argv[]) {
    QCoreApplication app(argc, argv);

    HttpServer server;

    ProxyHandler proxy(QUrl("http://localhost:8080"), QUrl("http://eldis.ru"));

    HttpServerRequestRouter router{
        {
            QRegularExpression("^/local(/.*)?$"), [](HttpServerRequest &, HttpServerResponse &rsp) {
                rsp.writeHead(HttpResponseStatus::OK);
                rsp.end("Hello!");
                return true;
            }
        },
        { QRegularExpression(""), proxy },
        { QRegularExpression(""), NotFoundHandler::handler() }
    };

    QObject::connect(&server, &HttpServer::requestReady, &router, &HttpServerRequestRouter::handleRequest);

    server.listen(QHostAddress::LocalHost, 8080);

    return app.exec();
}
