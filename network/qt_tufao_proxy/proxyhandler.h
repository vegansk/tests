#ifndef PROXYHANDLER_H
#define PROXYHANDLER_H

#include <QObject>
#include <QUrl>

#include <Tufao/AbstractHttpServerRequestHandler>

class QNetworkAccessManager;
class QNetworkRequest;
class QNetworkReply;

namespace Tufao {
    class HttpServerRequest;
    class HttpServerResponse;
}

class ProxyHandler : public QObject,
                     public Tufao::AbstractHttpServerRequestHandler
{
    Q_OBJECT

public:
    ProxyHandler(const QUrl &source, const QUrl &destination, QObject *parent = nullptr);

    // AbstractHttpServerRequestHandler interface
public:
    bool handleRequest(Tufao::HttpServerRequest &req, Tufao::HttpServerResponse &rsp);

    QUrl destination() const;
    void setDestination(const QUrl &destination);

    QUrl source() const;
    void setSource(const QUrl &source);

private:
    QUrl _source;
    QUrl _destination;

    void doHandleRequest(Tufao::HttpServerRequest *req, Tufao::HttpServerResponse *rsp);
    QNetworkRequest fromServerRequest(const Tufao::HttpServerRequest *req);
    // Does nothing if headers already created
    void createResponseHeaders(QNetworkReply *proxyRsp, Tufao::HttpServerResponse *rsp);
};

#endif // PROXYHANDLER_H
