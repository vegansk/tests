#include <Poco/URI.h>
#include <Poco/Path.h>
#include <Poco/Net/HTTPClientSession.h>
#include <Poco/Net/HTTPRequest.h>
#include <Poco/Net/HTTPResponse.h>
#include <Poco/Net/HTMLForm.h>
#include <Poco/Net/FilePartSource.h>
#include <vector>
#include <string>
#include <iostream>

using namespace std;
using namespace Poco;
using namespace Poco::Net;

using files = vector<Path>;

void sendMultipart(const string& url, const files& files) {
  URI uri(url);

  HTTPClientSession c(uri.getHost(), uri.getPort());

  HTMLForm form(HTMLForm::ENCODING_MULTIPART);

  for(const auto& f : files) {
    form.addPart(f.getFileName(), new FilePartSource(f.toString(), "application/octet-stream"));
  }

  HTTPRequest req("POST", uri.getPath(), HTTPRequest::HTTP_1_0);
  //HTTPRequest req("POST", uri.getPath(), HTTPRequest::HTTP_1_1);

  form.prepareSubmit(req);
  req.setContentLength(form.calculateContentLength());
  req.set("X-Session-Id", "ff5396fa-50e0-424c-8a7b-690ac7e9174b");

  auto& os = c.sendRequest(req);
  form.write(os);

  HTTPResponse rsp;

  auto& is = c.receiveResponse(rsp);

  cout << "Received response: " << rsp.getStatus() << ", " << rsp.getReason() << endl;
}

int main(int argc, char *argv[]) {

  auto dataDir = Path(__FILE__).parent().pushDirectory("data");

  sendMultipart("http://localhost:8080/zenith-portal/rest/client/v1/documents/drafts/baa7d96a-b77f-4645-8d45-1affc01e35f3/sign", { Path(dataDir, "file1.dat"), Path(dataDir, "file2.dat") });

  return 0;
}

