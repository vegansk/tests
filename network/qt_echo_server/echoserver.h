#ifndef ECHOSERVER_H
#define ECHOSERVER_H

#include <QObject>

class QTcpServer;
class QTcpSocket;

class EchoServer : public QObject
{
    Q_OBJECT
public:
    explicit EchoServer(QObject *parent = 0);

signals:
    void finished();

public slots:
    void run();
    void echo(QTcpSocket* s);

private:
    QTcpServer *_server;
};

#endif // ECHOSERVER_H
