set(APP_NAME qt_echo_server)

find_package(Qt5Core)
find_package(Qt5Network)

aux_source_directory(. SRC)

add_executable(${APP_NAME} ${SRC})

target_link_libraries(${APP_NAME} Qt5::Core Qt5::Network)

