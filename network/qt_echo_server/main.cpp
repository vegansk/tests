#include <QCoreApplication>
#include <QTimer>

#include "echoserver.h"

int main(int argc, char *argv[]) {
    QCoreApplication app(argc, argv);

    EchoServer server;

    QObject::connect(&server, &EchoServer::finished, &app, &QCoreApplication::quit);

    QTimer::singleShot(0, &server, SLOT(run()));

    return app.exec();
}
