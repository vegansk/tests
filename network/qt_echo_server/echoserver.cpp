#include "echoserver.h"

#include <QTcpServer>
#include <QTcpSocket>

EchoServer::EchoServer(QObject *parent) :
    QObject(parent), _server(nullptr) {
}

void EchoServer::run() {
    Q_ASSERT(nullptr == _server);

    _server = new QTcpServer(this);

    if(!_server->listen(QHostAddress::LocalHost, 3000)) {
        qCritical() << "Error initializing server: " << _server->errorString();
    }

    connect(_server, &QTcpServer::newConnection, [=](){
        echo(_server->nextPendingConnection());
    });
}

void EchoServer::echo(QTcpSocket *s) {
    Q_ASSERT(nullptr != s);
    qDebug() << "Connect from" << s->peerAddress().toString();

    connect(s, &QTcpSocket::readyRead, [=](){
        auto data = s->readAll();
        s->write(data);
        if(QString(data).startsWith("quit")) {
            emit finished();
        }
    });
}
