var express = require('express'),
    path = require('path');

var DATA_DIR = path.join(path.dirname(__filename), 'test_data');
var USER = 'testUser',
    PASS = 'testPassword';

app = express();

app.use('/basic_auth', express.basicAuth(USER, PASS));
app.use('/basic_auth', express['static'](path.join(DATA_DIR, 'basic_auth')));

server = require('http').createServer(app);

server.listen(3000, 'localhost');

