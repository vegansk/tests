#include "httpjsonserver.h"

#include <QCoreApplication>
#include <QTimer>
#include <QHostInfo>

#include <Tufao/HttpServer>
#include <Tufao/HttpFileServer>
#include <Tufao/HttpServerRequest>
#include <Tufao/HttpServerResponse>
#include <Tufao/HttpServerRequestRouter>
#include <Tufao/NotFoundHandler>
#include <Tufao/UrlRewriterHandler>

#include <qjson/parser.h>

using namespace Tufao;

int main(int argc, char *argv[]) {
    QCoreApplication app(argc, argv);

    UrlRewriterHandler index(QUrl("/test/index.html"));
    HttpJsonServer json;

    HttpServerRequestRouter router {
        {
            QRegularExpression(""), [](HttpServerRequest &req, HttpServerResponse &) -> bool {
                qDebug("Request url: %s", req.url().toString().toStdString().c_str());
                return false;
            }
        },
        { QRegularExpression("^/test/$"), index},
        {
            QRegularExpression("^/quit$"), [&app](HttpServerRequest &, HttpServerResponse &rsp) -> bool {
                rsp.writeHead(HttpResponseStatus::OK);
                rsp.end("Bye!");
                QTimer::singleShot(10, &app, SLOT(quit()));
                return true;
            }
        },
        { QRegularExpression("^/json$"), json },
        { QRegularExpression("^/test/(.*)?"), HttpFileServer::handler(":/public", "/test/") },
        { QRegularExpression(""), NotFoundHandler::handler() }
    };

    HttpServer server;

    QObject::connect(&server, &HttpServer::requestReady, &router, &HttpServerRequestRouter::handleRequest);

    if(!server.listen(QHostInfo::fromName("192.168.0.29").addresses()[0], 8090)) {
      qFatal("Error starting server");
    }

    return app.exec();
}
