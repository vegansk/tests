#ifndef HTTPJSONSERVER_H
#define HTTPJSONSERVER_H

#include <QObject>
#include <Tufao/AbstractHttpServerRequestHandler>

namespace Tufao {
    class HttpServerRequest;
    class HttpServerResponse;
}

class HttpJsonServer : public QObject,
    public Tufao::AbstractHttpServerRequestHandler {
    Q_OBJECT
public:
    explicit HttpJsonServer(QObject *parent = nullptr);

signals:

public slots:


    // AbstractHttpServerRequestHandler interface
public:
    bool handleRequest(Tufao::HttpServerRequest &req, Tufao::HttpServerResponse &rsp);
};

#endif // HTTPJSONSERVER_H
