#include "httpjsonserver.h"

#include <Tufao/Headers>
#include <Tufao/HttpServerRequest>
#include <Tufao/HttpServerResponse>

#include <qjson/serializer.h>
#include <qjson/qobjecthelper.h>

#include <models/account.h>

using namespace Tufao;

HttpJsonServer::HttpJsonServer(QObject *parent) :
    QObject(parent),
    AbstractHttpServerRequestHandler() {
}

bool HttpJsonServer::handleRequest(HttpServerRequest &req, HttpServerResponse &rsp) {
    Account a(1, "Vega");

    QJson::Serializer s;
    s.setIndentMode(QJson::IndentFull);
    bool ok;
    auto data = s.serialize(QJson::QObjectHelper::qobject2qvariant(&a), &ok);

    if(!ok) {
        rsp.writeHead(HttpResponseStatus::INTERNAL_SERVER_ERROR, s.errorMessage().toLocal8Bit());
        rsp.end(s.errorMessage().toLocal8Bit());
    } else {
        rsp.headers().insert("Content-Type", "application/json");
        rsp.writeHead(HttpResponseStatus::OK);
        rsp.end(data);
    }

    return true;
}
