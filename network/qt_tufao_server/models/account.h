#ifndef ACCOUNT_H
#define ACCOUNT_H

#include <QObject>

class Account : public QObject
{
    Q_OBJECT

    Q_PROPERTY(qint64 id READ id WRITE setId)
    Q_PROPERTY(QString name READ name WRITE setName)
public:
    explicit Account(QObject *parent = nullptr);
    Account(qint64 id, const QString &name, QObject *parent = nullptr);

    qint64 id() const;
    void setId(const qint64 &id);

    QString name() const;
    void setName(const QString &name);

signals:


public slots:

private:
    qint64 _id;
    QString _name;
};

#endif // ACCOUNT_H
