#include "account.h"

Account::Account(QObject *parent) :
    QObject(parent) {
}

Account::Account(qint64 id, const QString &name, QObject *parent) :
    QObject(parent), _id(id), _name(name) {

}

qint64 Account::id() const {
    return _id;
}

void Account::setId(const qint64 &id) {
    _id = id;
}

QString Account::name() const {
    return _name;
}

void Account::setName(const QString &name) {
    _name = name;
}
