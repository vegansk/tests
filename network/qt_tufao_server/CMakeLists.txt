set(APP_NAME qt_tufao_server)

find_package(Qt5Core)
find_package(Qt5Network)

FILE(GLOB_RECURSE SRC  *.cpp)

qt5_add_resources(RES public/public.qrc)

include_directories(${TUFAO_INCLUDE_DIR} ${QJSON_INCLUDE_DIR})

add_executable(${APP_NAME} ${SRC} ${RES})
add_dependencies(${APP_NAME} ext_tufao ext_qjson)

target_link_libraries(${APP_NAME} Qt5::Core Qt5::Network ${TUFAO_LIBRARIES} ${QJSON_LIBRARIES})


