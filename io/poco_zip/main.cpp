#include <Poco/Path.h>
#include <Poco/FileStream.h>
#include <Poco/TemporaryFile.h>
#include <Poco/Zip/Decompress.h>
#include <Poco/Zip/Compress.h>
#include <Poco/Zip/ZipArchive.h>
#include <Poco/Zip/ZipStream.h>
#include <Poco/Zip/Decompress.h>
#include <Poco/StreamCopier.h>
#include <fstream>
#include <sstream>
#include <iostream>

using namespace Poco;
using namespace Poco::Zip;
using namespace std;

const char* fileOneMsg = "Hello, world!";
const char* fileTwoMsg = "Shit happends!";

Path createTempFile(const string& content) {
  TemporaryFile f;
  f.keepUntilExit();

  ofstream os(f.path());
  os << content;
  os.close();

  return f.path();
}

void test_compress_decompress() {
  auto fileOne = createTempFile(fileOneMsg);
  auto fileTwo = createTempFile(fileTwoMsg);

  TemporaryFile zipFile;

  ofstream os(zipFile.path(), ios::binary);
  Compress c(os, true);

  c.addFile(fileOne, fileOne.getFileName(), ZipCommon::CM_DEFLATE, ZipCommon::CL_NORMAL);
  c.addFile(fileTwo, fileTwo.getFileName(), ZipCommon::CM_DEFLATE, ZipCommon::CL_NORMAL);

  c.close();
  os.close();

  ifstream is(zipFile.path(), ios::binary);
  // Use decompress is a bad idea because of it's simplified API. It's better to save zip from stream if needed and then
  // use ZipArchive and ZipInputStream
  //Decompress d(is, Path::temp());
  ZipArchive za(is);

  auto it = za.findHeader(fileTwo.getFileName());
  poco_assert(it != za.headerEnd());

  stringstream ss;
  ZipInputStream zis(is, it->second);
  StreamCopier::copyStream(zis, ss);

  poco_assert(ss.str() == fileTwoMsg);
}

void read_archive() {
  auto fp = Path(__FILE__).parent().pushDirectory("data").setFileName("test.zip");
  poco_assert(fp.isFile());

  FileInputStream is(fp.toString());
  ZipArchive za(is);

  auto it = za.findHeader("0");
  poco_assert(it != za.headerEnd());

  TemporaryFile tf;
  FileOutputStream os(Path::temp() + "/test_file");
  ZipInputStream zis(is, it->second);
  StreamCopier::copyStream(zis, os);
  os.close();

  cout << "File is " << tf.path();

  poco_assert(tf.getSize() == 993);
}

void extract_all() {
  auto fp = Path(__FILE__).parent().pushDirectory("data").setFileName("test2.zip");
  poco_assert(fp.isFile());
  ifstream is(fp.toString(), ios::binary);
  File tdir = Path::temp() + "/poco_temp";
  tdir.createDirectory();

  Decompress d(is, tdir.path());
  d.decompressAllFiles();
}

int main(int argc, char *argv[]) {
  // test_compress_decompress();
  // read_archive();
  extract_all();

  return 0;
}

