set(APP_NAME poco_zip)

FILE(GLOB_RECURSE SRC  *.cpp)

include_directories(${POCO_INCLUDE_DIR})

add_executable(${APP_NAME} ${SRC} ${RES})

target_link_libraries(${APP_NAME} ${POCO_LIBRARIES})


