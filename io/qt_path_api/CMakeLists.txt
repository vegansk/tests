set(APP_NAME qt_path_api)

find_package(Qt5Core)

FILE(GLOB_RECURSE SRC  *.cpp)

add_executable(${APP_NAME} ${SRC} ${RES})

target_link_libraries(${APP_NAME} Qt5::Core)

