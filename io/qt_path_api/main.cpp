#include <QObject>
#include <QDebug>
#include <QDir>

void create_dir() {
  QDir d;
  if(d.cd("test_dir//test2")) {
    d.cdUp();
    d.rmdir("test_dir");
  }

  d.mkdir("test_dir/test2");
  d.cd("test_dir//test2");
  d.makeAbsolute();
  qDebug() << d;
}

// This func will not work because test_dir2 not exist
void create_current() {
  QDir d("test_dir2");
  d.makeAbsolute();
  if(d.exists()) {
    d.rmdir(".");
  }
  d.mkdir(".");
}

int main(int argc, char *argv[]) {
  create_dir();
  create_current();

  return 0;
}


