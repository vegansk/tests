project(tests)

cmake_minimum_required(VERSION 2.8.11)

option(TESTS_GENERATE "Generate tests" ON)
option(CPPCHECK_USE "Use cppcheck" ON)
set(CPPCHECK_EXECUTABLE cppcheck CACHE FILEPATH "Path to the cppcheck's executable")
option(BUILD_DISTRIB "Build distributive version (Do not use full path to the libraries)" OFF)

set(CMAKE_MODULE_PATH "${CMAKE_CURRENT_LIST_DIR}/cmake_modules")
set(COTIRE_MINIMUM_NUMBER_OF_TARGET_SOURCES "1")

include(cotire)
include(ExternalProject)
include(GNUInstallDirs)

if(${TESTS_GENERATE})
    include(CTest)
endif()

set(CMAKE_INCLUDE_CURRENT_DIR ON)
set(CMAKE_AUTOMOC ON)

add_definitions(-std=c++11)

# See http://www.cmake.org/Wiki/CMake_RPATH_handling
set(CMAKE_SKIP_BUILD_RPATH ${BUILD_DISTRIB})

set(DEPENDENCIES_DIR ${CMAKE_BINARY_DIR}/dependencies)

set(POCO_CMAKE_ARGS -DCMAKE_INSTALL_PREFIX:PATH=${DEPENDENCIES_DIR}
                    -DCMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE})

ExternalProject_Add(ext_poco
    PREFIX ${DEPENDENCIES_DIR}/poco
    GIT_REPOSITORY https://github.com/pocoproject/poco
    GIT_TAG poco-1.6.0-release
    CMAKE_ARGS ${POCO_CMAKE_ARGS}
    BUILD_IN_SOURCE 1
)

set(POCO_INCLUDE_DIR ${DEPENDENCIES_DIR}/include)
if(${CMAKE_BUILD_TYPE} STREQUAL "Debug")
    set(POCO_LIBRARIES PocoFoundationd PocoNetd PocoZipd)
else()
    set(POCO_LIBRARIES PocoFoundation PocoNet PocoZip)
endif()

link_directories(${DEPENDENCIES_DIR}/lib ${DEPENDENCIES_DIR}/${CMAKE_INSTALL_LIBDIR})

add_subdirectory(basic)
add_subdirectory(cpptricks)
add_subdirectory(network)
add_subdirectory(io)
add_subdirectory(xml)
add_subdirectory(test_frameworks)

if(${CPPCHECK_USE})
    add_custom_target(cppcheck ALL ${CPPCHECK_EXECUTABLE} --error-exitcode=1 ${CMAKE_CURRENT_LIST_DIR})
endif()
