set(APP_NAME qtest)

find_package(Qt5Core)
find_package(Qt5Test)

FILE(GLOB_RECURSE SRC  *.cpp)

add_executable(${APP_NAME} ${SRC})

target_link_libraries(${APP_NAME} Qt5::Core Qt5::Test)

add_test(${APP_NAME} ${APP_NAME})
