#include "unittest.h"

#include <QtTest/QtTest>

UnitTest::UnitTest(QObject *parent) :
    QObject(parent) {
}

void UnitTest::initTestCase() {
    qDebug("Test case initialized");
}

void UnitTest::cleanupTestCase() {
    qDebug("Test case cleaned up");
}

void UnitTest::init() {
    qDebug("Init test");
}

void UnitTest::cleanup() {
    qDebug("Cleanup test");
}

void UnitTest::toUpper() {
    QString str = "Hello";
    QVERIFY(str.toUpper() == "HELLO");
}

void UnitTest::testQObjectProperties() {
    auto obj = QSharedPointer<QObject>(new QObject());
    QVERIFY(!obj->property("test").isValid());
}

QTEST_MAIN(UnitTest)
#include "unittest.moc"
