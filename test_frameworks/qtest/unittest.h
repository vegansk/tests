#ifndef UNITTEST_H
#define UNITTEST_H

#include <QObject>

class UnitTest : public QObject
{
    Q_OBJECT
public:
    explicit UnitTest(QObject *parent = 0);

signals:

private slots:
    // init/cleanup
    void initTestCase();
    void cleanupTestCase();
    void init();
    void cleanup();

    // tests
    void toUpper();
    void testQObjectProperties();
};

#endif // UNITTEST_H
