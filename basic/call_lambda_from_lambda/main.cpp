#include <iostream>
#include <string>

using namespace std;

class Test {
    string _tag;
public:
    Test(const string &tag): _tag(std::move(tag)) {
        cout << "Called Test() for " << _tag << endl;
    }

    Test(const Test &t): _tag(std::move(t._tag)) {
        cout << "Called Test(const Test&) for " << _tag << endl;
    }

    ~Test() {
        cout << "Called ~Test() for " << _tag << endl;
    }

    void ping() const {
        cout << "Ping Test for " << _tag << endl;
    }
};

int main(int argc, char *argv[]) {

    Test t("t");

    Test *pT = new Test("pT");

    auto lambda1 = [&]() {
        cout << "Called lambda1" << endl;
        t.ping();
        pT->ping();
    };

    auto lambda2 = [&]() {
        cout << "Called lambda2" << endl;

        t.ping();
        pT->ping();

        lambda1();
    };

    lambda2();

    delete pT;

    return 0;
}
