#ifndef STDAFX_H
#define STDAFX_H

#include <iostream>
#include <string>
#include <typeinfo>
#include <cxxabi.h>
#include <memory>

#define BOOST_SIGNALS_NO_DEPRECATION_WARNING
#include <boost/reflect/any_ptr.hpp>
#undef BOOST_SIGNALS_NO_DEPRECATION_WARNING

#endif // STDAFX_H
