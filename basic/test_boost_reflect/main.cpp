#include "stdafx.h"

using string = std::string;
using std::cout;
using std::endl;
namespace reflect = boost::reflect;

class SimpleData {
private:
    string str;
    int num;

public:
    SimpleData(const string& _str, int _num) : str(_str), num(_num) {}

    void func() {}
};

// We can't use SimpleData::func here :-(, all methods must be described with BOOST_REFLECT_ANY macro
BOOST_REFLECT(SimpleData, (str)(num))

namespace {
    string get_type_name(const std::type_info& i) {
        int status;
        std::unique_ptr<char, decltype(&std::free)> type(
                    abi::__cxa_demangle(i.name(), nullptr, nullptr, &status), std::free);
        return string{type.get()};
    }

    template<typename T> string get_type_name() {
        return get_type_name(typeid(T));
    }
}

template<typename T> struct type_name_value_visitor {
    const T& _d;
    type_name_value_visitor(const T& d) : _d(d) {}

    template<typename M, M T::*p> void operator ()(const char* name) const {
        cout << get_type_name<M>() << " " << name << " = " << _d.*p << endl;
    }
};

struct test_simple_data {
    void operator()() {
        SimpleData d {"Hello", 1};
        reflect::reflector<SimpleData>::visit(type_name_value_visitor<SimpleData>(d));
    }
};

int main(int argc, char *argv[]) {

    test_simple_data{}();

    return 0;
}
