#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
#include <cstring>
#include <cstdio>
#include <cassert>

using namespace std;

namespace {

  void streamCopy(ostream& to, istream& from) {
    from >> noskipws;
    char ch;
    while(from >> ch, !from.eof()) {
      to << ch;
    }
  }

}

class input_streambuf : public streambuf {
  private:
    string buff_;
    input_streambuf(const input_streambuf&);

  public:
    input_streambuf(const string& buff) : buff_{buff} {
      setg(buff_.begin().base(), buff_.begin().base(), buff_.end().base());
    }
};

class FILE_streambuf : public streambuf {
  private:
    FILE* file_;
    const size_t put_back_;
    vector<char> buff_;

    FILE_streambuf(const FILE_streambuf&);
    FILE_streambuf& operator = (const FILE_streambuf&);

  public:
    explicit FILE_streambuf(FILE* file, size_t buff_size = 256, size_t put_back_size = 8):
      file_ {file}, put_back_ { max(put_back_size, size_t(1)) }, buff_ (max(buff_size, put_back_) + put_back_) {
        auto end = &buff_.front() + buff_.size();
        setg(end, end, end);
      }

  private:
    streambuf::int_type underflow() override {
      if(gptr() < egptr())
        return traits_type::to_int_type(*gptr());

      auto base = &buff_.front();
      auto start = base;

      if(eback() == base) {
        memmove(base, egptr() - put_back_, put_back_);
        start = base + put_back_;
      }

      auto len = fread(start, 1, buff_.size() - put_back_, file_);
      if(len <= 0)
        return traits_type::eof();

      setg(base, start, start + len);
      return traits_type::to_int_type(*gptr());
    }
};

class capitalize_streambuf : public streambuf {
  private:
    ostream& sink_;
    vector<char> buff_;

  public:
    explicit capitalize_streambuf(ostream& sink, size_t buff_size = 256):
      sink_ (sink), buff_ (buff_size + 1) {
        // Sise increased to simplify putting symbol on overflow
        setp(&buff_.front(), &buff_.back() - 1);
      }

  private:
    int_type overflow(int_type ch = traits_type::eof()) override {
      if(sink_ && ch != traits_type::eof()) {
        assert(less_equal<char*>()(pptr(), epptr()));
        *pptr() = ch;
        pbump(1);
        if(!sync())
          return ch;
      }
      return traits_type::eof();
    }

    int sync() override {
      for_each(pbase(), pptr(), [](char& ch) { ch = toupper(ch); });
      ptrdiff_t size = pptr() - pbase();
      pbump(-size);
      return sink_.write(pbase(), size) ? 0 : -1;
    }
};

void input_from_string() {
  input_streambuf b("Hello, world!!!\nAnd some more!!!");
  istream s(&b);
  streamCopy(cout, s);
  cout << endl;
}

void input_from_file() {
  FILE *f = fopen("/proc/cpuinfo", "r");
  FILE_streambuf b(f);
  istream s(&b);
  streamCopy(cout, s);
  fclose(f);
}

void capitalize_output() {
  input_streambuf b("Hello, world!!!\nAnd some more!!!\n");
  istream s(&b);
  capitalize_streambuf ob(cout);
  ostream os(&ob);
  os << unitbuf;
  streamCopy(os, s);
}

int main() {
  input_from_string();

  input_from_file();

  capitalize_output();

  return 0;
}

