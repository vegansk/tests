#include <iostream>
#include <future>
#include <functional>

// g++ bug workaround in CMakeLists.txt target_link_libraries : https://stackoverflow.com/questions/19463602/compiling-multithread-code-with-g

using namespace std;

void simple_test() {
  auto f  = []() -> future<int> {
    cout << "simple_test: Computation started..." << endl;

    promise<int> p;

    p.set_value(100);

    return p.get_future();
  };

  auto res = f();

  cout << "simple_test: The result of the future is " << res.get() << endl;
}

void test_async() {
  auto res = async([]() {
      cout << "test_async: Computation started..." << endl;

      return 100;
      });

  cout << "test_async: The result of the future is " << res.get() << endl;
}

int main(int argc, char *argv[]) {
  simple_test();
  test_async();
  return 0;
}

