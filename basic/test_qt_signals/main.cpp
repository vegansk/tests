#include "testobject.h"

#include <QObject>
#include <QDebug>

int main(int argc, char *argv[]) {

    TestObject obj;

    obj.emitTestSignal();

    QObject::connect(&obj, &TestObject::testSignal, []() {
        qDebug() << "Catch test signal";
    });

    return 0;
}

