#include "testobject.h"

#include <QDebug>

TestObject::TestObject(QObject *parent) :
    QObject(parent) {
}

void TestObject::emitTestSignal() {
    qDebug() << "Emitting test signal...";
    emit testSignal();
}
