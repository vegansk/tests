#include <iostream>
#include <string>
#include <initializer_list>

using std::cout;
using std::endl;
using std::string;
using std::initializer_list;

class Handle {
private:
    string h;

public:
    explicit Handle() : h {} {}
    Handle(Handle&& v) : h {v.h} {
        v.h = "";
    }

    Handle(const string& h) : h(h) {}

    ~Handle() {
        clear();
    }

    void clear() {
        if(h != "") {
            cout << "Clearing value \"" << h << "\"" << endl;
            h = "";
        } else {
            cout << "Clearing empty value" << endl;
        }
    }

    void operator = (Handle&& v) {
        clear();
        h = v.h;
        v.h = "";
    }

    string get() const {
        return h;
    }

    string detach() {
        string tmp = h;
        h = "";
        return tmp;
    }

    string* ptr() {
        return &h;
    }

    operator bool () const {
        return h != "";
    }
};

// initializer_list can't handle references!

//void doSomethingWithHandles(initializer_list<Handle&> lst) {
//    for(const auto v : lst) {
//        cout << "Data: " << v.get() << endl;
//    }
//}

int main(int argc, char *argv[]) {

    Handle h1 {"a"};
    Handle h2 {"b"};
    Handle h3 {"c"};

    //doSomethingWithHandles({h1, h2, h3});

    return 0;
}

