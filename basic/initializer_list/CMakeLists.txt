set(APP_NAME initializer_list_app)

FILE(GLOB_RECURSE SRC  *.cpp)

add_executable(${APP_NAME} ${SRC})
