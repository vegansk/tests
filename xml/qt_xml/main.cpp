#include <QDomDocument>
#include <QFile>
#include <QDir>
#include <QDebug>

#include <assert.h>
#include <iostream>

QDomDocument readDocument() {
  auto dataDir = QDir(QFileInfo(__FILE__).dir().path() + "/data/");

  QDomDocument d;

  QFile f(dataDir.filePath("test.xml"));
  assert(f.open(QFile::ReadOnly));

  assert(d.setContent(&f));

  return d;
}

void changeDocument(QDomDocument& d) {
  auto root = d.documentElement();
  qDebug() << root.nodeName();
  auto signedF = root.firstChildElement("signed");
  if(!signedF.isNull()) {
    signedF.childNodes().at(0).setNodeValue("true");
  }

  auto nodes = root.elementsByTagName("attachment");
  for(auto idx = 0; idx < nodes.size(); idx++) {
    auto n = nodes.at(idx);
    auto hash = d.createElement("hash");
    hash.appendChild(d.createTextNode("12312313123123123123"));
    n.insertAfter(hash, n.firstChildElement("type"));
  }
}

void printDocument(const QDomDocument& d) {
  qDebug() << d.toString();
}

int main(int argc, char *argv[]) {

  auto d = readDocument();
  changeDocument(d);
  printDocument(d);

  return 0;
}
