<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="2.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:fn="http://www.w3.org/2005/xpath-functions" xmlns:xdt="http://www.w3.org/2005/xpath-datatypes"
    xmlns:xsd="http://www.w3.org/2001/XMLSchema-instance">
    <xsl:output method="html" version="2.0" encoding="utf-8"
        doctype-system="http://www.w3.org/TR/html4/loose.dtd" doctype-public="-//W3C//DTD HTML 4.01 Transitional//EN"
        indent="yes" />

    <xsl:strip-space elements="*" />

    <!-- начало разбора -->
    <xsl:template match="/">
        <html>
            <head>
                <link rel="stylesheet" type="text/css" href="edo_fes.css"
                    media="all" />
                <link rel="stylesheet" type="text/css" href="edoprint_fes.css"
                    media="print" />


                <meta http-equiv="CONTENT-TYPE" content="text/html; charset=utf-8" />
                <title>
                    <xsl:call-template name="fesCaption">
                        <xsl:with-param name="format" select='0' />
                    </xsl:call-template>
                </title>
            </head>
            <body>
                <!-- Вывод шапки документа -->
                <xsl:call-template name="fesDocHeader">
                    <xsl:with-param name="root" select="*" />
                </xsl:call-template>

                <!-- вывод заголовка -->
                <h1>
                    <xsl:call-template name="fesCaption">
                        <xsl:with-param name="format" select='1' />
                    </xsl:call-template>
                </h1>

                <!-- вывод данных эмитента -->
                <xsl:for-each select="*/issuer">
                    <xsl:call-template name="fesIssuer" />
                </xsl:for-each>

                <!-- вывод данных ЦБ -->
                <xsl:choose>
                    <xsl:when
                        test="STATEMENT_OF_TRANSACTIONS or STATEMENT_OF_CONFIRMED_TRANSACTION or STATEMENT_OF_REJECTED_TRANSACTION or STATEMENT_OF_HOLDINGS">
                    </xsl:when>
                    <xsl:when test="INSTRUCTION_TO_BLOCK">
                        <xsl:for-each select="*/block_info/block_info">
                            <xsl:call-template name="security_info_t" />
                        </xsl:for-each>
                    </xsl:when>
                    <xsl:when test="INSTRUCTION_TO_UNBLOCK">
                        <xsl:for-each select="*/unblock_info/block_info">
                            <xsl:call-template name="security_info_t">
                                <xsl:with-param name="cancelQuantity" select="//quantity_unblock" />
                            </xsl:call-template>
                        </xsl:for-each>
                    </xsl:when>
                    <xsl:when test="INSTRUCTION_TO_UNPAID_CANCEL">
                        <xsl:for-each select="*/unpaid_cancel_info/unpaid_info">
                            <xsl:call-template name="security_info_t">
                                <xsl:with-param name="cancelQuantity" select="../quantity_cancel" />
                            </xsl:call-template>
                        </xsl:for-each>
                    </xsl:when>
                    <xsl:when test="INSTRUCTION_TO_PLEDGE">
                        <xsl:for-each select="*/pledge_info/pledge_info">
                            <xsl:call-template name="security_info_t" />
                        </xsl:for-each>
                    </xsl:when>
                    <xsl:when test="INSTRUCTION_TO_UNPLEDGE">
                        <xsl:for-each select="*/unpledge_info/pledge_info">
                            <xsl:call-template name="security_info_t">
                                <xsl:with-param name="cancelQuantity" select="//quantity_unpledge" />
                            </xsl:call-template>
                        </xsl:for-each>
                    </xsl:when>
                    <xsl:when test="INSTRUCTION_TO_DELIVER">
                        <xsl:for-each select="*/transaction/transaction">
                            <xsl:call-template name="security_info_t" />
                        </xsl:for-each>
                    </xsl:when>
                    <xsl:when
                        test="REQUEST_FOR_REPORT_IN_EXTERNAL_FORMAT or REQUEST_FOR_STATEMENT_IN_XML_FORMAT">
                        <!-- для этих сообщений данные о ЦБ выводятся в специализированном 
                            виде -->
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:for-each select="//*[security]">
                            <xsl:call-template name="security_info_t" />
                        </xsl:for-each>
                    </xsl:otherwise>
                </xsl:choose>

                <!-- вывод данных ЗЛ -->
                <xsl:choose>
                    <!-- Для "Анкета совладельца" и "Распоряжение на удаление совладельца 
                        из счета общей собственности" в заголовок блока выводится "Счет ОС" -->
                    <xsl:when test="INSTRUCTION_TO_QUOTA_COOWNERS">
                        <xsl:call-template name="account_ident_t">
                            <xsl:with-param name="title" select="'Счет ОС'" />
                            <xsl:with-param name="root" select="//account_ident" />
                        </xsl:call-template>
                    </xsl:when>
                    <!-- Для "Распоряжение на блокирование ЦБ", "Распоряжение на блокирование 
                        операций по счету", "Распоряжение на конвертацию ЦБ по одному счету", "Распоряжение 
                        на снятие блокирования ЦБ", "Распоряжение об обременении ЦБ обязательством 
                        оплаты", "Распоряжение о снятии обременения обязательства оплаты" данные 
                        о ЗЛ берутся из account_owner -->
                    <xsl:when
                        test="INSTRUCTION_TO_BLOCK or 
                                    INSTRUCTION_TO_BLOCKOPER or
                                    INSTRUCTION_TO_UNBLOCKOPER or 
                                    INSTRUCTION_TO_CONVERT or 
                                    INSTRUCTION_TO_UNBLOCK or 
                                    INSTRUCTION_TO_UNPAID or 
                                    INSTRUCTION_TO_UNPAID_CANCEL">
                        <xsl:call-template name="account_ident_t">
                            <xsl:with-param name="title" select="'Зарегистрированное лицо'" />
                            <xsl:with-param name="root" select="//account_owner" />
                        </xsl:call-template>
                    </xsl:when>

                    <!-- Для "Передаточное распоряжение" есть счет списания и счет зачисления -->
                    <xsl:when test="INSTRUCTION_TO_DELIVER">
                        <xsl:call-template name="account_ident_t">
                            <xsl:with-param name="title" select="'Счет списания'" />
                            <xsl:with-param name="root" select="//account_deliv" />
                        </xsl:call-template>
                        <xsl:call-template name="account_ident_t">
                            <xsl:with-param name="title" select="'Счет зачисления'" />
                            <xsl:with-param name="root" select="//account_receiv" />
                        </xsl:call-template>
                    </xsl:when>

                    <!-- Для "Залоговое распоряжение", "Распоряжение о передаче права залога", 
                        "Распоряжение о внесении изменений в условия залога", "Распоряжение о прекращении 
                        залога" есть Залогодатель и Залогодержатель -->
                    <xsl:when
                        test="INSTRUCTION_TO_PLEDGE or 
                        INSTRUCTION_TO_PLEDGE_DELIVER or 
                        INSTRUCTION_TO_PLEDGE_MODIFY or 
                        INSTRUCTION_TO_UNPLEDGE">

                        <xsl:call-template name="account_ident_t">
                            <xsl:with-param name="title" select="'Залогодатель'" />
                            <xsl:with-param name="root" select="//account_owner" />
                        </xsl:call-template>
                        <xsl:call-template name="account_ident_t">
                            <xsl:with-param name="title" select="'Залогодержатель'" />
                            <xsl:with-param name="root" select="//account_pledgee" />
                        </xsl:call-template>

                        <xsl:if test="INSTRUCTION_TO_PLEDGE_DELIVER">
                            <xsl:call-template name="account_ident_t">
                                <xsl:with-param name="title"
                                    select="'Залогодержатель, принимающий права залога'" />
                                <xsl:with-param name="root" select="//account_pledgee_new" />
                            </xsl:call-template>
                        </xsl:if>
                    </xsl:when>
                    <!-- для этого сообщения идентификаоры выводятся отдельно -->
                    <xsl:when test="FORM_OF_COOWNERS" />
                    <xsl:otherwise>
                        <!-- Для всех остальных сообщений берется из account_ident -->
                        <xsl:call-template name="account_ident_t">
                            <xsl:with-param name="title" select="'Зарегистрированное лицо'" />
                            <xsl:with-param name="root" select="//account_ident" />
                        </xsl:call-template>
                    </xsl:otherwise>
                </xsl:choose>

                <xsl:apply-templates />

                <xsl:if test="*/scan_info">
                    <table class="identTable">
                        <tbody>
                            <tr>
                                <td class="titleForIdentTableRow sizeData">
                                    <xsl:text>Приложен скан-образ документа</xsl:text>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </xsl:if>

                <xsl:if test="*/appendix">
                    <table class="identTable">
                        <tbody>
                            <tr>
                                <td class="firstHeaderColBasedInfoTable sizeData">
                                    <xsl:text>Приложения к документу</xsl:text>
                                </td>
                                <td class="otherHeaderColBasedInfoTable">
                                    <xsl:text>Наличие скан-образа</xsl:text>
                                </td>
                            </tr>
                            <xsl:for-each select="*/appendix">
                                <tr>
                                    <td class="dataColBasedInfoTable sizeData">
                                        <xsl:call-template name="doc_type_t">
                                            <xsl:with-param name="type" select="app_type" />
                                        </xsl:call-template>

                                        <xsl:text> № </xsl:text>
                                        <xsl:value-of select="app_num" />

                                        <xsl:if test="app_date">
                                            <xsl:text> от </xsl:text>
                                            <xsl:call-template name="date_or_datetime_t">
                                                <xsl:with-param name="root" select="app_date" />
                                            </xsl:call-template>
                                        </xsl:if>

                                    </td>
                                    <td class="dataColBasedInfoTable sizeData">
                                        <xsl:if test="scan_info">
                                            <xsl:text>Имеется</xsl:text>
                                        </xsl:if>
                                    </td>
                                </tr>
                            </xsl:for-each>
                        </tbody>
                    </table>
                </xsl:if>

                <!-- вывод данных регистратора -->
                <xsl:for-each select="//registrar">
                    <xsl:call-template name="registrar_info_t" />
                </xsl:for-each>
            </body>
        </html>
    </xsl:template>

    <!-- Вывод заголовка сообщения -->
    <xsl:template name="fesCaption">
        <xsl:param name="format" />
    
    <xsl:choose>
      <xsl:when test="*/doc_TA/TA_doc_type">
        <xsl:call-template name="doc_type_t">
          <xsl:with-param name="type" select="*/doc_TA/TA_doc_type"/>
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:choose>
          <xsl:when test="FORM_OF_COOWNERS">
            <xsl:choose>
              <xsl:when test="$format=1">
                <xsl:text>Анкета совладельца</xsl:text>
                <br />
                <div class="addTextForH1">
                  <xsl:call-template name="coowner_form_for_et">
                    <xsl:with-param name="type" select="*/form_for" />
                  </xsl:call-template>
                </div>
              </xsl:when>
              <xsl:otherwise>
                <xsl:text>Анкета совладельца </xsl:text>
                <xsl:call-template name="coowner_form_for_et">
                  <xsl:with-param name="type" select="*/form_for" />
                </xsl:call-template>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:when>

          <xsl:when test="FORM_OF_SHAREHOLDERS">
            <xsl:text>Анкета зарегистрированного лица</xsl:text>
          </xsl:when>

          <xsl:when test="FREE_FORMAT_ANSWER">
            <xsl:text>Сообщение-ответ в свободном формате</xsl:text>
          </xsl:when>

          <xsl:when test="FREE_FORMAT_MESSAGE">
            <xsl:text>Сообщение-письмо в свободном формате</xsl:text>
          </xsl:when>

          <xsl:when test="FREE_FORMAT_QUERY">
            <xsl:text>Сообщение-запрос в свободном формате</xsl:text>
          </xsl:when>

          <xsl:when test="INSTRUCTION_TO_ACCOUNT_CLOSE">
            <xsl:text>Распоряжение на закрытие лицевого счета</xsl:text>
          </xsl:when>

          <xsl:when test="INSTRUCTION_TO_AUTHORISED_CANCEL">
            <xsl:text>Распоряжение на отмену полномочий представителя</xsl:text>
          </xsl:when>

          <xsl:when test="INSTRUCTION_TO_BLOCK">
            <xsl:text>Распоряжение на блокирование ЦБ</xsl:text>
          </xsl:when>

          <xsl:when test="INSTRUCTION_TO_BLOCKOPER">
            <xsl:text>Распоряжение на блокирование операций по счету</xsl:text>
          </xsl:when>

          <xsl:when test="INSTRUCTION_TO_CONVERT">
            <xsl:text>Распоряжение на конвертацию ЦБ по одному счету</xsl:text>
          </xsl:when>

          <xsl:when test="INSTRUCTION_TO_COOWNER_CLOSE">
            <xsl:text>Распоряжение на удаление совладельца из счета общей собственности</xsl:text>
          </xsl:when>

          <xsl:when test="INSTRUCTION_TO_DELIVER">
            <xsl:choose>
              <xsl:when test="$format=1">
                Передаточное распоряжение
                <xsl:if test="*/transaction/transaction/transaction_type">
                  <div class="addTextForH1">
                    <xsl:call-template name="transaction_type_et">
                      <xsl:with-param name="type"
                        select="*/transaction/transaction/transaction_type" />
                    </xsl:call-template>
                  </div>
                </xsl:if>
              </xsl:when>
              <xsl:otherwise>
                <xsl:text>Передаточное распоряжение </xsl:text>
                <xsl:if test="*/transaction/transaction/transaction_type">
                  <xsl:call-template name="transaction_type_et">
                    <xsl:with-param name="type"
                      select="*/transaction/transaction/transaction_type" />
                  </xsl:call-template>
                </xsl:if>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:when>

          <xsl:when test="INSTRUCTION_TO_DOC_RECALL">
            <xsl:text>Распоряжение на отзыв документа</xsl:text>
          </xsl:when>

          <xsl:when test="INSTRUCTION_TO_PLEDGE">
            <xsl:text>Залоговое распоряжение</xsl:text>
          </xsl:when>

          <xsl:when test="INSTRUCTION_TO_PLEDGE_DELIVER">
            <xsl:text>Распоряжение о передаче права залога</xsl:text>
          </xsl:when>

          <xsl:when test="INSTRUCTION_TO_PLEDGE_MODIFY">
            <xsl:text>Распоряжение о внесении изменений в условия залога</xsl:text>
          </xsl:when>

          <xsl:when test="INSTRUCTION_TO_QUOTA_COOWNERS">
            <xsl:text>Распоряжение на изменение долей совладельцев</xsl:text>
          </xsl:when>

          <xsl:when test="INSTRUCTION_TO_UNBLOCK">
            <xsl:text>Распоряжение на снятие блокирования ЦБ</xsl:text>
          </xsl:when>

          <xsl:when test="INSTRUCTION_TO_UNBLOCKOPER">
            <xsl:text>Распоряжение на прекращение блокирования операций по счету</xsl:text>
          </xsl:when>

          <xsl:when test="INSTRUCTION_TO_UNPAID">
            <xsl:text>Распоряжение об обременении ЦБ обязательством оплаты</xsl:text>
          </xsl:when>

          <xsl:when test="INSTRUCTION_TO_UNPAID_CANCEL">
            <xsl:text>Распоряжение о снятии обременения обязательства оплаты</xsl:text>
          </xsl:when>

          <xsl:when test="INSTRUCTION_TO_UNPLEDGE">
            <xsl:text>Распоряжение о прекращении залога</xsl:text>
          </xsl:when>

          <xsl:when test="NOTICE_OF_DOC_RECALL">
            <xsl:text>Уведомление об отзыве документа</xsl:text>
          </xsl:when>

          <xsl:when test="NOTICE_OF_DOC_RECEPTION">
            <xsl:text>Уведомление о получении документа</xsl:text>
          </xsl:when>

          <xsl:when test="NOTICE_OF_OUTDOC_GIVE">
            <xsl:text>Сведения о выдаче исходящего документа</xsl:text>
          </xsl:when>

          <xsl:when test="NOTICE_OF_REPORT">
            <xsl:text>
                      Уведомление о сформированном отчете (формируется регистратором в
                              ответ на запрос отчета во внешнем формате, файл самого отчета
                              передается вместе с ним)
            </xsl:text>
          </xsl:when>

          <xsl:when test="PROXY_OF_AUTHORISED_PERSON">
            <xsl:text>Доверенность на уполномоченного представителя</xsl:text>
          </xsl:when>

          <xsl:when test="REGISTER_OF_SHAREHOLDERS">
            <xsl:text>Список владельцев, Список лиц, имеющих право на получение доходов.</xsl:text>
          </xsl:when>
          
          <xsl:when test="REQUEST_FOR_RECONCILIATION or STATEMENT_OF_RECONCILIATION">
            <xsl:choose>
              <xsl:when test="$format=1">
                <xsl:choose>
                  <xsl:when test="REQUEST_FOR_RECONCILIATION">
                    <xsl:text>Журнал отправленных (принятых) документов</xsl:text>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:text>Акт сверки от трансфер-агента</xsl:text>
                  </xsl:otherwise>
                </xsl:choose>

                <br />
                <div class="addTextForH1">
                  <xsl:text>Трансфер агент: </xsl:text>
                  <xsl:if test="*/transfer_agent/ta_id">
                    <xsl:value-of select="*/transfer_agent/ta_id" />
                    <xsl:text> </xsl:text>
                  </xsl:if>
                  <xsl:value-of select="*/transfer_agent/ta_info/juridical_name" />
                  <br />

                  <xsl:text>Подразделение: </xsl:text>
                  <xsl:value-of select="*/reg_subdiv/id" />
                  <xsl:text> </xsl:text>
                  <xsl:value-of select="*/reg_subdiv/name" />
                  <br />

                  <xsl:text>за период </xsl:text>

                  <xsl:if test="not(normalize-space(*/beg_date[text()])='')">
                    <xsl:text>c </xsl:text>
                    <xsl:call-template name="convertDate">
                      <xsl:with-param name="date" select="*/beg_date" />
                    </xsl:call-template>
                  </xsl:if>

                  <xsl:if test="not(normalize-space(*/end_date[text()])='')">
                    <xsl:text> по </xsl:text>
                    <xsl:call-template name="convertDate">
                      <xsl:with-param name="date" select="*/end_date" />
                    </xsl:call-template>
                  </xsl:if>

                  <br />
                </div>
                <div class="addTextForH1Small">
                  <xsl:text>по состоянию на </xsl:text>
                  <xsl:call-template name="dateTime">
                    <xsl:with-param name="datetime" select="*/make_datetime" />
                  </xsl:call-template>
                </div>
              </xsl:when>
              <xsl:otherwise>
                <xsl:choose>
                  <xsl:when test="REQUEST_FOR_RECONCILIATION">
                    <xsl:text>Журнал отправленных (принятых) документов </xsl:text>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:text>Акт сверки от трансфер-агента </xsl:text>
                  </xsl:otherwise>
                </xsl:choose>
                <xsl:text>Трансфер агент: </xsl:text>
                <xsl:if test="*/transfer_agent/ta_id">
                  <xsl:value-of select="*/transfer_agent/ta_id" />
                  <xsl:text> </xsl:text>
                </xsl:if>
                <xsl:value-of select="*/transfer_agent/ta_info/juridical_name" />

                <xsl:text> Подразделение: </xsl:text>
                <xsl:value-of select="*/reg_subdiv/id" />
                <xsl:text> </xsl:text>
                <xsl:value-of select="*/reg_subdiv/name" />
                <xsl:text> за период </xsl:text>

                <xsl:if test="not(normalize-space(*/beg_date[text()])='')">
                  <xsl:text>c </xsl:text>
                  <xsl:call-template name="convertDate">
                    <xsl:with-param name="date" select="*/beg_date" />
                  </xsl:call-template>
                </xsl:if>

                <xsl:if test="not(normalize-space(*/end_date[text()])='')">
                  <xsl:text> по </xsl:text>
                  <xsl:call-template name="convertDate">
                    <xsl:with-param name="date" select="*/end_date" />
                  </xsl:call-template>
                </xsl:if>
                <xsl:text> по состоянию на </xsl:text>
                <xsl:call-template name="dateTime">
                  <xsl:with-param name="datetime" select="*/make_datetime" />
                </xsl:call-template>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:when>

          <xsl:when test="REQUEST_FOR_REGISTER_SHAREHOLDERS">
            <xsl:text>Запрос (требование) на раскрытие информации номинальному держателю</xsl:text>
          </xsl:when>

          <xsl:when test="REQUEST_FOR_REPORT_IN_EXTERNAL_FORMAT">
            <xsl:text>Запрос на отчет из системы ведения реестра</xsl:text>
          </xsl:when>

          <xsl:when test="REQUEST_FOR_STATEMENT_IN_XML_FORMAT">
            <xsl:text>Распоряжение на выдачу информации по лицевому счету</xsl:text>
          </xsl:when>

          <xsl:when test="REQUEST_OF_NOTARY">
            <xsl:text>Запрос нотариуса</xsl:text>
          </xsl:when>
          <xsl:when test="STATEMENT_OF_CONFIRMED_TRANSACTION">
            <xsl:text>Уведомление о проведении операции</xsl:text>
          </xsl:when>

          <xsl:when test="STATEMENT_OF_HOLDINGS">
            <xsl:choose>
              <xsl:when test="$format=1">
                <xsl:call-template name="statement_type_et">
                  <xsl:with-param name="type" select="*/statement_type" />
                </xsl:call-template>
                <br />
                <div class="addTextForH1">
                  <xsl:text>на дату </xsl:text>

                  <xsl:call-template name="date_or_datetime_t">
                    <xsl:with-param name="root" select="*/statement_date" />
                  </xsl:call-template>
                </div>
              </xsl:when>
              <xsl:otherwise>
                <xsl:call-template name="statement_type_et">
                  <xsl:with-param name="type" select="*/statement_type" />
                </xsl:call-template>
                <xsl:text> на дату </xsl:text>
                <xsl:call-template name="date_or_datetime_t">
                  <xsl:with-param name="root" select="*/statement_date" />
                </xsl:call-template>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:when>

          <xsl:when test="STATEMENT_OF_REJECTED_TRANSACTION">
            <xsl:text>Уведомление об отказе в проведении операции</xsl:text>
          </xsl:when>

          <xsl:when test="STATEMENT_OF_TRANSACTIONS">
            <xsl:choose>
              <xsl:when test="$format=1">
                Справка об операциях по счету
                <br />
                <div class="addTextForH1">
                  <xsl:text>за период </xsl:text>

                  <xsl:if test="not(normalize-space(*/beg_date[text()])='')">
                    <xsl:text>c </xsl:text>
                    <xsl:call-template name="convertDate">
                      <xsl:with-param name="date" select="*/beg_date" />
                    </xsl:call-template>
                  </xsl:if>

                  <xsl:if test="not(normalize-space(*/end_date[text()])='')">
                    <xsl:text> по </xsl:text>
                    <xsl:call-template name="convertDate">
                      <xsl:with-param name="date" select="*/end_date" />
                    </xsl:call-template>
                  </xsl:if>
                </div>
              </xsl:when>
              <xsl:otherwise>
                <xsl:text>Справка об операциях по счету за период </xsl:text>
                <xsl:if test="not(normalize-space(*/beg_date[text()])='')">
                  <xsl:text>c </xsl:text>
                  <xsl:call-template name="convertDate">
                    <xsl:with-param name="date" select="*/beg_date" />
                  </xsl:call-template>
                </xsl:if>

                <xsl:if test="not(normalize-space(*/end_date[text()])='')">
                  <xsl:text> по </xsl:text>
                  <xsl:call-template name="convertDate">
                    <xsl:with-param name="date" select="*/end_date" />
                  </xsl:call-template>
                </xsl:if>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:when>
          
          <xsl:otherwise>
            <xsl:text>Неизвестный тип документа - </xsl:text>
            <xsl:value-of select="name(*)" />
          </xsl:otherwise>
        </xsl:choose>    
      </xsl:otherwise>
    </xsl:choose>
    </xsl:template>

    <!-- номер операции в рег журнале -->
    <xsl:template name="numberOperation">
        <xsl:param name="number" />
        <xsl:value-of select="normalize-space($number)" />
    </xsl:template>

    <!-- дата операции -->
    <xsl:template name="dateOperation">
        <xsl:param name="date" />
        <xsl:call-template name="convertDate">
            <xsl:with-param name="date" select="$date" />
        </xsl:call-template>
    </xsl:template>
    <!-- тип операции -->
    <xsl:template name="typeOperation">
        <xsl:param name="type" />
        <xsl:param name="transaction" />
        <xsl:param name="account_ident" />
        <xsl:param name="account_deliv" />
        <xsl:param name="account_receiv" />

        <xsl:value-of select="normalize-space($type)" />
        <xsl:if test="$transaction">
            <xsl:if
                test="normalize-space($account_ident)=normalize-space($account_deliv)">
                <xsl:text> (списание)</xsl:text>
            </xsl:if>
            <xsl:if
                test="normalize-space($account_ident)=normalize-space($account_receiv)">
                <xsl:text> (зачисление)</xsl:text>
            </xsl:if>
        </xsl:if>
    </xsl:template>
    <!-- Документ-основание для операции -->
    <xsl:template name="baseDocOperation">
        <xsl:param name="doc_name" />
        <xsl:param name="out_doc_num" />
        <xsl:param name="out_doc_date" />
        <xsl:param name="in_doc_num" />
        <xsl:param name="in_reg_date" />

        <xsl:if test="$doc_name">
            <xsl:value-of select="normalize-space($doc_name)" />
            <xsl:text> </xsl:text>
        </xsl:if>

        <xsl:text>№ </xsl:text>
        <xsl:value-of select="normalize-space($out_doc_num)" />
        <xsl:if test="$out_doc_date">
            <xsl:text> от </xsl:text>
            <xsl:call-template name="date_or_datetime_t">
                <xsl:with-param name="root" select="$out_doc_date" />
            </xsl:call-template>
        </xsl:if>

        <xsl:text>, входящий номер у регистратора </xsl:text>
        <xsl:value-of select="normalize-space($in_doc_num)" />
        <xsl:if test="$in_reg_date">
            <xsl:text> от </xsl:text>
            <xsl:call-template name="date_or_datetime_t">
                <xsl:with-param name="root" select="$in_reg_date" />
            </xsl:call-template>
        </xsl:if>
    </xsl:template>
    <!-- Контрагенты для операции -->
    <xsl:template name="contrAgent">
        <xsl:param name="transaction" />
        <xsl:param name="pledge" />
        <xsl:param name="unpledge" />
        <xsl:param name="pledge_modify" />
        <xsl:param name="pledge_deliver" />

        <xsl:param name="account_ident" />
        <xsl:param name="account_deliv" />
        <xsl:param name="account_receiv" />
        <xsl:param name="account_owner" />
        <xsl:param name="account_pledgee" />
        <xsl:param name="account_pledgee_new" />

        <xsl:choose>
            <!-- Передаточное распоряжение -->
            <xsl:when test="$transaction">
                <xsl:if
                    test="not(normalize-space($account_ident)=normalize-space($account_deliv))">
                    <xsl:call-template name="accountRow">
                        <xsl:with-param name="root" select="$account_deliv" />
                        <xsl:with-param name="type" select="'Счет списания: '" />
                    </xsl:call-template>
                </xsl:if>
                <xsl:if
                    test="not(normalize-space($account_ident)=normalize-space($account_receiv))">
                    <xsl:if
                        test="not(normalize-space($account_ident)=normalize-space($account_deliv))">
                        <br />
                    </xsl:if>
                    <xsl:call-template name="accountRow">
                        <xsl:with-param name="root" select="$account_receiv" />
                        <xsl:with-param name="type" select="'Счет зачисления: '" />
                    </xsl:call-template>
                </xsl:if>
            </xsl:when>

            <!-- установка залога или снятие залога или изменение залога -->
            <xsl:when test="$pledge or $unpledge or $pledge_modify">
                <xsl:if
                    test="normalize-space($account_ident)=normalize-space($account_owner)">
                </xsl:if>

                <xsl:if
                    test="not(normalize-space($account_ident)=normalize-space($account_owner))">
                    <xsl:call-template name="accountRow">
                        <xsl:with-param name="root" select="$account_owner" />
                        <xsl:with-param name="type" select="'Залогодатель: '" />
                    </xsl:call-template>

                </xsl:if>
                <xsl:if
                    test="not(normalize-space($account_ident)=normalize-space($account_pledgee))">
                    <xsl:if
                        test="not(normalize-space($account_ident)=normalize-space($account_owner))">
                        <br />
                    </xsl:if>
                    <xsl:call-template name="accountRow">
                        <xsl:with-param name="root" select="$account_pledgee" />
                        <xsl:with-param name="type" select="'Залогодержатель: '" />
                    </xsl:call-template>

                </xsl:if>
            </xsl:when>

            <!-- Передача права залога -->
            <xsl:when test="$pledge_deliver">
                <xsl:if
                    test="not(normalize-space($account_ident)=normalize-space($account_owner))">
                    <xsl:call-template name="accountRow">
                        <xsl:with-param name="root" select="$account_owner" />
                        <xsl:with-param name="type" select="'Залогодатель: '" />
                    </xsl:call-template>
                    <br />
                </xsl:if>

                <xsl:if
                    test="not(normalize-space($account_ident)=normalize-space($account_pledgee))">
                    <xsl:call-template name="accountRow">
                        <xsl:with-param name="root" select="$account_pledgee" />
                        <xsl:with-param name="type" select="'Залогодержатель: '" />
                    </xsl:call-template>
                    <br />
                </xsl:if>

                <xsl:if
                    test="not(normalize-space($account_ident)=normalize-space($account_pledgee_new))">
                    <xsl:call-template name="accountRow">
                        <xsl:with-param name="root" select="$account_pledgee_new" />
                        <xsl:with-param name="type"
                            select="'Залогодержатель, принимающий права залога: '" />
                    </xsl:call-template>
                </xsl:if>
            </xsl:when>
        </xsl:choose>
    </xsl:template>
    <!-- вывод анкеты юридического лица -->
    <xsl:template name="juridicalProfile">
        <xsl:param name="juridical" />
        <tr>
            <td class="leftColumnIdentTable">
                <xsl:text>Полное наименование юридического лица: </xsl:text>
            </td>
            <td class="dataColBasedInfoTable mainListData">
                <xsl:value-of select="normalize-space($juridical/juridical_name)" />
            </td>
        </tr>
        <tr>
            <td class="leftColumnIdentTable">
                <xsl:text>Краткое наименование юридического лица: </xsl:text>
            </td>
            <td class="dataColBasedInfoTable mainListData">
                <xsl:value-of select="normalize-space($juridical/juridical_short_name)" />
            </td>
        </tr>
        <tr>
            <td class="leftColumnIdentTable">
                <xsl:text>Документ: </xsl:text>
            </td>
            <td class="dataColBasedInfoTable mainListData">
                <xsl:call-template name="juridical_registration_t">
                    <xsl:with-param name="root"
                        select="$juridical/juridical_registration" />
                </xsl:call-template>
            </td>
        </tr>

        <tr>
            <td class="leftColumnIdentTable">
                <xsl:text>Адрес места нахождения: </xsl:text>
            </td>
            <td class="dataColBasedInfoTable mainListData">
                <xsl:call-template name="address_legal_t">
                    <xsl:with-param name="root"
                        select="$juridical/juridical_legal_address" />
                </xsl:call-template>
            </td>
        </tr>

        <tr>
            <td class="leftColumnIdentTable">
                <xsl:text>Почтовый адрес: </xsl:text>
            </td>
            <td class="dataColBasedInfoTable mainListData">
                <xsl:call-template name="address_legal_t">
                    <xsl:with-param name="root"
                        select="$juridical/juridical_post_address" />
                </xsl:call-template>
            </td>
        </tr>

        <xsl:if test="$juridical/juridical_post_address/another">
            <tr>
                <td class="leftColumnIdentTable">
                    <xsl:text>Получатель корреспонденции: </xsl:text>
                </td>
                <td class="dataColBasedInfoTable mainListData">
                    <xsl:value-of
                        select="normalize-space($juridical/juridical_post_address/another)" />
                </td>
            </tr>
        </xsl:if>

        <xsl:if test="$juridical/registration_country">
            <tr>
                <td class="leftColumnIdentTable">
                    <xsl:text>Страна регистрации: </xsl:text>
                </td>
                <td class="dataColBasedInfoTable mainListData">
                    <xsl:call-template name="country_et">
                        <xsl:with-param name="type"
                            select="$juridical/registration_country" />
                    </xsl:call-template>
                </td>
            </tr>
        </xsl:if>

        <xsl:if test="$juridical/special">
            <tr>
                <td class="leftColumnIdentTable">
                    <xsl:text>Особый вид ЮЛ: </xsl:text>
                </td>
                <td class="dataColBasedInfoTable mainListData">
                    <xsl:call-template name="juridical_special_et">
                        <xsl:with-param name="type" select="$juridical/special" />
                    </xsl:call-template>
                </td>
            </tr>
        </xsl:if>

        <xsl:if test="$juridical/inn">
            <tr>
                <td class="leftColumnIdentTable">
                    <xsl:text>ИНН: </xsl:text>
                </td>
                <td class="dataColBasedInfoTable mainListData">
                    <xsl:value-of select="normalize-space($juridical/inn)" />
                </td>
            </tr>
        </xsl:if>

        <xsl:if test="$juridical/kpp">
            <tr>
                <td class="leftColumnIdentTable">
                    <xsl:text>КПП: </xsl:text>
                </td>
                <td class="dataColBasedInfoTable mainListData">
                    <xsl:value-of select="normalize-space($juridical/kpp)" />
                </td>
            </tr>
        </xsl:if>

        <xsl:if test="$juridical/okpo">
            <tr>
                <td class="leftColumnIdentTable">
                    <xsl:text>ОКПО: </xsl:text>
                </td>
                <td class="dataColBasedInfoTable mainListData">
                    <xsl:value-of select="normalize-space($juridical/okpo)" />
                </td>
            </tr>
        </xsl:if>

        <xsl:if test="$juridical/okved">
            <tr>
                <td class="leftColumnIdentTable">
                    <xsl:text>ОКВЭД: </xsl:text>
                </td>
                <td class="dataColBasedInfoTable mainListData">
                    <xsl:value-of select="normalize-space($juridical/okved)" />
                </td>
            </tr>
        </xsl:if>

        <xsl:if test="$juridical/opf">
            <tr>
                <td class="leftColumnIdentTable">
                    <xsl:text>ОПФ: </xsl:text>
                </td>
                <td class="dataColBasedInfoTable mainListData">
                    <xsl:call-template name="opf_et">
                        <xsl:with-param name="type" select="$juridical/opf/opf_type" />
                    </xsl:call-template>
                </td>
            </tr>
        </xsl:if>
        
        <xsl:if test="$juridical/licence">
            <tr>
                <td class="leftColumnIdentTable">
                    <xsl:text>Данные  лицензии для счета НД, ДУ: </xsl:text>
                </td>
                <td class="dataColBasedInfoTable mainListData">
                    <xsl:call-template name="licence_t">
                        <xsl:with-param name="root" select="$juridical/licence" />
                    </xsl:call-template>
                </td>
            </tr>
        </xsl:if>
        
        
    </xsl:template>
    <!-- вывод анкеты физического лица -->
    <xsl:template name="individualProfile">
        <xsl:param name="individual" />
        <tr>
            <td class="leftColumnIdentTable">
                <xsl:text>Фамилия, имя, отчество физического лица: </xsl:text>
            </td>
            <td class="dataColBasedInfoTable mainListData">
                <xsl:value-of select="normalize-space($individual/individual_name)" />
            </td>
        </tr>
        <xsl:if test="$individual/individual_short_name">
            <tr>
                <td class="leftColumnIdentTable">
                    <xsl:text>Фамилия И.О.: </xsl:text>
                </td>
                <td class="dataColBasedInfoTable mainListData">
                    <xsl:value-of select="normalize-space($individual/individual_short_name)" />
                </td>
            </tr>
        </xsl:if>
        <tr>
            <td class="leftColumnIdentTable">
                <xsl:text>Документ: </xsl:text>
            </td>
            <td class="dataColBasedInfoTable mainListData">
                <xsl:call-template name="accountDocument">
                    <xsl:with-param name="person_info" select="$individual/.." />
                </xsl:call-template>
            </td>
        </tr>

        <xsl:if test="$individual/birthtday">
            <tr>
                <td class="leftColumnIdentTable">
                    <xsl:text>Дата рождения: </xsl:text>
                </td>
                <td class="dataColBasedInfoTable mainListData">
                    <xsl:call-template name="convertDate">
                        <xsl:with-param name="date" select="$individual/birthtday" />
                    </xsl:call-template>
                </td>
            </tr>
        </xsl:if>

        <xsl:if test="$individual/place_of_birth">
            <tr>
                <td class="leftColumnIdentTable">
                    <xsl:text>Место рождения: </xsl:text>
                </td>
                <td class="dataColBasedInfoTable mainListData">
                    <xsl:value-of select="normalize-space($individual/place_of_birth)" />
                </td>
            </tr>
        </xsl:if>

        <xsl:if test="$individual/nationality">
            <tr>
                <td class="leftColumnIdentTable">
                    <xsl:text>Гражданство: </xsl:text>
                </td>
                <td class="dataColBasedInfoTable mainListData">
                    <xsl:call-template name="country_et">
                        <xsl:with-param name="type" select="$individual/nationality" />
                    </xsl:call-template>
                </td>
            </tr>
        </xsl:if>

        <tr>
            <td class="leftColumnIdentTable">
                <xsl:text>Адрес регистрации: </xsl:text>
            </td>
            <td class="dataColBasedInfoTable mainListData">
                <xsl:call-template name="address_legal_t">
                    <xsl:with-param name="root"
                        select="$individual/individual_legal_address" />
                </xsl:call-template>
            </td>
        </tr>

        <tr>
            <td class="leftColumnIdentTable">
                <xsl:text>Фактическое место жительства: </xsl:text>
            </td>
            <td class="dataColBasedInfoTable mainListData">
                <xsl:call-template name="address_legal_t">
                    <xsl:with-param name="root"
                        select="$individual/individual_post_address" />
                </xsl:call-template>
            </td>
        </tr>

        <xsl:if test="$individual/individual_post_address/another">
            <tr>
                <td class="leftColumnIdentTable">
                    <xsl:text>Получатель корреспонденции: </xsl:text>
                </td>
                <td class="dataColBasedInfoTable mainListData">
                    <xsl:value-of
                        select="normalize-space($individual/individual_post_address/another)" />
                </td>
            </tr>
        </xsl:if>

        <xsl:if test="$individual/inn">
            <tr>
                <td class="leftColumnIdentTable">
                    <xsl:text>ИНН: </xsl:text>
                </td>
                <td class="dataColBasedInfoTable mainListData">
                    <xsl:value-of select="normalize-space($individual/inn)" />
                </td>
            </tr>
        </xsl:if>

        <xsl:if test="$individual/additional_document">
            <tr>
                <td class="leftColumnIdentTable">
                    <xsl:text>Тип индивидуальной деятельности (ИП, КФХ, нотариус): </xsl:text>
                </td>
                <td class="dataColBasedInfoTable mainListData">
                    <xsl:call-template name="indiv_business_type_et">
                        <xsl:with-param name="type"
                            select="$individual/additional_document/indiv_business_type" />
                    </xsl:call-template>
                </td>
            </tr>
            <xsl:if test="$individual/additional_document/ogrn_indiv">
                <tr>
                    <td class="leftColumnIdentTable">
                        <xsl:text>Информация о регистрации индивидуального предпринимателя (фермерского хозяйства): </xsl:text>
                    </td>
                    <td class="dataColBasedInfoTable mainListData">
                        <xsl:text>ОГРН </xsl:text>
                        <xsl:value-of
                            select="normalize-space($individual/additional_document/ogrn_indiv/ogrn)" />
                        <xsl:text> выдан </xsl:text>
                        <xsl:call-template name="convertDate">
                            <xsl:with-param name="date"
                                select="$individual/additional_document/ogrn_indiv/ogrn_date" />
                        </xsl:call-template>
                        <xsl:text>г. </xsl:text>
                        <xsl:value-of
                            select="normalize-space($individual/additional_document/ogrn_indiv/ogrn_place)" />
                    </td>
                </tr>
            </xsl:if>
            <xsl:if test="$individual/additional_document/doc_notary">
                <tr>
                    <td class="leftColumnIdentTable">
                        <xsl:text>Документы нотариуса: </xsl:text>
                    </td>
                    <td class="dataColBasedInfoTable mainListData">
                       <xsl:call-template name="doc_notary">
                           <xsl:with-param name="root" select="$individual/additional_document/doc_notary" />
                       </xsl:call-template>
                    </td>
               </tr>
            </xsl:if>
        </xsl:if>

        <xsl:if
            test="$individual/foreign_public and ($individual/foreign_public/official='Yes' or $individual/foreign_public/relative='Yes')">
            <tr>
                <td class="leftColumnIdentTable">
                    <xsl:text>Принадлежность к иностранному публичному должностному лицу: </xsl:text>
                </td>
                <td class="dataColBasedInfoTable mainListData">

                    <xsl:if test="$individual/foreign_public/official='Yes'">
                        <xsl:text>Является иностранным публичным должностным лицом</xsl:text>
                        <xsl:if test="$individual/foreign_public/relative='Yes'">
                            <br />
                        </xsl:if>
                    </xsl:if>

                    <xsl:if test="$individual/foreign_public/relative='Yes'">
                        <xsl:text>Является супругом, близким родственником иностранного публичного должностного лица</xsl:text>
                    </xsl:if>

                    <xsl:if test="$individual/foreign_public/comment">
                        <br />
                        <xsl:value-of
                            select="normalize-space($individual/foreign_public/comment)" />
                    </xsl:if>
                </td>
            </tr>
        </xsl:if>

        <xsl:if test="$individual/owner_status">
            <tr>
                <td class="leftColumnIdentTable">
                    <xsl:text>Статус ЗЛ: </xsl:text>
                </td>
                <td class="dataColBasedInfoTable mainListData">
                    <xsl:call-template name="owner_status_et">
                        <xsl:with-param name="type" select="$individual/owner_status" />
                    </xsl:call-template>
                </td>
            </tr>
        </xsl:if>
    </xsl:template>
    <!-- вывод анкеты счета ОС -->
    <xsl:template name="coownersProfile">
        <xsl:param name="coowners" />

        <tr>
            <td class="leftColumnIdentTable">
                <xsl:text>Вид собственности: </xsl:text>
            </td>
            <td class="dataColBasedInfoTable mainListData">
                <xsl:call-template name="coowners_type_et">
                    <xsl:with-param name="type" select="$coowners/owners_type" />
                </xsl:call-template>
            </td>
        </tr>

        <tr>
            <td class="leftColumnIdentTable">
                <xsl:text>Полное наименование счёта ОС: </xsl:text>
            </td>
            <td class="dataColBasedInfoTable mainListData">
                <xsl:value-of select="normalize-space($coowners/coowners_name)" />
            </td>
        </tr>
        <tr>
            <td class="leftColumnIdentTable">
                <xsl:text>Краткое наименование счёта ОС: </xsl:text>
            </td>
            <td class="dataColBasedInfoTable mainListData">
                <xsl:value-of select="normalize-space($coowners/coowners_short_name)" />
            </td>
        </tr>

        <tr>
            <td class="leftColumnIdentTable">
                <xsl:text>Фактическое место жительства: </xsl:text>
            </td>
            <td class="dataColBasedInfoTable mainListData">
                <xsl:call-template name="address_legal_t">
                    <xsl:with-param name="root"
                        select="$coowners/coowners_post_address" />
                </xsl:call-template>
            </td>
        </tr>

        <xsl:if test="$coowners/coowners_post_address/another">
            <tr>
                <td class="leftColumnIdentTable">
                    <xsl:text>Получатель корреспонденции: </xsl:text>
                </td>
                <td class="dataColBasedInfoTable mainListData">
                    <xsl:value-of
                        select="normalize-space($coowners/coowners_post_address/another)" />
                </td>
            </tr>
        </xsl:if>
    </xsl:template>
    <!-- вывод банковский реквизитов для анкеты -->
    <xsl:template name="bankProfile">
        <xsl:param name="bank_details" />
        <tr>
            <td class="leftColumnIdentTable">
                <xsl:text>Банковские реквизиты: </xsl:text>
            </td>
            <td class="dataColBasedInfoTable mainListData">
                <xsl:choose>
                    <xsl:when test="$bank_details/bank_nostruct">
                        <xsl:value-of select="normalize-space($bank_details/bank_nostruct)" />
                    </xsl:when>
                    <xsl:when test="$bank_details/bank_struct">
                        <xsl:variable name="bank_struct" select="$bank_details/bank_struct" />
                        <xsl:variable name="cash_rub_dtls" select="$bank_struct/cash_rub_dtls" />

                        <xsl:if test="$cash_rub_dtls/account">
                            <xsl:text>Лицевой/расчетный счета лица </xsl:text>
                            <xsl:value-of select="normalize-space($cash_rub_dtls/account)" />
                        </xsl:if>

                        <xsl:if test="$cash_rub_dtls/account_person">
                            <xsl:text>Лицевой/расчетный счета лица </xsl:text>
                            <xsl:value-of select="normalize-space($cash_rub_dtls/account_person)" />
                            <xsl:text>, </xsl:text>
                            <xsl:if test="$cash_rub_dtls/bank_branch_name">
                                <xsl:text>Наименование филиала (отделения) банка: </xsl:text>
                                <xsl:value-of select="normalize-space($cash_rub_dtls/bank_branch_name)" />
                                <xsl:text>, </xsl:text>
                            </xsl:if>
                            <xsl:text>расчетный счет филиала (отделения) банка </xsl:text>
                            <xsl:value-of select="normalize-space($cash_rub_dtls/account_branch)" />
                        </xsl:if>

                        <xsl:text>, наименование банка: </xsl:text>
                        <xsl:value-of select="normalize-space($cash_rub_dtls/bank_name)" />

                        <xsl:text>, город банка: </xsl:text>
                        <xsl:value-of select="normalize-space($cash_rub_dtls/bank_city)" />

                        <xsl:text>, БИК банка: </xsl:text>
                        <xsl:value-of select="normalize-space($cash_rub_dtls/ruic)" />

                        <xsl:text>, корреспондентский счет банка: </xsl:text>
                        <xsl:value-of select="normalize-space($cash_rub_dtls/bank_corr)" />

                        <xsl:if test="$cash_rub_dtls/bank_inn">
                            <xsl:text>, ИНН банка: </xsl:text>
                            <xsl:value-of select="normalize-space($cash_rub_dtls/bank_inn)" />
                        </xsl:if>

                        <xsl:if test="$bank_struct/pay_add_info">
                            <xsl:text>, Дополнительные реквизиты платежа: </xsl:text>
                            <xsl:value-of select="normalize-space($bank_struct/pay_add_info)" />
                        </xsl:if>

                        <xsl:if test="$bank_struct/pay_name">
                            <xsl:text>, Наименование плательщика/ получателя: </xsl:text>
                            <xsl:value-of select="normalize-space($bank_struct/pay_name)" />
                        </xsl:if>

                        <xsl:if test="$bank_struct/inn">
                            <xsl:text>, код ИНН плательщика/ получателя: </xsl:text>
                            <xsl:value-of select="normalize-space($bank_struct/inn)" />
                        </xsl:if>
                    </xsl:when>
                </xsl:choose>
            </td>
        </tr>
    </xsl:template>
    <!-- категории нологоплательщика для анкеты -->
    <xsl:template name="tax_statusProfile">
        <xsl:param name="tax_status" />
        <tr>
            <td class="leftColumnIdentTable">
                <xsl:text>Категория налогоплательщика: </xsl:text>
            </td>
            <td class="dataColBasedInfoTable mainListData">
                <xsl:call-template name="tax_status_et">
                    <xsl:with-param name="type" select="$tax_status/tax_status_code" />
                </xsl:call-template>

                <xsl:if test="$tax_status/nonresident_type">
                    <xsl:text>, </xsl:text>
                    <xsl:call-template name="nonresident_type_et">
                        <xsl:with-param name="type" select="$tax_status/nonresident_type" />
                    </xsl:call-template>
                </xsl:if>

                <xsl:if test="$tax_status/tax_exempt_indicator='Yes'">
                    <xsl:text>, имеется налоговая льгота</xsl:text>
                </xsl:if>

                <xsl:if test="$tax_status/narrative">
                    <xsl:text>, </xsl:text>
                    <xsl:value-of select="normalize-space($tax_status/narrative)" />
                </xsl:if>
            </td>
        </tr>
    </xsl:template>
    <!-- Вывод базовых данных анкеты -->
    <xsl:template name="baseFormData">
        <xsl:param name="formData" />

        <xsl:if test="$formData/account_type">
            <tr>
                <td class="leftColumnIdentTable">
                    <xsl:text>Вид счёта: </xsl:text>
                </td>
                <td class="dataColBasedInfoTable mainListData">
                    <xsl:call-template name="account_type_et">
                        <xsl:with-param name="type" select="$formData/account_type" />
                    </xsl:call-template>
                </td>
            </tr>
        </xsl:if>

        <xsl:if test="$formData/*/juridical">
            <xsl:call-template name="juridicalProfile">
                <xsl:with-param name="juridical" select="$formData/*/juridical" />
            </xsl:call-template>
        </xsl:if>

        <xsl:if test="$formData/*/individual">
            <xsl:call-template name="individualProfile">
                <xsl:with-param name="individual" select="$formData/*/individual" />
            </xsl:call-template>
        </xsl:if>

        <xsl:if test="$formData/*/coowners">
            <xsl:call-template name="coownersProfile">
                <xsl:with-param name="coowners" select="$formData/*/coowners" />
            </xsl:call-template>
        </xsl:if>

        <tr>
            <td class="leftColumnIdentTable">
                <xsl:text>Контактная информация: </xsl:text>
            </td>
            <td class="dataColBasedInfoTable mainListData">
                <xsl:call-template name="contacts_t">
                    <xsl:with-param name="root" select="$formData/contacts" />
                </xsl:call-template>
            </td>
        </tr>

        <xsl:if test="$formData/bank_details">
            <xsl:call-template name="bankProfile">
                <xsl:with-param name="bank_details" select="$formData/bank_details" />
            </xsl:call-template>
        </xsl:if>

        <xsl:if test="$formData/tax_status">
            <xsl:call-template name="tax_statusProfile">
                <xsl:with-param name="tax_status" select="$formData/tax_status" />
            </xsl:call-template>
        </xsl:if>

        <tr>
            <td class="leftColumnIdentTable">
                <xsl:text>Форма выплаты доходов по ценным бумагам: </xsl:text>
            </td>
            <td class="dataColBasedInfoTable mainListData">
                <xsl:call-template name="distr_div_frm_et">
                    <xsl:with-param name="type" select="$formData/distr_div_frm" />
                </xsl:call-template>
            </td>
        </tr>

        <tr>
            <td class="leftColumnIdentTable">
                <xsl:text>Способ доставки корреспонденции: </xsl:text>
            </td>
            <td class="dataColBasedInfoTable mainListData">
                <xsl:call-template name="letter_go_type_et">
                    <xsl:with-param name="type" select="$formData/letter_go_type" />
                </xsl:call-template>
            </td>
        </tr>
        <xsl:if test="$formData/doc_by_post">
            <td class="leftColumnIdentTable">
                <xsl:text>Допустимо предоставление документов регистратору почтовым отправлением: </xsl:text>
            </td>
            <td class="dataColBasedInfoTable mainListData">
                <xsl:choose>
                    <xsl:when test="$formData/doc_by_post = 'Yes'">
                        <xsl:text>Да</xsl:text>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:text>Нет</xsl:text>
                    </xsl:otherwise>
                </xsl:choose>
            </td>
          <br />
        </xsl:if>
        
        <xsl:if test="$formData/comment">
            <tr>
                <td class="leftColumnIdentTable">
                    <xsl:text>Комментарий к анкете: </xsl:text>
                </td>
                <td class="dataColBasedInfoTable mainListData">
                    <xsl:value-of select="normalize-space($formData/comment)" />
                </td>
            </tr>
        </xsl:if>

    </xsl:template>
    <!-- Заголовки для подерживаемых сообщений -->
    <!-- Анкета совладельца -->
    <xsl:template match="FORM_OF_COOWNERS">

        <!-- Сначала вывводятся все данные о счете ОС (если есть) [идентификатор 
            + анкета] После выводятся все данные о совладенльце (если есть) [идентификатор 
            + анкета] -->

        <xsl:if test="//account_ident">
            <xsl:call-template name="account_ident_t">
                <xsl:with-param name="title" select="'Счет ОС'" />
                <xsl:with-param name="root" select="//account_ident" />
            </xsl:call-template>
        </xsl:if>

        <xsl:if test="./coowners_data">
            <table class="identTable">
                <tbody>
                    <tr>
                        <td class="titleForIdentTableRow sizeData">
                            <xsl:text>Анкетные данные счета ОС</xsl:text>
                        </td>
                    </tr>
                </tbody>
            </table>

            <table class="identTableRowEnd">
                <tbody>
                    <xsl:call-template name="baseFormData">
                        <xsl:with-param name="formData" select="./coowners_data" />
                    </xsl:call-template>
                </tbody>
            </table>
        </xsl:if>

        <xsl:if test="//coowner_ident">
            <xsl:call-template name="coowner_ident_t">
                <xsl:with-param name="root" select="//coowner_ident" />
            </xsl:call-template>
        </xsl:if>

        <xsl:if test="./coowner_form/coowner_form">
            <table class="identTable">
                <tbody>
                    <tr>
                        <td class="titleForIdentTableRow sizeData">
                            <xsl:text>Анкетные данные совладельца</xsl:text>
                        </td>
                    </tr>
                </tbody>
            </table>

            <table class="identTableRowEnd">
                <tbody>
                    <xsl:if test="./coowner_form/coowner_form/coowner_numb">
                        <tr>
                            <td class="leftColumnIdentTable">
                                <xsl:text>Номер совладельца по счету ОС: </xsl:text>
                            </td>
                            <td class="dataColBasedInfoTable mainListData">
                                <xsl:value-of
                                    select="normalize-space(./coowner_form/coowner_form/coowner_numb)" />
                            </td>
                        </tr>

                    </xsl:if>

                    <xsl:call-template name="baseFormData">
                        <xsl:with-param name="formData" select="./coowner_form/coowner_form" />
                    </xsl:call-template>


                    <xsl:if test="./coowner_form/quota">
                        <tr>
                            <td class="leftColumnIdentTable">
                                <xsl:text>Доля совладельца: </xsl:text>
                            </td>
                            <td class="dataColBasedInfoTable mainListData">
                                <xsl:value-of select="normalize-space(./coowner_form/quota/numerator)" />
                                <xsl:text>/</xsl:text>
                                <xsl:value-of
                                    select="normalize-space(./coowner_form/quota/denominator)" />
                            </td>
                        </tr>
                    </xsl:if>
                </tbody>
            </table>
        </xsl:if>
    </xsl:template>
    <!-- Анкета зарегистрированного лица -->
    <xsl:template match="FORM_OF_SHAREHOLDERS">
        <table class="identTable">
            <tbody>
                <tr>
                    <td class="titleForIdentTableRow sizeData">
                        <xsl:text>Анкетные данные (</xsl:text>
                        <xsl:call-template name="form_for_et">
                            <xsl:with-param name="type" select="./form_for" />
                        </xsl:call-template>
                        <xsl:text>)</xsl:text>
                    </td>
                </tr>
            </tbody>
        </table>

        <table class="identTableRowEnd">
            <tbody>
                <xsl:call-template name="baseFormData">
                    <xsl:with-param name="formData"
                        select="./account_form/shareholder_form" />
                </xsl:call-template>
                
                <xsl:if test="./account_form/add_trustee/profi">
                     <tr>
                        <td class="leftColumnIdentTable">
                            <xsl:text>ДУ - профучастник рынка ЦБ: </xsl:text>
                        </td>
                        <td class="dataColBasedInfoTable mainListData">
                            <xsl:choose>
                                 <xsl:when test="./account_form/add_trustee/profi='Yes'">
                                     <xsl:text>Да</xsl:text>
                                 </xsl:when>
                                 <xsl:otherwise>
                                     <xsl:text>Нет</xsl:text>
                                 </xsl:otherwise>
                            </xsl:choose>
                        </td>
                    </tr>
                </xsl:if>
            </tbody>
        </table>
        <xsl:if test="./account_form/add_trustee/founder_form">
            <table class="identTable">
                <tbody>
                    <tr>
                        <td class="titleForIdentTableRow sizeData">
                            <xsl:text>Анкета учредителя</xsl:text>
                        </td>
                    </tr>
                </tbody>
            </table>
            <table class="identTableRowEnd">
                <tbody>
                    <xsl:call-template name="baseFormData">
                        <xsl:with-param name="formData"
                            select="./account_form/add_trustee/founder_form" />
                    </xsl:call-template>
                    <xsl:if test="./account_form/add_trustee/du_conditions">
                       <xsl:if test="./account_form/add_trustee/du_conditions/contract_num">
                           <tr>
                                <td class="leftColumnIdentTable">
                                    <xsl:text>Номер договора ДУ: </xsl:text>
                                </td>
                                <td class="dataColBasedInfoTable mainListData">
                                    <xsl:value-of select="normalize-space(./account_form/add_trustee/du_conditions/contract_num)" />
                                </td>
                          </tr>
                       </xsl:if>
                       <xsl:if test="./account_form/add_trustee/du_conditions/contract_date">
                           <tr>
                                <td class="leftColumnIdentTable">
                                    <xsl:text>Дата договора ДУ: </xsl:text>
                                </td>
                                <td class="dataColBasedInfoTable mainListData">
                                    <xsl:call-template name="convertDate">
                                        <xsl:with-param name="date"
                                            select="./account_form/add_trustee/du_conditions/contract_date" />
                                    </xsl:call-template>                
                                </td>
                          </tr>
                       </xsl:if>
                       <tr>
                            <td class="leftColumnIdentTable">
                                <xsl:text>ДУ не имеет права распоряжаться ценными бумагами: </xsl:text>
                            </td>
                            <td class="dataColBasedInfoTable mainListData">
                                <xsl:choose>
                                    <xsl:when test="./account_form/add_trustee/du_conditions/right_du_nodispose='Yes'">
                                        <xsl:text>Да</xsl:text>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <xsl:text>Нет</xsl:text>
                                    </xsl:otherwise>
                                </xsl:choose>
                            </td>
                       </tr>
                       <tr>
                            <td class="leftColumnIdentTable">
                                <xsl:text>Право голоса принадлежит учредителю: </xsl:text>
                            </td>
                            <td class="dataColBasedInfoTable mainListData">
                                <xsl:choose>
                                    <xsl:when test="./account_form/add_trustee/du_conditions/right_founder_voting='Yes'">
                                        <xsl:text>Да</xsl:text>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <xsl:text>Нет</xsl:text>
                                    </xsl:otherwise>
                                </xsl:choose>
                            </td>
                       </tr>
                       <tr>
                            <td class="leftColumnIdentTable">
                                <xsl:text>Право получения дохода принадлежит учредителю: </xsl:text>
                            </td>
                            <td class="dataColBasedInfoTable mainListData">
                                <xsl:choose>
                                    <xsl:when test="./account_form/add_trustee/du_conditions/right_founder_divid='Yes'">
                                        <xsl:text>Да</xsl:text>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <xsl:text>Нет</xsl:text>
                                    </xsl:otherwise>
                                </xsl:choose>
                            </td>
                       </tr>
                    </xsl:if>
                </tbody>
            </table>
        </xsl:if>
    </xsl:template>
    <!-- Сообщение-ответ в свободном формате -->
    <xsl:template match="FREE_FORMAT_ANSWER" />
    <!-- Сообщение-письмо в свободном формате -->
    <xsl:template match="FREE_FORMAT_MESSAGE">
        <table class="identTable">
            <tbody>
                <tr>
                    <td class="titleForIdentTableRow sizeData">
                        <xsl:text>Содержание</xsl:text>
                    </td>
                </tr>
            </tbody>
        </table>
    
        <table class="identTableRowEnd">
            <tbody>
                <tr>
                    <td class="leftColumnIdentTable">
                        <xsl:text>Текст сообщения - письма: </xsl:text>
                    </td>
                    <td class="dataColBasedInfoTable mainListData">
                       <xsl:value-of select="text"/>
                    </td>
                </tr>
            </tbody>
        </table>
    </xsl:template>
    <!-- Сообщение-запрос в свободном формате -->
    <xsl:template match="FREE_FORMAT_QUERY" />
    <!-- Распоряжение на закрытие лицевого счета -->
    <xsl:template match="INSTRUCTION_TO_ACCOUNT_CLOSE" />
    <!-- Распоряжение на отмену полномочий представителя -->
    <xsl:template match="INSTRUCTION_TO_AUTHORISED_CANCEL">
        <table class="identTable">
            <tbody>
                <tr>
                    <td class="titleForIdentTableRow sizeData">
                        <xsl:text>Идентификатор уполномоченного представителя, по которому выполняется отмена полномочий </xsl:text>
                    </td>
                </tr>
            </tbody>
        </table>

        <table class="identTableRowEnd">
            <tbody>
                <tr>
                    <td class="leftColumnIdentTable">
                        <xsl:text>Тип уполномоченного представителя/должностного лица: </xsl:text>
                    </td>
                    <td class="dataColBasedInfoTable mainListData">
                        <xsl:call-template name="authorised_person_type_et">
                            <xsl:with-param name="type"
                                select="authorised_cancel/ident_authorised_person/authorised_person/authorised_person_type" />
                        </xsl:call-template>
                    </td>
                </tr>
                <xsl:if
                    test="authorised_cancel/ident_authorised_person/authorised_person/authorised_person_position">
                    <tr>
                        <td class="leftColumnIdentTable">
                            <xsl:text>Должность: </xsl:text>
                        </td>
                        <td class="dataColBasedInfoTable mainListData">
                            <xsl:value-of
                                select="authorised_cancel/ident_authorised_person/authorised_person/authorised_person_position" />
                        </td>
                    </tr>
                </xsl:if>
                <tr>
                    <td class="leftColumnIdentTable">
                        <xsl:text>Основные данные уполномоченного представителя: </xsl:text>
                    </td>
                    <td class="dataColBasedInfoTable mainListData">
                        <xsl:call-template name="shareholder_info_simple">
                            <xsl:with-param name="root"
                                select="authorised_cancel/ident_authorised_person/authorised_person/authorised_person_info" />
                        </xsl:call-template>
                    </td>
                </tr>
                <xsl:if
                    test="authorised_cancel/ident_authorised_person/authorised_person/bases_of_powers">
                    <tr>
                        <td class="leftColumnIdentTable">
                            <xsl:text>Основания для полномочий: </xsl:text>
                        </td>
                        <td class="dataColBasedInfoTable mainListData">
                            <xsl:call-template name="bases_of_powers_t">
                                <xsl:with-param name="root"
                                    select="authorised_cancel/ident_authorised_person/authorised_person/bases_of_powers" />
                            </xsl:call-template>
                        </td>
                    </tr>
                </xsl:if>
            </tbody>
        </table>
    </xsl:template>
    <!-- Распоряжение на блокирование ЦБ -->
    <xsl:template match="INSTRUCTION_TO_BLOCK">
        <xsl:call-template name="burden_block">
            <xsl:with-param name="root" select="./block_info" />
            <xsl:with-param name="mainHeader" select="'Описание обременения: '" />
        </xsl:call-template>
    </xsl:template>
    <!-- Распоряжение на блокирование операций по счету -->
    <xsl:template match="INSTRUCTION_TO_BLOCKOPER">
        <xsl:call-template name="operation_blockoper_t">
            <xsl:with-param name="root" select="blockoper_info" />
        </xsl:call-template>
    </xsl:template>
    <!-- Распоряжение на конвертацию ЦБ по одному счету -->
    <xsl:template match="INSTRUCTION_TO_CONVERT" />
    <!-- Распоряжение на удаление совладельца из счета общей собственности -->
    <xsl:template match="INSTRUCTION_TO_COOWNER_CLOSE" />
    <!-- Передаточное распоряжение -->
    <xsl:template match="INSTRUCTION_TO_DELIVER">
        <table class="identTable">
            <tbody>
                <tr>
                    <td class="titleForIdentTableRow sizeData">
                        <xsl:text>Описание сделки</xsl:text>
                    </td>
                </tr>
            </tbody>
        </table>

        <table class="identTableRowEnd">
            <tbody>
                <tr>
                    <td class="leftColumnIdentTable">
                        <xsl:text>Обременение: </xsl:text>
                    </td>
                    <td class="dataColBasedInfoTable mainListData">
                        <xsl:choose>
                            <xsl:when test="transaction/transaction/block_indicator='No'">
                                <xsl:text>Отсутсвует</xsl:text>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:text>Имеется</xsl:text>
                            </xsl:otherwise>
                        </xsl:choose>
                    </td>
                </tr>
                <xsl:if test="transaction/transaction/pledge_info">
                    <tr>
                        <td class="leftColumnIdentTable">
                            <xsl:text>Описание обременения: </xsl:text>
                        </td>
                        <td class="dataColBasedInfoTable mainListData">
                            <xsl:call-template name="burden_simple">
                                <xsl:with-param name="burdenPrev" select="No" />
                                <xsl:with-param name="burdenType"
                                    select="transaction/transaction/pledge_info/pledge_type" />
                                <xsl:with-param name="burdenRepead"
                                    select="transaction/transaction/pledge_info/repeat_pledge" />
                                <xsl:with-param name="burdenQuantity"
                                    select="transaction/transaction/pledge_info/quantity" />
                                <xsl:with-param name="burdenDate"
                                    select="transaction/transaction/pledge_info/pledge_date" />
                                <xsl:with-param name="burdenCondition"
                                    select="transaction/transaction/pledge_info/condition_pledge" />
                                <xsl:with-param name="burdenComment"
                                    select="transaction/transaction/pledge_info/comment" />
                                <xsl:with-param name="burdenIdentificator"
                                    select="transaction/transaction/pledge_info/id_pledge" />
                                <xsl:with-param name="burdenInfo"
                                    select="transaction/transaction/pledge_info" />
                            </xsl:call-template>
                        </td>
                    </tr>
                </xsl:if>
                <xsl:if test="transaction/transaction/unpaid_info">
                    <tr>
                        <td class="leftColumnIdentTable">
                            <xsl:text>Описание обременения: </xsl:text>
                        </td>
                        <td class="dataColBasedInfoTable mainListData">
                            <xsl:call-template name="burden_simple">
                                <xsl:with-param name="burdenPrev" select="No" />
                                <xsl:with-param name="burdenType"
                                    select="transaction/transaction/unpaid_info/unpaid_type" />
                                <xsl:with-param name="burdenRepead"
                                    select="transaction/transaction/unpaid_info/repeat_unpaid" />
                                <xsl:with-param name="burdenQuantity"
                                    select="transaction/transaction/unpaid_info/quantity" />
                                <xsl:with-param name="burdenDate"
                                    select="transaction/transaction/unpaid_info/unpaid_date" />
                                <xsl:with-param name="burdenCondition"
                                    select="transaction/transaction/unpaid_info/condition_unpaid" />
                                <xsl:with-param name="burdenComment"
                                    select="transaction/transaction/unpaid_info/comment" />
                                <xsl:with-param name="burdenIdentificator"
                                    select="transaction/transaction/unpaid_info/id_unpaid" />
                                <xsl:with-param name="burdenInfo"
                                    select="transaction/transaction/unpaid_info" />
                            </xsl:call-template>
                        </td>
                    </tr>
                </xsl:if>
                <xsl:if test="transaction/transaction/comment">
                    <tr>
                        <td class="leftColumnIdentTable">
                            <xsl:text>Дополнительные данные: </xsl:text>
                        </td>
                        <td class="dataColBasedInfoTable mainListData">
                            <xsl:value-of select="transaction/transaction/comment" />
                        </td>
                    </tr>
                </xsl:if>
                <xsl:if test="transaction/transaction/based_info">
                    <tr>
                        <td class="leftColumnIdentTable">
                            <xsl:text>Документы-основания: </xsl:text>
                        </td>
                        <td class="dataColBasedInfoTable mainListData">
                            <xsl:call-template name="based_infoRow">
                                <xsl:with-param name="root" select="transaction/transaction" />
                            </xsl:call-template>
                        </td>
                    </tr>
                </xsl:if>
                <xsl:if test="transaction/transaction/reference">
                    <tr>
                        <td class="leftColumnIdentTable">
                            <xsl:text>Референс поручения: </xsl:text>
                        </td>
                        <td class="dataColBasedInfoTable mainListData">
                            <xsl:value-of select="transaction/transaction/reference" />
                        </td>
                    </tr>
                </xsl:if>
                <xsl:if test="transaction/transaction/owner">
                    <tr>
                        <td class="leftColumnIdentTable">
                            <xsl:text>Наименование владельца: </xsl:text>
                        </td>
                        <td class="dataColBasedInfoTable mainListData">
                            <xsl:value-of select="transaction/transaction/owner" />
                        </td>
                    </tr>
                </xsl:if>
            </tbody>
        </table>
    </xsl:template>
    <!-- Распоряжение на отзыв документа -->
    <xsl:template match="INSTRUCTION_TO_DOC_RECALL">
        <table class="identTable">
            <tbody>
                <tr>
                    <td class="titleForIdentTableRow sizeData">
                        <xsl:text>Описание распоряжения</xsl:text>
                    </td>
                </tr>
            </tbody>
        </table>

        <table class="identTableRowEnd">
            <tbody>
                <tr>
                    <td class="leftColumnIdentTable">
                        <xsl:text>Документ, который отзывается: </xsl:text>
                    </td>
                    <td class="dataColBasedInfoTable mainListData">
                        <xsl:text>Исходящий у отправителя № </xsl:text>
                        <xsl:value-of select="doc_link/out_doc_num" />

                        <xsl:if test="doc_link/out_doc_date">
                            <xsl:text> от </xsl:text>
                            <xsl:call-template name="date_or_datetime_t">
                                <xsl:with-param name="root" select="doc_link/out_doc_date" />
                            </xsl:call-template>
                        </xsl:if>

                        <xsl:if test="doc_link/in_doc_num or doc_link/in_reg_date">
                            <br />
                            <xsl:text>Входящий у регистратора </xsl:text>
                            <xsl:if test="doc_link/in_doc_num">
                                <xsl:text> № </xsl:text>
                                <xsl:value-of select="doc_link/in_doc_num" />
                            </xsl:if>

                            <xsl:if test="doc_link/in_reg_date">
                                <xsl:text> от </xsl:text>
                                <xsl:call-template name="date_or_datetime_t">
                                    <xsl:with-param name="root" select="doc_link/in_reg_date" />
                                </xsl:call-template>
                            </xsl:if>
                        </xsl:if>

                        <xsl:if test="doc_link/ta_doc_num or doc_link/ta_doc_date">
                            <br />
                            <xsl:text>Входящий у транфер-агента </xsl:text>
                            <xsl:if test="doc_link/in_doc_num">
                                <xsl:text> № </xsl:text>
                                <xsl:value-of select="doc_link/ta_doc_num" />
                            </xsl:if>

                            <xsl:if test="doc_link/ta_doc_date">
                                <xsl:text> от </xsl:text>
                                <xsl:call-template name="date_or_datetime_t">
                                    <xsl:with-param name="root" select="doc_link/ta_doc_date" />
                                </xsl:call-template>
                            </xsl:if>
                        </xsl:if>
                    </td>
                </tr>
                <tr>
                    <td class="leftColumnIdentTable">
                        <xsl:text>Основание отзыва документа: </xsl:text>
                    </td>
                    <td class="dataColBasedInfoTable mainListData">
                        <xsl:value-of select="reason" />
                    </td>
                </tr>
            </tbody>
        </table>
    </xsl:template>
    <!-- Залоговое распоряжение -->
    <xsl:template match="INSTRUCTION_TO_PLEDGE">
        <xsl:call-template name="burden_block">
            <xsl:with-param name="root" select="./pledge_info" />
            <xsl:with-param name="mainHeader" select="'Описание обременения: '" />
        </xsl:call-template>
    </xsl:template>
    <!-- Распоряжение о передаче права залога -->
    <xsl:template match="INSTRUCTION_TO_PLEDGE_DELIVER">
        <xsl:call-template name="burden_block">
            <xsl:with-param name="root"
                select="./pledge_deliver_info" />
            <xsl:with-param name="mainHeader" select="'Описание обременения: '" />
        </xsl:call-template>
    </xsl:template>
    <!-- Распоряжение о внесении изменений в условия залога -->
    <xsl:template match="INSTRUCTION_TO_PLEDGE_MODIFY">
        <xsl:call-template name="burden_block">
            <xsl:with-param name="root" select="./pledge_modify_info" />
            <xsl:with-param name="mainHeader" select="'Описание обременения: '" />
        </xsl:call-template>
    </xsl:template>
    <!-- Распоряжение на изменение долей совладельцев -->
    <xsl:template match="INSTRUCTION_TO_QUOTA_COOWNERS">
        <table class="identTable">
            <tbody>
                <tr>
                    <td class="titleForIdentTableRow sizeData">
                        <xsl:text>Данные распоряжения на установку долей совладельцев</xsl:text>
                    </td>
                </tr>
            </tbody>
        </table>

        <table class="identTableRowEnd">
            <tbody>
                <xsl:for-each select="coowner_quota/coowner_quota">
                    <tr>
                        <td class="leftColumnIdentTable">
                            <xsl:text>Совладелец № </xsl:text>
                            <xsl:value-of select="position()" />
                            <xsl:text>: </xsl:text>
                        </td>
                        <td class="dataColBasedInfoTable mainListData">
                            <xsl:if test="coowner_ident/id_coowner">
                                <xsl:text>Уникальный номер совладельца по счету ОС: </xsl:text>
                                <xsl:value-of select="coowner_ident/id_coowner" />
                                <br />
                            </xsl:if>
                            <xsl:call-template name="shareholder_info_simple">
                                <xsl:with-param name="root" select="coowner_ident" />
                            </xsl:call-template>
                            <xsl:if test="quota">
                                <br />
                                <xsl:text>Доля совладельца: </xsl:text>
                                <xsl:value-of select="quota/numerator" />
                                <xsl:text>/</xsl:text>
                                <xsl:value-of select="quota/denominator" />
                            </xsl:if>
                        </td>
                    </tr>
                </xsl:for-each>
            </tbody>
        </table>
    </xsl:template>
    <!-- Распоряжение на снятие блокирования ЦБ -->
    <xsl:template match="INSTRUCTION_TO_UNBLOCK">
        <xsl:call-template name="burden_block">
            <xsl:with-param name="root" select="./unblock_info" />
            <xsl:with-param name="burdenQuantity"
                select="./unblock_info/block_info/quantity" />
            <xsl:with-param name="mainHeader" select="'По обременению: '" />
        </xsl:call-template>
    </xsl:template>
    <!-- Распоряжение на прекращение блокирования операций по счету -->
    <xsl:template match="INSTRUCTION_TO_UNBLOCKOPER">
        <xsl:call-template name="operation_blockoper_t">
            <xsl:with-param name="root" select="blockoper_info" />
        </xsl:call-template>
    </xsl:template>
    <!-- Распоряжение об обременении ЦБ обязательством оплаты -->
    <xsl:template match="INSTRUCTION_TO_UNPAID">
        <xsl:call-template name="burden_block">
            <xsl:with-param name="root" select="./unpaid_info" />
            <xsl:with-param name="mainHeader" select="'По обременению: '" />
        </xsl:call-template>
    </xsl:template>
    <!-- Распоряжение о снятии обременения обязательства оплаты -->
    <xsl:template match="INSTRUCTION_TO_UNPAID_CANCEL">
        <xsl:call-template name="burden_block">
            <xsl:with-param name="root" select="./unpaid_cancel_info" />
            <xsl:with-param name="burdenQuantity"
                select="./unpaid_cancel_info/unpaid_info/quantity" />
            <xsl:with-param name="mainHeader" select="'По обременению: '" />
        </xsl:call-template>
    </xsl:template>
    <!-- Распоряжение о прекращении залога -->
    <xsl:template match="INSTRUCTION_TO_UNPLEDGE">
        <xsl:call-template name="burden_block">
            <xsl:with-param name="root" select="./unpledge_info" />
            <xsl:with-param name="mainHeader" select="'По обременению: '" />
        </xsl:call-template>
    </xsl:template>
    <!-- Уведомление об отзыве документа -->
    <xsl:template match="NOTICE_OF_DOC_RECALL" />
    <!-- Уведомление о получении документа -->
    <xsl:template match="NOTICE_OF_DOC_RECEPTION" />
    <!-- Сведения о выдаче исходящего документа -->
    <xsl:template match="NOTICE_OF_OUTDOC_GIVE" />
    <!-- Уведомление о сформированном отчете (формируется регистратором в ответ 
        на запрос отчета во внешнем формате, файл самого отчета передается вместе 
        с ним) -->
    <xsl:template match="NOTICE_OF_REPORT" />
    <!-- Доверенность на уполномоченного представителя -->
    <xsl:template match="PROXY_OF_AUTHORISED_PERSON">
        <table class="identTable">
            <tbody>
                <tr>
                    <td class="titleForIdentTableRow sizeData">
                        <xsl:text>Описание </xsl:text>
                    </td>
                </tr>
            </tbody>
        </table>

        <table class="identTableRowEnd">
            <tbody>
                <tr>
                    <td class="leftColumnIdentTable">
                        <xsl:text>Цель подачи документа: </xsl:text>
                    </td>
                    <td class="dataColBasedInfoTable mainListData">
                        <xsl:call-template name="proxy_for_et">
                            <xsl:with-param name="type" select="proxy_for" />
                        </xsl:call-template>
                    </td>
                </tr>
                <tr>
                    <td class="leftColumnIdentTable">
                        <xsl:text>Тип уполномоченного представителя: </xsl:text>
                    </td>
                    <td class="dataColBasedInfoTable mainListData">
                        <xsl:call-template name="authorised_person_type_et">
                            <xsl:with-param name="type"
                                select="proxy_open/authorised_person_type" />
                        </xsl:call-template>
                    </td>
                </tr>
                <tr>
                    <td class="leftColumnIdentTable">
                        <xsl:text>Анкета представителя: </xsl:text>
                    </td>
                    <td class="dataColBasedInfoTable mainListData">
                        <xsl:call-template name="content_form_of_authorised_t">
                            <xsl:with-param name="root"
                                select="proxy_open/authorised_person_form" />
                        </xsl:call-template>
                    </td>
                </tr>
                <tr>
                    <td class="leftColumnIdentTable">
                        <xsl:text>Основания для полономочий: </xsl:text>
                    </td>
                    <td class="dataColBasedInfoTable mainListData">
                        <xsl:call-template name="bases_of_powers_t">
                            <xsl:with-param name="root"
                                select="proxy_open/powers/bases_of_powers" />
                        </xsl:call-template>
                    </td>
                </tr>
                <xsl:if
                    test="proxy_open/powers/rights_signature='Yes' or proxy_open/powers/rights_voting='Yes' 
                or proxy_open/powers/rights_dividend='Yes' or proxy_open/powers/rights_docum='Yes' 
                or proxy_open/powers/rights_courier='Yes'">
                    <tr>
                        <td class="leftColumnIdentTable">
                            <xsl:text>Права представителя: </xsl:text>
                        </td>
                        <td class="dataColBasedInfoTable mainListData">
                            <ul>
                                <xsl:if test="proxy_open/powers/rights_signature='Yes'">
                                    <li class="sizeData">Право на подписание документов на проведение
                                        операций в
                                        реестре;</li>
                                </xsl:if>
                                <xsl:if test="proxy_open/powers/rights_voting='Yes'">
                                    <li class="sizeData">Право голосования;</li>
                                </xsl:if>
                                <xsl:if test="proxy_open/powers/rights_dividend='Yes'">
                                    <li class="sizeData">Право на получение дивидендов;</li>
                                </xsl:if>
                                <xsl:if test="proxy_open/powers/rights_docum='Yes'">
                                    <li class="sizeData">Право на получение информации из реестра;</li>
                                </xsl:if>
                                <xsl:if test="proxy_open/powers/rights_courier='Yes'">
                                    <li class="sizeData">Право на подачу документов;</li>
                                </xsl:if>
                            </ul>
                        </td>
                    </tr>
                </xsl:if>
                <xsl:if test="proxy_open/powers/comment">
                    <tr>
                        <td class="leftColumnIdentTable">
                            <xsl:text>Коментарий: </xsl:text>
                        </td>
                        <td class="dataColBasedInfoTable mainListData">
                            <xsl:value-of select="proxy_open/powers/comment" />
                        </td>
                    </tr>
                </xsl:if>
            </tbody>
        </table>
    </xsl:template>
    <!-- Список владельцев, Список лиц, имеющих право на получение доходов. -->
    <xsl:template match="REGISTER_OF_SHAREHOLDERS">
    </xsl:template>
    <!-- Запрос на сверку с трансфер-агентом (Журнал принятых/отправленных документов). -->
    <xsl:template match="REQUEST_FOR_RECONCILIATION">
        <xsl:call-template name="RECONCILIATION_DATA">
            <xsl:with-param name="root" select="." />
        </xsl:call-template>
    </xsl:template>
    <!-- Запрос (требование) на раскрытие информации номинальному держателю -->
    <xsl:template match="REQUEST_FOR_REGISTER_SHAREHOLDERS" />
    <!-- Запрос на отчет из системы ведения реестра во внешнем формате -->
    <xsl:template match="REQUEST_FOR_REPORT_IN_EXTERNAL_FORMAT">
        <table class="identTable">
            <tbody>
                <tr>
                    <td class="headerIdentTable sizeData">
                        <xsl:text>Описание запроса</xsl:text>

                    </td>
                    <td class="sizeData rightColumnIdentTable" />
                </tr>

                <xsl:if test="./report_info/report_type/narrative">
                    <tr>
                        <td class="leftColumnIdentTable">
                            <xsl:text>Тип запроса: </xsl:text>
                        </td>
                        <td class="sizeData rightColumnIdentTable">
                            <xsl:value-of
                                select="normalize-space(./report_info/report_type/narrative)"></xsl:value-of>
                        </td>
                    </tr>
                </xsl:if>

                <xsl:if test="./report_info/date_or_period">
                    <tr>
                        <xsl:choose>
                            <xsl:when test="./report_info/date_or_period/date">

                                <td class="leftColumnIdentTable">
                                    <xsl:text>На дату: </xsl:text>
                                </td>
                                <td class="sizeData rightColumnIdentTable">
                                    <xsl:choose>
                                        <xsl:when
                                            test="normalize-space(./report_info/date_or_period/date/*[text()])=''">
                                            <xsl:text>[дата формирования отчета]</xsl:text>
                                        </xsl:when>
                                        <xsl:otherwise>

                                            <xsl:call-template name="date_or_datetime_t">
                                                <xsl:with-param name="root"
                                                    select="./report_info/date_or_period/date" />
                                            </xsl:call-template>
                                        </xsl:otherwise>
                                    </xsl:choose>
                                </td>
                            </xsl:when>

                            <xsl:otherwise>
                                <td class="leftColumnIdentTable">
                                    <xsl:text>Период: </xsl:text>
                                </td>
                                <td class="sizeData rightColumnIdentTable">
                                    <xsl:text> с </xsl:text>

                                    <xsl:choose>
                                        <xsl:when
                                            test="normalize-space(./report_info/date_or_period/begin_date[text()])=''">
                                            <xsl:text>[дата формирования отчета]</xsl:text>
                                        </xsl:when>
                                        <xsl:otherwise>
                                            <xsl:call-template name="convertDate">
                                                <xsl:with-param name="date"
                                                    select="./report_info/date_or_period/begin_date" />
                                            </xsl:call-template>
                                        </xsl:otherwise>
                                    </xsl:choose>

                                    <xsl:text> по </xsl:text>

                                    <xsl:choose>
                                        <xsl:when
                                            test="normalize-space(./report_info/date_or_period/end_date[text()])=''">
                                            <xsl:text>[дата формирования отчета]</xsl:text>
                                        </xsl:when>
                                        <xsl:otherwise>
                                            <xsl:call-template name="convertDate">
                                                <xsl:with-param name="date"
                                                    select="./report_info/date_or_period/end_date" />
                                            </xsl:call-template>
                                        </xsl:otherwise>
                                    </xsl:choose>
                                </td>
                            </xsl:otherwise>
                        </xsl:choose>
                    </tr>
                </xsl:if>

                <xsl:if test="./report_info/operation">
                    <tr>
                        <td class="leftColumnIdentTable">
                            <xsl:text>Документ-основание: </xsl:text>
                        </td>
                        <td class="sizeData rightColumnIdentTable">
                            <xsl:call-template name="baseDocOperation">
                                <xsl:with-param name="doc_name"
                                    select="./report_info/operation/base_doc/doc_name" />
                                <xsl:with-param name="out_doc_num"
                                    select="./report_info/operation/base_doc/doc_info/out_doc_num" />
                                <xsl:with-param name="out_doc_date"
                                    select="./report_info/operation/base_doc/doc_info/out_doc_date" />
                                <xsl:with-param name="in_doc_num"
                                    select="./report_info/operation/base_doc/doc_info/in_doc_num" />
                                <xsl:with-param name="in_reg_date"
                                    select="./report_info/operation/base_doc/doc_info/in_reg_date" />
                            </xsl:call-template>

                            <xsl:if test="./report_info/operation/processing_info">
                                <xsl:text>Операция: </xsl:text>
                                <xsl:value-of
                                    select="normalize-space(./report_info/operation/processing_info/operation_name)" />
                                <xsl:text> № </xsl:text>
                                <xsl:value-of
                                    select="normalize-space(./report_info/operation/processing_info/operation_numb)" />
                                <xsl:text> от </xsl:text>
                                <xsl:call-template name="convertDate">
                                    <xsl:with-param name="date"
                                        select="./report_info/operation/processing_info/operation_date" />
                                </xsl:call-template>
                            </xsl:if>
                        </td>
                    </tr>
                </xsl:if>

                <xsl:if test="./report_info/filter_by_percent">
                    <tr>
                        <td class="leftColumnIdentTable">
                            <xsl:text>Процент: </xsl:text>
                        </td>
                        <td class="sizeData rightColumnIdentTable">
                            <xsl:text>Не менее </xsl:text>
                            <xsl:value-of
                                select="normalize-space(./report_info/filter_by_percent/percent)" />
                            <xsl:text>% </xsl:text>
                            <xsl:call-template name="filter_by_percent_et">
                                <xsl:with-param name="type"
                                    select="./report_info/filter_by_percent/percent_kind" />
                            </xsl:call-template>
                        </td>
                    </tr>
                </xsl:if>

                <xsl:if test="./report_info/*/security">
                    <tr>
                        <td class="leftColumnIdentTable">
                            <xsl:text>Ценная бумага: </xsl:text>
                        </td>
                        <td class="sizeData rightColumnIdentTable">
                            <xsl:call-template name="securityData">
                                <xsl:with-param name="security" select="./report_info/*/security" />
                            </xsl:call-template>

                            <xsl:text> Регистрационный № </xsl:text>
                            <xsl:call-template name="securityRegNumber">
                                <xsl:with-param name="security" select="./report_info/*/security" />
                            </xsl:call-template>
                        </td>
                    </tr>
                </xsl:if>

                <xsl:if test="./report_info/*/quantity">
                    <tr>
                        <td class="leftColumnIdentTable">
                            <xsl:text>Количество ЦБ: </xsl:text>
                        </td>
                        <td class="sizeData rightColumnIdentTable">
                            <xsl:call-template name="units_or_fraction_t">
                                <xsl:with-param name="root" select="./report_info/*/quantity" />
                            </xsl:call-template>
                            <xsl:text> шт.</xsl:text>
                        </td>
                    </tr>
                </xsl:if>
            </tbody>
        </table>
        <xsl:call-template name="repotForm">
            <xsl:with-param name="root" select="." />
        </xsl:call-template>
    </xsl:template>
    
    <!-- Запрос нотариуса -->
    <xsl:template match="REQUEST_OF_NOTARY">
        <table class="identTable">
            <tbody>
                <tr>
                    <td class="titleForIdentTableRow sizeData">
                        <xsl:text>Описание </xsl:text>
                    </td>
                </tr>
            </tbody>
        </table>

        <table class="identTableRowEnd">
            <tbody>
                <tr>
                    <td class="leftColumnIdentTable">
                        <xsl:text>Дата, на которую выполняется запрос нотариуса: </xsl:text>
                    </td>
                    <td class="dataColBasedInfoTable mainListData">
                        <xsl:call-template name="date_or_datetime_t">
                            <xsl:with-param name="root" select="report_info/date" />
                        </xsl:call-template>
                    </td>
                </tr>
            </tbody>
        </table>
        
        <xsl:call-template name="repotForm">
            <xsl:with-param name="root" select="." />
        </xsl:call-template>
    </xsl:template>
    
    <!-- Распоряжение на выдачу информации по лицевому счету в xml-формате -->
    <xsl:template match="REQUEST_FOR_STATEMENT_IN_XML_FORMAT">
        <table class="identTable">
            <tbody>
                <tr>
                    <td class="headerIdentTable sizeData">
                        <xsl:text>Описание запроса: </xsl:text>

                    </td>
                    <td class="sizeData rightColumnIdentTable" />
                </tr>
                <tr>
                    <td class="leftColumnIdentTable">
                        <xsl:text>Тип запрашиваемой информации: </xsl:text>
                    </td>
                    <td class="sizeData rightColumnIdentTable">
                        <xsl:call-template name="statement_type_et">
                            <xsl:with-param name="type"
                                select="./report_info/statement_type" />
                        </xsl:call-template>
                    </td>
                </tr>

                <xsl:if test="./report_info/operation">
                    <tr>
                        <td class="leftColumnIdentTable">
                            <xsl:text>Документ-основание: </xsl:text>
                        </td>
                        <td class="sizeData rightColumnIdentTable">
                            <xsl:call-template name="baseDocOperation">
                                <xsl:with-param name="doc_name"
                                    select="./report_info/operation/base_doc/doc_name" />
                                <xsl:with-param name="out_doc_num"
                                    select="./report_info/operation/base_doc/doc_info/out_doc_num" />
                                <xsl:with-param name="out_doc_date"
                                    select="./report_info/operation/base_doc/doc_info/out_doc_date" />
                                <xsl:with-param name="in_doc_num"
                                    select="./report_info/operation/base_doc/doc_info/in_doc_num" />
                                <xsl:with-param name="in_reg_date"
                                    select="./report_info/operation/base_doc/doc_info/in_reg_date" />
                            </xsl:call-template>

                            <xsl:if test="./report_info/operation/processing_info">
                                <xsl:text>Операция: </xsl:text>
                                <xsl:value-of
                                    select="normalize-space(./report_info/operation/processing_info/operation_name)" />
                                <xsl:text> № </xsl:text>
                                <xsl:value-of
                                    select="normalize-space(./report_info/operation/processing_info/operation_numb)" />
                                <xsl:text> от </xsl:text>
                                <xsl:call-template name="convertDate">
                                    <xsl:with-param name="date"
                                        select="./report_info/operation/processing_info/operation_date" />
                                </xsl:call-template>
                            </xsl:if>
                        </td>
                    </tr>
                </xsl:if>

                <xsl:if test="./report_info/statement">
                    <tr>
                        <xsl:choose>
                            <xsl:when test="./report_info/statement/date_or_period/date">

                                <td class="leftColumnIdentTable">
                                    <xsl:text>На дату: </xsl:text>
                                </td>
                                <td class="sizeData rightColumnIdentTable">
                                    <xsl:choose>
                                        <xsl:when
                                            test="normalize-space(./report_info/statement/date_or_period/date/*[text()])=''">
                                            <xsl:text>[дата формирования отчета]</xsl:text>
                                        </xsl:when>
                                        <xsl:otherwise>

                                            <xsl:call-template name="date_or_datetime_t">
                                                <xsl:with-param name="root"
                                                    select="./report_info/statement/date_or_period/date" />
                                            </xsl:call-template>
                                        </xsl:otherwise>
                                    </xsl:choose>
                                </td>
                            </xsl:when>

                            <xsl:otherwise>
                                <td class="leftColumnIdentTable">
                                    <xsl:text>Период: </xsl:text>
                                </td>
                                <td class="sizeData rightColumnIdentTable">
                                    <xsl:text> с </xsl:text>

                                    <xsl:choose>
                                        <xsl:when
                                            test="normalize-space(./report_info/statement/date_or_period/begin_date[text()])=''">
                                            <xsl:text>[дата формирования отчета]</xsl:text>
                                        </xsl:when>
                                        <xsl:otherwise>
                                            <xsl:call-template name="convertDate">
                                                <xsl:with-param name="date"
                                                    select="./report_info/statement/date_or_period/begin_date" />
                                            </xsl:call-template>
                                        </xsl:otherwise>
                                    </xsl:choose>

                                    <xsl:text> по </xsl:text>

                                    <xsl:choose>
                                        <xsl:when
                                            test="normalize-space(./report_info/statement/date_or_period/end_date[text()])=''">
                                            <xsl:text>[дата формирования отчета]</xsl:text>
                                        </xsl:when>
                                        <xsl:otherwise>
                                            <xsl:call-template name="convertDate">
                                                <xsl:with-param name="date"
                                                    select="./report_info/statement/date_or_period/end_date" />
                                            </xsl:call-template>
                                        </xsl:otherwise>
                                    </xsl:choose>
                                </td>
                            </xsl:otherwise>
                        </xsl:choose>
                    </tr>
                </xsl:if>

                <xsl:if test="./report_info/*/security">
                    <tr>
                        <td class="leftColumnIdentTable">
                            <xsl:text>Ценная бумага: </xsl:text>
                        </td>
                        <td class="sizeData rightColumnIdentTable">
                            <xsl:call-template name="securityData">
                                <xsl:with-param name="security" select="./report_info/*/security" />
                            </xsl:call-template>

                            <xsl:text> Регистрационный № </xsl:text>
                            <xsl:call-template name="securityRegNumber">
                                <xsl:with-param name="security" select="./report_info/*/security" />
                            </xsl:call-template>
                        </td>
                    </tr>
                </xsl:if>

                <xsl:if test="./report_info/*/quantity">
                    <tr>
                        <td class="leftColumnIdentTable">
                            <xsl:text>Количество ЦБ: </xsl:text>
                        </td>
                        <td class="sizeData rightColumnIdentTable">
                            <xsl:call-template name="units_or_fraction_t">
                                <xsl:with-param name="root" select="./report_info/*/quantity" />
                            </xsl:call-template>
                            <xsl:text> шт.</xsl:text>
                        </td>
                    </tr>
                </xsl:if>
                <xsl:if test="not(normalize-space(./comment)='')">
                    <tr>
                        <td class="leftColumnIdentTable">
                            <xsl:text>Комментарий: </xsl:text>
                        </td>
                        <td class="sizeData rightColumnIdentTable">
                            <xsl:value-of select="normalize-space(./comment)" />
                        </td>
                    </tr>
                </xsl:if>
            </tbody>
        </table>
    </xsl:template>
    <!-- Выписка из реестра / Справка на дату / Справка о наличии -->
    <xsl:template match="STATEMENT_OF_HOLDINGS">
        <!-- Данные о ЦБ (заголовок) -->
        <table class="identTable">
            <tbody>
                <tr>
                    <td class="titleForIdentTableRow sizeData">
                        <xsl:value-of select="normalize-space(./security_data/security_info)" />
                    </td>
                </tr>
            </tbody>
        </table>

        <xsl:variable name="statementTypeValue" select="statement_type" />
        
        <xsl:for-each select="./security_data/security_balance">
            <xsl:choose>
                <xsl:when test="position()=last()">
                    <table class="identTableRowEnd">
                        <xsl:call-template name="statementOfHoldingsSecurityBalance">
                            <xsl:with-param name="statementType" select='$statementTypeValue' /> 
                        </xsl:call-template>
                    </table>
                </xsl:when>
                <xsl:otherwise>
                    <table class="identTableRow">
                       <xsl:call-template name="statementOfHoldingsSecurityBalance">
                            <xsl:with-param name="statementType" select='$statementTypeValue' /> 
                        </xsl:call-template>
                    </table>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:for-each>
    </xsl:template>
    <!-- Акт сверки от трансфер-агента -->
    <xsl:template match="STATEMENT_OF_RECONCILIATION">
        <xsl:call-template name="RECONCILIATION_DATA">
            <xsl:with-param name="root" select="." />
        </xsl:call-template>
    </xsl:template>
    <!-- Уведомление об отказе в проведении операции -->
    <xsl:template match="STATEMENT_OF_REJECTED_TRANSACTION">
        <xsl:variable name="accountIdent" select="account_ident" />
        <table class="identTable">
            <tbody>
                <tr>
                    <td class="headerIdentTable sizeData">
                        <xsl:text>Описание отказа: </xsl:text>
                    </td>
                    <td class="sizeData rightColumnIdentTable" />

                </tr>
                <tr>
                    <td class="leftColumnIdentTable">
                        <xsl:text>Документ, по которому произведен отказ: </xsl:text>
                    </td>
                    <td class="sizeData rightColumnIdentTable">
                        <xsl:call-template name="baseDocOperation">
                            <xsl:with-param name="doc_name" select="base_doc/doc_name" />
                            <xsl:with-param name="out_doc_num"
                                select="base_doc/doc_info/out_doc_num" />
                            <xsl:with-param name="out_doc_date"
                                select="base_doc/doc_info/out_doc_date" />
                            <xsl:with-param name="in_doc_num"
                                select="base_doc/doc_info/in_doc_num" />
                            <xsl:with-param name="in_reg_date"
                                select="base_doc/doc_info/in_reg_date" />
                        </xsl:call-template>
                    </td>
                </tr>
                <tr>
                    <td class="leftColumnIdentTable">
                        <xsl:text>Причина отказа: </xsl:text>
                    </td>
                    <td class="sizeData rightColumnIdentTable">

                        <xsl:for-each select="rejected_operation/reject_reason">
                            <xsl:if test="not(position()=1)">
                                <br />
                            </xsl:if>
                            <xsl:value-of select="normalize-space(.)" />
                        </xsl:for-each>
                    </td>
                </tr>
                <tr>
                    <td class="leftColumnIdentTable">
                        <xsl:text>Действия по устранению причины отказа: </xsl:text>
                    </td>
                    <td class="sizeData rightColumnIdentTable">
                        <xsl:for-each select="rejected_operation/recommendation">
                            <xsl:if test="not(position()=1)">
                                <br />
                            </xsl:if>
                            <xsl:value-of select="normalize-space(.)" />
                        </xsl:for-each>
                    </td>
                </tr>
                <xsl:if test="rejected_operation/operation">
                    <xsl:call-template name="operation_t">
                        <xsl:with-param name="operation" select="rejected_operation/operation" />
                        <xsl:with-param name="accountIdent" select="$accountIdent" />
                    </xsl:call-template>
                </xsl:if>
            </tbody>
        </table>
    </xsl:template>
    <!-- Уведомление о проведении операции -->
    <xsl:template match="STATEMENT_OF_CONFIRMED_TRANSACTION">
        <xsl:variable name="accountIdent" select="account_ident" />
        <table class="identTable">
            <tbody>
                <xsl:call-template name="operation_t">
                    <xsl:with-param name="operation" select="operation" />
                    <xsl:with-param name="accountIdent" select="$accountIdent" />
                </xsl:call-template>
            </tbody>
        </table>
    </xsl:template>
    <!-- Справка об операциях по счету -->
    <xsl:template match="STATEMENT_OF_TRANSACTIONS">
        <xsl:variable name="accountIdent" select="account_ident" />

        <!-- Данные об операциях (заголовок) -->
        <table class="identTable">
            <tbody>
                <tr>
                    <td class="titleForIdentTableRow sizeData">
                        <xsl:value-of select="normalize-space(./operation_data/operation_info)" />
                    </td>
                </tr>
            </tbody>
        </table>

        <!-- Данные об операциях (данные) -->
        <xsl:if test="operation_data/operation">
            <table class="identTableRowEnd">
                <tbody>
                    <tr>
                        <td class="mainListHeaderTable mainListData">№ операции</td>
                        <td class="mainListHeaderTable mainListData">Дата</td>
                        <td class="mainListHeaderTable mainListData">Тип операции</td>
                        <td class="mainListHeaderTable mainListData">Документ-основание</td>
                        <td class="mainListHeaderTable mainListData">Ценная бумага</td>
                        <td class="mainListHeaderTable mainListData">Количество ЦБ</td>
                        <td class="mainListHeaderTable mainListData">Сумма</td>
                        <td class="mainListHeaderTable mainListData">Контрагент</td>
                        <td class="mainListHeaderTable mainListData">Ссылки на документы</td>
                        <td class="mainListHeaderTable mainListData">Дополнительная информация</td>
                    </tr>
                    <xsl:for-each select="operation_data/operation">
                        <tr>
                            <td class="dataColBasedInfoTable mainListData">
                                <xsl:call-template name="numberOperation">
                                    <xsl:with-param name="number"
                                        select="processing_info/operation_numb" />
                                </xsl:call-template>
                            </td>
                            <td class="dataColBasedInfoTable mainListData">
                                <xsl:call-template name="dateOperation">
                                    <xsl:with-param name="date"
                                        select="processing_info/operation_date" />
                                </xsl:call-template>

                            </td>
                            <td class="dataColBasedInfoTable mainListData">
                                <xsl:call-template name="typeOperation">
                                    <xsl:with-param name="type"
                                        select="processing_info/operation_name" />
                                    <xsl:with-param name="transaction" select="transaction" />
                                    <xsl:with-param name="account_ident" select="$accountIdent" />
                                    <xsl:with-param name="account_deliv" select="transaction/account_deliv" />
                                    <xsl:with-param name="account_receiv" select="transaction/account_receiv" />
                                </xsl:call-template>

                            </td>
                            <td class="dataColBasedInfoTable mainListData">
                                <xsl:call-template name="baseDocOperation">
                                    <xsl:with-param name="doc_name" select="base_doc/doc_name" />
                                    <xsl:with-param name="out_doc_num"
                                        select="base_doc/doc_info/out_doc_num" />
                                    <xsl:with-param name="out_doc_date"
                                        select="base_doc/doc_info/out_doc_date" />
                                    <xsl:with-param name="in_doc_num"
                                        select="base_doc/doc_info/in_doc_num" />
                                    <xsl:with-param name="in_reg_date"
                                        select="base_doc/doc_info/in_reg_date" />
                                </xsl:call-template>
                            </td>
                            <td class="dataColBasedInfoTable mainListData">
                                <xsl:call-template name="securityData">
                                    <xsl:with-param name="security" select="*/*/security" />
                                </xsl:call-template>
                                <xsl:if test="*/*/security">
                                    <xsl:text> Регистрационный № </xsl:text>
                                    <xsl:call-template name="securityRegNumber">
                                        <xsl:with-param name="security" select="*/*/security" />
                                    </xsl:call-template>
                                </xsl:if>
                            </td>
                            <td class="dataColBasedInfoTable mainListData">
                                <xsl:choose>
                                    <xsl:when test="unblock">
                                        <xsl:call-template name="securityQuantity">
                                            <xsl:with-param name="quantity"
                                                select="unblock/quantity_unblock" />
                                        </xsl:call-template>
                                        <xsl:text> шт. </xsl:text>
                                    </xsl:when>
                                    <xsl:when test="unpledge">
                                        <xsl:call-template name="securityQuantity">
                                            <xsl:with-param name="quantity"
                                                select="unpledge/quantity_unpledge" />
                                        </xsl:call-template>
                                        <xsl:text> шт. </xsl:text>
                                    </xsl:when>
                                    <xsl:when test="pledge_deliver">
                                        <xsl:call-template name="securityQuantity">
                                            <xsl:with-param name="quantity"
                                                select="pledge_deliver/quantity_deliver" />
                                        </xsl:call-template>
                                        <xsl:text> шт. </xsl:text>
                                    </xsl:when>
                                    <xsl:when test="unpaid_cancel">
                                        <xsl:call-template name="securityQuantity">
                                            <xsl:with-param name="quantity"
                                                select="unpaid_cancel/quantity_cancel" />
                                        </xsl:call-template>
                                        <xsl:text> шт. </xsl:text>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <xsl:if test="*/*/quantity">
                                            <xsl:call-template name="securityQuantity">
                                                <xsl:with-param name="quantity" select="*/*/quantity" />
                                            </xsl:call-template>
                                            <xsl:text> шт. </xsl:text>
                                        </xsl:if>
                                    </xsl:otherwise>
                                </xsl:choose>
                            </td>
                            <td class="dataColBasedInfoTable mainListData">
                                <xsl:if test="*/*/settlement_amount">
                                    <xsl:call-template name="securitySettlementAmount">
                                        <xsl:with-param name="settlement_amount"
                                            select="*/*/settlement_amount" />
                                    </xsl:call-template>

                                </xsl:if>
                            </td>
                            <td class="dataColBasedInfoTable mainListData">
                                <xsl:if
                                    test="transaction or pledge or unpledge or pledge_modify or pledge_deliver">

                                    <xsl:call-template name="contrAgent">
                                        <xsl:with-param name="transaction" select="transaction" />
                                        <xsl:with-param name="pledge" select="pledge" />
                                        <xsl:with-param name="unpledge" select="unpledge" />
                                        <xsl:with-param name="pledge_modify" select="pledge_modify" />
                                        <xsl:with-param name="pledge_deliver" select="pledge_deliver" />

                                        <xsl:with-param name="account_ident" select="$accountIdent" />
                                        <xsl:with-param name="account_deliv"
                                            select="transaction/account_deliv" />
                                        <xsl:with-param name="account_receiv"
                                            select="transaction/account_receiv" />
                                        <xsl:with-param name="account_owner" select="*/account_owner" />
                                        <xsl:with-param name="account_pledgee"
                                            select="*/pledge_info/account_pledgee" />
                                        <xsl:with-param name="account_pledgee_new"
                                            select="pledge_deliver/account_pledgee_new" />
                                    </xsl:call-template>

                                </xsl:if>
                            </td>
                            <td class="dataColBasedInfoTable mainListData">
                                <xsl:if test="*/*/based_info">
                                    <xsl:call-template name="based_infoRow">
                                        <xsl:with-param name="root" select="*/*" />
                                    </xsl:call-template>
                                </xsl:if>
                            </td>
                            <td class="dataColBasedInfoTable mainListData">
                                <xsl:call-template name="description_operation_t">
                                    <xsl:with-param name="root" select="." />
                                </xsl:call-template>
                            </td>
                        </tr>
                    </xsl:for-each>
                </tbody>
            </table>
        </xsl:if>


        <!-- данные об остатках на счете (заголовок) -->
        <table class="identTable">
            <tbody>
                <tr>
                    <td class="titleForIdentTableRow sizeData">
                        <xsl:value-of select="normalize-space(./security_data/security_info)" />
                    </td>
                </tr>
            </tbody>
        </table>
        <!-- данные об остатках на счете (данные) -->
        <xsl:if test="./security_data/security_balance">
            <table class="identTableRowEnd">
                <tbody>
                    <tr>
                        <td class="mainListHeaderTable mainListData" rowspan="2">Ценная
                            бумага
                        </td>
                        <td class="mainListHeaderTable mainListData" colspan="2">На начало
                            периода
                        </td>
                        <td class="mainListHeaderTable mainListData" colspan="2">На конец
                            периода
                        </td>
                    </tr>
                    <tr>
                        <td class="mainListHeaderTable mainListData">Остаток</td>
                        <td class="mainListHeaderTable mainListData">Обременения</td>
                        <td class="mainListHeaderTable mainListData">Остаток</td>
                        <td class="mainListHeaderTable mainListData">Обременения</td>
                    </tr>

                    <xsl:for-each select="./security_data/security_balance">
                        <tr>
                            <td class="dataColBasedInfoTable mainListData">

                                <xsl:call-template name="securityData">
                                    <xsl:with-param name="security" select="security" />
                                </xsl:call-template>

                                <xsl:text> Регистрационный № </xsl:text>

                                <xsl:call-template name="securityRegNumber">
                                    <xsl:with-param name="security" select="security" />
                                </xsl:call-template>

                            </td>

                            <td class="dataColBasedInfoTable mainListData">
                                <xsl:call-template name="units_or_fraction_t">
                                    <xsl:with-param name="root" select="opening_balance/total" />
                                </xsl:call-template>
                            </td>

                            <td class="dataColBasedInfoTable mainListData">
                                <xsl:for-each select="opening_balance/blocked">

                                    <xsl:if test="not(position()=1)">
                                        <br />
                                    </xsl:if>

                                    <xsl:call-template name="block_type_et">
                                        <xsl:with-param name="type"
                                            select="block_type/block_type_code" />
                                    </xsl:call-template>

                                    <xsl:text> </xsl:text>

                                    <xsl:call-template name="units_or_fraction_t">
                                        <xsl:with-param name="root" select="quantity" />
                                    </xsl:call-template>
                                </xsl:for-each>
                            </td>

                            <td class="dataColBasedInfoTable mainListData">
                                <xsl:call-template name="units_or_fraction_t">
                                    <xsl:with-param name="root" select="closing_balance/total" />
                                </xsl:call-template>
                            </td>

                            <td class="dataColBasedInfoTable mainListData">
                                <xsl:for-each select="closing_balance/blocked">
                                    <xsl:if test="not(position()=1)">
                                        <br />
                                    </xsl:if>

                                    <xsl:call-template name="block_type_et">
                                        <xsl:with-param name="type"
                                            select="block_type/block_type_code" />
                                    </xsl:call-template>

                                    <xsl:text> </xsl:text>

                                    <xsl:call-template name="units_or_fraction_t">
                                        <xsl:with-param name="root" select="quantity" />
                                    </xsl:call-template>
                                </xsl:for-each>
                            </td>

                        </tr>
                    </xsl:for-each>
                </tbody>
            </table>
        </xsl:if>
    </xsl:template>
    <!-- Вывод таблицы "Журнал отправленных/принятых документов" -->
    <xsl:template name="RECONCILIATION_DATA">
        <xsl:param name="root" />

        <!-- Данные об операциях (заголовок) -->
        <table class="identTable">
            <tbody>
                <tr>
                    <td class="titleForIdentTableRow sizeData">
                        <xsl:text>Количество документов в запросе на сверку: </xsl:text>
                        <xsl:value-of select="total_docs" />
                    </td>
                </tr>

                <xsl:if test="name(.)='STATEMENT_OF_RECONCILIATION'">
                    <tr>
                        <td class="titleForIdentTableRow sizeData">
                            <xsl:text>Итоговый результат сверки: </xsl:text>
                            <xsl:call-template name="result_reconciliation_total_et">
                                <xsl:with-param name="type" select="total_result" />
                            </xsl:call-template>
                        </td>
                    </tr>
                </xsl:if>
            </tbody>
        </table>
        <xsl:if test="doc_list">
            <table class="identTableRowEnd">
                <tbody>
                    <tr>
                        <td class="mainListHeaderTable mainListData">Номер, дата документа у регистратора</td>
                        <td class="mainListHeaderTable mainListData">Номер, дата документа у трансфер-агента</td>
                        <td class="mainListHeaderTable mainListData">Наименование документа</td>
                        <td class="mainListHeaderTable mainListData">Отправитель документа</td>
                        <td class="mainListHeaderTable mainListData">Эмитент</td>
                        <td class="mainListHeaderTable mainListData">Состояние документа</td>
                        <td class="mainListHeaderTable mainListData">Дата обработки документа / выполнения отказа</td>
                        <td class="mainListHeaderTable mainListData">Данные исходящего документа</td>
                        <xsl:if test="name(.)='STATEMENT_OF_RECONCILIATION'">
                            <td class="mainListHeaderTable mainListData">Результат сверки</td>
                        </xsl:if>
                    </tr>
                    <xsl:for-each select="doc_list">
                        <tr>
                            <xsl:choose>
                                <xsl:when test="name(..)='STATEMENT_OF_RECONCILIATION'">
                                    <xsl:call-template name="doc_info_t">
                                        <xsl:with-param name="root" select="doc_info" />
                                    </xsl:call-template>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:call-template name="doc_info_t">
                                        <xsl:with-param name="root" select="." />
                                    </xsl:call-template>
                                </xsl:otherwise>
                            </xsl:choose>
                            <xsl:if test="name(..)='STATEMENT_OF_RECONCILIATION'">
                                <td class="dataColBasedInfoTable mainListData">
                                    <xsl:call-template name="result_reconciliation_et">
                                        <xsl:with-param name="type" select="reconciliation_info" />
                                    </xsl:call-template>
                                </td>
                            </xsl:if>
                        </tr>
                    </xsl:for-each>
                </tbody>
            </table>
        </xsl:if>
    </xsl:template>

    <!-- Описание блокирования операций -->
    <xsl:template name="operation_blockoper_t">
        <xsl:param name="root" />
        <table class="identTable">
            <tbody>
                <tr>
                    <td class="titleForIdentTableRow sizeData">
                        <xsl:text>Описание блокирования операций с ценными бумагами</xsl:text>
                    </td>
                </tr>
            </tbody>
        </table>

        <table class="identTableRowEnd">
            <tbody>
                <xsl:if test="$root/blockoper_info/id_blockoper!=0">
                    <tr>
                        <td class="leftColumnIdentTable">
                            <xsl:text>Уникальный номер блокирования операций: </xsl:text>
                        </td>
                        <td class="dataColBasedInfoTable mainListData">
                            <xsl:value-of select="$root/blockoper_info/id_blockoper" />
                        </td>
                    </tr>
                </xsl:if>
                <xsl:if test="$root/blockoper_info/blockoper_date">
                    <tr>
                        <td class="leftColumnIdentTable">
                            <xsl:text>Дата установки блокирования операций: </xsl:text>
                        </td>
                        <td class="dataColBasedInfoTable mainListData">
                            <xsl:call-template name="convertDate">
                                <xsl:with-param name="date"
                                    select="$root/blockoper_info/blockoper_date" />
                            </xsl:call-template>
                        </td>
                    </tr>
                </xsl:if>
                <tr>
                    <td class="leftColumnIdentTable">
                        <xsl:text>Вид блокирования операций: </xsl:text>
                    </td>
                    <td class="dataColBasedInfoTable mainListData">
                        <xsl:call-template name="blockoper_type_t">
                            <xsl:with-param name="type"
                                select="$root/blockoper_info/blockoper_type/blockoper_type_code" />
                        </xsl:call-template>
                    </td>
                </tr>
                <tr>
                    <td class="leftColumnIdentTable">
                        <xsl:text>Основание для блокирования: </xsl:text>
                    </td>
                    <td class="dataColBasedInfoTable mainListData">
                        <xsl:call-template name="block_reason_et">
                            <xsl:with-param name="type"
                                select="$root/blockoper_info/block_reason" />
                        </xsl:call-template>
                    </td>
                </tr>
                
                <xsl:if test="$root/blockoper_info/comment">
                    <tr>
                        <td class="leftColumnIdentTable">
                            <xsl:text>Комментарий: </xsl:text>
                        </td>
                        <td class="dataColBasedInfoTable mainListData">
                            <xsl:value-of select="$root/blockoper_info/comment" />
                        </td>
                    </tr>
                </xsl:if>
            </tbody>
        </table>
    </xsl:template>
    <!-- Вывод данных операции -->
    <xsl:template name="description_operation_t">
        <xsl:param name="root" />

        <xsl:choose>
            <xsl:when test="account_open">
                <xsl:call-template name="content_form_of_shareholders_t">
                    <xsl:with-param name="root" select="account_open/shareholder_form" />
                </xsl:call-template>
            </xsl:when>
            <xsl:when test="account_modify">
                <xsl:call-template name="content_form_of_shareholders_t">
                    <xsl:with-param name="root"
                        select="account_modify/shareholder_form" />
                </xsl:call-template>
            </xsl:when>
            <xsl:when test="transaction">

                <xsl:if test="transaction/transaction/transaction_type">
                    <xsl:text>Тип операции перехода прав собственности: </xsl:text>
                    <xsl:call-template name="transaction_type_et">
                        <xsl:with-param name="type"
                            select="transaction/transaction/transaction_type" />
                    </xsl:call-template>
                </xsl:if>

                <xsl:choose>
                    <xsl:when test="transaction/transaction/block_indicator = 'Yes'">
                        <br />
                        <xsl:text>Имеется обременение: </xsl:text>
                        <br />
                        <xsl:if test="transaction/transaction/pledge_info">
                            <xsl:if test="transaction/transaction/pledge_info/id_pledge">
                                <xsl:text>Уникальный номер обременения: </xsl:text>
                                <xsl:value-of select="transaction/transaction/pledge_info/id_pledge" />
                                <br />
                            </xsl:if>
                            <xsl:if test="transaction/transaction/pledge_info/pledge_date">
                                <xsl:text>Дата установки обременения: </xsl:text>
                                <xsl:call-template name="convertDate">
                                    <xsl:with-param name="date"
                                        select="transaction/transaction/pledge_info/pledge_date" />
                                </xsl:call-template>
                                <br />
                            </xsl:if>

                            <xsl:text>Вид обременения: </xsl:text>
                            <xsl:call-template name="pledgeType">
                                <xsl:with-param name="pledge_info"
                                    select="transaction/transaction/pledge_info" />
                            </xsl:call-template>

                            <xsl:if test="transaction/transaction/pledge_info/condition_pledge">
                                <br />
                                <xsl:call-template name="condition_pledge_t">
                                    <xsl:with-param name="root"
                                        select="transaction/transaction/pledge_info/condition_pledge" />
                                    <xsl:with-param name="header" select="'Условия залога: '" />
                                </xsl:call-template>
                            </xsl:if>

                            <xsl:if test="transaction/transaction/pledge_info/comment">
                                <br />
                                <xsl:text>Коментарий: </xsl:text>
                                <xsl:value-of select="transaction/transaction/pledge_info/comment" />
                            </xsl:if>
                        </xsl:if>
                        <xsl:if test="transaction/transaction/unpaid_info">
                            <xsl:if test="transaction/transaction/unpaid_info/id_unpaid">
                                <xsl:text>Уникальный номер обременения: </xsl:text>
                                <xsl:value-of select="transaction/transaction/unpaid_info/id_unpaid" />
                                <br />
                            </xsl:if>
                            <xsl:if test="transaction/transaction/unpaid_info/unpaid_date">
                                <xsl:text>Дата установки обременения: </xsl:text>
                                <xsl:call-template name="convertDate">
                                    <xsl:with-param name="date"
                                        select="transaction/transaction/unpaid_info/unpaid_date" />
                                </xsl:call-template>
                                <br />
                            </xsl:if>

                            <xsl:text>Вид обременения: </xsl:text>
                            <xsl:call-template name="block_type_et">
                                <xsl:with-param name="type"
                                    select="transaction/transaction/unpaid_info/unpaid_type/block_type_code" />
                            </xsl:call-template>

                            <xsl:if test="transaction/transaction/unpaid_info/condition_unpaid">
                                <br />
                                <xsl:text>Условия обременения: </xsl:text>
                                <xsl:call-template name="condition_block_t">
                                    <xsl:with-param name="root"
                                        select="transaction/transaction/unpaid_info/condition_unpaid" />
                                </xsl:call-template>
                            </xsl:if>

                            <xsl:if test="transaction/transaction/unpaid_info/comment">
                                <br />
                                <xsl:text>Коментарий: </xsl:text>
                                <xsl:value-of select="transaction/transaction/unpaid_info/comment" />
                            </xsl:if>
                        </xsl:if>
                    </xsl:when>
                    <xsl:otherwise>
                        <br />
                        <xsl:text>Обременения осутствуют</xsl:text>
                    </xsl:otherwise>
                </xsl:choose>

                <xsl:if test="transaction/transaction/comment">
                    <br />
                    <xsl:text>Коментарий: </xsl:text>
                    <xsl:value-of select="transaction/transaction/comment" />
                </xsl:if>
                
                <xsl:if test="transaction/transaction/reference">
                    <br />
                    <xsl:text>Референс поручения: </xsl:text>
                    <xsl:value-of select="transaction/transaction/reference" />
                </xsl:if>
                
                <xsl:if test="transaction/transaction/owner">
                    <br />
                    <xsl:text>Наименование владельца: </xsl:text>
                    <xsl:value-of select="transaction/transaction/owner" />
                </xsl:if>
            </xsl:when>
            <xsl:when test="block/block_info">
                <xsl:text>Описание блокирования: </xsl:text>
                <xsl:call-template name="burden_simple">
                    <xsl:with-param name="burdenPrev" select="No" />
                    <xsl:with-param name="burdenType" select="block/block_info/block_type" />
                    <xsl:with-param name="burdenRepead" select="block/block_info/repeat_block" />
                    <xsl:with-param name="burdenQuantity" select="block/block_info/quantity" />
                    <xsl:with-param name="burdenDate" select="block/block_info/block_date" />
                    <xsl:with-param name="burdenCondition"
                        select="block/block_info/condition_block" />
                    <xsl:with-param name="burdenComment" select="block/block_info/comment" />
                    <xsl:with-param name="burdenIdentificator" select="block/block_info/id_block" />
                    <xsl:with-param name="burdenInfo" select="block/block_info" />
                </xsl:call-template>

                <xsl:if test="block/block_prev">
                    <br />
                    <xsl:call-template name="burden_simple">
                        <xsl:with-param name="burdenPrev" select="'Yes'" />
                        <xsl:with-param name="burdenType" select="block/block_prev/block_type" />
                        <xsl:with-param name="burdenRepead"
                            select="block/block_prev/repeat_block" />
                        <xsl:with-param name="burdenQuantity" select="block/block_prev/quantity" />
                        <xsl:with-param name="burdenDate" select="block/block_prev/block_date" />
                        <xsl:with-param name="burdenCondition"
                            select="block/block_prev/condition_block" />
                        <xsl:with-param name="burdenComment" select="block/block_prev/comment" />
                        <xsl:with-param name="burdenIdentificator" select="block/block_prev/id_block" />
                        <xsl:with-param name="burdenInfo" select="block/block_prev" />
                    </xsl:call-template>
                </xsl:if>
                <xsl:if test="block/pledge_prev">
                    <br />
                    <xsl:call-template name="burden_simple">
                        <xsl:with-param name="burdenPrev" select="'Yes'" />
                        <xsl:with-param name="burdenType"
                            select="block/pledge_prev/pledge_type" />
                        <xsl:with-param name="burdenRepead"
                            select="block/block_prev/repeat_block" />
                        <xsl:with-param name="burdenQuantity" select="block/pledge_prev/quantity" />
                        <xsl:with-param name="burdenDate" select="block/pledge_prev/block_date" />
                        <xsl:with-param name="burdenCondition"
                            select="block/pledge_prev/condition_pledge" />
                        <xsl:with-param name="burdenComment" select="block/pledge_prev/comment" />
                        <xsl:with-param name="burdenIdentificator" select="block/pledge_prev/id_pledge" />
                        <xsl:with-param name="burdenInfo" select="block/pledge_prev" />
                    </xsl:call-template>
                </xsl:if>
                <xsl:if test="block/unpaid_prev">
                    <br />
                    <xsl:call-template name="burden_simple">
                        <xsl:with-param name="burdenPrev" select="'Yes'" />
                        <xsl:with-param name="burdenType"
                            select="block/unpaid_prev/unpaid_type" />
                        <xsl:with-param name="burdenQuantity" select="block/unpaid_prev/quantity" />
                        <xsl:with-param name="burdenDate"
                            select="block/unpaid_prev/unpaid_date" />
                        <xsl:with-param name="burdenCondition"
                            select="block/unpaid_prev/condition_unpaid" />
                        <xsl:with-param name="burdenComment" select="block/unpaid_prev/comment" />
                        <xsl:with-param name="burdenIdentificator" select="block/unpaid_prev/id_unpaid" />
                        <xsl:with-param name="burdenInfo" select="block/unpaid_prev" />
                    </xsl:call-template>
                </xsl:if>
            </xsl:when>
            <xsl:when test="unblock">
                <xsl:text>Описание блокирования: </xsl:text>
                <xsl:call-template name="burden_simple">
                    <xsl:with-param name="burdenPrev" select="No" />
                    <xsl:with-param name="burdenType" select="unblock/block_info/block_type" />
                    <xsl:with-param name="burdenRepead"
                        select="unblock/block_info/repeat_block" />
                    <xsl:with-param name="burdenQuantity" select="unblock/block_info/quantity" />
                    <xsl:with-param name="burdenDate" select="unblock/block_info/block_date" />
                    <xsl:with-param name="burdenCondition"
                        select="unblock/block_info/condition_block" />
                    <xsl:with-param name="burdenComment" select="unblock/block_info/comment" />
                    <xsl:with-param name="burdenIdentificator" select="unblock/block_info/id_block" />
                    <xsl:with-param name="burdenInfo" select="unblock/block_info" />
                </xsl:call-template>
                <br />
                <xsl:text>Количество ЦБ, на которое снимается блокирование: </xsl:text>
                <xsl:call-template name="units_or_fraction_t">
                    <xsl:with-param name="root" select="unblock/quantity_unblock" />
                </xsl:call-template>
            </xsl:when>
            <xsl:when test="pledge">
                <xsl:text>Условия залога: </xsl:text>
                <xsl:call-template name="burden_simple">
                    <xsl:with-param name="burdenPrev" select="No" />
                    <xsl:with-param name="burdenType"
                        select="pledge/pledge_info/pledge_type" />
                    <xsl:with-param name="burdenRepead"
                        select="pledge/pledge_info/repeat_pledge" />
                    <xsl:with-param name="burdenQuantity" select="pledge/pledge_info/quantity" />
                    <xsl:with-param name="burdenDate"
                        select="pledge/pledge_info/pledge_date" />
                    <xsl:with-param name="burdenCondition"
                        select="pledge/pledge_info/condition_pledge" />
                    <xsl:with-param name="burdenComment" select="pledge/pledge_info/comment" />
                    <xsl:with-param name="burdenIdentificator" select="pledge/pledge_info/id_pledge" />
                    <xsl:with-param name="burdenInfo" select="pledge/pledge_info" />
                </xsl:call-template>

                <xsl:if test="pledge/pledge_prev">
                    <br />
                    <xsl:call-template name="burden_simple">
                        <xsl:with-param name="burdenPrev" select="'Yes'" />
                        <xsl:with-param name="burdenType"
                            select="pledge/pledge_prev/pledge_type" />
                        <xsl:with-param name="burdenRepead"
                            select="pledge/block_prev/repeat_block" />
                        <xsl:with-param name="burdenQuantity" select="pledge/pledge_prev/quantity" />
                        <xsl:with-param name="burdenDate"
                            select="pledge/pledge_prev/block_date" />
                        <xsl:with-param name="burdenCondition"
                            select="pledge/pledge_prev/condition_pledge" />
                        <xsl:with-param name="burdenComment" select="pledge/pledge_prev/comment" />
                        <xsl:with-param name="burdenIdentificator" select="pledge/pledge_prev/id_pledge" />
                        <xsl:with-param name="burdenInfo" select="pledge/pledge_prev" />
                    </xsl:call-template>
                </xsl:if>
                <xsl:if test="pledge/unpaid_prev">
                    <br />
                    <xsl:call-template name="burden_simple">
                        <xsl:with-param name="burdenPrev" select="'Yes'" />
                        <xsl:with-param name="burdenType"
                            select="pledge/unpaid_prev/unpaid_type" />
                        <xsl:with-param name="burdenQuantity" select="pledge/unpaid_prev/quantity" />
                        <xsl:with-param name="burdenDate"
                            select="pledge/unpaid_prev/unpaid_date" />
                        <xsl:with-param name="burdenCondition"
                            select="pledge/unpaid_prev/condition_unpaid" />
                        <xsl:with-param name="burdenComment" select="pledge/unpaid_prev/comment" />
                        <xsl:with-param name="burdenIdentificator" select="pledge/unpaid_prev/id_unpaid" />
                        <xsl:with-param name="burdenInfo" select="pledge/unpaid_prev" />
                    </xsl:call-template>
                </xsl:if>
            </xsl:when>
            <xsl:when test="unpledge">
                <xsl:text>Описание залога: </xsl:text>
                <xsl:call-template name="burden_simple">
                    <xsl:with-param name="burdenPrev" select="No" />
                    <xsl:with-param name="burdenType"
                        select="unpledge/pledge_info/pledge_type" />
                    <xsl:with-param name="burdenRepead"
                        select="unpledge/pledge_info/repeat_pledge" />
                    <xsl:with-param name="burdenQuantity" select="unpledge/pledge_info/quantity" />
                    <xsl:with-param name="burdenDate"
                        select="unpledge/pledge_info/pledge_date" />
                    <xsl:with-param name="burdenCondition"
                        select="unpledge/pledge_info/condition_pledge" />
                    <xsl:with-param name="burdenComment" select="unpledge/pledge_info/comment" />
                    <xsl:with-param name="burdenIdentificator"
                        select="unpledge/pledge_info/id_pledge" />
                    <xsl:with-param name="burdenInfo" select="unpledge/pledge_info" />
                </xsl:call-template>
                <br />
                <xsl:text>Количество ЦБ, на которое снимается залог: </xsl:text>
                <xsl:call-template name="units_or_fraction_t">
                    <xsl:with-param name="root" select="unpledge/quantity_unpledge" />
                </xsl:call-template>
                <xsl:if test="quantity_convert">
                    <br />
                    <xsl:text>Количество, на которое нужно уменьшить указанное в условиях количество ЦБ после конвертации: </xsl:text>
                    <xsl:call-template name="units_or_fraction_t">
                        <xsl:with-param name="root" select="quantity_convert" />
                    </xsl:call-template>
                </xsl:if>
                <xsl:if test="quantity_divid">
                    <br />
                    <xsl:text>Количество, на которое нужно уменьшить указанное в условиях количество ЦБ, получателем дохода на которое является залогодержатель: </xsl:text>
                    <xsl:call-template name="units_or_fraction_t">
                        <xsl:with-param name="root" select="quantity_divid" />
                    </xsl:call-template>
                </xsl:if>
                <xsl:if test="quantity_vote">
                    <br />
                    <xsl:text>Количество, на которое нужно уменьшить указанное в условиях количество ЦБ, правом голоса на которое обладает залогодержатель : </xsl:text>
                    <xsl:call-template name="units_or_fraction_t">
                        <xsl:with-param name="root" select="quantity_vote" />
                    </xsl:call-template>
                </xsl:if>
            </xsl:when>
            <xsl:when test="pledge_modify">
                <xsl:call-template name="condition_pledge_t">
                    <xsl:with-param name="root"
                        select="pledge_modify/condition_pledge_new" />
                    <xsl:with-param name="header" select="'Условия залога: '" />
                </xsl:call-template>
            </xsl:when>
            <xsl:when test="pledge_deliver">
                <xsl:text>Описание залога: </xsl:text>
                <xsl:call-template name="burden_simple">
                    <xsl:with-param name="burdenPrev" select="No" />
                    <xsl:with-param name="burdenType"
                        select="pledge_deliver/pledge_info/pledge_type" />
                    <xsl:with-param name="burdenRepead"
                        select="pledge_deliver/pledge_info/repeat_pledge" />
                    <xsl:with-param name="burdenQuantity"
                        select="pledge_deliver/pledge_info/quantity" />
                    <xsl:with-param name="burdenDate"
                        select="pledge_deliver/pledge_info/pledge_date" />
                    <xsl:with-param name="burdenCondition"
                        select="pledge_deliver/pledge_info/condition_pledge" />
                    <xsl:with-param name="burdenComment"
                        select="pledge_deliver/pledge_info/comment" />
                    <xsl:with-param name="burdenIdentificator"
                        select="pledge_deliver/pledge_info/id_pledge" />
                    <xsl:with-param name="burdenInfo" select="pledge_deliver/pledge_info" />
                </xsl:call-template>
                <br />
            </xsl:when>
            <xsl:when test="unpaid">
                <xsl:text>Описание обременения ЦБ обязательством неоплаты: </xsl:text>
                <xsl:call-template name="burden_simple">
                    <xsl:with-param name="burdenPrev" select="No" />
                    <xsl:with-param name="burdenType"
                        select="unpaid/unpaid_info/unpaid_type" />
                    <xsl:with-param name="burdenRepead"
                        select="unpaid/unpaid_info/unpaid_pledge" />
                    <xsl:with-param name="burdenQuantity" select="unpaid/unpaid_info/quantity" />
                    <xsl:with-param name="burdenDate"
                        select="unpaid/unpaid_info/unpaid_date" />
                    <xsl:with-param name="burdenCondition"
                        select="unpaid/unpaid_info/condition_unpaid" />
                    <xsl:with-param name="burdenComment" select="unpaid/unpaid_info/comment" />
                    <xsl:with-param name="burdenIdentificator" select="unpaid/unpaid_info/id_unpaid" />
                    <xsl:with-param name="burdenInfo" select="unpaid/unpaid_info" />
                </xsl:call-template>
            </xsl:when>
            <xsl:when test="unpaid_cancel">
                <xsl:text>Описание обременения ЦБ обязательством оплаты: </xsl:text>
                <xsl:call-template name="burden_simple">
                    <xsl:with-param name="burdenPrev" select="No" />
                    <xsl:with-param name="burdenType"
                        select="unpaid_cancel/unpaid_info/unpaid_type" />
                    <xsl:with-param name="burdenRepead"
                        select="unpaid_cancel/unpaid_info/unpaid_pledge" />
                    <xsl:with-param name="burdenQuantity"
                        select="unpaid_cancel/unpaid_info/quantity" />
                    <xsl:with-param name="burdenDate"
                        select="unpaid_cancel/unpaid_info/unpaid_date" />
                    <xsl:with-param name="burdenCondition"
                        select="unpaid_cancel/unpaid_info/condition_unpaid" />
                    <xsl:with-param name="burdenComment"
                        select="unpaid_cancel/unpaid_info/comment" />
                    <xsl:with-param name="burdenIdentificator"
                        select="unpaid_cancel/unpaid_info/id_unpaid" />
                    <xsl:with-param name="burdenInfo" select="unpaid_cancel/unpaid_info" />
                </xsl:call-template>
                <br />
                <xsl:text>Количество ЦБ, на которое снимается обременение: </xsl:text>
                <xsl:call-template name="units_or_fraction_t">
                    <xsl:with-param name="root" select="unpaid_cancel/quantity_cancel" />
                </xsl:call-template>
            </xsl:when>
            <xsl:when test="blockoper">
                <xsl:text>Описание блокирования операций: </xsl:text>
                <br />
                <xsl:if test="blockoper/blockoper_info/id_blockoper">
                    <xsl:text>Уникальный номер блокирования операций: </xsl:text>
                    <xsl:value-of select="blockoper/blockoper_info/id_blockoper" />
                    <br />
                </xsl:if>
                <xsl:if test="blockoper/blockoper_info/id_blockoper">
                    <xsl:text>Дата установки блокирования операций: </xsl:text>
                    <xsl:call-template name="convertDate">
                        <xsl:with-param name="date"
                            select="blockoper/blockoper_info/blockoper_date" />
                    </xsl:call-template>
                    <br />
                </xsl:if>
                <xsl:text>Вид блокирования: </xsl:text>
                <xsl:call-template name="blockoper_type_t">
                    <xsl:with-param name="type"
                        select="blockoper/blockoper_info/blockoper_type/blockoper_type_code" />
                </xsl:call-template>
                <br />
                <xsl:text>Основание для блокирования: </xsl:text>
                <xsl:call-template name="block_reason_et">
                    <xsl:with-param name="type"
                        select="blockoper/blockoper_info/block_reason" />
                </xsl:call-template>
                <xsl:if test="blockoper/comment">
                    <br />
                    <xsl:value-of select="blockoper/comment" />
                </xsl:if>
            </xsl:when>
            <xsl:when test="unblockoper">
                <xsl:text>Описание блокирования операций: 
                </xsl:text>
                <br />
                <xsl:if test="unblockoper/blockoper_info/id_blockoper">
                    <xsl:text>Уникальный номер блокирования операций: </xsl:text>
                    <xsl:value-of select="unblockoper/blockoper_info/id_blockoper" />
                    <br />
                </xsl:if>
                <xsl:if test="unblockoper/blockoper_info/id_blockoper">
                    <xsl:text>Дата установки 
                блокирования операций: </xsl:text>
                    <xsl:call-template name="convertDate">
                        <xsl:with-param name="date"
                            select="unblockoper/blockoper_info/blockoper_date"></xsl:with-param>
                    </xsl:call-template>
                    <br />
                </xsl:if>
                <xsl:text>Вид блокирования: </xsl:text>
                <xsl:call-template name="blockoper_type_t">
                    <xsl:with-param name="type"
                        select="unblockoper/blockoper_info/blockoper_type/blockoper_type_code" />
                </xsl:call-template>
                <br />
                <xsl:text>Основание для блокирования: </xsl:text>
                <xsl:call-template name="block_reason_et">
                    <xsl:with-param name="type"
                        select="blockoper/blockoper_info/block_reason" />
                </xsl:call-template>
                <xsl:if test="unblockoper/comment">
                    <br />
                    <xsl:value-of select="unblockoper/comment" />
                </xsl:if>
            </xsl:when>
            <xsl:when test="convert">
                <xsl:text>Идентификатор 
                ЦБ списания: </xsl:text>
                <br />
                <xsl:call-template name="securityData">
                    <xsl:with-param name="security" select="convert/security_deliv" />
                </xsl:call-template>
                <xsl:text> 
                Регистрационный № </xsl:text>
                <xsl:call-template name="securityRegNumber">
                    <xsl:with-param name="security" select="convert/security_deliv" />
                </xsl:call-template>
                <br />
                <xsl:text>Идентификатор ЦБ зачисления: </xsl:text>
                <br />
                <xsl:call-template name="securityData">
                    <xsl:with-param name="security" select="convert/security_receip" />
                </xsl:call-template>
                <xsl:text> Регистрационный № </xsl:text>
                <xsl:call-template name="securityRegNumber">
                    <xsl:with-param name="security" select="convert/security_receip" />
                </xsl:call-template>
                <br />
                <xsl:text>Количество конвертируемых ЦБ: </xsl:text>
                <xsl:call-template name="units_or_fraction_t">
                    <xsl:with-param name="root" select="convert/quantity" />
                </xsl:call-template>
                <br />
                <xsl:text>Коеффициент 
                конвертиции: </xsl:text>
                <xsl:value-of select="convert/numerator" />
                <xsl:text>/</xsl:text>
                <xsl:value-of select="convert/denominator" />
            </xsl:when>
            <xsl:when test="coowner_open">
                <xsl:if test="coowner_open/coowner_ident">
                    <xsl:text>Идентификатор совладельца: 
                </xsl:text>
                    <br />
                    <xsl:if test="coowner_open/coowner_ident/id_coowner">
                        <xsl:text>Уникальный номер совладельца по счету ОС: </xsl:text>
                        <xsl:value-of select="coowner_open/coowner_ident/id_coowner" />
                        <br />
                    </xsl:if>
                    <xsl:call-template name="shareholder_info_simple">
                        <xsl:with-param name="root" select="coowner_open/coowner_ident" />
                    </xsl:call-template>
                </xsl:if>
                <br />
                <xsl:text>Анкета совладельца: </xsl:text>
                <xsl:call-template name="content_form_of_coowner_t">
                    <xsl:with-param name="root" select="coowner_open/coowner_ident" />
                </xsl:call-template>
                <xsl:if test="coowner_open/quota">
                    <xsl:text>Доля совладельца: </xsl:text>
                    <xsl:value-of select="coowner_open/quota/numerator" />
                    <xsl:text>/</xsl:text>
                    <xsl:value-of select="coowner_open/quota/denominator" />
                </xsl:if>
            </xsl:when>
            <xsl:when test="coowner_modify">
                <xsl:if test="coowner_modify/coowner_ident">
                    <xsl:text>Идентификатор совладельца: </xsl:text>
                    <br />
                    <xsl:if test="coowner_modify/coowner_ident/id_coowner">
                        <xsl:text>Уникальный номер совладельца по счету ОС: </xsl:text>
                        <xsl:value-of select="coowner_open/coowner_ident/id_coowner" />
                        <br />
                    </xsl:if>
                    <xsl:call-template name="shareholder_info_simple">
                        <xsl:with-param name="root" select="coowner_modify/coowner_ident" />
                    </xsl:call-template>
                </xsl:if>
                <br />
                <xsl:text>Анкета совладельца: </xsl:text>
                <xsl:call-template name="content_form_of_coowner_t">
                    <xsl:with-param name="root" select="coowner_modify/coowner_ident" />
                </xsl:call-template>
                <xsl:if test="coowner_modifyn/quota">
                    <xsl:text>Доля совладельца: </xsl:text>
                    <xsl:value-of select="coowner_modify/quota/numerator" />
                    <xsl:text>/</xsl:text>
                    <xsl:value-of select="coowner_modify/quota/denominator" />
                </xsl:if>
            </xsl:when>
            <xsl:when test="coowner_close">
                <xsl:text>Идентификатор 
                совладельца: </xsl:text>
                <br />
                <xsl:if test="coowner_close/coowner_ident/id_coowner">
                    <xsl:text>Уникальный номер совладельца по счету ОС: </xsl:text>
                    <xsl:value-of select="coowner_close/coowner_ident/id_coowner" />
                    <br />
                </xsl:if>
                <xsl:call-template name="shareholder_info_simple">
                    <xsl:with-param name="root" select="coowner_close/coowner_ident" />
                </xsl:call-template>
            </xsl:when>
            <xsl:when test="coowner_quota">
                <xsl:for-each select="coowner_quota/coowner_quota">
                    <xsl:if test="not(position()=1)">
                        <br />
                    </xsl:if>
                    <xsl:text>Идентификатор совладельца: </xsl:text>
                    <br />
                    <xsl:if test="coowner_ident/id_coowner">
                        <xsl:text>Уникальный номер совладельца по 
                счету ОС: </xsl:text>
                        <xsl:value-of select="coowner_ident/id_coowner" />
                        <br />
                    </xsl:if>
                    <xsl:call-template name="shareholder_info_simple">
                        <xsl:with-param name="root" select="coowner_ident" />
                    </xsl:call-template>
                    <br />
                    <xsl:text>Доля 
                совладельца: </xsl:text>
                    <xsl:value-of select="quota/numerator" />
                    <xsl:text>/</xsl:text>
                    <xsl:value-of select="quota/denominator" />
                </xsl:for-each>
            </xsl:when>
            <xsl:when test="proxy_open">
                <xsl:text>Тип уполномоченного представителя: </xsl:text>
                <xsl:call-template name="authorised_person_type_et">
                    <xsl:with-param name="type"
                        select="proxy_open/authorised_person_type" />
                </xsl:call-template>
                <br />
                <xsl:text>Анкета представителя: </xsl:text>
                <br />
                <xsl:call-template name="content_form_of_authorised_t">
                    <xsl:with-param name="root"
                        select="proxy_open/authorised_person_type" />
                </xsl:call-template>
                <br />
                <xsl:text>Основания для полономочий: </xsl:text>
                <xsl:call-template name="bases_of_powers_t">
                    <xsl:with-param name="root"
                        select="proxy_open/powers/bases_of_powers" />
                </xsl:call-template>
                <xsl:if
                    test="proxy_open/powers/rights_signature='Yes' or proxy_open/powers/rights_voting='Yes' 
                or proxy_open/powers/rights_dividend='Yes' or proxy_open/powers/rights_docum='Yes' 
                or proxy_open/powers/rights_courier='Yes'">
                    <xsl:text>Права представителя: 
                </xsl:text>
                    <xsl:if test="proxy_open/powers/rights_signature='Yes'">
                        <br />
                        <xsl:text>Право на подписание документов на проведение операций в реестре;</xsl:text>
                    </xsl:if>
                    <xsl:if test="proxy_open/powers/rights_voting='Yes'">
                        <br />
                        <xsl:text>Право голосования;</xsl:text>
                    </xsl:if>
                    <xsl:if test="proxy_open/powers/rights_dividend='Yes'">
                        <br />
                        <xsl:text>Право на получение дивидендов;</xsl:text>
                    </xsl:if>
                    <xsl:if test="proxy_open/powers/rights_docum='Yes'">
                        <br />
                        <xsl:text>Право на получение информации из реестра;</xsl:text>
                    </xsl:if>
                    <xsl:if test="proxy_open/powers/rights_courier='Yes'">
                        <br />
                        <xsl:text>Право на подачу документов;</xsl:text>
                    </xsl:if>
                </xsl:if>
                <xsl:if test="proxy_open/powers/comment">
                    <xsl:value-of select="proxy_open/powers/comment" />
                </xsl:if>
            </xsl:when>
            <xsl:when test="proxy_modify">
                <xsl:text>Тип уполномоченного 
                представителя: </xsl:text>
                <xsl:call-template name="authorised_person_type_et">
                    <xsl:with-param name="type"
                        select="proxy_modify/authorised_person_form" />
                </xsl:call-template>
                <br />
                <xsl:text>Анкета представителя: </xsl:text>
                <br />
                <xsl:call-template name="content_form_of_authorised_t">
                    <xsl:with-param name="root"
                        select="proxy_modify/authorised_person_form" />
                </xsl:call-template>
                <br />
                <xsl:text>Основания для полономочий: </xsl:text>
                <xsl:call-template name="bases_of_powers_t">
                    <xsl:with-param name="root"
                        select="proxy_modify/powers/bases_of_powers" />
                </xsl:call-template>
                <xsl:if
                    test="proxy_modify/powers/rights_signature='Yes' or proxy_modify/powers/rights_voting='Yes' 
                or proxy_modify/powers/rights_dividend='Yes' or proxy_modify/powers/rights_docum='Yes' 
                or proxy_modify/powers/rights_courier='Yes'">
                    <xsl:text>Права представителя: 
                </xsl:text>
                    <xsl:if test="proxy_modify/powers/rights_signature='Yes'">
                        <br />
                        <xsl:text>Право на подписание документов на проведение операций в реестре;</xsl:text>
                    </xsl:if>
                    <xsl:if test="proxy_modify/powers/rights_voting='Yes'">
                        <br />
                        <xsl:text>Право голосования;</xsl:text>
                    </xsl:if>
                    <xsl:if test="proxy_modify/powers/rights_dividend='Yes'">
                        <br />
                        <xsl:text>Право на получение дивидендов;</xsl:text>
                    </xsl:if>
                    <xsl:if test="proxy_modify/powers/rights_docum='Yes'">
                        <br />
                        <xsl:text>Право на получение информации из реестра;</xsl:text>
                    </xsl:if>
                    <xsl:if test="proxy_modify/powers/rights_courier='Yes'">
                        <br />
                        <xsl:text>Право на подачу документов;</xsl:text>
                    </xsl:if>
                </xsl:if>
                <xsl:if test="proxy_modify/powers/comment">
                    <xsl:value-of select="proxy_modify/powers/comment" />
                </xsl:if>
            </xsl:when>
            <xsl:when test="proxy_cancel">
                <xsl:text>Идентификатор 
                уполномоченного представителя, по которому выполняется отмена полномочий: 
                </xsl:text>
                <br />
                <xsl:if test="proxy_cancel/ident_authorised_person/authorised_person_id">
                    <xsl:text>Номер представителя уникальный: </xsl:text>
                    <xsl:value-of
                        select="proxy_cancel/ident_authorised_person/authorised_person_id" />
                    <br />
                </xsl:if>
                <xsl:text>Тип уполномоченного представителя / должностного 
                лица: </xsl:text>
                <xsl:call-template name="authorised_person_type_et">
                    <xsl:with-param name="type"
                        select="proxy_cancel/authorised_person/authorised_person_type"></xsl:with-param>
                </xsl:call-template>
                <br />
                <xsl:if test="proxy_cancel/authorised_person/authorised_person_position">
                    <xsl:text>Должность: </xsl:text>
                    <xsl:value-of
                        select="proxy_cancel/authorised_person/authorised_person_position" />
                    <br />
                </xsl:if>
                <xsl:text>Основные данные уполномоченного представителя: 
                </xsl:text>
                <br />
                <xsl:call-template name="shareholder_info_simple">
                    <xsl:with-param name="root"
                        select="proxy_cancel/authorised_person/authorised_person_info" />
                </xsl:call-template>
                <xsl:if test="proxy_cancel/authorised_person/bases_of_powers">
                    <xsl:text>Основания для полномочий: </xsl:text>
                    <xsl:call-template name="bases_of_powers_t">
                        <xsl:with-param name="root"
                            select="proxy_cancel/authorised_person/bases_of_powers/bases_of_powers" />
                    </xsl:call-template>
                </xsl:if>
            </xsl:when>
            <xsl:when test="account_close">
            </xsl:when>
            
            <xsl:when test="founder_open">
                <xsl:call-template name="content_form_of_shareholders_t">
                    <xsl:with-param name="root"
                        select="founder_open/founder_form" />
                </xsl:call-template>
                <br />
                <xsl:call-template name="du_conditions_t">
                    <xsl:with-param name="root" select="founder_open/du_conditions"/>
                </xsl:call-template>
            </xsl:when>
            <xsl:when test="founder_modify">
                <xsl:call-template name="content_form_of_shareholders_t">
                    <xsl:with-param name="root"
                        select="founder_modify/founder_form" />
                </xsl:call-template>
                <br />
                <xsl:call-template name="du_conditions_t">
                    <xsl:with-param name="root" select="founder_modify/du_conditions"/>
                </xsl:call-template>
            </xsl:when>
            <xsl:when test="founder_close">
            </xsl:when>
        </xsl:choose>
    </xsl:template>

    <!-- вывод данных описания полномочий -->
    <xsl:template name="bases_of_powers_t">
        <xsl:param name="root" />
        <xsl:value-of select="$root/bases_of_powers_document" />
        <xsl:if test="$root/bases_of_powers_number">
            <xsl:text> № </xsl:text>
            <xsl:value-of select="$root/bases_of_powers_number" />
        </xsl:if>
        <xsl:if test="$root/bases_of_powers_date">
            <xsl:text> от </xsl:text>
            <xsl:value-of select="$root/bases_of_powers_date" />
        </xsl:if>
        <xsl:if test="$root/bases_of_powers_end_date">
            <xsl:text> по </xsl:text>
            <xsl:value-of select="$root/bases_of_powers_end_date" />
        </xsl:if>
    </xsl:template>
    <!-- вывод данных исходящих документов для колонки в таблице "Журнал отправленных/принятых 
        документов" -->
    <xsl:template name="doc_info_t">
        <xsl:param name="root" />
        <td class="dataColBasedInfoTable mainListData">
            <xsl:text>№ </xsl:text>
            <xsl:value-of select="normalize-space($root/doc_link/in_doc_num)" />
            <xsl:if test="$root/doc_link/in_reg_date">
                <xsl:text> от </xsl:text>
                <xsl:call-template name="date_or_datetime_t">
                    <xsl:with-param name="root" select="$root/doc_link/in_reg_date" />
                </xsl:call-template>
            </xsl:if>
        </td>
        <td class="dataColBasedInfoTable mainListData">
            <xsl:text>№ </xsl:text>
            <xsl:value-of select="normalize-space($root/doc_TA/TA_doc_num)" />
            <xsl:text> от </xsl:text>
            <xsl:call-template name="date_or_datetime_t">
                <xsl:with-param name="root" select="$root/doc_TA/TA_doc_date" />
            </xsl:call-template>

        </td>
        <td class="dataColBasedInfoTable mainListData">
            <xsl:value-of select="normalize-space($root/doc_name)" />
            <xsl:text> № </xsl:text>
            <xsl:value-of select="normalize-space($root/doc_link/out_doc_num)" />
            <xsl:if test="$root/doc_link/out_doc_date">
                <xsl:text> от </xsl:text>
                <xsl:call-template name="date_or_datetime_t">
                    <xsl:with-param name="root" select="$root/doc_link/out_doc_date" />
                </xsl:call-template>
            </xsl:if>
        </td>
        <td class="dataColBasedInfoTable mainListData">
            <xsl:if test="$root/sender">
                <xsl:text>Тип: </xsl:text>
                <xsl:call-template name="sender_type_et">
                    <xsl:with-param name="type" select="$root/sender/sender_type" />
                </xsl:call-template>
                <br />
                <xsl:if test="$root/sender/account_number">
                    <xsl:text>Номер счета: </xsl:text>
                    <xsl:value-of select="normalize-space($root/sender/account_number)" />
                    <br />
                </xsl:if>

                <!-- если это ЮЛ -->
                <xsl:if test="$root/sender/person_info/juridical">
                    <xsl:value-of
                        select="normalize-space($root/sender/person_info/juridical/juridical_name)" />
                    <br />
                    <xsl:call-template name="juridical_registration_t">
                        <xsl:with-param name="root"
                            select="$root/sender/person_info/juridical/juridical_registration" />
                    </xsl:call-template>
                </xsl:if>

                <!-- если это ФЛ -->
                <xsl:if test="$root/sender/person_info/individual">
                    <xsl:value-of
                        select="normalize-space($root/sender/person_info/individual/individual_name)" />
                    <br />
                    <xsl:call-template name="individual_document_t">
                        <xsl:with-param name="root"
                            select="$root/sender/person_info/individual/individual_document" />
                    </xsl:call-template>
                </xsl:if>

                <!-- если это ОС -->
                <xsl:if test="$root/sender/person_info/coowners">
                    <xsl:value-of
                        select="normalize-space($root/sender/person_info/coowners/coowners_name)" />
                </xsl:if>
            </xsl:if>
        </td>
        <td class="dataColBasedInfoTable mainListData">
            <xsl:if test="$root/issuer">
                <xsl:value-of
                    select="normalize-space($root/issuer/issuer_info/juridical_name)" />
            </xsl:if>
        </td>
        <td class="dataColBasedInfoTable mainListData">
            <xsl:call-template name="doc_processing_et">
                <xsl:with-param name="type" select="$root/doc_state" />
            </xsl:call-template>
        </td>
        <td class="dataColBasedInfoTable mainListData">
            <xsl:if test="$root/process_date">
                <xsl:call-template name="date_or_datetime_t">
                    <xsl:with-param name="root" select="$root/process_date" />
                </xsl:call-template>
            </xsl:if>
        </td>
        <td class="dataColBasedInfoTable mainListData">
            <xsl:for-each select="$root/outdoc_info">

                <xsl:text>№: </xsl:text>
                <xsl:value-of select="outdoc_num" />
                <br />
                <xsl:text>Дата: </xsl:text>
                <xsl:call-template name="date_or_datetime_t">
                    <xsl:with-param name="root" select="outdoc_date" />
                </xsl:call-template>
                <br />
                <xsl:text>Наименование: </xsl:text>
                <xsl:value-of select="outdoc_name" />
                <br />
                <xsl:if test="receipt_date">
                    <xsl:text>Дата получения: </xsl:text>
                    <xsl:call-template name="date_or_datetime_t">
                        <xsl:with-param name="root" select="receipt_date" />
                    </xsl:call-template>
                    <br />
                </xsl:if>

                <xsl:if test="delivery_date">
                    <xsl:text>Дата выдачи: </xsl:text>
                    <xsl:call-template name="date_or_datetime_t">
                        <xsl:with-param name="root" select="delivery_date" />
                    </xsl:call-template>
                    <br />
                </xsl:if>

                <xsl:if test="delivery/delivery_place">
                    <xsl:text>Место выдачи: </xsl:text>
                    <xsl:call-template name="delivery_place_et">
                        <xsl:with-param name="type" select="delivery/delivery_place" />
                    </xsl:call-template>
                    <br />
                </xsl:if>
                <br />
            </xsl:for-each>
        </td>
    </xsl:template>

    <!-- Вид обременения ЦБ -->
    <xsl:template name="blockType">
        <xsl:param name="block_info" />

        <xsl:call-template name="block_type_et">
            <xsl:with-param name="type"
                select="$block_info/block_type/block_type_code" />
        </xsl:call-template>
        <xsl:if test="$block_info/repeat_block='Yes'">
            <xsl:text> (повторно)</xsl:text>
        </xsl:if>
    </xsl:template>

    <xsl:template name="blockoper_info_t">
        <xsl:param name="type" />
        <xsl:choose>
            <xsl:when test="$type/blockoper_type_code = 'ALLS'">
                <xsl:text>Блокирование всех операций</xsl:text>
            </xsl:when>
            <xsl:when test="$type/blockoper_type_code = 'DELI'">
                <xsl:text>Блокирование расходных операций</xsl:text>
            </xsl:when>
            <xsl:when test="$type/blockoper_type_code = 'DERE'">
                <xsl:text>Блокирование приходно-расходных операций</xsl:text>
            </xsl:when>
            <xsl:when test="$type/blockoper_type_code='STOC'">
                <xsl:text>Блокирование всех операций с ЦБ</xsl:text>
            </xsl:when>
            <xsl:when test="$type/blockoper_type_code='DEPL'">
                <xsl:text>Блокирование расходных операций и залогов</xsl:text>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="$type/narrative" />
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <!-- Вид залога -->
    <xsl:template name="pledgeType">
        <xsl:param name="pledge_info" />

        <xsl:call-template name="block_type_et">
            <xsl:with-param name="type"
                select="$pledge_info/pledge_type/block_type_code" />
        </xsl:call-template>
        <xsl:if test="$pledge_info/repeat_pledge='Yes'">
            <xsl:text> (повторно)</xsl:text>
        </xsl:if>
    </xsl:template>

    <!-- данные обременения -->
    <xsl:template name="burden">
        <xsl:param name="burdenPrev" />
        <xsl:param name="burdenType" />
        <xsl:param name="burdenRepead" />
        <xsl:param name="burdenQuantity" />
        <xsl:param name="burdenDate" />
        <xsl:param name="burdenCondition" />
        <xsl:param name="burdenComment" />
        <xsl:param name="burdenInfo" />

        <xsl:if test="$burdenPrev='Yes'">
            <tr>
                <td class="leftColumnIdentTable sizeData">
                    <xsl:text>Предыдущее обременение: </xsl:text>
                </td>
                <td class="sizeData rightColumnIdentTable">
                </td>
            </tr>
        </xsl:if>
        <tr>
            <td class="leftColumnIdentTable">
                <xsl:text>Тип обременения: </xsl:text>
            </td>
            <td class="sizeData rightColumnIdentTable">
                <xsl:call-template name="block_type_et">
                    <xsl:with-param name="type" select="$burdenType/block_type_code" />
                </xsl:call-template>
                <xsl:if test="$burdenRepead='Yes'">
                    <xsl:text> (повторно)</xsl:text>
                </xsl:if>
            </td>
        </tr>
        <xsl:if test="$burdenQuantity">
            <tr>
                <td class="leftColumnIdentTable">
                    <xsl:text>Количество ЦБ в обременении: </xsl:text>
                </td>
                <td class="sizeData rightColumnIdentTable">
                    <xsl:call-template name="units_or_fraction_t">
                        <xsl:with-param name="root" select="$burdenQuantity" />
                    </xsl:call-template>
                    <xsl:text> шт.</xsl:text>
                </td>
            </tr>
        </xsl:if>
        <xsl:if test="$burdenDate">
            <tr>
                <td class="leftColumnIdentTable">
                    <xsl:text>Дата установки обременения: </xsl:text>
                </td>
                <td class="sizeData rightColumnIdentTable">
                    <xsl:call-template name="convertDate">
                        <xsl:with-param name="date" select="$burdenDate" />
                    </xsl:call-template>
                </td>
            </tr>
        </xsl:if>
        <xsl:if test="$burdenCondition">
            <tr>
                <td class="leftColumnIdentTable">
                    <xsl:text>Условия обременения: </xsl:text>
                </td>
                <td class="sizeData rightColumnIdentTable">
                    <xsl:choose>
                        <xsl:when
                            test="$burdenType/block_type_code='BLOK' or $burdenType/block_type_code='BLOP' or $burdenType/block_type_code='BLON'">
                            <xsl:call-template name="condition_block_t">
                                <xsl:with-param name="root" select="$burdenCondition" />
                            </xsl:call-template>
                        </xsl:when>
                        <xsl:when
                            test="$burdenType/block_type_code='PLDG' or $burdenType/block_type_code='PLDN'">
                            <xsl:call-template name="condition_pledge_t">
                                <xsl:with-param name="root" select="$burdenCondition" />
                            </xsl:call-template>
                        </xsl:when>
                        <xsl:when test="$burdenType/block_type_code='UNPA'">
                            <xsl:call-template name="condition_block_t">
                                <xsl:with-param name="root" select="$burdenCondition" />
                            </xsl:call-template>
                        </xsl:when>
                    </xsl:choose>
                </td>
            </tr>
        </xsl:if>
        <xsl:if test="$burdenComment">
            <tr>
                <td class="leftColumnIdentTable">
                    <xsl:text>Комментарий: </xsl:text>
                </td>
                <td class="sizeData rightColumnIdentTable">
                    <xsl:value-of select="$burdenComment" />
                </td>
            </tr>
        </xsl:if>
        <xsl:if test="$burdenInfo/based_info">
            <tr>
                <td class="leftColumnIdentTable">
                    <xsl:text>Документы-основания: </xsl:text>
                </td>
                <td class="sizeData rightColumnIdentTable">
                    <xsl:call-template name="based_infoRow">
                        <xsl:with-param name="root" select="$burdenInfo" />
                    </xsl:call-template>
                </td>
            </tr>
        </xsl:if>
    </xsl:template>

    <xsl:template name="burden_simple">
        <xsl:param name="burdenPrev" />
        <xsl:param name="burdenType" />
        <xsl:param name="burdenRepead" />
        <xsl:param name="burdenQuantity" />
        <xsl:param name="burdenDate" />
        <xsl:param name="burdenCondition" />
        <xsl:param name="burdenComment" />
        <xsl:param name="burdenIdentificator" />
        <xsl:param name="burdenInfo" />

        <xsl:if test="$burdenPrev='Yes'">
            <xsl:text>Предыдущее обременение: </xsl:text>
        </xsl:if>
        <br />
        <xsl:if test="$burdenIdentificator">
            <xsl:text>Уникальный номер обременения: </xsl:text>
            <xsl:value-of select="$burdenIdentificator" />
            <br />
        </xsl:if>
        <xsl:text>Тип обременения: </xsl:text>
        <xsl:call-template name="block_type_et">
            <xsl:with-param name="type" select="$burdenType/block_type_code" />
        </xsl:call-template>
        <xsl:if test="$burdenRepead='Yes'">
            <xsl:text> (повторно)</xsl:text>
        </xsl:if>
        <xsl:if test="$burdenQuantity">
            <br />
            <xsl:text>Количество ЦБ в обременении: </xsl:text>
            <xsl:call-template name="units_or_fraction_t">
                <xsl:with-param name="root" select="$burdenQuantity" />
            </xsl:call-template>
            <xsl:text> шт.</xsl:text>
        </xsl:if>
        <xsl:if test="$burdenDate">
            <br />
            <xsl:text>Дата установки обременения: </xsl:text>
            <xsl:call-template name="convertDate">
                <xsl:with-param name="date" select="$burdenDate" />
            </xsl:call-template>
        </xsl:if>
        <xsl:if test="$burdenCondition">
            <br />
            <xsl:text>Условия обременения: </xsl:text>
            <br />
            <xsl:choose>
                <xsl:when
                    test="$burdenType/block_type_code='BLOK' or $burdenType/block_type_code='BLOP' or $burdenType/block_type_code='BLON'">
                    <xsl:call-template name="condition_block_t">
                        <xsl:with-param name="root" select="$burdenCondition" />
                    </xsl:call-template>
                </xsl:when>
                <xsl:when
                    test="$burdenType/block_type_code='PLDG' or $burdenType/block_type_code='PLDN'">
                    <xsl:call-template name="condition_pledge_t">
                        <xsl:with-param name="root" select="$burdenCondition" />
                    </xsl:call-template>
                </xsl:when>
                <xsl:when test="$burdenType/block_type_code='UNPA'">
                    <xsl:call-template name="condition_block_t">
                        <xsl:with-param name="root" select="$burdenCondition" />
                    </xsl:call-template>
                </xsl:when>
            </xsl:choose>
        </xsl:if>
        <xsl:if test="$burdenComment">
            <br />
            <xsl:text>Комментарий к описанию обременения: </xsl:text>
            <xsl:value-of select="$burdenComment" />
        </xsl:if>
        <xsl:if test="$burdenInfo/based_info">
            <br />
            <xsl:text>Документы-основания: </xsl:text>
            <br />
            <xsl:call-template name="based_infoRow">
                <xsl:with-param name="root" select="$burdenInfo" />
            </xsl:call-template>
        </xsl:if>
    </xsl:template>
    <!-- Вывод блока обременения -->
    <xsl:template name="burden_block">
        <xsl:param name="root" />
        <xsl:param name="mainHeader" />
        <xsl:param name="burdenQuantity" />
        <table class="identTable">
            <tbody>
                <tr>
                    <td class="headerIdentTable sizeData">
                        <xsl:value-of select="normalize-space($mainHeader)" />
                    </td>
                    <td class="sizeData rightColumnIdentTable">
                    </td>
                </tr>
                <xsl:choose>
                    <xsl:when test="$root/block_info">
                        <xsl:call-template name="burden">
                            <xsl:with-param name="burdenPrev" select="'No'" />
                            <xsl:with-param name="burdenType" select="$root/block_info/block_type" />
                            <xsl:with-param name="burdenRepead"
                                select="$root/block_info/repeat_block" />
                            <xsl:with-param name="burdenQuantity" select="burdenQuantity" />
                            <xsl:with-param name="burdenDate" select="$root/block_info/block_date" />
                            <xsl:with-param name="burdenCondition"
                                select="$root/block_info/condition_block" />
                            <xsl:with-param name="burdenComment" select="$root/block_info/comment" />
                            <xsl:with-param name="burdenInfo" select="$root/block_info" />
                        </xsl:call-template>
                    </xsl:when>
                    <xsl:when test="$root/pledge_info">

                        <xsl:choose>
                            <xsl:when test="$root/condition_pledge_new">
                                <xsl:call-template name="burden">
                                    <xsl:with-param name="burdenPrev" select="'No'" />
                                    <xsl:with-param name="burdenType"
                                        select="$root/pledge_info/pledge_type" />
                                    <xsl:with-param name="burdenRepead"
                                        select="$root/pledge_info/repeat_pledge" />
                                    <xsl:with-param name="burdenQuantity" select="$burdenQuantity" />
                                    <xsl:with-param name="burdenDate"
                                        select="$root/pledge_info/pledge_date" />
                                    <xsl:with-param name="burdenCondition"
                                        select="$root/condition_pledge_new" />
                                    <xsl:with-param name="burdenComment" select="$root/comment" />
                                    <xsl:with-param name="burdenInfo" select="$root/pledge_info" />
                                </xsl:call-template>
                            </xsl:when>
                            <xsl:when test="$root/pledge_info/condition_pledge">
                                <xsl:call-template name="burden">
                                    <xsl:with-param name="burdenPrev" select="'No'" />
                                    <xsl:with-param name="burdenType"
                                        select="$root/pledge_info/pledge_type" />
                                    <xsl:with-param name="burdenRepead"
                                        select="$root/pledge_info/repeat_pledge" />
                                    <xsl:with-param name="burdenQuantity" select="$burdenQuantity" />
                                    <xsl:with-param name="burdenDate"
                                        select="$root/pledge_info/pledge_date" />
                                    <xsl:with-param name="burdenCondition"
                                        select="$root/pledge_info/condition_pledge" />
                                    <xsl:with-param name="burdenComment" select="$root/pledge_info/comment" />
                                    <xsl:with-param name="burdenInfo" select="$root/pledge_info" />
                                </xsl:call-template>
                            </xsl:when>
                        </xsl:choose>
                    </xsl:when>
                    <xsl:when test="$root/unpaid_info">
                        <xsl:call-template name="burden">
                            <xsl:with-param name="burdenPrev" select="'No'" />
                            <xsl:with-param name="burdenType"
                                select="$root/unpaid_info/unpaid_type" />
                            <xsl:with-param name="burdenQuantity" select="$burdenQuantity" />
                            <xsl:with-param name="burdenDate"
                                select="$root/unpaid_info/unpaid_date" />
                            <xsl:with-param name="burdenCondition"
                                select="$root/unpaid_info/condition_unpaid" />
                            <xsl:with-param name="burdenComment" select="$root/unpaid_info/comment" />
                            <xsl:with-param name="burdenInfo" select="$root/unpaid_info" />
                        </xsl:call-template>
                    </xsl:when>
                </xsl:choose>

                <xsl:if test="$root/block_prev">
                    <xsl:call-template name="burden">
                        <xsl:with-param name="burdenPrev" select="'Yes'" />
                        <xsl:with-param name="burdenType" select="$root/block_prev/block_type" />
                        <xsl:with-param name="burdenRepead"
                            select="$root/block_prev/repeat_block" />
                        <xsl:with-param name="burdenQuantity" select="$root/block_prev/quantity" />
                        <xsl:with-param name="burdenDate" select="$root/block_prev/block_date" />
                        <xsl:with-param name="burdenCondition"
                            select="$root/block_prev/condition_block" />
                        <xsl:with-param name="burdenComment" select="$root/block_prev/comment" />
                        <xsl:with-param name="burdenInfo" select="$root/block_prev" />
                    </xsl:call-template>
                </xsl:if>
                <xsl:if test="$root/pledge_prev">
                    <xsl:call-template name="burden">
                        <xsl:with-param name="burdenPrev" select="'Yes'" />
                        <xsl:with-param name="burdenType"
                            select="$root/pledge_prev/pledge_type" />
                        <xsl:with-param name="burdenRepead"
                            select="$root/block_prev/repeat_block" />
                        <xsl:with-param name="burdenQuantity" select="$root/pledge_prev/quantity" />
                        <xsl:with-param name="burdenDate" select="$root/pledge_prev/block_date" />
                        <xsl:with-param name="burdenCondition"
                            select="$root/pledge_prev/condition_pledge" />
                        <xsl:with-param name="burdenComment" select="$root/pledge_prev/comment" />
                        <xsl:with-param name="burdenInfo" select="$root/pledge_prev" />
                    </xsl:call-template>
                </xsl:if>
                <xsl:if test="$root/unpaid_prev">
                    <xsl:call-template name="burden">
                        <xsl:with-param name="burdenPrev" select="'Yes'" />
                        <xsl:with-param name="burdenType"
                            select="$root/unpaid_prev/unpaid_type" />
                        <xsl:with-param name="burdenQuantity" select="$root/unpaid_prev/quantity" />
                        <xsl:with-param name="burdenDate"
                            select="$root/unpaid_prev/unpaid_date" />
                        <xsl:with-param name="burdenCondition"
                            select="$root/unpaid_prev/condition_unpaid" />
                        <xsl:with-param name="burdenComment" select="$root/unpaid_prev/comment" />
                        <xsl:with-param name="burdenInfo" select="$root/unpaid_prev" />
                    </xsl:call-template>
                </xsl:if>
            </tbody>
        </table>
    </xsl:template>

    <!-- Условия залога -->
    <xsl:template name="condition_pledge_block">
        <xsl:param name="root" />
        <table class="identTable">
            <tbody>
                <tr>
                    <td class="headerIdentTable sizeData">
                        Условия залога:
                    </td>
                    <td class="sizeData rightColumnIdentTable">
                        <xsl:call-template name="condition_pledge_t">
                            <xsl:with-param name="root" select="$root" />
                        </xsl:call-template>
                    </td>
                </tr>
            </tbody>
        </table>
    </xsl:template>

    <!-- данные ЦБ -->
    <xsl:template name="statementOfHoldingsSecurityBalance">
        <xsl:param name="statementType" /> 
        
        <tbody>
            <tr>
                <td class="leftColumnIdentTable identTableRowLeftBorder">
                    <xsl:text>Ценная бумага: </xsl:text>
                </td>
                <td class="sizeData rightColumnIdentTable identTableRowLeftBorder">

                    <xsl:call-template name="securityData">
                        <xsl:with-param name="security" select="security" />
                    </xsl:call-template>

                </td>
            </tr>
            <tr>
                <td class="leftColumnIdentTable identTableRowLeftBorder">
                    <xsl:text>Регистрационный номер: </xsl:text>
                </td>
                <td class="sizeData rightColumnIdentTable identTableRowLeftBorder">
                    <xsl:value-of select="normalize-space(security/state_reg_num)" />
                </td>
            </tr>

            <tr>
                <td class="leftColumnIdentTable identTableRowLeftBorder">
                    <xsl:text>Количество ЦБ: </xsl:text>
                </td>
                <td class="sizeData rightColumnIdentTable identTableRowLeftBorder">
                    <xsl:call-template name="units_or_fraction_t">
                        <xsl:with-param name="root" select="quantity/total" />
                    </xsl:call-template>

                    <xsl:text> шт. </xsl:text>
                </td>
            </tr>
            
            <xsl:if test="$statementType!='AVAI'">
                <xsl:call-template name="printBlock">
                    <xsl:with-param name="root" select="quantity" />
                </xsl:call-template>
            </xsl:if>        
        </tbody>
    </xsl:template>

      
    <xsl:template name="printBlock">
        <xsl:param name="root"/>
        <tr>
            <td class="leftColumnIdentTable identTableRowLeftBorder">
                <xsl:text>Обременения: </xsl:text>
            </td>
            <xsl:choose>
                <xsl:when test="$root/blocked"> 
                    <td class="sizeData rightColumnIdentTable identTableRowLeftBorder">
                        <xsl:for-each select="$root/blocked">

                            <xsl:if test="not(position()=1)">
                                <br />
                            </xsl:if>

                            <xsl:call-template name="block_type_et">
                                <xsl:with-param name="type"
                                    select="block_type/block_type_code" />
                            </xsl:call-template>

                            <xsl:text> </xsl:text>

                            <xsl:call-template name="units_or_fraction_t">
                                <xsl:with-param name="root" select="quantity" />
                            </xsl:call-template>
                        </xsl:for-each>
                    </td>
                </xsl:when>
                <xsl:otherwise>
                    <td class="sizeData rightColumnIdentTable identTableRowLeftBorder">
                        <xsl:text>Отсутствуют</xsl:text>
                    </td>
                </xsl:otherwise>
            </xsl:choose>
        </tr>
    </xsl:template>
    
    <!-- вывод данных ЦБ -->
    <xsl:template name="securityData">
        <xsl:param name="security" />
        <xsl:call-template name="security_classification_et">
            <xsl:with-param name="type"
                select="$security/security_classification" />
        </xsl:call-template>

        <xsl:if test="$security/security_classification='SHAR'">
            <xsl:if test="$security/security_category">
                <xsl:text> </xsl:text>
                <xsl:call-template name="security_category_et">
                    <xsl:with-param name="type" select="$security/security_category" />
                </xsl:call-template>
            </xsl:if>
        </xsl:if>

        <xsl:if test="$security/security_type">
            <xsl:if test="not($security/security_type='&lt;Не указан&gt;')">
                <xsl:text> </xsl:text>
                <xsl:value-of select="normalize-space($security/security_type)" />
            </xsl:if>
        </xsl:if>

        <xsl:if test="$security/nominal_value">
            <xsl:text>, номинал </xsl:text>
            <xsl:call-template name="units_or_fraction_t">
                <xsl:with-param name="root" select="$security/nominal_value" />
            </xsl:call-template>
            <xsl:text> руб.</xsl:text>
        </xsl:if>
    </xsl:template>

    <!-- вывод данных документа-основания -->
    <xsl:template name="based_infoRow">
        <xsl:param name="root" />
        <xsl:for-each select="$root/based_info">
            <xsl:if test="not(position()=1)">
                <br />
            </xsl:if>
            <xsl:call-template name="contract_et">
                <xsl:with-param name="type" select="contract_type" />
            </xsl:call-template>
            <xsl:text> № </xsl:text>
            <xsl:value-of select="normalize-space(doc_num)" />
            <xsl:text> от </xsl:text>
            <xsl:call-template name="convertDate">
                <xsl:with-param name="date" select="doc_date" />
            </xsl:call-template>
        </xsl:for-each>
    </xsl:template>

    <!-- Количество ЦБ -->
    <xsl:template name="securityQuantity">
        <xsl:param name="quantity" />
        <xsl:call-template name="units_or_fraction_t">
            <xsl:with-param name="root" select="$quantity" />
        </xsl:call-template>
    </xsl:template>

    <!-- Регномер ЦБ -->
    <xsl:template name="securityRegNumber">
        <xsl:param name="security" />
        <xsl:value-of select="normalize-space($security/state_reg_num)" />
    </xsl:template>

    <!-- сумма ЦБ -->
    <xsl:template name="securitySettlementAmount">
        <xsl:param name="settlement_amount" />
        <xsl:call-template name="currency_and_amount_t">
            <xsl:with-param name="root" select="$settlement_amount" />
        </xsl:call-template>
    </xsl:template>

    <!-- Идентификатор ЦБ -->
    <xsl:template name="security_info_t">
        <xsl:param name="cancelQuantity" />
        <table class="identTable">
            <tbody>
                <tr>
                    <td class="headerIdentTable sizeData">
                        <xsl:text>Ценная бумага: </xsl:text>
                    </td>
                    <td class="sizeData rightColumnIdentTable">

                        <xsl:call-template name="securityData">
                            <xsl:with-param name="security" select="security" />
                        </xsl:call-template>

                    </td>
                </tr>
                <tr>
                    <td class="leftColumnIdentTable">
                        <xsl:text>Регистрационный номер: </xsl:text>
                    </td>
                    <td class="sizeData rightColumnIdentTable">
                        <xsl:value-of select="normalize-space(security/state_reg_num)" />
                    </td>
                </tr>
                <xsl:choose>
                    <xsl:when test="$cancelQuantity">
                        <tr>
                            <td class="leftColumnIdentTable">
                                <xsl:text>Количество ЦБ: </xsl:text>
                            </td>
                            <td class="sizeData rightColumnIdentTable">
                                <xsl:call-template name="units_or_fraction_t">
                                    <xsl:with-param name="root" select="$cancelQuantity" />
                                </xsl:call-template>
                                <xsl:text> шт.</xsl:text>
                            </td>
                        </tr>
                    </xsl:when>
                    <xsl:when test="quantity">
                        <tr>
                            <td class="leftColumnIdentTable">
                                <xsl:text>Количество ЦБ: </xsl:text>
                            </td>
                            <td class="sizeData rightColumnIdentTable">
                                <xsl:call-template name="units_or_fraction_t">
                                    <xsl:with-param name="root" select="quantity" />
                                </xsl:call-template>
                                <xsl:text> шт.</xsl:text>
                            </td>
                        </tr>
                    </xsl:when>
                </xsl:choose>
                <xsl:if test="settlement_amount">
                    <tr>
                        <td class="leftColumnIdentTable">
                            <xsl:text>Сумма: </xsl:text>
                        </td>
                        <td class="sizeData rightColumnIdentTable">
                            <xsl:call-template name="currency_and_amount_t">
                                <xsl:with-param name="root" select="settlement_amount" />
                            </xsl:call-template>
                        </td>
                    </tr>
                </xsl:if>
            </tbody>
        </table>
    </xsl:template>

    <!-- вывод данных лицевого счета и типа счета -->
    <xsl:template name="accountNumberAndType">
        <xsl:param name="root" />
        <xsl:if test="$root/account_number">
            <xsl:text>Счет № </xsl:text>
            <xsl:value-of select="normalize-space($root/account_number)" />
            <xsl:text> </xsl:text>
        </xsl:if>
        <xsl:text>(</xsl:text>
        <xsl:call-template name="account_type_et">
            <xsl:with-param name="type" select="$root/account_type" />
        </xsl:call-template>
        <xsl:text>)</xsl:text>
    </xsl:template>

    <!-- вывод данных о наименовании ЗЛ -->
    <xsl:template name="accountName">
        <xsl:param name="person_info" />
        <xsl:choose>
            <xsl:when test="$person_info/juridical">
                <xsl:value-of
                    select="normalize-space($person_info/juridical/juridical_name)" />
            </xsl:when>
            <xsl:when test="$person_info/individual">
                <xsl:value-of
                    select="normalize-space($person_info/individual/individual_name)" />
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="normalize-space($person_info/coowners/coowners_name)" />
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <!-- вывод докумената ЗЛ -->
    <xsl:template name="accountDocument">
        <xsl:param name="person_info" />
        <xsl:choose>
            <xsl:when test="$person_info/juridical">
                <xsl:call-template name="juridical_registration_t">
                    <xsl:with-param name="root"
                        select="$person_info/juridical/juridical_registration" />
                </xsl:call-template>
            </xsl:when>
            <xsl:when test="$person_info/individual">
                <xsl:call-template name="individual_document_t">
                    <xsl:with-param name="root"
                        select="$person_info/individual/individual_document" />
                </xsl:call-template>
            </xsl:when>
        </xsl:choose>
    </xsl:template>

    <!-- вывод строчки с данными ЗЛ -->
    <xsl:template name="accountRow">
        <xsl:param name="root" />
        <xsl:param name="type" />

        <xsl:value-of select="normalize-space($type)" />

        <xsl:call-template name="accountNumberAndType">
            <xsl:with-param name="root" select="$root" />
        </xsl:call-template>

        <xsl:if test="not($root/account_type='ISSUE')">
            <br />
            <xsl:call-template name="accountName">
                <xsl:with-param name="person_info" select="$root/person_info" />
            </xsl:call-template>
            <br />
            <xsl:call-template name="accountDocument">
                <xsl:with-param name="person_info" select="$root/person_info" />
            </xsl:call-template>
        </xsl:if>
    </xsl:template>

    <!--Вывод идентификатора совладельца -->
    <xsl:template name="coowner_ident_t">
        <xsl:param name="root" />
        <xsl:if test="$root">
            <table class="identTable">
                <tbody>
                    <tr>
                        <td class="headerIdentTable sizeData">
                            <xsl:text>Совладелец: </xsl:text>
                        </td>
                        <td class="sizeData rightColumnIdentTable">
                        </td>
                    </tr>
                    <tr>
                        <td class="leftColumnIdentTable">
                            <xsl:choose>
                                <xsl:when test="$root/individual">
                                    <xsl:text>ФИО: </xsl:text>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:text>Наименование: </xsl:text>
                                </xsl:otherwise>
                            </xsl:choose>
                        </td>
                        <td class="sizeData rightColumnIdentTable">
                            <!--наименование -->
                            <xsl:call-template name="accountName">
                                <xsl:with-param name="person_info" select="$root" />
                            </xsl:call-template>
                        </td>
                    </tr>
                    <tr>
                        <td class="leftColumnIdentTable">
                            <xsl:text>Документ: </xsl:text>
                        </td>
                        <td class="sizeData rightColumnIdentTable">
                            <xsl:call-template name="accountDocument">
                                <xsl:with-param name="person_info" select="$root" />
                            </xsl:call-template>
                        </td>
                    </tr>
                    <xsl:if test="$root/id_coowner">
                        <tr>
                            <td class="leftColumnIdentTable">
                                <xsl:text>Уникальный номер совладельца по счету ОС: </xsl:text>
                            </td>
                            <td class="sizeData rightColumnIdentTable">
                                <xsl:value-of select="$root/id_coowner" />
                            </td>
                        </tr>
                    </xsl:if>
                </tbody>
            </table>
        </xsl:if>
    </xsl:template>

    <!--Вывод идентификатора ЗЛ -->
    <xsl:template name="account_ident_t">
        <xsl:param name="title" />
        <xsl:param name="root" />
        <xsl:if test="$root">
            <table class="identTable">
                <tbody>
                    <tr>
                        <td class="headerIdentTable sizeData">
                            <xsl:value-of select="normalize-space($title)" />
                            <xsl:text>: </xsl:text>
                        </td>
                        <td class="sizeData rightColumnIdentTable">
                            <!-- "номер счета" ("вид счёта") -->
                            <xsl:call-template name="accountNumberAndType">
                                <xsl:with-param name="root" select="$root" />
                            </xsl:call-template>
                        </td>
                    </tr>
                    <tr>
                        <td class="leftColumnIdentTable">
                            <xsl:choose>
                                <xsl:when test="$root/person_info/individual">
                                    <xsl:text>ФИО: </xsl:text>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:text>Наименование: </xsl:text>
                                </xsl:otherwise>
                            </xsl:choose>
                        </td>
                        <td class="sizeData rightColumnIdentTable">
                            <!--наименование -->
                            <xsl:call-template name="accountName">
                                <xsl:with-param name="person_info" select="$root/person_info" />
                            </xsl:call-template>
                        </td>
                    </tr>
                    <xsl:if test="not($root/person_info/coowners)">
                        <tr>
                            <td class="leftColumnIdentTable">
                                <xsl:text>Документ: </xsl:text>
                            </td>
                            <td class="sizeData rightColumnIdentTable">
                                <xsl:call-template name="accountDocument">
                                    <xsl:with-param name="person_info" select="$root/person_info" />
                                </xsl:call-template>
                            </td>
                        </tr>
                    </xsl:if>
                </tbody>
            </table>
        </xsl:if>
    </xsl:template>

    <!-- Вывод шапки документа -->
    <xsl:template name="fesDocHeader">
        <xsl:param name="root" />
        <xsl:choose>
            <xsl:when test="$root/doc_link">
                <table id="header">
                    <tbody>
                        <tr>
                            <td id="colLeft">
                                <table id="docNum">
                                    <tbody>

                                        <tr>
                                            <td class="leftColumnDocNumTable">Исх. №: </td>
                                            <td>
                                                <xsl:value-of select="normalize-space($root/doc_num)" />
                                                <xsl:text> от </xsl:text>
                                                <xsl:call-template name="date_or_datetime_t">
                                                    <xsl:with-param name="root" select="$root/doc_date" />
                                                </xsl:call-template>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td class="leftColumnDocNumTable">На вх. №: </td>
                                            <td>
                                                <xsl:value-of select="normalize-space($root/doc_link/in_doc_num)" />
                                                <xsl:if test="$root/doc_link/in_reg_date">
                                                    <xsl:text> от </xsl:text>
                                                    <xsl:call-template name="date_or_datetime_t">
                                                        <xsl:with-param name="root"
                                                            select="$root/doc_link/in_reg_date" />
                                                    </xsl:call-template>
                                                </xsl:if>

                                            </td>
                                        </tr>

                                        <tr>
                                            <td class="leftColumnDocNumTable">На исх. №: </td>
                                            <td>
                                                <xsl:value-of select="normalize-space($root/doc_link/out_doc_num)" />
                                                <xsl:if test="$root/doc_link/out_doc_date">
                                                    <xsl:text> от </xsl:text>
                                                    <xsl:call-template name="date_or_datetime_t">
                                                        <xsl:with-param name="root"
                                                            select="$root/doc_link/out_doc_date" />
                                                    </xsl:call-template>
                                                </xsl:if>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                <xsl:text>Тип ЭД: </xsl:text>
                <xsl:value-of select="name(*)" />
                            </td>
                            <td id="colRight">
                                <xsl:if test="$root/doc_delivery">
                                    <div>
                                        <xsl:if test="$root/doc_delivery/receiver_name">
                                            <span class="valueTitle">
                                                <xsl:text>Получатель документа: </xsl:text>
                                            </span>
                                            <br />
                                            <span class="sizeData">
                                                <xsl:value-of
                                                    select="normalize-space($root/doc_delivery/receiver_name)" />
                                            </span>
                                            <br />
                                        </xsl:if>
                                        <xsl:if test="$root/doc_delivery/receiver_address">
                                            <xsl:text>Почтовый адрес: </xsl:text>
                                            <br />
                                            <span class="sizeData">
                                                <xsl:value-of
                                                    select="normalize-space($root/doc_delivery/receiver_address)" />
                                            </span>
                                            <br />
                                        </xsl:if>
                                        <xsl:if test="$root/doc_delivery/delivery_place">
                                            <xsl:text>Место выдачи: </xsl:text>
                                            <span class="sizeData">
                                                <xsl:call-template name="delivery_place_et">
                                                    <xsl:with-param name="type"
                                                        select="$root/doc_delivery/delivery_place" />
                                                </xsl:call-template>
                                            </span>
                                            <br />
                                        </xsl:if>
                                        <xsl:text>Способ выдачи: </xsl:text>
                                        <span class="sizeData">
                                            <xsl:call-template name="delivery_method_et">
                                                <xsl:with-param name="type"
                                                    select="$root/doc_delivery/delivery_method" />
                                            </xsl:call-template>
                                        </span>
                                    </div>
                                </xsl:if>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </xsl:when>
            <xsl:otherwise>
                <table id="header">
                    <tbody>
                        <tr>
                            <td id="colLeft">
                                <!-- Номер и дата -->
                                <table id="docNum">
                                    <tbody>
                                        <tr>
                                            <td class="leftColumnDocNumTable">Вх. №: </td>
                                            <td>
                                                <xsl:value-of select="normalize-space($root/doc_num)" />
                                                <xsl:text> от </xsl:text>
                                                <xsl:call-template name="date_or_datetime_t">
                                                    <xsl:with-param name="root" select="$root/doc_date" />
                                                </xsl:call-template>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <!-- Информация о трансфер-агенте -->
                <xsl:choose>
                  <xsl:when test="$root/doc_TA">
                    <table id="docTa">
                      <tbody>
                        <tr>
                          <td colspan="2">
                            <span class="valueTitle">Трансфер-агент: </span>
                          </td>
                        </tr>
                        <tr>
                          <td colspan="2">
                            <span class="sizeData">
                              <xsl:value-of
                                select="normalize-space($root/doc_TA/transfer_agent/ta_info/juridical_name)" />
                            </span>
                          </td>
                        </tr>
                        <tr>
                          <td>
                            № Документа:
                          </td>
                          <td>
                            <xsl:value-of select="normalize-space($root/doc_TA/TA_doc_num)" />
                          </td>
                        </tr>
                        <tr>
                          <td>Дата: </td>
                          <td>
                            <xsl:call-template name="date_or_datetime_t">
                              <xsl:with-param name="root"
                                select="$root/doc_TA/TA_doc_date" />
                            </xsl:call-template>
                          </td>
                        </tr>
                        <tr>
                          <td>Тип ЭД: </td>
                          <td>
                            <xsl:value-of select="name(*)" />
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:text>Тип ЭД: </xsl:text>
                    <xsl:value-of select="name(*)" />
                  </xsl:otherwise>
                </xsl:choose>
                            </td>
                            <td id="colRight">
                                <!-- Информация об отправителе -->
                                <div>
                                    <span class="valueTitle">Отправитель документа: </span>
                                    <xsl:call-template name="sender_type_et">
                                        <xsl:with-param name="type" select="$root/sender/sender_type" />
                                    </xsl:call-template>
                                    <br />
                                    <!-- если это ЮЛ -->
                                    <xsl:if test="$root/sender/person_info/juridical">
                                        <span class="sizeData">
                                            <xsl:value-of
                                                select="normalize-space($root/sender/person_info/juridical/juridical_name)" />
                                        </span>
                                        <br />
                                    </xsl:if>
                                    <!-- если это ФЛ -->
                                    <xsl:if test="$root/sender/person_info/individual">
                                        <span class="sizeData">
                                            <xsl:value-of
                                                select="normalize-space($root/sender/person_info/individual/individual_name)" />
                                        </span>
                                        <br />
                                    </xsl:if>
                                    <!-- если это ОС -->
                                    <xsl:if test="$root/sender/person_info/coowners">
                                        <span class="sizeData">
                                            <xsl:value-of
                                                select="normalize-space($root/sender/person_info/coowners/coowners_name)" />
                                        </span>
                                        <br />
                                    </xsl:if>
                                    <!-- если есть блок почтового адреса - выводим -->
                                    <xsl:if test="$root/sender/post_address">
                                        Почтовый адрес:
                                        <br />
                                        <span class="sizeData">
                                            <xsl:value-of select="normalize-space($root/sender/post_address)" />
                                        </span>
                                        <br />
                                    </xsl:if>
                                    <!-- если есть блок лица подписавшего документ - выводим -->
                                    <xsl:if test="$root/sender/person_sign">
                                        Лицо, подписавшее документ:
                                        <br />
                                        <span class="sizeData">
                                            <xsl:value-of select="normalize-space($root/sender/person_sign)" />
                                        </span>
                                    </xsl:if>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <!-- Вывод данных регистратора -->
    <xsl:template name="registrar_info_t">
        <table class="identTable">
            <tbody>
                <tr>
                    <td class="headerIdentTable sizeData">
                        Регистратор:
                    </td>
                    <td class="sizeData rightColumnIdentTable">
                        <xsl:value-of select="normalize-space(registrar_info/juridical_name)" />
                    </td>
                </tr>
                <tr>
                    <td class="leftColumnIdentTable">
                        Документ:
                    </td>
                    <td class="sizeData rightColumnIdentTable">
                        <xsl:call-template name="juridical_registration_t">
                            <xsl:with-param name="root"
                                select="registrar_info/juridical_registration" />
                        </xsl:call-template>
                    </td>
                </tr>
                <tr>
                    <td class="leftColumnIdentTable">
                        Адрес:
                    </td>
                    <td class="sizeData rightColumnIdentTable">
                        <xsl:if test="registrar_legal_address">
                            <xsl:call-template name="address_legal_t">
                                <xsl:with-param name="root" select="registrar_legal_address" />
                            </xsl:call-template>
                        </xsl:if>
                    </td>
                </tr>
                <tr>
                    <td class="leftColumnIdentTable">
                        Контактная информация:
                    </td>

                    <td class="sizeData rightColumnIdentTable">
                        <xsl:call-template name="contacts_t">
                            <xsl:with-param name="root" select="contacts" />
                        </xsl:call-template>
                    </td>
                </tr>
            </tbody>
        </table>
    </xsl:template>

    <!-- вывод контактных данных -->
    <xsl:template name="contacts_t">
        <xsl:param name="root" />
        <xsl:for-each select="$root/phone_or_fax">
            <xsl:if test="not(position()=1)">
                <xsl:text>, </xsl:text>
            </xsl:if>
            <xsl:if test="phone_type">
                <xsl:call-template name="phone_type_et">
                    <xsl:with-param name="type" select="phone_type" />
                </xsl:call-template>
                <xsl:text> </xsl:text>
            </xsl:if>
            <xsl:value-of select="normalize-space(phone_num)" />
        </xsl:for-each>
        <xsl:if test="$root/phone_or_fax and $root/e_mail">
            <xsl:text>; </xsl:text>
            <br />
        </xsl:if>
        <xsl:for-each select="$root/e_mail">
            <xsl:choose>
                <xsl:when test="position()=1">
                    <xsl:text>e-mail: </xsl:text>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:text>, </xsl:text>
                </xsl:otherwise>
            </xsl:choose>
            <xsl:value-of select="normalize-space(.)" />
        </xsl:for-each>
    </xsl:template>

    <!-- формирование блока эмитент -->
    <xsl:template name="fesIssuer">
        <table class="identTable">
            <tbody>
                <tr>
                    <td class="headerIdentTable sizeData">
                        Эмитент:
                    </td>
                    <td class="sizeData rightColumnIdentTable">
                        <xsl:value-of select="normalize-space(issuer_info/juridical_name)" />
                    </td>
                </tr>
                <tr>
                    <td class="leftColumnIdentTable">
                        Документ:
                    </td>
                    <td class="sizeData rightColumnIdentTable">
                        <xsl:call-template name="juridical_registration_t">
                            <xsl:with-param name="root"
                                select="issuer_info/juridical_registration" />
                        </xsl:call-template>
                    </td>
                </tr>
                <tr>
                    <td class="leftColumnIdentTable">
                        Адрес:
                    </td>
                    <td class="sizeData rightColumnIdentTable">
                        <xsl:if test="issuer_legal_address">
                            <xsl:call-template name="address_legal_t">
                                <xsl:with-param name="root" select="issuer_legal_address" />
                            </xsl:call-template>
                        </xsl:if>
                    </td>
                </tr>
            </tbody>
        </table>
    </xsl:template>

    <!-- форматированный вывод юридичесткого адреса -->
    <xsl:template name="address_legal_t">
        <xsl:param name="root" />
        <xsl:choose>
            <!-- если есть структурированный адрес -->
            <xsl:when test="$root/address_kladr">
                <xsl:call-template name="address_struct_t">
                    <xsl:with-param name="root" select="$root/address_kladr" />
                </xsl:call-template>
            </xsl:when>
            <!-- если есть не стурктурированный адрес -->
            <xsl:when test="$root/address_nostruct">
                <xsl:call-template name="address_nostruct_t">
                    <xsl:with-param name="root" select="$root/address_nostruct" />
                </xsl:call-template>
            </xsl:when>
        </xsl:choose>
    </xsl:template>

    <!-- Выводит тип адреса и ставит после типа "." если такой тип адреса есть -->
    <xsl:template name="dotKind">
        <xsl:param name="root" />

        <xsl:text> </xsl:text>
        <xsl:value-of select="normalize-space($root)" />

        <xsl:choose>
            <xsl:when test="$root='обл'">
                <xsl:text>.</xsl:text>
            </xsl:when>
            <xsl:when test="$root='г'">
                <xsl:text>.</xsl:text>
            </xsl:when>
            <xsl:when test="$root='д'">
                <xsl:text>.</xsl:text>
            </xsl:when>
            <xsl:when test="$root='с'">
                <xsl:text>.</xsl:text>
            </xsl:when>
            <xsl:when test="$root='ул'">
                <xsl:text>.</xsl:text>
            </xsl:when>
            <xsl:when test="$root='наб'">
                <xsl:text>.</xsl:text>
            </xsl:when>
            <xsl:when test="$root='Респ'">
                <xsl:text>.</xsl:text>
            </xsl:when>
            <xsl:when test="$root='п'">
                <xsl:text>.</xsl:text>
            </xsl:when>
            <xsl:when test="$root='пер'">
                <xsl:text>.</xsl:text>
            </xsl:when>
            <xsl:when test="$root='пл'">
                <xsl:text>.</xsl:text>
            </xsl:when>
            <xsl:when test="$root='пр'">
                <xsl:text>.</xsl:text>
            </xsl:when>
            <xsl:when test="$root='ш'">
                <xsl:text>.</xsl:text>
            </xsl:when>
            <xsl:when test="$root='туп'">
                <xsl:text>.</xsl:text>
            </xsl:when>
            <xsl:when test="$root='жилмас'">
                <xsl:text>.</xsl:text>
            </xsl:when>
            <xsl:when test="$root='км'">
                <xsl:text>.</xsl:text>
            </xsl:when>
            <xsl:when test="$root='м'">
                <xsl:text>.</xsl:text>
            </xsl:when>
            <xsl:when test="$root='отд'">
                <xsl:text>.</xsl:text>
            </xsl:when>
            <xsl:when test="$root='платф'">
                <xsl:text>.</xsl:text>
            </xsl:when>
            <xsl:when test="$root='рзд'">
                <xsl:text>.</xsl:text>
            </xsl:when>
            <xsl:when test="$root='свх'">
                <xsl:text>.</xsl:text>
            </xsl:when>
            <xsl:when test="$root='ст'">
                <xsl:text>.</xsl:text>
            </xsl:when>
            <xsl:when test="$root='стр'">
                <xsl:text>.</xsl:text>
            </xsl:when>
            <xsl:when test="$root='тер'">
                <xsl:text>.</xsl:text>
            </xsl:when>
            <xsl:when test="$root='х'">
                <xsl:text>.</xsl:text>
            </xsl:when>
            <xsl:when test="$root='д/интер'">
                <xsl:text>.</xsl:text>
            </xsl:when>
            <xsl:when test="$root='сан'">
                <xsl:text>.</xsl:text>
            </xsl:when>
            <xsl:when test="$root='панс'">
                <xsl:text>.</xsl:text>
            </xsl:when>
            <xsl:when test="$root='проф'">
                <xsl:text>.</xsl:text>
            </xsl:when>
            <xsl:when test="$root='Аобл'">
                <xsl:text>.</xsl:text>
            </xsl:when>
            <xsl:when test="$root='у'">
                <xsl:text>.</xsl:text>
            </xsl:when>
            <xsl:when test="$root='ж/д_казарм'">
                <xsl:text>.</xsl:text>
            </xsl:when>
            <xsl:when test="$root='ж/д_рзд'">
                <xsl:text>.</xsl:text>
            </xsl:when>
            <xsl:when test="$root='ж/д_ст'">
                <xsl:text>.</xsl:text>
            </xsl:when>
        </xsl:choose>
    </xsl:template>

    <!-- Вывод дом/строения/квартиры -->
    <xsl:template name="houseBuildingFlat">
        <xsl:param name="root" />

        <xsl:if test="$root/house">
            <xsl:text> д.</xsl:text>
            <xsl:value-of select="normalize-space($root/house)" />
        </xsl:if>

        <xsl:if test="$root/building">
            <xsl:text> корп.(стр.)</xsl:text>
            <xsl:value-of select="normalize-space($root/building)" />
        </xsl:if>

        <xsl:if test="$root/flat">
            <xsl:text> кв.(оф.)</xsl:text>
            <xsl:value-of select="normalize-space($root/flat)" />
        </xsl:if>

    </xsl:template>

    <!-- Вывод улицы -->
    <xsl:template name="street">
        <xsl:param name="root" />
        <xsl:param name="dot" />

        <xsl:choose>
            <xsl:when test="$root/street">
                <xsl:if test="$dot='Yes'">
                    <xsl:text>, </xsl:text>
                </xsl:if>

                <xsl:call-template name="dotKind">
                    <xsl:with-param name="root" select="$root/street_type" />
                </xsl:call-template>
                <xsl:text> </xsl:text>
                <xsl:value-of select="normalize-space($root/street)" />

                <xsl:call-template name="houseBuildingFlat">
                    <xsl:with-param name="root" select="$root" />
                </xsl:call-template>
            </xsl:when>

            <xsl:otherwise>
                <xsl:call-template name="houseBuildingFlat">
                    <xsl:with-param name="root" select="$root" />
                </xsl:call-template>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <!--вывод населенного пункта -->
    <xsl:template name="place">
        <xsl:param name="root" />
        <xsl:param name="dot" />

        <xsl:choose>
            <xsl:when test="$root/place">
                <xsl:if test="$dot='Yes'">
                    <xsl:text>, </xsl:text>
                </xsl:if>

                <xsl:call-template name="dotKind">
                    <xsl:with-param name="root" select="$root/place_type" />
                </xsl:call-template>
                <xsl:text> </xsl:text>
                <xsl:value-of select="normalize-space($root/place)" />

                <xsl:call-template name="street">
                    <xsl:with-param name="root" select="$root" />
                    <xsl:with-param name="dot" select="'Yes'" />
                </xsl:call-template>
            </xsl:when>

            <xsl:otherwise>
                <xsl:call-template name="street">
                    <xsl:with-param name="root" select="$root" />
                    <xsl:with-param name="dot" select="$dot" />
                </xsl:call-template>
            </xsl:otherwise>
        </xsl:choose>

    </xsl:template>

    <!-- Вывод города -->
    <xsl:template name="city">
        <xsl:param name="root" />
        <xsl:param name="dot" />

        <xsl:choose>
            <xsl:when test="$root/city">
                <xsl:if test="$dot='Yes'">
                    <xsl:text>, </xsl:text>
                </xsl:if>

                <xsl:call-template name="dotKind">
                    <xsl:with-param name="root" select="$root/city_type" />
                </xsl:call-template>
                <xsl:text> </xsl:text>
                <xsl:value-of select="normalize-space($root/city)" />

                <xsl:call-template name="place">
                    <xsl:with-param name="root" select="$root" />
                    <xsl:with-param name="dot" select="'Yes'" />
                </xsl:call-template>

            </xsl:when>

            <xsl:otherwise>
                <xsl:call-template name="place">
                    <xsl:with-param name="root" select="$root" />
                    <xsl:with-param name="dot" select="$dot" />
                </xsl:call-template>
            </xsl:otherwise>

        </xsl:choose>

    </xsl:template>

    <!-- Вывод области -->
    <xsl:template name="zone">
        <xsl:param name="root" />
        <xsl:param name="dot" />

        <xsl:choose>
            <xsl:when test="$root/zone">
                <xsl:if test="$dot='Yes'">
                    <xsl:text>, </xsl:text>
                </xsl:if>

                <xsl:call-template name="dotKind">
                    <xsl:with-param name="root" select="$root/zonet_type" />
                </xsl:call-template>
                <xsl:text> </xsl:text>
                <xsl:value-of select="normalize-space($root/zone)" />

                <xsl:call-template name="city">
                    <xsl:with-param name="root" select="$root" />
                    <xsl:with-param name="dot" select="'Yes'" />
                </xsl:call-template>
            </xsl:when>

            <xsl:otherwise>
                <xsl:call-template name="city">
                    <xsl:with-param name="root" select="$root" />
                    <xsl:with-param name="dot" select="$dot" />
                </xsl:call-template>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <!-- вывод региона -->
    <xsl:template name="region">
        <xsl:param name="root" />
        <xsl:param name="dot" />

        <xsl:choose>
            <xsl:when test="$root/region"> <!--указан регион -->
                <xsl:if test="$dot='Yes'">
                    <xsl:text>, </xsl:text>
                </xsl:if>

                <xsl:value-of select="normalize-space($root/region)" />
                <xsl:text> </xsl:text>
                <xsl:call-template name="dotKind">
                    <xsl:with-param name="root" select="$root/region_type" />
                </xsl:call-template>

                <xsl:call-template name="zone">
                    <xsl:with-param name="root" select="$root" />
                    <xsl:with-param name="dot" select="'Yes'" />
                </xsl:call-template>

            </xsl:when>

            <xsl:when test="not(normalize-space($root/region_number[text()])='')"> <!-- указан регион по справочнику -->
                <xsl:if test="$dot='Yes'">
                    <xsl:text>, </xsl:text>
                </xsl:if>

                <xsl:call-template name="region_et">
                    <xsl:with-param name="type" select="$root/region_number" />
                </xsl:call-template>

                <xsl:call-template name="zone">
                    <xsl:with-param name="root" select="$root" />
                    <xsl:with-param name="dot" select="'Yes'" />
                </xsl:call-template>
            </xsl:when>

            <xsl:otherwise>
                <xsl:call-template name="zone">
                    <xsl:with-param name="root" select="$root" />
                    <xsl:with-param name="dot" select="$dot" />
                </xsl:call-template>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <!-- Вывод индекса структурированного адреса (если надо) -->
    <xsl:template name="index">
        <xsl:param name="root" />
        <xsl:param name="dot" />

        <xsl:choose>
            <xsl:when test="$root/index">
                <xsl:if test="$dot='Yes'">
                    <xsl:text>, </xsl:text>
                </xsl:if>

                <xsl:value-of select="normalize-space($root/index)" />
                <xsl:call-template name="region">
                    <xsl:with-param name="root" select="$root" />
                    <xsl:with-param name="dot" select="'Yes'" />
                </xsl:call-template>
            </xsl:when>
            <xsl:otherwise>
                <xsl:call-template name="region">
                    <xsl:with-param name="root" select="$root" />
                    <xsl:with-param name="dot" select="$dot" />
                </xsl:call-template>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <!-- Формирование структурированного адреса -->
    <xsl:template name="address_struct_t">
        <xsl:param name="root" />

        <xsl:choose>
            <xsl:when test="$root/country = 'RU'">

                <xsl:call-template name="index">
                    <xsl:with-param name="root" select="$root" />
                    <xsl:with-param name="dot" select="'No'" />
                </xsl:call-template>

            </xsl:when>
            <xsl:otherwise>
                <xsl:call-template name="country_et">
                    <xsl:with-param name="type" select="$root/country" />
                </xsl:call-template>

                <xsl:call-template name="index">
                    <xsl:with-param name="root" select="$root" />
                    <xsl:with-param name="dot" select="'Yes'" />
                </xsl:call-template>

            </xsl:otherwise>
        </xsl:choose>

    </xsl:template>

    <!-- Формирование не структурированного адреса -->
    <xsl:template name="address_nostruct_t">
        <xsl:param name="root" />

        <xsl:if
            test="$root/country = not('RU') and not(normalize-space($root/country)='')">
            <xsl:call-template name="country_et">
                <xsl:with-param name="type" select="$root/country" />
            </xsl:call-template>
            <xsl:text>, </xsl:text>
        </xsl:if>

        <xsl:if test="not(normalize-space($root/index)='')">
            <xsl:value-of select="normalize-space($root/index)" />
            <xsl:text>, </xsl:text>
        </xsl:if>

        <xsl:value-of select="normalize-space($root/address)" />
    </xsl:template>

    <!-- форматированный вывод документа ЮЛ -->
    <xsl:template name="juridical_registration_t">
        <xsl:param name="root" />
        <xsl:if test="$root/ogrn">
            <xsl:text>ОГРН </xsl:text>
            <xsl:value-of select="normalize-space($root/ogrn/ogrn)" />
            <xsl:text> выдан </xsl:text>
            <xsl:call-template name="convertDate">
                <xsl:with-param name="date" select="$root/ogrn/ogrn_date" />
            </xsl:call-template>
            <xsl:text>г. </xsl:text>
            <xsl:value-of select="normalize-space($root/ogrn/ogrn_place)" />
            <xsl:if test="$root/reg_doc">
                <xsl:text>;</xsl:text>
                <br />
            </xsl:if>
        </xsl:if>
        <xsl:if test="$root/reg_doc">
            <xsl:if test="not($root/reg_doc/reg_doc_type/juridical_doc_type='NULL')">
                <!-- тип документа -->
                <xsl:call-template name="juridical_doc_type_et">
                    <xsl:with-param name="type"
                        select="$root/reg_doc/reg_doc_type/juridical_doc_type" />
                </xsl:call-template>
                <!-- серия (если есть) -->
                <xsl:if test="$root/reg_doc/reg_series">
                    <xsl:text> </xsl:text>
                    <xsl:value-of select="normalize-space($root/reg_doc/reg_series)" />
                </xsl:if>
                <!-- номер -->
                <xsl:text> № </xsl:text>
                <xsl:value-of select="normalize-space($root/reg_doc/reg_num)" />
                <!--дата регистрации -->
                <xsl:text> выдан </xsl:text>
                <xsl:call-template name="convertDate">
                    <xsl:with-param name="date" select="$root/reg_doc/reg_date" />
                </xsl:call-template>
                <xsl:text>г. </xsl:text>
                <!-- место выдачи -->
                <xsl:if test="$root/reg_doc/reg_place">
                    <xsl:text> </xsl:text>
                    <xsl:value-of select="normalize-space($root/reg_doc/reg_place)" />
                </xsl:if>
            </xsl:if>
        </xsl:if>
    </xsl:template>

    <!-- форматированный вывод документа ФЛ -->
    <xsl:template name="individual_document_t">
        <xsl:param name="root" />

        <xsl:if test="not($root/doc_type/individual_doc_type='00')">
            <!-- тип документа -->
            <xsl:call-template name="individual_document_type_et">
                <xsl:with-param name="type"
                    select="$root/doc_type/individual_doc_type" />
            </xsl:call-template>
            <!-- серия (если есть) -->
            <xsl:if test="$root/doc_series">
                <xsl:text> </xsl:text>
                <xsl:choose>
                    <xsl:when test="$root/doc_type/individual_doc_type='01' or $root/doc_type/individual_doc_type='03'">
                        <xsl:variable name="num" select="substring-before(normalize-space($root/doc_series), '-')" />
                        <xsl:choose>
                            <xsl:when test="string(number($num))='NaN'">
                                <xsl:value-of select="normalize-space($root/doc_series)" />
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:number value="$num"    format="I"/>
                                <xsl:variable name="other"  select="substring-after(normalize-space($root/doc_series), $num)" />
	                            <xsl:value-of select="$other"/>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="normalize-space($root/doc_series)" />
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:if>
            <!-- номер -->
            <xsl:text> № </xsl:text>
            <xsl:value-of select="normalize-space($root/doc_num)" />
            <!--дата регистрации -->
            <xsl:text> выдан </xsl:text>
            <xsl:call-template name="convertDate">
                <xsl:with-param name="date" select="$root/doc_date" />
            </xsl:call-template>
            <xsl:text>г. </xsl:text>
            <!-- место выдачи -->
            <xsl:if test="$root/doc_place">
                <xsl:text> </xsl:text>
                <xsl:value-of select="normalize-space($root/doc_place)" />
            </xsl:if>
        </xsl:if>
    </xsl:template>

    <!-- вывод даты в нужном варианте -->
    <xsl:template name="convertDate">
        <xsl:param name="date" />

        <xsl:choose>
            <xsl:when test="normalize-space($date)=''">
                <xsl:text>00.00.0000</xsl:text>
            </xsl:when>
            <xsl:otherwise>
                <xsl:variable name="year" select="substring-before($date, '-')" />
                <xsl:variable name="month"
                    select="substring-before(substring-after($date, '-'), '-')" />
                <xsl:variable name="day"
                    select="substring-after(substring-after($date, '-'), '-')" />

                <!-- сформировать дату в нужном сформате -->
                <xsl:value-of
                    select="concat(normalize-space($day), '.', normalize-space($month), '.', normalize-space($year))" />
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <!-- дата и время -->
    <xsl:template name="dateTime">
        <xsl:param name="datetime" />
        <xsl:call-template name="convertDate">
            <xsl:with-param name="date"
                select="substring-before($datetime, 'T')" />
        </xsl:call-template>
        <xsl:text> </xsl:text>
        <xsl:value-of select="substring-before(substring-after($datetime, 'T'), 'Z')" />
    </xsl:template>
    <!-- форматированный вывод даты (переворачивает в правильное представление) -->
    <xsl:template name="date_or_datetime_t">
        <xsl:param name="root" />
        <xsl:choose>
            <xsl:when test="$root/date">
                <xsl:call-template name="convertDate">
                    <xsl:with-param name="date" select="$root/date" />
                </xsl:call-template>
            </xsl:when>
            <xsl:otherwise>
                <xsl:call-template name="convertDate">
                    <xsl:with-param name="date"
                        select="substring-before($root/datetime, 'T')" />
                </xsl:call-template>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <!-- вывод данных типа units_or_fraction_t -->
    <xsl:template name="units_or_fraction_t">
        <xsl:param name="root" />
        <xsl:value-of select="normalize-space($root/units)" />
        <xsl:if test="$root/fraction">
            <xsl:text> </xsl:text>
            <xsl:value-of select="normalize-space($root/fraction/numerator)" />
            <xsl:text>/</xsl:text>
            <xsl:value-of select="normalize-space($root/fraction/denominator)" />
        </xsl:if>
    </xsl:template>

    <!-- вывод данных типа currency_and_amount_t -->
    <xsl:template name="currency_and_amount_t">
        <xsl:param name="root" />
        <xsl:value-of select="normalize-space($root/amount)" />
        <xsl:text> (</xsl:text>
        <xsl:value-of select="normalize-space($root/ccy_code)" />
        <xsl:text>)</xsl:text>
    </xsl:template>

    <!-- формирование текста, если это первая строчка, то ставится заголовок, 
        иначе ставится заппятая -->
    <xsl:template name="text_and_dot">
        <xsl:param name="text" />
        <xsl:param name="dot" />
        <xsl:param name="header" />

        <xsl:choose>
            <xsl:when test="$dot='Yes'">
                <xsl:text>,</xsl:text>
                <br />
            </xsl:when>
            <xsl:otherwise>
                <xsl:if test="$header">
                    <xsl:value-of select="normalize-space($header)" />
                    <br />
                </xsl:if>
            </xsl:otherwise>
        </xsl:choose>

        <xsl:value-of select="$text" />
    </xsl:template>

    <!-- вывод остальных данных из описания залога -->
    <xsl:template name="otherDataConditionPledge">
        <xsl:param name="root" />
        <xsl:param name="dot" />
        <xsl:param name="header" />

        <xsl:if test="$root/date">
            <xsl:call-template name="text_and_dot">
                <xsl:with-param name="text"
                    select="'Обращение взыскания на заложенные ЦБ осуществляется во внесудебном порядке'" />
                <xsl:with-param name="dot" select="$dot" />
                <xsl:with-param name="header" select="$header" />
            </xsl:call-template>

            <xsl:if test="not(normalize-space($root/date[text()])='')">
                <xsl:text>, дата обращения взыскания </xsl:text>
                <xsl:call-template name="convertDate">
                    <xsl:with-param name="date" select="$root/date" />
                </xsl:call-template>
            </xsl:if>

            <xsl:if test="$root/term">
                <xsl:text>, срок реализации заложенных ЦБ </xsl:text>
                <xsl:value-of select="normalize-space($root/term)" />
            </xsl:if>

            <xsl:if
                test="$root/doc_statement='Yes' or $root/doc_commission='Yes' or $root/doc_protocol='Yes' or $root/doc_copy='Yes' or $root/doc_sale='Yes'">
                <xsl:text>. Документы, предоставляемые залогодержателем реестродержателю: </xsl:text>
                <ul>
                    <xsl:if test="$root/doc_statement='Yes'">
                        <li class="sizeData">Выписка из реестра сделок организатора торгов,
                            подтверждающая заключение сделки;
                        </li>
                    </xsl:if>
                    <xsl:if test="$root/doc_commission='Yes'">
                        <li class="sizeData">Договор купли-продажи ЦБ, заключенный комиссионером,
                            и договор комиссии между залогодержателем и комиссионером;
                        </li>
                    </xsl:if>
                    <xsl:if test="$root/doc_protocol='Yes'">
                        <li class="sizeData">Протокол несостоявшихся повторных торгов;</li>
                    </xsl:if>
                    <xsl:if test="$root/doc_copy='Yes'">
                        <li class="sizeData">Копии уведомлений залогодержателем залогодателя (о
                            начале обращения взыскания и (или) о приобретении заложенных ЦБ);
                        </li>
                    </xsl:if>
                    <xsl:if test="$root/doc_sale='Yes'">
                        <li class="sizeData">Договор купли-продажи заложенных ЦБ, заключенный по
                            результатам торгов;
                        </li>
                    </xsl:if>
                </ul>
            </xsl:if>
        </xsl:if>
        <xsl:if test="$root/case_num">
             <xsl:text>. ЦБ передаются в залог суду или органу, в производстве которого находится уголовное дело </xsl:text>
             <xsl:text>, Номер уголовного дела </xsl:text>
             <xsl:value-of select="normalize-space($root/case_num)" />
             <xsl:if test="$root/person">
                <xsl:text>, ФИО лица, за которое вносится залог </xsl:text>
                <xsl:value-of select="normalize-space($root/person)" />
             </xsl:if>
        </xsl:if>
        <xsl:if test="$root/info_add">
            <xsl:if test="dot='Yes'">
                <br />
            </xsl:if>
            <xsl:call-template name="text_and_dot">
                <xsl:with-param name="text"
                    select="'Дополнительные условия обременения: '" />
                <xsl:with-param name="dot" select="'Yes'" />
            </xsl:call-template>

            <xsl:value-of select="normalize-space($root/info_add)" />
        </xsl:if>
    </xsl:template>

    <!-- вывод данных о залоге (Залог в обеспечение исполнения обязательств 
        по облигациям) -->
    <xsl:template name="bonds_provision">
        <xsl:param name="root" />
        <xsl:param name="dot" />
        <xsl:param name="header" />

        <xsl:choose>
            <xsl:when test="$root/bonds_provision='Yes'">
                <xsl:call-template name="text_and_dot">
                    <xsl:with-param name="text"
                        select="'Залог в обеспечение исполнения обязательств по облигациям'" />
                    <xsl:with-param name="dot" select="$dot" />
                    <xsl:with-param name="header" select="$header" />
                </xsl:call-template>

                <xsl:text>: </xsl:text>
                <xsl:value-of select="normalize-space($root/bonds_info)" />

                <xsl:call-template name="otherDataConditionPledge">
                    <xsl:with-param name="root" select="$root" />
                    <xsl:with-param name="dot" select="'Yes'" />
                    <xsl:with-param name="header" select="$header" />
                </xsl:call-template>
            </xsl:when>
            <xsl:otherwise>
                <xsl:call-template name="otherDataConditionPledge">
                    <xsl:with-param name="root" select="$root" />
                    <xsl:with-param name="dot" select="$dot" />
                    <xsl:with-param name="header" select="$header" />
                </xsl:call-template>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <!-- вывод данных о залоге (Правом голоса обладает залогодержатель) -->
    <xsl:template name="deleg_vote">
        <xsl:param name="root" />
        <xsl:param name="dot" />
        <xsl:param name="header" />

        <xsl:choose>
            <xsl:when test="$root/deleg_vote='no'">
                <xsl:call-template name="bonds_provision">
                    <xsl:with-param name="root" select="$root" />
                    <xsl:with-param name="dot" select="$dot" />
                    <xsl:with-param name="header" select="$header" />
                </xsl:call-template>
            </xsl:when>
            <xsl:when test="$root/deleg_vote='all'">
                <xsl:call-template name="text_and_dot">
                    <xsl:with-param name="text"
                        select="'Правом голоса обладает залогодержатель'" />
                    <xsl:with-param name="dot" select="$dot" />
                    <xsl:with-param name="header" select="$header" />
                </xsl:call-template>

                <xsl:text> на все количество</xsl:text>

                <xsl:call-template name="takeOfQuantity">
                    <xsl:with-param name="root" select="./*/quantity_vote" />
                </xsl:call-template>

                <xsl:call-template name="bonds_provision">
                    <xsl:with-param name="root" select="$root" />
                    <xsl:with-param name="dot" select="'Yes'" />
                    <xsl:with-param name="header" select="$header" />
                </xsl:call-template>
            </xsl:when>
            <xsl:when test="$root/deleg_vote='part'">
                <xsl:call-template name="text_and_dot">
                    <xsl:with-param name="text"
                        select="'Правом голоса обладает залогодержатель'" />
                    <xsl:with-param name="dot" select="$dot" />
                    <xsl:with-param name="header" select="$header" />
                </xsl:call-template>

                <xsl:text> на </xsl:text>

                <xsl:call-template name="units_or_fraction_t">
                    <xsl:with-param name="root" select="$root/quantity_vote" />
                </xsl:call-template>

                <xsl:text> шт.</xsl:text>

                <xsl:call-template name="takeOfQuantity">
                    <xsl:with-param name="root" select="./*/quantity_vote" />
                </xsl:call-template>

                <xsl:call-template name="bonds_provision">
                    <xsl:with-param name="root" select="$root" />
                    <xsl:with-param name="dot" select="'Yes'" />
                    <xsl:with-param name="header" select="$header" />
                </xsl:call-template>
            </xsl:when>
        </xsl:choose>
    </xsl:template>

    <!-- вывод данных о залоге (Получателем дохода является залогодержатель) -->
    <xsl:template name="deleg_divid">
        <xsl:param name="root" />
        <xsl:param name="dot" />
        <xsl:param name="header" />

        <xsl:choose>
            <xsl:when test="$root/deleg_divid='no'">
                <xsl:call-template name="deleg_vote">
                    <xsl:with-param name="root" select="$root" />
                    <xsl:with-param name="dot" select="$dot" />
                    <xsl:with-param name="header" select="$header" />
                </xsl:call-template>
            </xsl:when>
            <xsl:when test="$root/deleg_divid='all'">
                <xsl:call-template name="text_and_dot">
                    <xsl:with-param name="text"
                        select="'Получателем дохода является залогодержатель'" />
                    <xsl:with-param name="dot" select="$dot" />
                    <xsl:with-param name="header" select="$header" />
                </xsl:call-template>

                <xsl:text> на все количество</xsl:text>

                <xsl:call-template name="takeOfQuantity">
                    <xsl:with-param name="root" select="./*/quantity_divid" />
                </xsl:call-template>

                <xsl:call-template name="deleg_vote">
                    <xsl:with-param name="root" select="$root" />
                    <xsl:with-param name="dot" select="'Yes'" />
                </xsl:call-template>
            </xsl:when>
            <xsl:when test="$root/deleg_divid='part'">
                <xsl:call-template name="text_and_dot">
                    <xsl:with-param name="text"
                        select="'Получателем дохода является залогодержатель'" />
                    <xsl:with-param name="dot" select="$dot" />
                    <xsl:with-param name="header" select="$header" />
                </xsl:call-template>

                <xsl:text> на </xsl:text>

                <xsl:call-template name="units_or_fraction_t">
                    <xsl:with-param name="root" select="$root/quantity_divid" />
                </xsl:call-template>

                <xsl:text> шт.</xsl:text>

                <xsl:call-template name="takeOfQuantity">
                    <xsl:with-param name="root" select="./*/quantity_divid" />
                </xsl:call-template>

                <xsl:call-template name="deleg_vote">
                    <xsl:with-param name="root" select="$root" />
                    <xsl:with-param name="dot" select="'Yes'" />
                    <xsl:with-param name="header" select="$header" />
                </xsl:call-template>
            </xsl:when>
        </xsl:choose>
    </xsl:template>

    <!-- вывод данных о залоге (Залог распространяется на дополнительно зачисляемые 
        на счет залогодателя ЦБ) -->
    <xsl:template name="addition">
        <xsl:param name="root" />
        <xsl:param name="dot" />
        <xsl:param name="header" />

        <xsl:choose>
            <xsl:when test="$root/addition='Yes'">
                <xsl:choose>
                    <xsl:when test="$root/pro_all_quantity"> <!-- НОВАЯ СХЕМА -->
                        <xsl:call-template name="text_and_dot">
                            <xsl:with-param name="text"
                                select="'Залог распространяется на количество ЦБ вида: '" />
                            <xsl:with-param name="dot" select="$dot" />
                            <xsl:with-param name="header" select="$header" />
                        </xsl:call-template>
                        <xsl:call-template name="security_classification_et">
                            <xsl:with-param name="type"
                                select="$root/security_classification" />
                        </xsl:call-template>
                        <xsl:text> </xsl:text>
                        <xsl:if test="$root/security_category">
                            <xsl:call-template name="security_category_et">
                                <xsl:with-param name="type" select="$root/security_category" />
                            </xsl:call-template>
                            <xsl:text> </xsl:text>
                        </xsl:if>
                        <xsl:text> дополнительно зачисляемых на счет залогодателя, пропорциональное</xsl:text>
                        <xsl:choose>
                            <xsl:when test="$root/pro_all_quantity='Yes'">
                                <xsl:text> всем заложенным ЦБ.</xsl:text>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:text> части заложенных ЦБ в количестве </xsl:text>
                                <xsl:call-template name="units_or_fraction_t">
                                    <xsl:with-param name="root" select="$root/quantity_part" />
                                </xsl:call-template>
                                <xsl:text> шт.</xsl:text>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:when>
                    <xsl:otherwise> <!-- СТАРАЯ СХЕМА -->
                        <xsl:call-template name="text_and_dot">
                            <xsl:with-param name="text"
                                select="'Залог распространяется на дополнительно зачисляемые на счет залогодателя ЦБ: '" />
                            <xsl:with-param name="dot" select="$dot" />
                            <xsl:with-param name="header" select="$header" />
                        </xsl:call-template>
                        <xsl:call-template name="security_classification_et">
                            <xsl:with-param name="type"
                                select="$root/security_classification" />
                        </xsl:call-template>

                        <xsl:text> </xsl:text>

                        <xsl:if test="$root/security_category">
                            <xsl:call-template name="security_category_et">
                                <xsl:with-param name="type" select="$root/security_category" />
                            </xsl:call-template>
                            <xsl:text> </xsl:text>
                        </xsl:if>

                        <xsl:text>в количестве </xsl:text>
                        <xsl:call-template name="units_or_fraction_t">
                            <xsl:with-param name="root" select="$root/quantity_addition" />
                        </xsl:call-template>
                        <xsl:text> шт.</xsl:text>
                    </xsl:otherwise>
                </xsl:choose>

                <xsl:call-template name="deleg_divid">
                    <xsl:with-param name="root" select="$root" />
                    <xsl:with-param name="dot" select="'Yes'" />
                    <xsl:with-param name="header" select="$header" />
                </xsl:call-template>
            </xsl:when>
            <xsl:otherwise>
                <xsl:call-template name="deleg_divid">
                    <xsl:with-param name="root" select="$root" />
                    <xsl:with-param name="dot" select="$dot" />
                    <xsl:with-param name="header" select="$header" />
                </xsl:call-template>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <!-- Вывод количество ЦБ для снятия -->
    <xsl:template name="takeOfQuantity">
        <xsl:param name="root" />
        <xsl:if test="$root">
            <xsl:text> Cнять на </xsl:text>
            <xsl:call-template name="units_or_fraction_t">
                <xsl:with-param name="root" select="$root" />
            </xsl:call-template>
            <xsl:text> шт.</xsl:text>
        </xsl:if>
    </xsl:template>

    <!-- вывод данных о залоге (Залог распространяется на ЦБ после конвертации) -->
    <xsl:template name="convert">
        <xsl:param name="root" />
        <xsl:param name="dot" />
        <xsl:param name="header" />

        <xsl:choose>
            <xsl:when test="$root/convert='no'">
                <xsl:call-template name="addition">
                    <xsl:with-param name="root" select="$root" />
                    <xsl:with-param name="dot" select="$dot" />
                    <xsl:with-param name="header" select="$header" />
                </xsl:call-template>
            </xsl:when>
            <xsl:when test="$root/convert='all'">
                <xsl:call-template name="text_and_dot">
                    <xsl:with-param name="text"
                        select="'Залог распространяется на ЦБ после конвертации'" />
                    <xsl:with-param name="dot" select="$dot" />
                    <xsl:with-param name="header" select="$header" />
                </xsl:call-template>

                <xsl:text> на все количество</xsl:text>

                <xsl:call-template name="takeOfQuantity">
                    <xsl:with-param name="root" select="./*/quantity_convert" />
                </xsl:call-template>

                <xsl:call-template name="addition">
                    <xsl:with-param name="root" select="$root" />
                    <xsl:with-param name="dot" select="'Yes'" />
                    <xsl:with-param name="header" select="$header" />
                </xsl:call-template>
            </xsl:when>
            <xsl:when test="$root/convert='part'">
                <xsl:call-template name="text_and_dot">
                    <xsl:with-param name="text"
                        select="'Залог распространяется на ЦБ после конвертации'" />
                    <xsl:with-param name="dot" select="$dot" />
                    <xsl:with-param name="header" select="$header" />
                </xsl:call-template>

                <xsl:text> </xsl:text>

                <xsl:call-template name="units_or_fraction_t">
                    <xsl:with-param name="root" select="$root/quantity_convert" />
                </xsl:call-template>

                <xsl:text> шт.</xsl:text>

                <xsl:call-template name="takeOfQuantity">
                    <xsl:with-param name="root" select="./*/quantity_convert" />
                </xsl:call-template>


                <xsl:call-template name="addition">
                    <xsl:with-param name="root" select="$root" />
                    <xsl:with-param name="dot" select="'Yes'" />
                    <xsl:with-param name="header" select="$header" />
                </xsl:call-template>
            </xsl:when>
        </xsl:choose>
    </xsl:template>

    <!-- вывод данных о залоге (Уступка прав по договору залога без согласия 
        залогодержателя запрещается) -->
    <xsl:template name="concession">
        <xsl:param name="root" />
        <xsl:param name="dot" />
        <xsl:param name="header" />

        <xsl:choose>
            <xsl:when test="$root/concession='Yes'">
                <xsl:call-template name="text_and_dot">
                    <xsl:with-param name="text"
                        select="'Уступка прав по договору залога без согласия залогодержателя запрещается'" />
                    <xsl:with-param name="dot" select="$dot" />
                    <xsl:with-param name="header" select="$header" />
                </xsl:call-template>

                <xsl:call-template name="convert">
                    <xsl:with-param name="root" select="$root" />
                    <xsl:with-param name="dot" select="'Yes'" />
                    <xsl:with-param name="header" select="$header" />
                </xsl:call-template>
            </xsl:when>
            <xsl:otherwise>
                <xsl:call-template name="convert">
                    <xsl:with-param name="root" select="$root" />
                    <xsl:with-param name="dot" select="$dot" />
                    <xsl:with-param name="header" select="$header" />
                </xsl:call-template>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <!-- вывод данных о залоге (Передача заложенных допускается без согласия 
        залогодержателя) -->
    <xsl:template name="transfer">
        <xsl:param name="root" />
        <xsl:param name="dot" />
        <xsl:param name="header" />

        <xsl:choose>
            <xsl:when test="$root/transfer='Yes'">
                <xsl:call-template name="text_and_dot">
                    <xsl:with-param name="text"
                        select="'Передача заложенных допускается без согласия залогодержателя'" />
                    <xsl:with-param name="dot" select="$dot" />
                    <xsl:with-param name="header" select="$header" />
                </xsl:call-template>

                <xsl:call-template name="concession">
                    <xsl:with-param name="root" select="$root" />
                    <xsl:with-param name="dot" select="'Yes'" />
                    <xsl:with-param name="header" select="$header" />
                </xsl:call-template>
            </xsl:when>
            <xsl:otherwise>
                <xsl:call-template name="concession">
                    <xsl:with-param name="root" select="$root" />
                    <xsl:with-param name="dot" select="$dot" />
                    <xsl:with-param name="header" select="$header" />
                </xsl:call-template>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <!-- вывод данных о залоге -->
    <xsl:template name="condition_pledge_t">
        <xsl:param name="root" />
        <xsl:param name="header" />
        <xsl:choose>
            <xsl:when test="$root/no_repledge='Yes'">
                <xsl:call-template name="text_and_dot">
                    <xsl:with-param name="text"
                        select="'Последующий залог запрещается'" />
                    <xsl:with-param name="dot" select="'No'" />
                    <xsl:with-param name="header" select="$header" />
                </xsl:call-template>

                <xsl:call-template name="transfer">
                    <xsl:with-param name="root" select="$root" />
                    <xsl:with-param name="dot" select="'Yes'" />
                    <xsl:with-param name="header" select="$header" />
                </xsl:call-template>
            </xsl:when>

            <xsl:otherwise>
                <xsl:call-template name="transfer">
                    <xsl:with-param name="root" select="$root" />
                    <xsl:with-param name="dot" select="'No'" />
                    <xsl:with-param name="header" select="$header" />
                </xsl:call-template>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <!-- вывод данных о блокировании (Дополнительные условия) -->
    <xsl:template name="info_add">
        <xsl:param name="root" />
        <xsl:param name="dot" />
        <xsl:param name="header" />

        <xsl:if test="$root/info_add">
            <xsl:call-template name="text_and_dot">
                <xsl:with-param name="text" select="'Дополнительные условия: '" />
                <xsl:with-param name="dot" select="$dot" />
                <xsl:with-param name="header" select="$header" />
            </xsl:call-template>

            <xsl:value-of select="normalize-space($root/info_add)" />
        </xsl:if>

    </xsl:template>

    <!-- вывод данных о блокировании (Планируемая дата окончания блокирования) -->
    <xsl:template name="date_end">
        <xsl:param name="root" />
        <xsl:param name="dot" />
        <xsl:param name="header" />

        <xsl:choose>
            <xsl:when test="$root/date_end">
                <xsl:call-template name="text_and_dot">
                    <xsl:with-param name="text"
                        select="'Планируемая дата окончания блокирования '" />
                    <xsl:with-param name="dot" select="$dot" />
                    <xsl:with-param name="header" select="$header" />
                </xsl:call-template>

                <xsl:call-template name="convertDate">
                    <xsl:with-param name="date" select="$root/date_end" />
                </xsl:call-template>

                <xsl:call-template name="info_add">
                    <xsl:with-param name="root" select="$root" />
                    <xsl:with-param name="dot" select="'Yes'" />
                    <xsl:with-param name="header" select="$header" />
                </xsl:call-template>
            </xsl:when>
            <xsl:otherwise>
                <xsl:call-template name="info_add">
                    <xsl:with-param name="root" select="$root" />
                    <xsl:with-param name="dot" select="$dot" />
                    <xsl:with-param name="header" select="$header" />
                </xsl:call-template>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <!-- вывод данных о блокировании (Блокируются голоса) -->
    <xsl:template name="block_vote">
        <xsl:param name="root" />
        <xsl:param name="dot" />
        <xsl:param name="header" />

        <xsl:choose>
            <xsl:when test="$root/block_vote='Yes'">
                <xsl:call-template name="text_and_dot">
                    <xsl:with-param name="text" select="'Блокируются голоса'" />
                    <xsl:with-param name="dot" select="$dot" />
                    <xsl:with-param name="header" select="$header" />
                </xsl:call-template>

                <xsl:call-template name="date_end">
                    <xsl:with-param name="root" select="$root" />
                    <xsl:with-param name="dot" select="'Yes'" />
                    <xsl:with-param name="header" select="$header" />
                </xsl:call-template>
            </xsl:when>
            <xsl:otherwise>
                <xsl:call-template name="date_end">
                    <xsl:with-param name="root" select="$root" />
                    <xsl:with-param name="dot" select="$dot" />
                    <xsl:with-param name="header" select="$header" />
                </xsl:call-template>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <!-- вывод данных о блокировании -->
    <xsl:template name="block_info_t">
        <xsl:param name="root" />
        <xsl:param name="header" />

        <xsl:choose>
            <xsl:when test="$root/block_date">
                <xsl:if test="$header">
                    <xsl:value-of select="normalize-space($header)" />
                    <br />
                </xsl:if>
                <xsl:text>Дата установки блокирования ЦБ </xsl:text>
                <xsl:call-template name="convertDate">
                    <xsl:with-param name="date" select="$root/block_date" />
                </xsl:call-template>

                <xsl:text>Основание для блокирования ЦБ </xsl:text>
                <xsl:call-template name="block_reason_et">
                    <xsl:with-param name="type" select="$root/block_reason" />
                </xsl:call-template>
                
                <xsl:call-template name="condition_block_t">
                    <xsl:with-param name="root" select="$root/condition_block" />
                    <xsl:with-param name="header" select="$header" />
                    <xsl:with-param name="dot" select="'Yes'" />
                </xsl:call-template>

            </xsl:when>
            <xsl:otherwise>
                <xsl:call-template name="condition_block_t">
                    <xsl:with-param name="root" select="$root/condition_block" />
                    <xsl:with-param name="header" select="$header" />
                    <xsl:with-param name="dot" select="'No'" />
                </xsl:call-template>
                
                <xsl:text>Основание для блокирования ЦБ </xsl:text>
                <xsl:call-template name="block_reason_et">
                    <xsl:with-param name="type" select="$root/block_reason" />
                </xsl:call-template>
            </xsl:otherwise>
        </xsl:choose>

    </xsl:template>

    <!-- Условия блокирования -->
    <xsl:template name="condition_block_t">
        <xsl:param name="root" />
        <xsl:param name="header" />
        <xsl:param name="dot" />

        <xsl:choose>
            <xsl:when test="$root/block_divid='Yes'">
                <xsl:call-template name="text_and_dot">
                    <xsl:with-param name="text"
                        select="'Блокируются права на получение дивидендов'" />
                    <xsl:with-param name="dot" select="$dot" />
                    <xsl:with-param name="header" select="$header" />
                </xsl:call-template>

                <xsl:call-template name="block_vote">
                    <xsl:with-param name="root" select="$root" />
                    <xsl:with-param name="dot" select="'Yes'" />
                    <xsl:with-param name="header" select="$header" />
                </xsl:call-template>
            </xsl:when>

            <xsl:otherwise>
                <xsl:call-template name="block_vote">
                    <xsl:with-param name="root" select="$root" />
                    <xsl:with-param name="dot" select="$dot" />
                    <xsl:with-param name="header" select="$header" />
                </xsl:call-template>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template name="du_conditions_t">
        <xsl:param name="root" />
        <xsl:if test="$root/contract_num">
            <xsl:text>Номер договора ДУ: </xsl:text>
            <xsl:value-of select="$root/contract_num"/>
            <br />
        </xsl:if>
        <xsl:if test="$root/contract_date">
            <xsl:text>Дата договора ДУ: </xsl:text>
            <xsl:call-template name="convertDate">
                <xsl:with-param name="date" select="$root/contract_date"/>
            </xsl:call-template>
            <br />
        </xsl:if>
        <xsl:text>ДУ не имеет права распоряжаться ценными бумагами: </xsl:text>
        <xsl:choose>
            <xsl:when test="$root/right_du_nodispose='Yes'">
                <xsl:text>Да</xsl:text>
            </xsl:when>
            <xsl:otherwise>
                <xsl:text>Нет</xsl:text>
            </xsl:otherwise>
        </xsl:choose>
        <br />
        <xsl:text>Право голоса принадлежит учредителю: </xsl:text>
        <xsl:choose>
            <xsl:when test="$root/right_founder_voting='Yes'">
                <xsl:text>Да</xsl:text>
            </xsl:when>
            <xsl:otherwise>
                <xsl:text>Нет</xsl:text>
            </xsl:otherwise>
        </xsl:choose>
        <br />
        <xsl:text>Право получения дохода принадлежит учредителю: </xsl:text>
        <xsl:choose>
            <xsl:when test="$root/right_founder_divid='Yes'">
                <xsl:text>Да</xsl:text>
            </xsl:when>
            <xsl:otherwise>
                <xsl:text>Нет</xsl:text>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    <!-- Анкета ЗЛ -->
    <xsl:template name="content_form_of_shareholders_t">
        <xsl:param name="root" />

        <xsl:call-template name="account_type_et">
            <xsl:with-param name="type" select="$root/account_type" />
        </xsl:call-template>
        <br />

        <xsl:call-template name="shareholder_info_simple">
            <xsl:with-param name="root" select="$root/shareholder_info" />
        </xsl:call-template>

        <xsl:if test="count($root/contacts/*)>0">
            <br/>
            <xsl:text>Контактная информация: </xsl:text>
            <xsl:call-template name="contacts_t">
                <xsl:with-param name="root" select="$root/contacts" />
            </xsl:call-template>
            <br />
        </xsl:if>

        <xsl:if test="$root/bank_details">
            <xsl:text>Банковские реквизиты: </xsl:text>
            <xsl:call-template name="bankProfile_simple">
                <xsl:with-param name="bank_details" select="$root/bank_details" />
            </xsl:call-template>
            <br />
        </xsl:if>

        <xsl:if test="$root/tax_status">
            <xsl:call-template name="tax_statusProfile_simple">
                <xsl:with-param name="tax_status" select="$root/tax_status" />
            </xsl:call-template>
            <br />
        </xsl:if>

        <xsl:text>Форма выплаты доходов по ценным бумагам: </xsl:text>
        <xsl:call-template name="distr_div_frm_et">
            <xsl:with-param name="type" select="$root/distr_div_frm" />
        </xsl:call-template>
        <br />

        <xsl:text>Способ доставки корреспонденции: </xsl:text>
        <xsl:call-template name="letter_go_type_et">
            <xsl:with-param name="type" select="$root/letter_go_type" />
        </xsl:call-template>
        <br />
        
        <xsl:if test="$root/doc_by_post">
          <xsl:text>Допустимо предоставление документов регистратору почтовым отправлением</xsl:text>
          <br/>
        </xsl:if>

        <xsl:if test="$root/comment">
            <xsl:text>Комментарий к анкете: </xsl:text>
            <xsl:value-of select="normalize-space($root/comment)" />
        </xsl:if>
        
    </xsl:template>

    <xsl:template name="content_form_of_coowner_t">
        <xsl:param name="root" />

        <xsl:if test="$root/coowner_numb">
            <xsl:text>Номер совладельца по счету ОС: </xsl:text>
            <xsl:value-of select="$root/coowner_numb" />
            <br />
        </xsl:if>
        <xsl:call-template name="shareholder_info_simple">
            <xsl:with-param name="root" select="$root/coowner_info" />
        </xsl:call-template>
        <br />
        <xsl:if test="count($root/contacts/*)>0">
            <xsl:text>Контактная информация: </xsl:text>
            <xsl:call-template name="contacts_t">
                <xsl:with-param name="root" select="$root/contacts" />
            </xsl:call-template>
            <br />
        </xsl:if>

        <xsl:if test="$root/bank_details">
            <xsl:text>Банковские реквизиты: </xsl:text>
            <xsl:call-template name="bankProfile_simple">
                <xsl:with-param name="bank_details" select="$root/bank_details" />
            </xsl:call-template>
            <br />
        </xsl:if>

        <xsl:if test="$root/tax_status">
            <xsl:call-template name="tax_statusProfile_simple">
                <xsl:with-param name="tax_status" select="$root/tax_status" />
            </xsl:call-template>
            <br />
        </xsl:if>

        <xsl:text>Форма выплаты доходов по ценным бумагам: </xsl:text>
        <xsl:call-template name="distr_div_frm_et">
            <xsl:with-param name="type" select="$root/distr_div_frm" />
        </xsl:call-template>
        <br />

        <xsl:text>Способ доставки корреспонденции: </xsl:text>
        <xsl:call-template name="letter_go_type_et">
            <xsl:with-param name="type" select="$root/letter_go_type" />
        </xsl:call-template>
        <br />

        <xsl:if test="$root/doc_by_post">
          <xsl:text>Допустимо предоставление документов регистратору почтовым отправлением</xsl:text>
          <br />
        </xsl:if>
        
        <xsl:if test="$root/comment">
            <xsl:text>Комментарий к анкете: </xsl:text>
            <xsl:value-of select="normalize-space($root/comment)" />
        </xsl:if>
    </xsl:template>

    <xsl:template name="content_form_of_authorised_t">
        <xsl:param name="root" />

        <xsl:call-template name="shareholder_info_simple">
            <xsl:with-param name="root" select="$root/authorised_info" />
        </xsl:call-template>
        <br />
        <xsl:if test="count($root/contacts/*)>0">
            <xsl:text>Контактная информация: </xsl:text>
            <xsl:call-template name="contacts_t">
                <xsl:with-param name="root" select="$root/contacts" />
            </xsl:call-template>
            <br />
        </xsl:if>

        <xsl:text>Способ доставки корреспонденции: </xsl:text>
        <xsl:call-template name="letter_go_type_et">
            <xsl:with-param name="type" select="$root/letter_go_type" />
        </xsl:call-template>
        <br />

        <xsl:if test="$root/comment">
            <xsl:text>Комментарий к анкете: </xsl:text>
            <xsl:value-of select="normalize-space($root/comment)" />
        </xsl:if>
    </xsl:template>

    <xsl:template name="tax_statusProfile_simple">
        <xsl:param name="tax_status" />
        <xsl:text>Категория налогоплательщика: </xsl:text>
        <xsl:call-template name="tax_status_et">
            <xsl:with-param name="type" select="$tax_status/tax_status_code" />
        </xsl:call-template>

        <xsl:if test="$tax_status/nonresident_type">
            <xsl:text>, </xsl:text>
            <xsl:call-template name="nonresident_type_et">
                <xsl:with-param name="type" select="$tax_status/nonresident_type" />
            </xsl:call-template>
        </xsl:if>

        <xsl:if test="$tax_status/tax_exempt_indicator='Yes'">
            <xsl:text>, имеется налоговая льгота</xsl:text>
        </xsl:if>

        <xsl:if test="$tax_status/narrative">
            <xsl:text>, </xsl:text>
            <xsl:value-of select="normalize-space($tax_status/narrative)" />
        </xsl:if>
    </xsl:template>
    <xsl:template name="bankProfile_simple">
        <xsl:param name="bank_details" />
        <xsl:choose>
            <xsl:when test="$bank_details/bank_nostruct">
                <xsl:value-of select="normalize-space($bank_details/bank_nostruct)" />
            </xsl:when>
            <xsl:when test="$bank_details/bank_struct">
                <xsl:variable name="bank_struct" select="$bank_details/bank_struct" />
                <xsl:variable name="cash_rub_dtls" select="$bank_struct/cash_rub_dtls" />

                <xsl:if test="$cash_rub_dtls/account">
                    <xsl:text>Лицевой/расчетный счета лица </xsl:text>
                    <xsl:value-of select="normalize-space($cash_rub_dtls/account)" />
                </xsl:if>

                <xsl:if test="$cash_rub_dtls/account_person">
                    <xsl:text>Лицевой/расчетный счета лица </xsl:text>
                    <xsl:value-of select="normalize-space($cash_rub_dtls/account_person)" />
                    <xsl:text>, </xsl:text>
                    <xsl:if test="$cash_rub_dtls/bank_branch_name">
                        <xsl:text>Наименование филиала (отделения) банка: </xsl:text>
                        <xsl:value-of select="normalize-space($cash_rub_dtls/bank_branch_name)" />
                        <xsl:text>, </xsl:text>
                    </xsl:if>
                    <xsl:text>расчетный счет филиала (отделения) банка </xsl:text>
                    <xsl:value-of select="normalize-space($cash_rub_dtls/account_branch)" />
                </xsl:if>

                <xsl:text>, наименование банка: </xsl:text>
                <xsl:value-of select="normalize-space($cash_rub_dtls/bank_name)" />

                <xsl:text>, город банка: </xsl:text>
                <xsl:value-of select="normalize-space($cash_rub_dtls/bank_city)" />

                <xsl:text>, БИК банка: </xsl:text>
                <xsl:value-of select="normalize-space($cash_rub_dtls/ruic)" />

                <xsl:text>, корреспондентский счет банка: </xsl:text>
                <xsl:value-of select="normalize-space($cash_rub_dtls/bank_corr)" />

                <xsl:if test="$cash_rub_dtls/bank_inn">
                    <xsl:text>, ИНН банка: </xsl:text>
                    <xsl:value-of select="normalize-space($cash_rub_dtls/bank_inn)" />
                </xsl:if>

                <xsl:if test="$bank_struct/pay_add_info">
                    <xsl:text>, Дополнительные реквизиты платежа: </xsl:text>
                    <xsl:value-of select="normalize-space($bank_struct/pay_add_info)" />
                </xsl:if>

                <xsl:if test="$bank_struct/pay_name">
                    <xsl:text>, Наименование плательщика/ получателя: </xsl:text>
                    <xsl:value-of select="normalize-space($bank_struct/pay_name)" />
                </xsl:if>

                <xsl:if test="$bank_struct/inn">
                    <xsl:text>, код ИНН плательщика/ получателя: </xsl:text>
                    <xsl:value-of select="normalize-space($bank_struct/inn)" />
                </xsl:if>
            </xsl:when>
        </xsl:choose>
    </xsl:template>
    <xsl:template name="shareholder_info_simple">
        <xsl:param name="root" />
        <xsl:if test="$root/juridical">
            <xsl:value-of select="normalize-space($root/juridical/juridical_name)" />
            <xsl:if test="$root/juridical/juridical_short_name">
                <xsl:text>,</xsl:text>
                <br />
                <xsl:value-of
                    select="normalize-space($root/juridical/juridical_short_name)" />
            </xsl:if>
            <xsl:text>,</xsl:text>
            <br />
            <xsl:text>Документ: </xsl:text>
            <xsl:call-template name="juridical_registration_t">
                <xsl:with-param name="root"
                    select="$root/juridical/juridical_registration" />
            </xsl:call-template>
            <xsl:if test="$root/juridical/juridical_post_address">
                <xsl:text>,</xsl:text>
                <br />
                <xsl:text>Адрес места нахождения: </xsl:text>
                <xsl:call-template name="address_legal_t">
                    <xsl:with-param name="root"
                        select="$root/juridical/juridical_legal_address" />
                </xsl:call-template>
            </xsl:if>
            <xsl:if test="$root/juridical/juridical_legal_address">
                <xsl:text>,</xsl:text>
                <br />
                <xsl:text>Почтовый адрес: </xsl:text>
                <xsl:call-template name="address_legal_t">
                    <xsl:with-param name="root"
                        select="$root/juridical/juridical_post_address" />
                </xsl:call-template>
            </xsl:if>
            <xsl:if test="$root/juridical/juridical_post_address/another">
                <xsl:text>,</xsl:text>
                <br />
                <xsl:text>Получатель корреспонденции: </xsl:text>
                <xsl:value-of
                    select="normalize-space($root/juridical/juridical_post_address/another)" />
            </xsl:if>

            <xsl:if test="$root/juridical/registration_country">
                <xsl:text>,</xsl:text>
                <br />
                <xsl:text>Страна регистрации: </xsl:text>
                <xsl:call-template name="country_et">
                    <xsl:with-param name="type"
                        select="$root/juridical/registration_country" />
                </xsl:call-template>
            </xsl:if>

            <xsl:if test="$root/juridical/special">
                <xsl:text>,</xsl:text>
                <br />
                <xsl:text>Особый вид ЮЛ: </xsl:text>
                <xsl:call-template name="juridical_special_et">
                    <xsl:with-param name="type" select="$root/juridical/special" />
                </xsl:call-template>
            </xsl:if>

            <xsl:if test="$root/juridical/inn">
                <xsl:text>,</xsl:text>
                <br />
                <xsl:text>ИНН: </xsl:text>
                <xsl:value-of select="normalize-space($root/juridical/inn)" />
            </xsl:if>

            <xsl:if test="$root/juridical/kpp">
                <xsl:text>,</xsl:text>
                <br />
                <xsl:text>КПП: </xsl:text>
                <xsl:value-of select="normalize-space($root/juridical/kpp)" />
            </xsl:if>

            <xsl:if test="$root/juridical/okpo">
                <xsl:text>,</xsl:text>
                <br />
                <xsl:text>ОКПО: </xsl:text>
                <xsl:value-of select="normalize-space($root/juridical/okpo)" />
            </xsl:if>

            <xsl:if test="$root/juridical/okved">
                <xsl:text>,</xsl:text>
                <br />
                <xsl:text>ОКВЭД: </xsl:text>
                <xsl:value-of select="normalize-space($root/juridical/okved)" />
            </xsl:if>

            <xsl:if test="$root/juridical/opf">
                <xsl:text>,</xsl:text>
                <br />
                <xsl:text>ОПФ: </xsl:text>
                <xsl:call-template name="opf_et">
                    <xsl:with-param name="type" select="$root/juridical/opf/opf_type" />
                </xsl:call-template>
            </xsl:if>
            
            <xsl:if test="$root/juridical/licence">
                <xsl:text>,</xsl:text>
                <br />
                <xsl:text>Данные лицензии для счета НД, ДУ: </xsl:text>
                <xsl:call-template name="licence_t">
                    <xsl:with-param name="root" select="$root/juridical/licence" />
                </xsl:call-template>
            </xsl:if>
            
        </xsl:if>
        <xsl:if test="$root/individual">
            <xsl:value-of select="normalize-space($root/individual/individual_name)" />
            <xsl:if test="$root/individulal/individual_short_name">
                <xsl:text>,</xsl:text>
                <br />
                <xsl:value-of
                    select="normalize-space($root/individual/individual_short_name)" />
            </xsl:if>
            <xsl:text>,</xsl:text>
            <br />
            <xsl:text>Документ: </xsl:text>
            <xsl:call-template name="accountDocument">
                <xsl:with-param name="person_info" select="$root/individual/.." />
            </xsl:call-template>
            <xsl:if test="$root/individual/birthtday">
                <xsl:text>,</xsl:text>
                <br />
                <xsl:text>Дата рождения: </xsl:text>
                <xsl:call-template name="convertDate">
                    <xsl:with-param name="date" select="$root/individual/birthtday" />
                </xsl:call-template>
            </xsl:if>
            <xsl:if test="$root/individual/place_of_birth">
                <xsl:text>,</xsl:text>
                <br />
                <xsl:text>Место рождения: </xsl:text>
                <xsl:value-of select="normalize-space($root/individual/place_of_birth)" />
            </xsl:if>

            <xsl:if test="$root/individual/nationality">
                <xsl:text>,</xsl:text>
                <br />
                <xsl:text>Гражданство: </xsl:text>
                <xsl:call-template name="country_et">
                    <xsl:with-param name="type" select="$root/individual/nationality" />
                </xsl:call-template>
            </xsl:if>


            <xsl:if test="count($root/individual/individual_legal_address/*)>0">
                <xsl:text>,</xsl:text>
                <br />
                <xsl:text>Место регистрации: </xsl:text>
                <xsl:call-template name="address_legal_t">
                    <xsl:with-param name="root"
                        select="$root/individual/individual_legal_address" />
                </xsl:call-template>
            </xsl:if>

            <xsl:if test="count($root/individual/individual_post_address/*)>0">
                <xsl:text>,</xsl:text>
                <br />
                <xsl:text>Фактическое место жительства: </xsl:text>
                <xsl:call-template name="address_legal_t">
                    <xsl:with-param name="root"
                        select="$root/individual/individual_post_address" />
                </xsl:call-template>

                <xsl:if test="$root/individual/individual_post_address/another">
                    <xsl:text>,</xsl:text>
                    <br />
                    <xsl:text>Получатель корреспонденции: </xsl:text>
                    <xsl:value-of
                        select="normalize-space($root/individual/individual_post_address/another)" />
                </xsl:if>
            </xsl:if>

            <xsl:if test="$root/individual/inn">
                <xsl:text>,</xsl:text>
                <br />
                <xsl:text>ИНН: </xsl:text>
                <xsl:value-of select="normalize-space($root/individual/inn)" />
            </xsl:if>

            <xsl:if test="$root/individual/additional_document">
                <xsl:text>,</xsl:text>
                <br />
                <xsl:text>Тип предпринимателя (ИП, КФХ, нотариус): </xsl:text>
                <xsl:call-template name="indiv_business_type_et">
                    <xsl:with-param name="type"
                        select="$root/individual/additional_document/indiv_business_type" />
                </xsl:call-template>
                <xsl:text>,</xsl:text>
                <br />
                <xsl:if test="$root/individual/additional_document/ogrn_indiv"> 
                    <xsl:text>Информация о регистрации индивидуального предпринимателя (фермерского хозяйства): </xsl:text>
                    <xsl:text>ОГРН </xsl:text>
                    <xsl:value-of
                        select="normalize-space($root/individual/additional_document/ogrn_indiv/ogrn)" />
                    <xsl:text> выдан </xsl:text>
                    <xsl:call-template name="convertDate">
                        <xsl:with-param name="date"
                            select="$root/individual/additional_document/ogrn_indiv/ogrn_date" />
                    </xsl:call-template>
                    <xsl:text>г. </xsl:text>
                    <xsl:value-of
                        select="normalize-space($root/individual/additional_document/ogrn_indiv/ogrn_place)" />
                </xsl:if>
                <xsl:if test="$root/individual/additional_document/doc_notary">
                    <br />
                    <xsl:text>Документы нотариуса: </xsl:text>
                    <xsl:call-template name="doc_notary">
                        <xsl:with-param name="root" select="$root/individual/additional_document/doc_notary" />
                    </xsl:call-template>
                </xsl:if> 
            </xsl:if>

            <xsl:if
                test="$root/individual/foreign_public and ($root/individual/foreign_public/official='Yes' or $root/individual/foreign_public/relative='Yes')">
                <xsl:text>,</xsl:text>
                <br />
                <xsl:text>Принадлежность к иностранному публичному должностному лицу: </xsl:text>
                <xsl:if test="$root/individual/foreign_public/official='Yes'">
                    <xsl:text>Является иностранным публичным должностным лицом</xsl:text>
                    <xsl:if test="$root/individual/foreign_public/relative='Yes'">
                        <br />
                    </xsl:if>
                </xsl:if>

                <xsl:if test="$root/individual/foreign_public/relative='Yes'">
                    <xsl:text>Является супругом, близким родственником иностранного публичного должностного лица</xsl:text>
                </xsl:if>

                <xsl:if test="$root/individual/foreign_public/comment">
                    <xsl:text>,</xsl:text>
                    <br />
                    <xsl:value-of
                        select="normalize-space($root/individual/foreign_public/comment)" />
                </xsl:if>
            </xsl:if>

            <xsl:if test="$root/individual/owner_status">
                <xsl:text>,</xsl:text>
                <br />
                <xsl:text>Статус ЗЛ: </xsl:text>
                <xsl:call-template name="owner_status_et">
                    <xsl:with-param name="type" select="$root/individual/owner_status" />
                </xsl:call-template>
            </xsl:if>
        </xsl:if>
        <xsl:if test="$root/coowners">
            <xsl:text>Вид собственности: </xsl:text>
            <xsl:call-template name="coowners_type_et">
                <xsl:with-param name="type" select="$root/coowners/owners_type" />
            </xsl:call-template>
            <br />
            <xsl:value-of select="normalize-space($root/coowners/coowners_name)" />
            <br />
            <xsl:value-of select="normalize-space($root/coowners/coowners_short_name)" />
            <br />
            <xsl:text>Фактическое место жительства: </xsl:text>
            <xsl:call-template name="address_legal_t">
                <xsl:with-param name="root"
                    select="$root/coowners/coowners_post_address" />
            </xsl:call-template>
            <br />
            <xsl:if test="$root/coowners/coowners_post_address/another">
                <xsl:text>Получатель корреспонденции: </xsl:text>
                <xsl:value-of
                    select="normalize-space($root/coowners/coowners_post_address/another)" />
                <br />
            </xsl:if>
        </xsl:if>
    </xsl:template>
    
    <!-- Описание данных операции -->
    <xsl:template name="operation_t">
        <xsl:param name="operation" />
        <xsl:param name="accountIdent"/>
        <tr>
            <td class="headerIdentTable sizeData">
                <xsl:text>Описание операции: </xsl:text>
            </td>
            <td class="sizeData rightColumnIdentTable">
    
            </td>
        </tr>
        <tr>
            <td class="leftColumnIdentTable">
                <xsl:text>№ операции в рег. журнале: </xsl:text>
            </td>
            <td class="sizeData rightColumnIdentTable">
                <xsl:call-template name="numberOperation">
                    <xsl:with-param name="number"
                        select="$operation/processing_info/operation_numb" />
                </xsl:call-template>
            </td>
        </tr>
        <tr>
            <td class="leftColumnIdentTable">
                <xsl:text>Дата: </xsl:text>
            </td>
            <td class="sizeData rightColumnIdentTable">
                <xsl:call-template name="dateOperation">
                    <xsl:with-param name="date"
                        select="$operation/processing_info/operation_date" />
                </xsl:call-template>
            </td>
        </tr>
        <tr>
            <td class="leftColumnIdentTable">
                <xsl:text>Тип операции: </xsl:text>
            </td>
            <td class="sizeData rightColumnIdentTable">
                <xsl:call-template name="typeOperation">
                    <xsl:with-param name="type"
                        select="$operation/processing_info/operation_name" />
                    <xsl:with-param name="transaction" select="operation/transaction" />
                    <xsl:with-param name="account_ident" select="$accountIdent" />
                    <xsl:with-param name="account_deliv"
                        select="$operation/transaction/account_deliv" />
                    <xsl:with-param name="account_receiv"
                        select="$operation/transaction/account_receiv" />
                </xsl:call-template>
            </td>
        </tr>
        <tr>
            <td class="leftColumnIdentTable">
                <xsl:text>Документ-основание: </xsl:text>
            </td>
            <td class="sizeData rightColumnIdentTable">
                <xsl:call-template name="baseDocOperation">
                    <xsl:with-param name="doc_name" select="operation/base_doc/doc_name" />
                    <xsl:with-param name="out_doc_num"
                        select="$operation/base_doc/doc_info/out_doc_num" />
                    <xsl:with-param name="out_doc_date"
                        select="$operation/base_doc/doc_info/out_doc_date" />
                    <xsl:with-param name="in_doc_num"
                        select="$operation/base_doc/doc_info/in_doc_num" />
                    <xsl:with-param name="in_reg_date"
                        select="$operation/base_doc/doc_info/in_reg_date" />
                </xsl:call-template>
            </td>
        </tr>
        <!-- Для отмены блокирования, отмены залога, отмены неоплаты, pledge_deliver -->
        <xsl:if
            test="$operation/unblock or $operation/unpledge or $operation/unpaid_cancel or $operation/pledge_deliver">
            <tr>
                <td class="leftColumnIdentTable">
                    <xsl:text>Количество ЦБ в операции: </xsl:text>
                </td>
    
                <td class="sizeData rightColumnIdentTable">
                    <xsl:choose>
                        <xsl:when test="$operation/unblock">
                            <xsl:call-template name="securityQuantity">
                                <xsl:with-param name="quantity"
                                    select="$operation/unblock/quantity_unblock" />
                            </xsl:call-template>
                        </xsl:when>
                        <xsl:when test="$operation/unpledge">
                            <xsl:call-template name="securityQuantity">
                                <xsl:with-param name="quantity"
                                    select="$operation/unpledge/quantity_unpledge" />
                            </xsl:call-template>
                        </xsl:when>
                        <xsl:when test="$operation/unpaid_cancel">
                            <xsl:call-template name="securityQuantity">
                                <xsl:with-param name="quantity"
                                    select="$operation/unpaid_cancel/quantity_cancel" />
                            </xsl:call-template>
                        </xsl:when>
                        <xsl:when test="$operation/pledge_deliver">
                            <xsl:call-template name="securityQuantity">
                                <xsl:with-param name="quantity"
                                    select="$operation/pledge_deliver/quantity_deliver" />
                            </xsl:call-template>
                        </xsl:when>
                    </xsl:choose>
                    <xsl:text> шт. </xsl:text>
                </td>
            </tr>
            <tr>
                <td class="sizeData leftColumnIdentTable">
                    <xsl:text>Описание обременения: </xsl:text>
                </td>
                <td class="sizeData rightColumnIdentTable" />
            </tr>
        </xsl:if>
        <!-- Если есть в описании операции данные о ЦБ - вывести -->
        <xsl:if test="$operation/*/*/security">
            <tr>
                <td class="leftColumnIdentTable">
                    <xsl:text>Ценная бумага: </xsl:text>
                </td>
                <td class="sizeData rightColumnIdentTable">
                    <xsl:call-template name="securityData">
                        <xsl:with-param name="security" select="$operation/*/*/security" />
                    </xsl:call-template>
                </td>
            </tr>
            <tr>
                <td class="leftColumnIdentTable">
                    <xsl:text>Регистрационный № ЦБ: </xsl:text>
                </td>
                <td class="sizeData rightColumnIdentTable">
                    <xsl:call-template name="securityRegNumber">
                        <xsl:with-param name="security" select="$operation/*/*/security" />
                    </xsl:call-template>
                </td>
            </tr>
        </xsl:if>
        <xsl:if test="$operation/*/*/quantity">
            <tr>
                <td class="leftColumnIdentTable">
                    <xsl:text>Количество ЦБ: </xsl:text>
                </td>
                <td class="sizeData rightColumnIdentTable">
                    <xsl:call-template name="securityQuantity">
                        <xsl:with-param name="quantity" select="$operation/*/*/quantity" />
                    </xsl:call-template>
                    <xsl:text> шт. </xsl:text>
                </td>
            </tr>
        </xsl:if>
        <xsl:if test="$operation/*/*/settlement_amount">
            <tr>
                <td class="leftColumnIdentTable">
                    <xsl:text>Сумма: </xsl:text>
                </td>
                <td class="sizeData rightColumnIdentTable">
                    <xsl:call-template name="securitySettlementAmount">
                        <xsl:with-param name="settlement_amount"
                            select="$operation/*/*/settlement_amount" />
                    </xsl:call-template>
                </td>
            </tr>
        </xsl:if>
        <!-- контрагенты -->
        <xsl:if
            test="$operation/transaction or $operation/pledge or $operation/unpledge or $operation/pledge_modify or $operation/pledge_deliver">
            <tr>
                <td class="leftColumnIdentTable">
                    <xsl:text>Контрагент: </xsl:text>
                </td>
                <td class="sizeData rightColumnIdentTable">
                    <xsl:call-template name="contrAgent">
                        <xsl:with-param name="transaction" select="$operation/transaction" />
                        <xsl:with-param name="pledge" select="$operation/pledge" />
                        <xsl:with-param name="unpledge" select="$operation/unpledge" />
                        <xsl:with-param name="pledge_modify" select="$operation/pledge_modify" />
                        <xsl:with-param name="pledge_deliver" select="$operation/pledge_deliver" />
    
                        <xsl:with-param name="account_ident" select="$accountIdent" />
                        <xsl:with-param name="account_deliv"
                            select="$operation/transaction/account_deliv" />
                        <xsl:with-param name="account_receiv"
                            select="$operation/transaction/account_receiv" />
                        <xsl:with-param name="account_owner" select="$operation/*/account_owner" />
                        <xsl:with-param name="account_pledgee"
                            select="$operation/*/pledge_info/account_pledgee" />
                        <xsl:with-param name="account_pledgee_new"
                            select="$operation/pledge_deliver/account_pledgee_new" />
                    </xsl:call-template>
                </td>
            </tr>
        </xsl:if>
        <!-- Если есть в описании операции данные ссылках не документы - вывести -->
        <xsl:if test="$operation/*/*/based_info">
            <tr>
                <td class="leftColumnIdentTable">
                    <xsl:text>Ссылки на документы: </xsl:text>
                </td>
                <td class="sizeData rightColumnIdentTable">
                    <xsl:call-template name="based_infoRow">
                        <xsl:with-param name="root" select="$operation/*/*" />
                    </xsl:call-template>
                </td>
            </tr>
        </xsl:if>
        <tr>
            <td class="leftColumnIdentTable">
                <xsl:text>Дополнительная информация: </xsl:text>
            </td>
            <td class="sizeData rightColumnIdentTable">
                <xsl:for-each select="$operation">
                    <xsl:call-template name="description_operation_t">
                        <xsl:with-param name="root" select="." />
                    </xsl:call-template>
                </xsl:for-each>
            </td>
        </tr>
    </xsl:template>
    <!-- Enums -->

  <!-- Вид счета -->
    <xsl:template name="account_type_et">
        <xsl:param name="type" />
        <xsl:choose>
            <xsl:when test="$type = 'ISSUE'">
                <xsl:text>Эмиссионный счет</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'EMITENT'">
                <xsl:text>Лицевой счет эмитента</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'OWNER'">
                <xsl:text>Владелец</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'NOMINEE'">
                <xsl:text>Номинальный держатель</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'TRUSTEE'">
                <xsl:text>Доверительный управляющий</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'PLEDGEE'">
                <xsl:text>Залогодержатель</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'UNKNOWN'">
                <xsl:text>Счет неустановленных лиц</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'NOTARY'">
                <xsl:text>Нотариус</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'RIGHTS'">
                <xsl:text>Доверительный управляющий правами</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'TREASURY'">
                <xsl:text>Казначейский счет эмитента</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'DEPOSIT'">
                <xsl:text>Депозитарный счет</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'NDCD'">
                <xsl:text>Номинальный держатель Центральный депозитарий</xsl:text>
            </xsl:when>

        </xsl:choose>
    </xsl:template>

    <!-- Тип уполномоченного представителя -->
    <xsl:template name="authorised_person_type_et">
        <xsl:param name="type" />
        <xsl:choose>
            <xsl:when test="$type = '001'">
                <xsl:text>Уполномоченный представитель юридического лица по уставу</xsl:text>
            </xsl:when>
            <xsl:when test="$type = '002'">
                <xsl:text>Единоличный исполнительный орган</xsl:text>
            </xsl:when>
            <xsl:when test="$type = '003'">
                <xsl:text>Федеральный орган исполнительной власти, государственный орган или орган муниципального образования, имеющий право осуществлять права собственника от имени зарегистрированного лица</xsl:text>
            </xsl:when>
            <xsl:when test="$type = '004'">
                <xsl:text>Должностное лицо уполномоченного государственного органа</xsl:text>
            </xsl:when>
            <xsl:when test="$type = '005'">
                <xsl:text>Уполномоченный представитель по доверенности</xsl:text>
            </xsl:when>
            <xsl:when test="$type = '006'">
                <xsl:text>Опекун</xsl:text>
            </xsl:when>
            <xsl:when test="$type = '007'">
                <xsl:text>Попечитель</xsl:text>
            </xsl:when>
            <xsl:when test="$type = '008'">
                <xsl:text>Родитель</xsl:text>
            </xsl:when>
            <xsl:when test="$type = '009'">
                <xsl:text>Учредитель доверительного управления</xsl:text>
            </xsl:when>
        </xsl:choose>
    </xsl:template>

    <!-- Тип блокировния -->
    <xsl:template name="block_type_et">
        <xsl:param name="type" />
        <xsl:choose>
            <xsl:when test="$type = 'BLOK'">
                <xsl:text>Блокирование ЦБ</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'BLOP'">
                <xsl:text>Блокирование заложенных ЦБ</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'BLON'">
                <xsl:text>Блокирование неоплаченных ЦБ</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'PLDG'">
                <xsl:text>Залог ЦБ</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'PLDN'">
                <xsl:text>Залог неоплаченных ЦБ</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'UNPA'">
                <xsl:text>Обременение обязательством оплаты ЦБ</xsl:text>
            </xsl:when>
        </xsl:choose>
    </xsl:template>

    <xsl:template name="blockoper_type_t">
        <xsl:param name="type" />
        <xsl:choose>
            <xsl:when test="$type = 'ALLS'">
                <xsl:text>Блокирование всех операций</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'DELI'">
                <xsl:text>Блокирование расходных операций</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'DERE'">
                <xsl:text>Блокирование приходно-расходных операций</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'STOC'">
                <xsl:text>Блокирование всех операций с ЦБ</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'DEPL'">
                <xsl:text>Блокирование расходных операций и залогов</xsl:text>
            </xsl:when>
        </xsl:choose>
    </xsl:template>

    <!-- тип документа-основания -->
    <xsl:template name="contract_et">
        <xsl:param name="type" />
        <xsl:choose>
            <xsl:when test="$type/contract_code = 'AGTC'">
                <xsl:text>Агентский договор (договор комиссии)</xsl:text>
            </xsl:when>
            <xsl:when test="$type/contract_code = 'BYSA'">
                <xsl:text>Договор купли-продажи</xsl:text>
            </xsl:when>
            <xsl:when test="$type/contract_code = 'COLA'">
                <xsl:text>Договор займа (кредитный договор)</xsl:text>
            </xsl:when>
            <xsl:when test="$type/contract_code = 'COMC'">
                <xsl:text>Договор комиссии</xsl:text>
            </xsl:when>
            <xsl:when test="$type/contract_code = 'DEPA'">
                <xsl:text>Депозитарный договор</xsl:text>
            </xsl:when>
            <xsl:when test="$type/contract_code = 'DSTA'">
                <xsl:text>Договор вклада/ хранения (депозитное соглашение)</xsl:text>
            </xsl:when>
            <xsl:when test="$type/contract_code = 'EXGA'">
                <xsl:text>Договор мены </xsl:text>
            </xsl:when>
            <xsl:when test="$type/contract_code = 'GIFA'">
                <xsl:text>Договор дарения</xsl:text>
            </xsl:when>
            <xsl:when test="$type/contract_code = 'INDA'">
                <xsl:text>Междепозитарный договор </xsl:text>
            </xsl:when>
            <xsl:when test="$type/contract_code = 'NCBO'">
                <xsl:text>Поручение клиента </xsl:text>
            </xsl:when>
            <xsl:when test="$type/contract_code = 'NOMA'">
                <xsl:text>Договор о номинальном держании</xsl:text>
            </xsl:when>
            <xsl:when test="$type/contract_code = 'ORDA'">
                <xsl:text>Договор поручения </xsl:text>
            </xsl:when>
            <xsl:when test="$type/contract_code = 'OTHR'">
                <xsl:value-of select="$type/narrative" />
            </xsl:when>
            <xsl:when test="$type/contract_code = 'PLGA'">
                <xsl:text>Договор залога </xsl:text>
            </xsl:when>
            <xsl:when test="$type/contract_code = 'TRSA'">
                <xsl:text>Договор доверительного управления </xsl:text>
            </xsl:when>
            <xsl:when test="$type/contract_code = 'JUST'">
                <xsl:text>Решение, определение суда </xsl:text>
            </xsl:when>
            <xsl:when test="$type/contract_code = 'ILST'">
                <xsl:text>Исполнительный лист, постановление судебного пристава </xsl:text>
            </xsl:when>
            <xsl:when test="$type/contract_code = 'INHE'">
                <xsl:text>Свидетельство о праве на наследство по закону </xsl:text>
            </xsl:when>
            <xsl:when test="$type/contract_code = 'REPO'">
                <xsl:text>Договор РЕПО </xsl:text>
            </xsl:when>
            <xsl:when test="$type/contract_code = 'CONT'">
                <xsl:text>Договор на осуществление контрольных функций</xsl:text>
            </xsl:when>
        </xsl:choose>
    </xsl:template>

    <!-- Цель подачи анкеты совладельца -->
    <xsl:template name="coowner_form_for_et">
        <xsl:param name="type" />
        <xsl:choose>
            <xsl:when test="$type = 'ACOP'">
                <xsl:text>Открытие лицевого счета общей собственности</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'COAD'">
                <xsl:text>Добавление совладельца в счет общей собственности</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'COCH'">
                <xsl:text>Изменение данных анкеты совладельца</xsl:text>
            </xsl:when>
        </xsl:choose>
    </xsl:template>

    <!-- вид общей собственности -->
    <xsl:template name="coowners_type_et">
        <xsl:param name="type" />
        <xsl:choose>
            <xsl:when test="$type = '10'">
                <xsl:text>долевая</xsl:text>
            </xsl:when>
            <xsl:when test="$type = '11'">
                <xsl:text>совместная</xsl:text>
            </xsl:when>
        </xsl:choose>
    </xsl:template>

    <!-- Страны -->
    <xsl:template name="country_et">
        <xsl:param name="type" />
        <xsl:choose>
            <xsl:when test="$type = 'AB'">
                <xsl:text>АБХАЗИЯ</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'AU'">
                <xsl:text>АВСТРАЛИЯ</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'AT'">
                <xsl:text>АВСТРИЯ</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'AZ'">
                <xsl:text>АЗЕРБАЙДЖАН</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'AL'">
                <xsl:text>АЛБАНИЯ</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'DZ'">
                <xsl:text>АЛЖИР</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'AS'">
                <xsl:text>АМЕРИКАНСКОЕ САМОА</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'AI'">
                <xsl:text>АНГИЛЬЯ</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'AO'">
                <xsl:text>АНГОЛА</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'AD'">
                <xsl:text>АНДОРРА</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'AQ'">
                <xsl:text>АНТАРКТИДА</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'AG'">
                <xsl:text>АНТИГУА И БАРБУДА</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'AR'">
                <xsl:text>АРГЕНТИНА</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'AM'">
                <xsl:text>АРМЕНИЯ</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'AW'">
                <xsl:text>АРУБА</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'AF'">
                <xsl:text>АФГАНИСТАН</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'BS'">
                <xsl:text>БАГАМЫ</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'BD'">
                <xsl:text>БАНГЛАДЕШ</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'BB'">
                <xsl:text>БАРБАДОС</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'BH'">
                <xsl:text>БАХРЕЙН</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'BY'">
                <xsl:text>БЕЛАРУСЬ</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'BZ'">
                <xsl:text>БЕЛИЗ</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'BE'">
                <xsl:text>БЕЛЬГИЯ</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'BJ'">
                <xsl:text>БЕНИН</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'BM'">
                <xsl:text>БЕРМУДЫ</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'BG'">
                <xsl:text>БОЛГАРИЯ</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'BO'">
                <xsl:text>БОЛИВИЯ, МНОГОНАЦИОНАЛЬНОЕ ГОСУДАРСТВО</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'BA'">
                <xsl:text>БОСНИЯ И ГЕРЦЕГОВИНА</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'BW'">
                <xsl:text>БОТСВАНА</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'BR'">
                <xsl:text>БРАЗИЛИЯ</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'IO'">
                <xsl:text>БРИТАНСКАЯ ТЕРРИТОРИЯ В ИНДИЙСКОМ ОКЕАНЕ</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'BN'">
                <xsl:text>БРУНЕЙ-ДАРУССАЛАМ</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'BF'">
                <xsl:text>БУРКИНА-ФАСО</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'BI'">
                <xsl:text>БУРУНДИ</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'BT'">
                <xsl:text>БУТАН</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'VU'">
                <xsl:text>ВАНУАТУ</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'HU'">
                <xsl:text>ВЕНГРИЯ</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'VE'">
                <xsl:text>ВЕНЕСУЭЛА БОЛИВАРИАНСКАЯ РЕСПУБЛИКА</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'VG'">
                <xsl:text>ВИРГИНСКИЕ ОСТРОВА, БРИТАНСКИЕ</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'VI'">
                <xsl:text>ВИРГИНСКИЕ ОСТРОВА, США</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'VN'">
                <xsl:text>ВЬЕТНАМ</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'GA'">
                <xsl:text>ГАБОН</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'GY'">
                <xsl:text>ГАЙАНА</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'HT'">
                <xsl:text>ГАИТИ</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'GM'">
                <xsl:text>ГАМБИЯ</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'GH'">
                <xsl:text>ГАНА</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'GP'">
                <xsl:text>ГВАДЕЛУПА</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'GT'">
                <xsl:text>ГВАТЕМАЛА</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'GN'">
                <xsl:text>ГВИНЕЯ</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'GW'">
                <xsl:text>ГВИНЕЯ-БИСАУ</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'DE'">
                <xsl:text>ГЕРМАНИЯ</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'GG'">
                <xsl:text>ГЕРНСИ</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'GI'">
                <xsl:text>ГИБРАЛТАР</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'HN'">
                <xsl:text>ГОНДУРАС</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'HK'">
                <xsl:text>ГОНКОНГ</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'GD'">
                <xsl:text>ГРЕНАДА</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'GL'">
                <xsl:text>ГРЕНЛАНДИЯ</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'GR'">
                <xsl:text>ГРЕЦИЯ</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'GE'">
                <xsl:text>ГРУЗИЯ</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'GU'">
                <xsl:text>ГУАМ</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'DK'">
                <xsl:text>ДАНИЯ</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'JE'">
                <xsl:text>ДЖЕРСИ</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'DJ'">
                <xsl:text>ДЖИБУТИ</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'DM'">
                <xsl:text>ДОМИНИКА</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'DO'">
                <xsl:text>ДОМИНИКАНСКАЯ РЕСПУБЛИКА</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'EG'">
                <xsl:text>ЕГИПЕТ</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'ZM'">
                <xsl:text>ЗАМБИЯ</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'EH'">
                <xsl:text>ЗАПАДНАЯ САХАРА</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'ZW'">
                <xsl:text>ЗИМБАБВЕ</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'YE'">
                <xsl:text>ЙЕМЕН</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'IL'">
                <xsl:text>ИЗРАИЛЬ</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'IN'">
                <xsl:text>ИНДИЯ</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'ID'">
                <xsl:text>ИНДОНЕЗИЯ</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'JO'">
                <xsl:text>ИОРДАНИЯ</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'IQ'">
                <xsl:text>ИРАК</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'IR'">
                <xsl:text>ИРАН, ИСЛАМСКАЯ РЕСПУБЛИКА</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'IE'">
                <xsl:text>ИРЛАНДИЯ</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'IS'">
                <xsl:text>ИСЛАНДИЯ</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'ES'">
                <xsl:text>ИСПАНИЯ</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'IT'">
                <xsl:text>ИТАЛИЯ</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'CV'">
                <xsl:text>КАБО-ВЕРДЕ</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'KZ'">
                <xsl:text>КАЗАХСТАН</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'KH'">
                <xsl:text>КАМБОДЖА</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'CM'">
                <xsl:text>КАМЕРУН</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'CA'">
                <xsl:text>КАНАДА</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'QA'">
                <xsl:text>КАТАР</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'KE'">
                <xsl:text>КЕНИЯ</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'CY'">
                <xsl:text>КИПР</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'KG'">
                <xsl:text>КИРГИЗИЯ</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'KI'">
                <xsl:text>КИРИБАТИ</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'CN'">
                <xsl:text>КИТАЙ</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'CC'">
                <xsl:text>КОКОСОВЫЕ (КИЛИНГ) ОСТРОВА</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'CO'">
                <xsl:text>КОЛУМБИЯ</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'KM'">
                <xsl:text>КОМОРЫ</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'CG'">
                <xsl:text>КОНГО</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'CD'">
                <xsl:text>КОНГО, ДЕМОКРАТИЧЕСКАЯ РЕСПУБЛИКА</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'KP'">
                <xsl:text>КОРЕЯ, НАРОДНО-ДЕМОКРАТИЧЕСКАЯ РЕСПУБЛИКА</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'KR'">
                <xsl:text>КОРЕЯ, РЕСПУБЛИКА</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'CR'">
                <xsl:text>КОСТА-РИКА</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'CI'">
                <xsl:text>КОТ Д'ИВУАР</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'CU'">
                <xsl:text>КУБА</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'KW'">
                <xsl:text>КУВЕЙТ</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'LA'">
                <xsl:text>ЛАОССКАЯ НАРОДНО-ДЕМОКРАТИЧЕСКАЯ РЕСПУБЛИКА</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'LV'">
                <xsl:text>ЛАТВИЯ</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'LS'">
                <xsl:text>ЛЕСОТО</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'LR'">
                <xsl:text>ЛИБЕРИЯ</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'LB'">
                <xsl:text>ЛИВАН</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'LY'">
                <xsl:text>ЛИВИЙСКАЯ АРАБСКАЯ ДЖАМАХИРИЯ</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'LT'">
                <xsl:text>ЛИТВА</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'LI'">
                <xsl:text>ЛИХТЕНШТЕЙН</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'LU'">
                <xsl:text>ЛЮКСЕМБУРГ</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'MU'">
                <xsl:text>МАВРИКИЙ</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'MR'">
                <xsl:text>МАВРИТАНИЯ</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'MG'">
                <xsl:text>МАДАГАСКАР</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'YT'">
                <xsl:text>МАЙОТТА</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'MO'">
                <xsl:text>МАКАО</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'MW'">
                <xsl:text>МАЛАВИ</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'MY'">
                <xsl:text>МАЛАЙЗИЯ</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'ML'">
                <xsl:text>МАЛИ</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'UM'">
                <xsl:text>МАЛЫЕ ТИХООКЕАНСКИЕ ОТДАЛЕННЫЕ ОСТРОВА СОЕДИНЕННЫХ ШТАТОВ</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'MV'">
                <xsl:text>МАЛЬДИВЫ</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'MT'">
                <xsl:text>МАЛЬТА</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'MA'">
                <xsl:text>МАРОККО</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'MQ'">
                <xsl:text>МАРТИНИКА</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'MH'">
                <xsl:text>МАРШАЛЛОВЫ ОСТРОВА</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'MX'">
                <xsl:text>МЕКСИКА</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'FM'">
                <xsl:text>МИКРОНЕЗИЯ, ФЕДЕРАТИВНЫЕ ШТАТЫ</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'MZ'">
                <xsl:text>МОЗАМБИК</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'MD'">
                <xsl:text>МОЛДОВА, РЕСПУБЛИКА</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'MC'">
                <xsl:text>МОНАКО</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'MN'">
                <xsl:text>МОНГОЛИЯ</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'MS'">
                <xsl:text>МОНТСЕРРАТ</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'MM'">
                <xsl:text>МЬЯНМА</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'NA'">
                <xsl:text>НАМИБИЯ</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'NR'">
                <xsl:text>НАУРУ</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'NP'">
                <xsl:text>НЕПАЛ</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'NE'">
                <xsl:text>НИГЕР</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'NG'">
                <xsl:text>НИГЕРИЯ</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'AN'">
                <xsl:text>НИДЕРЛАНДСКИЕ АНТИЛЫ</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'NL'">
                <xsl:text>НИДЕРЛАНДЫ</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'NI'">
                <xsl:text>НИКАРАГУА</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'NU'">
                <xsl:text>НИУЭ</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'NZ'">
                <xsl:text>НОВАЯ ЗЕЛАНДИЯ</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'NC'">
                <xsl:text>НОВАЯ КАЛЕДОНИЯ</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'NO'">
                <xsl:text>НОРВЕГИЯ</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'AE'">
                <xsl:text>ОБЪЕДИНЕННЫЕ АРАБСКИЕ ЭМИРАТЫ</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'OM'">
                <xsl:text>ОМАН</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'BV'">
                <xsl:text>ОСТРОВ БУВЕ</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'IM'">
                <xsl:text>ОСТРОВ МЭН</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'NF'">
                <xsl:text>ОСТРОВ НОРФОЛК</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'CX'">
                <xsl:text>ОСТРОВ РОЖДЕСТВА</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'HM'">
                <xsl:text>ОСТРОВ ХЕРД И ОСТРОВА МАКДОНАЛЬД</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'KY'">
                <xsl:text>ОСТРОВА КАЙМАН</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'CK'">
                <xsl:text>ОСТРОВА КУКА</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'TC'">
                <xsl:text>ОСТРОВА ТЕРКС И КАЙКОС</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'PK'">
                <xsl:text>ПАКИСТАН</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'PW'">
                <xsl:text>ПАЛАУ</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'PS'">
                <xsl:text>ПАЛЕСТИНСКАЯ ТЕРРИТОРИЯ, ОККУПИРОВАННАЯ</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'PA'">
                <xsl:text>ПАНАМА</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'VA'">
                <xsl:text>ПАПСКИЙ ПРЕСТОЛ (ГОСУДАРСТВО - ГОРОД ВАТИКАН)</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'PG'">
                <xsl:text>ПАПУА-НОВАЯ ГВИНЕЯ</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'PY'">
                <xsl:text>ПАРАГВАЙ</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'PE'">
                <xsl:text>ПЕРУ</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'PN'">
                <xsl:text>ПИТКЕРН</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'PL'">
                <xsl:text>ПОЛЬША</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'PT'">
                <xsl:text>ПОРТУГАЛИЯ</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'PR'">
                <xsl:text>ПУЭРТО-РИКО</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'MK'">
                <xsl:text>РЕСПУБЛИКА МАКЕДОНИЯ[3]</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'RE'">
                <xsl:text>РЕЮНЬОН</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'RU'">
                <xsl:text>РОССИЯ</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'RW'">
                <xsl:text>РУАНДА</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'RO'">
                <xsl:text>РУМЫНИЯ</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'WS'">
                <xsl:text>САМОА</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'SM'">
                <xsl:text>САН-МАРИНО</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'ST'">
                <xsl:text>САН-ТОМЕ И ПРИНСИПИ</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'SA'">
                <xsl:text>САУДОВСКАЯ АРАВИЯ</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'SZ'">
                <xsl:text>СВАЗИЛЕНД</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'SH'">
                <xsl:text>СВЯТАЯ ЕЛЕНА</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'MP'">
                <xsl:text>СЕВЕРНЫЕ МАРИАНСКИЕ ОСТРОВА</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'SC'">
                <xsl:text>СЕЙШЕЛЫ</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'BL'">
                <xsl:text>СЕН-БАРТЕЛЕМИ</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'SN'">
                <xsl:text>СЕНЕГАЛ</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'MF'">
                <xsl:text>СЕН-МАРТЕН</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'VC'">
                <xsl:text>СЕНТ-ВИНСЕНТ И ГРЕНАДИНЫ</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'KN'">
                <xsl:text>СЕНТ-КИТС И НЕВИС</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'LC'">
                <xsl:text>СЕНТ-ЛЮСИЯ</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'PM'">
                <xsl:text>СЕНТ-ПЬЕР И МИКЕЛОН</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'RS'">
                <xsl:text>СЕРБИЯ</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'SG'">
                <xsl:text>СИНГАПУР</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'SY'">
                <xsl:text>СИРИЙСКАЯ АРАБСКАЯ РЕСПУБЛИКА</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'SK'">
                <xsl:text>СЛОВАКИЯ</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'SI'">
                <xsl:text>СЛОВЕНИЯ</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'GB'">
                <xsl:text>СОЕДИНЕННОЕ КОРОЛЕВСТВО</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'US'">
                <xsl:text>СОЕДИНЕННЫЕ ШТАТЫ</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'SB'">
                <xsl:text>СОЛОМОНОВЫ ОСТРОВА</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'SO'">
                <xsl:text>СОМАЛИ</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'SD'">
                <xsl:text>СУДАН</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'SR'">
                <xsl:text>СУРИНАМ</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'SL'">
                <xsl:text>СЬЕРРА-ЛЕОНЕ</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'TJ'">
                <xsl:text>ТАДЖИКИСТАН</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'TW'">
                <xsl:text>ТАЙВАНЬ (КИТАЙ)</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'TH'">
                <xsl:text>ТАИЛАНД</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'TZ'">
                <xsl:text>ТАНЗАНИЯ, ОБЪЕДИНЕННАЯ РЕСПУБЛИКА</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'TL'">
                <xsl:text>ТИМОР-ЛЕСТЕ</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'TG'">
                <xsl:text>ТОГО</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'TK'">
                <xsl:text>ТОКЕЛАУ</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'TO'">
                <xsl:text>ТОНГА</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'TT'">
                <xsl:text>ТРИНИДАД И ТОБАГО</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'TV'">
                <xsl:text>ТУВАЛУ</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'TN'">
                <xsl:text>ТУНИС</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'TM'">
                <xsl:text>ТУРКМЕНИЯ</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'TR'">
                <xsl:text>ТУРЦИЯ</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'UG'">
                <xsl:text>УГАНДА</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'UZ'">
                <xsl:text>УЗБЕКИСТАН</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'UA'">
                <xsl:text>УКРАИНА</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'WF'">
                <xsl:text>УОЛЛИС И ФУТУНА</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'UY'">
                <xsl:text>УРУГВАЙ</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'FO'">
                <xsl:text>ФАРЕРСКИЕ ОСТРОВА</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'FJ'">
                <xsl:text>ФИДЖИ</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'PH'">
                <xsl:text>ФИЛИППИНЫ</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'FI'">
                <xsl:text>ФИНЛЯНДИЯ</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'FK'">
                <xsl:text>ФОЛКЛЕНДСКИЕ ОСТРОВА (МАЛЬВИНСКИЕ)</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'FR'">
                <xsl:text>ФРАНЦИЯ</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'GF'">
                <xsl:text>ФРАНЦУЗСКАЯ ГВИАНА</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'PF'">
                <xsl:text>ФРАНЦУЗСКАЯ ПОЛИНЕЗИЯ</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'TF'">
                <xsl:text>ФРАНЦУЗСКИЕ ЮЖНЫЕ ТЕРРИТОРИИ</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'HR'">
                <xsl:text>ХОРВАТИЯ</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'CF'">
                <xsl:text>ЦЕНТРАЛЬНО-АФРИКАНСКАЯ РЕСПУБЛИКА</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'TD'">
                <xsl:text>ЧАД</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'ME'">
                <xsl:text>ЧЕРНОГОРИЯ</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'CZ'">
                <xsl:text>ЧЕШСКАЯ РЕСПУБЛИКА</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'CL'">
                <xsl:text>ЧИЛИ</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'CH'">
                <xsl:text>ШВЕЙЦАРИЯ</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'SE'">
                <xsl:text>ШВЕЦИЯ</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'SJ'">
                <xsl:text>ШПИЦБЕРГЕН И ЯН МАЙЕН</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'LK'">
                <xsl:text>ШРИ-ЛАНКА</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'EC'">
                <xsl:text>ЭКВАДОР</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'GQ'">
                <xsl:text>ЭКВАТОРИАЛЬНАЯ ГВИНЕЯ</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'AX'">
                <xsl:text>ЭЛАНДСКИЕ ОСТРОВА</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'SV'">
                <xsl:text>ЭЛЬ-САЛЬВАДОР</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'ER'">
                <xsl:text>ЭРИТРЕЯ</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'EE'">
                <xsl:text>ЭСТОНИЯ</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'ET'">
                <xsl:text>ЭФИОПИЯ</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'ZA'">
                <xsl:text>ЮЖНАЯ АФРИКА</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'GS'">
                <xsl:text>ЖНАЯ ДЖОРДЖИЯ И ЮЖНЫЕ САНДВИЧЕВЫ ОСТРОВА</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'OS'">
                <xsl:text>ЮЖНАЯ ОСЕТИЯ</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'JM'">
                <xsl:text>ЯМАЙКА</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'JP'">
                <xsl:text>ЯПОНИЯ</xsl:text>
            </xsl:when>
        </xsl:choose>
    </xsl:template>

    <!-- спососб доставки -->
    <xsl:template name="delivery_method_et">
        <xsl:param name="type" />
        <xsl:choose>
            <xsl:when test="$type = '01'">
                <xsl:text>Лично зарегистрированным лицом</xsl:text>
            </xsl:when>
            <xsl:when test="$type = '02'">
                <xsl:text>Уполномоченным представителем</xsl:text>
            </xsl:when>
            <xsl:when test="$type = '03'">
                <xsl:text>Курьером</xsl:text>
            </xsl:when>
            <xsl:when test="$type = '04'">
                <xsl:text>Заказным письмом</xsl:text>
            </xsl:when>
            <xsl:when test="$type = '05'">
                <xsl:text>Другим способом</xsl:text>
            </xsl:when>
        </xsl:choose>
    </xsl:template>

    <!-- место выдачи -->
    <xsl:template name="delivery_place_et">
        <xsl:param name="type" />
        <xsl:choose>
            <xsl:when test="$type = 'REGR'">
                <xsl:text>Регистратор</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'TAGN'">
                <xsl:text>Трансфер-агент</xsl:text>
            </xsl:when>
        </xsl:choose>
    </xsl:template>

    <!-- Форма выплаты доходов по ценным бумагам -->
    <xsl:template name="distr_div_frm_et">
        <xsl:param name="type" />
        <xsl:choose>
            <xsl:when test="$type = 'CASH'">
                <xsl:text>Наличная</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'BANK'">
                <xsl:text>Банковским переводом</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'POST'">
                <xsl:text>Почтовым переводом</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'NULL'">
                <xsl:text>Не указан</xsl:text>
            </xsl:when>
        </xsl:choose>
    </xsl:template>

    <!-- Состояние документа -->
    <xsl:template name="doc_processing_et">
        <xsl:param name="type" />
        <xsl:choose>
            <xsl:when test="$type = 'REGIST'">
                <xsl:text>Зарегистрирован регистратором</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'EXEC'">
                <xsl:text> Обработан (исполнен)</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'REJECT'">
                <xsl:text>Отказ в исполнении документа</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'ANNUL'">
                <xsl:text>Аннулирован</xsl:text>
            </xsl:when>
        </xsl:choose>
    </xsl:template>

    <!-- Тип фильтрации по процентам -->
    <xsl:template name="filter_by_percent_et">
        <xsl:param name="type" />
        <xsl:choose>
            <xsl:when test="$type = 'CAPITAL_TOTAL'">
                <xsl:text>От общей номинальной стоимости всех акций, включенных в УК</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'ISSUED_TOTAL'">
                <xsl:text>От общей номинальной стоимости всех выпущенных акций</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'PLACED_TOTAL'">
                <xsl:text>От общей номинальной стоимости всех размещенных акций</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'CAPITAL_ORDINARY'">
                <xsl:text>От общей номинальной стоимости обыкновенных, включенных в УК</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'ISSUED_ORDINARY'">
                <xsl:text>От общей номинальной стоимости обыкновенных выпущенных</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'PLACED_ORDINARY'">
                <xsl:text>От общей номинальной стоимости обыкновенных размещенных</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'CAPITAL_PREFFERED'">
                <xsl:text>От общей номинальной стоимости привилегированных включ. в УК</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'ISSUED_PREFFERED'">
                <xsl:text>От общей номинальной стоимости привилегированных выпущенных</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'PLACED_PREFFERED'">
                <xsl:text>От общей номинальной стоимости привилегированных размещенных</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'PERCENTAGE_OF_VOTE'">
                <xsl:text>Процент голосов</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'PERCENTAGE_OF_SHARES_TOTAL'">
                <xsl:text>Акций на счете от общего количества всех акций</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'PERCENTAGE_OF_SHARES_ORDINARY'">
                <xsl:text>Обыкновенных от общего количества обыкновенных акций</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'PERCENTAGE_OF_SHARES_PREFFERED'">
                <xsl:text>Привилегированных от общего кол-ва привилегированных акций</xsl:text>
            </xsl:when>
        </xsl:choose>
    </xsl:template>

    <!-- Цель подачи анкеты: открытие счета или внесение изменений -->
    <xsl:template name="form_for_et">
        <xsl:param name="type" />
        <xsl:choose>
            <xsl:when test="$type = 'ACOP'">
                <xsl:text>Открытие лицевого счета</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'ACCH'">
                <xsl:text>Изменение данных анкеты</xsl:text>
            </xsl:when>
        </xsl:choose>
    </xsl:template>

    <!-- Тип предпринимателя -->
    <xsl:template name="indiv_business_type_et">
        <xsl:param name="type" />
        <xsl:choose>
            <xsl:when test="$type = 'INDV'">
                <xsl:text>Индивидуальный предприниматель</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'FERM'">
                <xsl:text>Крестьянско-фермерское хозяйство</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'NOTAR'">
                <xsl:text>Нотариус</xsl:text>
            </xsl:when>
        </xsl:choose>
    </xsl:template>

    <!-- Типы документов ФЛ -->
    <xsl:template name="individual_document_type_et">
        <xsl:param name="type" />
        <xsl:choose>
            <xsl:when test="$type = '01'">
                <xsl:text>Паспорт гражданина СССР</xsl:text>
            </xsl:when>
            <xsl:when test="$type = '02'">
                <xsl:text>Заграничный паспорт гражданина СССР</xsl:text>
            </xsl:when>
            <xsl:when test="$type = '03'">
                <xsl:text>Свидетельство о рождении</xsl:text>
            </xsl:when>
            <xsl:when test="$type = '04'">
                <xsl:text>Удостоверение личности офицера</xsl:text>
            </xsl:when>
            <xsl:when test="$type = '05'">
                <xsl:text>Справка об освобождении из места лишения свободы</xsl:text>
            </xsl:when>
            <xsl:when test="$type = '06'">
                <xsl:text>Паспорт моряка Минморфлота</xsl:text>
            </xsl:when>
            <xsl:when test="$type = '07'">
                <xsl:text>Военный билет</xsl:text>
            </xsl:when>
            <xsl:when test="$type = '08'">
                <xsl:text>Временное удостоверение, выданное взамен военного билета</xsl:text>
            </xsl:when>
            <xsl:when test="$type = '09'">
                <xsl:text>Дипломатический паспорт гражданина РФ</xsl:text>
            </xsl:when>
            <xsl:when test="$type = '10'">
                <xsl:text>Паспорт иностранного гражданина</xsl:text>
            </xsl:when>
            <xsl:when test="$type = '11'">
                <xsl:text>Свидетельство о рассмотрении ходатайства о признании лица беженцем</xsl:text>
            </xsl:when>
            <xsl:when test="$type = '12'">
                <xsl:text>Вид на жительство в РФ</xsl:text>
            </xsl:when>
            <xsl:when test="$type = '13'">
                <xsl:text>Удостоверение беженца в РФ</xsl:text>
            </xsl:when>
            <xsl:when test="$type = '14'">
                <xsl:text>Временное удостоверение личности гражданина РФ</xsl:text>
            </xsl:when>
            <xsl:when test="$type = '15'">
                <xsl:text>Разрешение на временное проживание в РФ</xsl:text>
            </xsl:when>
            <xsl:when test="$type = '18'">
                <xsl:text>Свидетельство о предоставлении убежища на территории РФ</xsl:text>
            </xsl:when>
            <xsl:when test="$type = '21'">
                <xsl:text>Паспорт гражданина РФ</xsl:text>
            </xsl:when>
            <xsl:when test="$type = '22'">
                <xsl:text>Заграничный паспорт гражданина РФ</xsl:text>
            </xsl:when>
            <xsl:when test="$type = '23'">
                <xsl:text>Свидетельство о рождении, выданное уполномоченным органом
                иностранного государства</xsl:text>
            </xsl:when>
            <xsl:when test="$type = '24'">
                <xsl:text>Удостоверение личности военнослужащего Российской Федерации</xsl:text>
            </xsl:when>
            <xsl:when test="$type = '26'">
                <xsl:text>Паспорт моряка</xsl:text>
            </xsl:when>
            <xsl:when test="$type = '27'">
                <xsl:text>Военный билет офицера запаса</xsl:text>
            </xsl:when>
            <xsl:when test="$type = '91'">
                <xsl:text>Иные, выдаваемые органами внутренних дел РФ</xsl:text>
            </xsl:when>
            <xsl:when test="$type = '00'">
                <xsl:text>Не указан</xsl:text>
            </xsl:when>
        </xsl:choose>
    </xsl:template>

    <!-- Типы документов ЮЛ -->
    <xsl:template name="juridical_doc_type_et">
        <xsl:param name="type" />
        <xsl:choose>
            <xsl:when test="$type = 'LICS'">
                <xsl:text>Лицензия</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'REGN'">
                <xsl:text>Свидетельство о регистрации</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'INCR'">
                <xsl:text>Сертификат об инкорпорации</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'USTV'">
                <xsl:text>Устав</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'UKZP'">
                <xsl:text>Указ Президента</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'PSTN'">
                <xsl:text>Постановление</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'OTHR'">
                <xsl:text>Другой документ</xsl:text>
            </xsl:when>
            <xsl:otherwise>
                <xsl:text>Не указан</xsl:text>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <!-- Особый вид ЮЛ -->
    <xsl:template name="juridical_special_et">
        <xsl:param name="type" />
        <xsl:choose>
            <xsl:when test="$type = 'FORG'">
                <xsl:text>Федеральный гос.орган</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'MORG'">
                <xsl:text>Муниципальный орган</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'SBRU'">
                <xsl:text>Субъект РФ</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'COMM'">
                <xsl:text>Общественное объединение</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'CRED'">
                <xsl:text>Кредитная организация</xsl:text>
            </xsl:when>
        </xsl:choose>
    </xsl:template>

    <!-- Способ получения корреспонденции (выписки) -->
    <xsl:template name="letter_go_type_et">
        <xsl:param name="type" />
        <xsl:choose>
            <xsl:when test="$type = 'LETR'">
                <xsl:text>Письмо</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'KURR'">
                <xsl:text>Курьер</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'REGR'">
                <xsl:text>У регистратора (в депозитарии)</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'AGNT'">
                <xsl:text>В месте приема документов</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'RLTR'">
                <xsl:text>Заказное письмо</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'NULL'">
                <xsl:text>Не указан</xsl:text>
            </xsl:when>
        </xsl:choose>
    </xsl:template>

    <!-- Тип нерезидента юридического лица -->
    <xsl:template name="nonresident_type_et">
        <xsl:param name="type" />
        <xsl:choose>
            <xsl:when test="$type = '00'">
                <xsl:text>Нерезидент с постоянным представительством</xsl:text>
            </xsl:when>
            <xsl:when test="$type = '01'">
                <xsl:text>Нерезидент без постоянного представительства</xsl:text>
            </xsl:when>
        </xsl:choose>
    </xsl:template>

    <!-- Организационно-правовая форма ЮЛ -->
    <xsl:template name="opf_et">
        <xsl:param name="type" />
        <xsl:choose>
            <xsl:when test="$type = '00'">
                <xsl:text>Иная форма</xsl:text>
            </xsl:when>
            <xsl:when test="$type = '41'">
                <xsl:text>Унитарное предприятия на праве оперативного управления</xsl:text>
            </xsl:when>
            <xsl:when test="$type = '42'">
                <xsl:text>Унитарное предприятия на праве хозяйственного ведения</xsl:text>
            </xsl:when>
            <xsl:when test="$type = '47'">
                <xsl:text>Открытое акционерное общество</xsl:text>
            </xsl:when>
            <xsl:when test="$type = '51'">
                <xsl:text>Полное товарищество</xsl:text>
            </xsl:when>
            <xsl:when test="$type = '52'">
                <xsl:text>Производственный кооператив</xsl:text>
            </xsl:when>
            <xsl:when test="$type = '53'">
                <xsl:text>Крестьянское (фермерское) хозяйство</xsl:text>
            </xsl:when>
            <xsl:when test="$type = '64'">
                <xsl:text>Товарищество на вере</xsl:text>
            </xsl:when>
            <xsl:when test="$type = '65'">
                <xsl:text>Общество с ограниченной ответственностью</xsl:text>
            </xsl:when>
            <xsl:when test="$type = '66'">
                <xsl:text>Общество с дополнительной ответственностью</xsl:text>
            </xsl:when>
            <xsl:when test="$type = '67'">
                <xsl:text>Закрытое акционерное общество</xsl:text>
            </xsl:when>
            <xsl:when test="$type = '68'">
                <xsl:text>Дочернее унитарное предприятие</xsl:text>
            </xsl:when>
            <xsl:when test="$type = '71'">
                <xsl:text>Частное учреждение</xsl:text>
            </xsl:when>
            <xsl:when test="$type = '72'">
                <xsl:text>Бюджетное учреждение</xsl:text>
            </xsl:when>
            <xsl:when test="$type = '73'">
                <xsl:text>Автономное учреждение</xsl:text>
            </xsl:when>
            <xsl:when test="$type = '76'">
                <xsl:text>Садоводческое (огородническое, дачное) некомерческое товарищество</xsl:text>
            </xsl:when>
            <xsl:when test="$type = '77'">
                <xsl:text>Ассоциация крестьянских (фермерских) хозяйств</xsl:text>
            </xsl:when>
            <xsl:when test="$type = '78'">
                <xsl:text>Орган общественной самодеятельности</xsl:text>
            </xsl:when>
            <xsl:when test="$type = '80'">
                <xsl:text>Территориальное общественное самоуправление</xsl:text>
            </xsl:when>
            <xsl:when test=" $type = '82'">
                <xsl:text>Государственная корпорация</xsl:text>
            </xsl:when>
            <xsl:when test="$type = '83'">
                <xsl:text>Общественная ( религиозная ) организация ( объединение )</xsl:text>
            </xsl:when>
            <xsl:when test="$type = '84'">
                <xsl:text>Общественное движение</xsl:text>
            </xsl:when>
            <xsl:when test="$type = '85'">
                <xsl:text>Потребительский кооператив</xsl:text>
            </xsl:when>
            <xsl:when test="$type = '86'">
                <xsl:text>Государственная компания enumeration</xsl:text>
            </xsl:when>
            <xsl:when test="$type = '87'">
                <xsl:text>Простое товарищество</xsl:text>
            </xsl:when>
            <xsl:when test="$type = '88'">
                <xsl:text>Фонд</xsl:text>
            </xsl:when>
            <xsl:when test="$type = '89'">
                <xsl:text>Прочие некоммерческие организации</xsl:text>
            </xsl:when>
            <xsl:when test="$type = '90'">
                <xsl:text>Представительство ( филиал )</xsl:text>
            </xsl:when>
            <xsl:when test="$type = '91'">
                <xsl:text>Индивидуальный предприниматель</xsl:text>
            </xsl:when>
            <xsl:when test="$type = '93'">
                <xsl:text>Паевой инвестиционный фонд</xsl:text>
            </xsl:when>
            <xsl:when test="$type = '93'">
                <xsl:text>Объединение юридических лиц ( ассоциация и союз )</xsl:text>
            </xsl:when>
            <xsl:when test="$type = '94'">
                <xsl:text>Товарищество собственников жилья</xsl:text>
            </xsl:when>
            <xsl:when test="$type = '96'">
                <xsl:text>Некоммерческое партнерство</xsl:text>
            </xsl:when>
            <xsl:when test="$type = '97'">
                <xsl:text>Автономная некоммерческая организация</xsl:text>
            </xsl:when>
            <xsl:when test="$type = '98'">
                <xsl:text>Иные неюридические лица</xsl:text>
            </xsl:when>
        </xsl:choose>
    </xsl:template>

    <!-- Статус владельца (дееспособность) -->
    <xsl:template name="owner_status_et">
        <xsl:param name="type" />
        <xsl:choose>
            <xsl:when test="$type = 'LPRS'">
                <xsl:text>Дееспособный</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'LIPR'">
                <xsl:text>Недееспособный</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'LLCA'">
                <xsl:text>Ограниченно дееспособный</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'MINR'">
                <xsl:text>Несовершеннолетний</xsl:text>
            </xsl:when>
        </xsl:choose>
    </xsl:template>

    <!-- Цель подачи документа -->
    <xsl:template name="proxy_for_et">
        <xsl:param name="type" />
        <xsl:choose>
            <xsl:when test="$type = 'AUOP'">
                <xsl:text>Ввод представителя</xsl:text>
            </xsl:when>

            <xsl:when test="$type = 'AUCH'">
                <xsl:text>Изменение данных анкеты или полномочий</xsl:text>
            </xsl:when>
        </xsl:choose>
    </xsl:template>

    <!-- тип телефона -->
    <xsl:template name="phone_type_et">
        <xsl:param name="type" />
        <xsl:choose>
            <xsl:when test="$type = 'HOME'">
                <xsl:text>домашний телефон</xsl:text>
            </xsl:when>

            <xsl:when test="$type = 'BIZZ'">
                <xsl:text>рабочий телефон</xsl:text>
            </xsl:when>

            <xsl:when test="$type = 'MOBI'">
                <xsl:text>мобильный телефон</xsl:text>
            </xsl:when>

            <xsl:when test="$type = 'FAXI'">
                <xsl:text>номер факса</xsl:text>
            </xsl:when>
        </xsl:choose>
    </xsl:template>

    <!-- Справочник регионов России -->
    <xsl:template name="region_et">
        <xsl:param name="type" />
        <xsl:choose>
            <xsl:when test="$type='1'">
                <xsl:text>Адыгея республика</xsl:text>
            </xsl:when>
            <xsl:when test="$type='2'">
                <xsl:text>Башкортостан республика</xsl:text>
            </xsl:when>
            <xsl:when test="$type='3'">
                <xsl:text>Бурятия республика</xsl:text>
            </xsl:when>
            <xsl:when test="$type='4'">
                <xsl:text>Алтай республика</xsl:text>
            </xsl:when>
            <xsl:when test="$type='5'">
                <xsl:text>Дагестан республика</xsl:text>
            </xsl:when>
            <xsl:when test="$type='6'">
                <xsl:text>Ингушетия республика</xsl:text>
            </xsl:when>
            <xsl:when test="$type='7'">
                <xsl:text>Кабардино-Балкарская республика</xsl:text>
            </xsl:when>
            <xsl:when test="$type='8'">
                <xsl:text>Калмыкия республика</xsl:text>
            </xsl:when>
            <xsl:when test="$type='9'">
                <xsl:text>Карачаево-Черкесская республика</xsl:text>
            </xsl:when>
            <xsl:when test="$type='10'">
                <xsl:text>Карелия республика</xsl:text>
            </xsl:when>
            <xsl:when test="$type='11'">
                <xsl:text>Коми республика</xsl:text>
            </xsl:when>
            <xsl:when test="$type='12'">
                <xsl:text>Марий Эл республика</xsl:text>
            </xsl:when>
            <xsl:when test="$type='13'">
                <xsl:text>Мордовия республика</xsl:text>
            </xsl:when>
            <xsl:when test="$type='14'">
                <xsl:text>Саха /Якутия/ республика</xsl:text>
            </xsl:when>
            <xsl:when test="$type='15'">
                <xsl:text>Северная Осетия - Алания республика</xsl:text>
            </xsl:when>
            <xsl:when test="$type='16'">
                <xsl:text>Татарстан республика</xsl:text>
            </xsl:when>
            <xsl:when test="$type='17'">
                <xsl:text>Тыва республика</xsl:text>
            </xsl:when>
            <xsl:when test="$type='18'">
                <xsl:text>Удмуртская республика</xsl:text>
            </xsl:when>
            <xsl:when test="$type='19'">
                <xsl:text>Хакасия республика</xsl:text>
            </xsl:when>
            <xsl:when test="$type='20'">
                <xsl:text>Чеченская республика</xsl:text>
            </xsl:when>
            <xsl:when test="$type='21'">
                <xsl:text>Чувашская республика</xsl:text>
            </xsl:when>
            <xsl:when test="$type='22'">
                <xsl:text>Алтайский край</xsl:text>
            </xsl:when>
            <xsl:when test="$type='23'">
                <xsl:text>Краснодарский край</xsl:text>
            </xsl:when>
            <xsl:when test="$type='24'">
                <xsl:text>Красноярский край</xsl:text>
            </xsl:when>
            <xsl:when test="$type='25'">
                <xsl:text>Приморский край</xsl:text>
            </xsl:when>
            <xsl:when test="$type='26'">
                <xsl:text>Ставропольский край</xsl:text>
            </xsl:when>
            <xsl:when test="$type='27'">
                <xsl:text>Хабаровский край</xsl:text>
            </xsl:when>
            <xsl:when test="$type='28'">
                <xsl:text>Амурская область</xsl:text>
            </xsl:when>
            <xsl:when test="$type='29'">
                <xsl:text>Архангельская область</xsl:text>
            </xsl:when>
            <xsl:when test="$type='30'">
                <xsl:text>Астраханская область</xsl:text>
            </xsl:when>
            <xsl:when test="$type='31'">
                <xsl:text>Белгородская область</xsl:text>
            </xsl:when>
            <xsl:when test="$type='32'">
                <xsl:text>Брянская область</xsl:text>
            </xsl:when>
            <xsl:when test="$type='33'">
                <xsl:text>Владимирская область</xsl:text>
            </xsl:when>
            <xsl:when test="$type='34'">
                <xsl:text>Волгоградская область</xsl:text>
            </xsl:when>
            <xsl:when test="$type='35'">
                <xsl:text>Вологодская область</xsl:text>
            </xsl:when>
            <xsl:when test="$type='36'">
                <xsl:text>Воронежская область</xsl:text>
            </xsl:when>
            <xsl:when test="$type='37'">
                <xsl:text>Ивановская область</xsl:text>
            </xsl:when>
            <xsl:when test="$type='38'">
                <xsl:text>Иркутская область</xsl:text>
            </xsl:when>
            <xsl:when test="$type='39'">
                <xsl:text>Калининградская область</xsl:text>
            </xsl:when>
            <xsl:when test="$type='40'">
                <xsl:text>Калужская область</xsl:text>
            </xsl:when>
            <xsl:when test="$type='41'">
                <xsl:text>Камчатский край</xsl:text>
            </xsl:when>
            <xsl:when test="$type='42'">
                <xsl:text>Кемеровская область</xsl:text>
            </xsl:when>
            <xsl:when test="$type='43'">
                <xsl:text>Кировская область</xsl:text>
            </xsl:when>
            <xsl:when test="$type='44'">
                <xsl:text>Костромская область</xsl:text>
            </xsl:when>
            <xsl:when test="$type='45'">
                <xsl:text>Курганская область</xsl:text>
            </xsl:when>
            <xsl:when test="$type='46'">
                <xsl:text>Курская область</xsl:text>
            </xsl:when>
            <xsl:when test="$type='47'">
                <xsl:text>Ленинградская область</xsl:text>
            </xsl:when>
            <xsl:when test="$type='48'">
                <xsl:text>Липецкая область</xsl:text>
            </xsl:when>
            <xsl:when test="$type='49'">
                <xsl:text>Магаданская область</xsl:text>
            </xsl:when>
            <xsl:when test="$type='50'">
                <xsl:text>Московская область</xsl:text>
            </xsl:when>
            <xsl:when test="$type='51'">
                <xsl:text>Мурманская область</xsl:text>
            </xsl:when>
            <xsl:when test="$type='52'">
                <xsl:text>Нижегородская область</xsl:text>
            </xsl:when>
            <xsl:when test="$type='53'">
                <xsl:text>Новгородская область</xsl:text>
            </xsl:when>
            <xsl:when test="$type='54'">
                <xsl:text>Новосибирская область</xsl:text>
            </xsl:when>
            <xsl:when test="$type='55'">
                <xsl:text>Омская область</xsl:text>
            </xsl:when>
            <xsl:when test="$type='56'">
                <xsl:text>Оренбургская область</xsl:text>
            </xsl:when>
            <xsl:when test="$type='57'">
                <xsl:text>Орловская область</xsl:text>
            </xsl:when>
            <xsl:when test="$type='58'">
                <xsl:text>Пензенская область</xsl:text>
            </xsl:when>
            <xsl:when test="$type='59'">
                <xsl:text>Пермский край</xsl:text>
            </xsl:when>
            <xsl:when test="$type='60'">
                <xsl:text>Псковская область</xsl:text>
            </xsl:when>
            <xsl:when test="$type='61'">
                <xsl:text>Ростовская область</xsl:text>
            </xsl:when>
            <xsl:when test="$type='62'">
                <xsl:text>Рязанская область</xsl:text>
            </xsl:when>
            <xsl:when test="$type='63'">
                <xsl:text>Самарская область</xsl:text>
            </xsl:when>
            <xsl:when test="$type='64'">
                <xsl:text>Саратовская область</xsl:text>
            </xsl:when>
            <xsl:when test="$type='65'">
                <xsl:text>Сахалинская область</xsl:text>
            </xsl:when>
            <xsl:when test="$type='66'">
                <xsl:text>Свердловская область</xsl:text>
            </xsl:when>
            <xsl:when test="$type='67'">
                <xsl:text>Смоленская область</xsl:text>
            </xsl:when>
            <xsl:when test="$type='68'">
                <xsl:text>Тамбовская область</xsl:text>
            </xsl:when>
            <xsl:when test="$type='69'">
                <xsl:text>Тверская область</xsl:text>
            </xsl:when>
            <xsl:when test="$type='70'">
                <xsl:text>Томская область</xsl:text>
            </xsl:when>
            <xsl:when test="$type='71'">
                <xsl:text>Тульская область</xsl:text>
            </xsl:when>
            <xsl:when test="$type='72'">
                <xsl:text>Тюменская область</xsl:text>
            </xsl:when>
            <xsl:when test="$type='73'">
                <xsl:text>Ульяновская область</xsl:text>
            </xsl:when>
            <xsl:when test="$type='74'">
                <xsl:text>Челябинская область</xsl:text>
            </xsl:when>
            <xsl:when test="$type='75'">
                <xsl:text>Забайкальский край</xsl:text>
            </xsl:when>
            <xsl:when test="$type='76'">
                <xsl:text>Ярославская область</xsl:text>
            </xsl:when>
            <xsl:when test="$type='77'">
                <xsl:text>Москва г.</xsl:text>
            </xsl:when>
            <xsl:when test="$type='78'">
                <xsl:text>Санкт-Петербург г.</xsl:text>
            </xsl:when>
            <xsl:when test="$type='79'">
                <xsl:text>Еврейская автономная область</xsl:text>
            </xsl:when>
            <xsl:when test="$type='83'">
                <xsl:text>Ненецкий автономный округ</xsl:text>
            </xsl:when>
            <xsl:when test="$type='84'">
                <xsl:text>Таймырский (Долгано-Ненецкий) автономный округ</xsl:text>
            </xsl:when>
            <xsl:when test="$type='86'">
                <xsl:text>Ханты-Мансийский - Югра автономный округ</xsl:text>
            </xsl:when>
            <xsl:when test="$type='87'">
                <xsl:text>Чукотский автономный округ</xsl:text>
            </xsl:when>
            <xsl:when test="$type='89'">
                <xsl:text>Ямало-Ненецкий автономный округ</xsl:text>
            </xsl:when>
            <xsl:when test="$type='99'">
                <xsl:text>Байконур г.</xsl:text>
            </xsl:when>
        </xsl:choose>
    </xsl:template>

    <!-- Форма представления отчета -->
    <xsl:template name="report_mode_et">
        <xsl:param name="type" />
        <xsl:choose>
            <xsl:when test="$type = 'PAPER'">
                <xsl:text>Бумажный оригинал</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'ELECR'">
                <xsl:text>Электронный документ</xsl:text>
            </xsl:when>
        </xsl:choose>
    </xsl:template>

    <!-- Результат сверки по документу -->
    <xsl:template name="result_reconciliation_et">
        <xsl:param name="type" />
        <xsl:choose>
            <xsl:when test="$type/result_reconciliation = 'GOOD'">
                <xsl:text>Документ найден, сверка выполнена</xsl:text>
            </xsl:when>
            <xsl:when test="$type/result_reconciliation = 'DIVER'">
                <xsl:text>Документ найден, но есть расхождения в реквизитах документа</xsl:text>
                <xsl:if test="not(normalize-space($type/narrative)='')">
                    <xsl:text> (</xsl:text>
                    <xsl:value-of select="$type/narrative" />
                    <xsl:text>) </xsl:text>
                </xsl:if>
            </xsl:when>
            <xsl:when test="$type/result_reconciliation = 'SPARE'">
                <xsl:text>Документ, пришедший в запросе на сверку от регистратора, не найден у трансфер-агента (лишний документ)</xsl:text>
            </xsl:when>
            <xsl:when test="$type/result_reconciliation = 'MISSI'">
                <xsl:text>Документ, имеющийся  у трансфер-агента, отсутствует в запросе регистратора на сверку</xsl:text>
            </xsl:when>
            <xsl:when test="$type/result_reconciliation = 'MANUAL'">
                <xsl:text>Документ найден, сверка выполнена вручную</xsl:text>
            </xsl:when>
        </xsl:choose>
    </xsl:template>

    <!-- Итоговый результата сверки -->
    <xsl:template name="result_reconciliation_total_et">
        <xsl:param name="type" />
        <xsl:choose>
            <xsl:when test="$type = 'GOOD'">
                <xsl:text>Cверка выполнена, несоответствий в журналах регистратора и трансфер-агента не выявлено</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'DIVER'">
                <xsl:text>Выявлены несоответствия в журналах регистратора и трансфер-агента</xsl:text>
            </xsl:when>
        </xsl:choose>
    </xsl:template>

    <!-- категория ЦБ -->
    <xsl:template name="security_category_et">
        <xsl:param name="type" />
        <xsl:choose>
            <xsl:when test="$type = 'ORDN'">
                <xsl:text>Обыкновенная</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'PREF'">
                <xsl:text>Привилегированная</xsl:text>
            </xsl:when>
        </xsl:choose>
    </xsl:template>

    <!-- вид ЦБ -->
    <xsl:template name="security_classification_et">
        <xsl:param name="type" />
        <xsl:choose>
            <xsl:when test="$type = 'BOND'">
                <xsl:text>Облигация</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'SHAR'">
                <xsl:text>Акция</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'MFUN'">
                <xsl:text>Пай</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'RDRP'">
                <xsl:text>Российская депозитарная расписка</xsl:text>
            </xsl:when>
        </xsl:choose>
    </xsl:template>

    <!-- тип отправителя -->
    <xsl:template name="sender_type_et">
        <xsl:param name="type" />
        <xsl:choose>
            <xsl:when test="$type = '01'">
                <xsl:text>Зарегистрированное лицо</xsl:text>
            </xsl:when>
            <xsl:when test="$type = '02'">
                <xsl:text>Эмитент</xsl:text>
            </xsl:when>
            <xsl:when test="$type = '03'">
                <xsl:text>Другие</xsl:text>
            </xsl:when>
        </xsl:choose>
    </xsl:template>

    <!-- Тип выписки -->
    <xsl:template name="statement_type_et">
        <xsl:param name="type" />
        <xsl:choose>
            <xsl:when test="$type = 'HOLD'">
                <xsl:text>Выписка из реестра</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'TRAN'">
                <xsl:text>Справка о движении ценных бумаг</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'CONF'">
                <xsl:text>Уведомление о проведении операции</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'AVAI'">
                <xsl:text>Справка о наличии ценных бумаг</xsl:text>
            </xsl:when>
        </xsl:choose>
    </xsl:template>

    <!-- Код статуса налогоплательщика -->
    <xsl:template name="tax_status_et">
        <xsl:param name="type" />
        <xsl:choose>
            <xsl:when test="$type = '01'">
                <xsl:text>Налоговый резидент</xsl:text>
            </xsl:when>
            <xsl:when test="$type = '02'">
                <xsl:text>Налоговый нерезидент</xsl:text>
            </xsl:when>
        </xsl:choose>
    </xsl:template>

    <!-- Тип операции передачи ЦБ -->
    <xsl:template name="transaction_type_et">
        <xsl:param name="type" />
        <xsl:choose>
            <xsl:when test="$type = '60101'">
                <xsl:text>Сделка</xsl:text>
            </xsl:when>
            <xsl:when test="$type = '60102'">
                <xsl:text>Дарение</xsl:text>
            </xsl:when>
            <xsl:when test="$type = '60103'">
                <xsl:text>Наследование</xsl:text>
            </xsl:when>
            <xsl:when test="$type = '60104'">
                <xsl:text>Реорганизация/ ликвидация ЗЛ</xsl:text>
            </xsl:when>
            <xsl:when test="$type = '60105'">
                <xsl:text>По решению суда</xsl:text>
            </xsl:when>
            <xsl:when test="$type = '60106'">
                <xsl:text>По договору мены</xsl:text>
            </xsl:when>
            <xsl:when test="$type = '60107'">
                <xsl:text>Внесение в уставный капитал/в имущество общества</xsl:text>
            </xsl:when>
            <xsl:when test="$type = '60108'">
                <xsl:text>При невыполнении условий залога</xsl:text>
            </xsl:when>
            <xsl:when test="$type = '60112'">
                <xsl:text>При выкупе по требованию лица</xsl:text>
            </xsl:when>
            <xsl:when test="$type = '60120'">
                <xsl:text>По иным основаниям</xsl:text>
            </xsl:when>
            <xsl:when test="$type = '60121'">
                <xsl:text>При объединении лицевых счетов</xsl:text>
            </xsl:when>
            <xsl:when test="$type = '60130'">
                <xsl:text>При выкупе по требованию лица, имеющего 95%</xsl:text>
            </xsl:when>
            <xsl:when test="$type = '60131'">
                <xsl:text>При выкупе по требованию акционеров</xsl:text>
            </xsl:when>
            <xsl:when test="$type = '60132'">
                <xsl:text>При выкупе по добровольному предложению</xsl:text>
            </xsl:when>
            <xsl:when test="$type = '60133'">
                <xsl:text>При выкупе по обязательному предложению</xsl:text>
            </xsl:when>
            <xsl:when test="$type = '60201'">
                <xsl:text>Первичное размещение</xsl:text>
            </xsl:when>
            <xsl:when test="$type = '60204'">
                <xsl:text>Размещение ЦБ при реорганизации АО</xsl:text>
            </xsl:when>
            <xsl:when test="$type = '60301'">
                <xsl:text>Выкуп (приобретение) ценных бумаг эмитентом</xsl:text>
            </xsl:when>
            <xsl:when test="$type = '60302'">
                <xsl:text>Приобретение ЦБ эмитентом для погашения</xsl:text>
            </xsl:when>
            <xsl:when test="$type = '60304'">
                <xsl:text>Списание ЦБ при реорганизации/ликвидации АО</xsl:text>
            </xsl:when>
            <xsl:when test="$type = '60305'">
                <xsl:text>Выкуп дробных акций эмитентом</xsl:text>
            </xsl:when>
            <xsl:when test="$type = '60308'">
                <xsl:text>Выкуп ЦБ эмитентом по требованию акицонеров</xsl:text>
            </xsl:when>
            <xsl:when test="$type = '60401'">
                <xsl:text>Зачисление ЦБ на счет номинального держателя</xsl:text>
            </xsl:when>
            <xsl:when test="$type = '60402'">
                <xsl:text>Списание ЦБ со счета номинального держателя</xsl:text>
            </xsl:when>
            <xsl:when test="$type = '60411'">
                <xsl:text>Передача ЦБ в номинальное держание НД ЦД</xsl:text>
            </xsl:when>
            <xsl:when test="$type = '60412'">
                <xsl:text>Возврат ЦБ от НД ЦД</xsl:text>
            </xsl:when>
            <xsl:when test="$type = '60413'">
                <xsl:text>Возврат ЦБ от НД ЦД при прекращении депозитарного договора</xsl:text>
            </xsl:when>
            <xsl:when test="$type = '60414'">
                <xsl:text>Передача заложенных ЦБ в номинальное держание НД ЦД   </xsl:text>
            </xsl:when>
            <xsl:when test="$type = '60451'">
                <xsl:text>Перевод ЦБ в депозит нотариуса</xsl:text>
            </xsl:when>
            <xsl:when test="$type = '60452'">
                <xsl:text>Возврат ЦБ из депозита нотариуса</xsl:text>
            </xsl:when>
            <xsl:when test="$type = '60501'">
                <xsl:text>Передача ЦБ в доверительное управление</xsl:text>
            </xsl:when>
            <xsl:when test="$type = '60502'">
                <xsl:text>Возврат ЦБ из доверительного управления</xsl:text>
            </xsl:when>
            <xsl:when test="$type = '60551'">
                <xsl:text>Передача ЦБ доверительному управляющему правами</xsl:text>
            </xsl:when>
            <xsl:when test="$type = '60552'">
                <xsl:text>Возврат ЦБ от доверительного управляющего правами</xsl:text>
            </xsl:when>
        </xsl:choose>
    </xsl:template>
  <!-- Тип операции передачи ЦБ -->
  <xsl:template name="doc_type_t">
    <xsl:param name="type"/>
    <xsl:choose>
      <xsl:when test="$type/narrative">
        <xsl:value-of select="$type/narrative"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:choose>
          <xsl:when test="$type/doc_type_code = 'OTHR'">
            <xsl:text>Другой документ</xsl:text>
          </xsl:when>
          <xsl:when test="$type/doc_type_code = 'FORM'">
            <xsl:text>Анкета зарегистрированного лица</xsl:text>
          </xsl:when>
          <xsl:when test="$type/doc_type_code = 'TRAN'">
            <xsl:text>Передаточное распоряжение</xsl:text>
          </xsl:when>
          <xsl:when test="$type/doc_type_code = 'ORDR'">
            <xsl:text>Исполнительный лист</xsl:text>
          </xsl:when>
          <xsl:when test="$type/doc_type_code = 'INHE'">
            <xsl:text>Свидетельство о праве на наследство</xsl:text>
          </xsl:when>
          <xsl:when test="$type/doc_type_code = 'NOTR'">
            <xsl:text>Запрос нотариуса</xsl:text>
          </xsl:when>
          <xsl:when test="$type/doc_type_code = 'BLOK'">
            <xsl:text>Распоряжение на блокирование</xsl:text>
          </xsl:when>
          <xsl:when test="$type/doc_type_code = 'UNBL'">
            <xsl:text>Распоряжение на отмену блокирования</xsl:text>
          </xsl:when>
          <xsl:when test="$type/doc_type_code = 'PLDG'">
            <xsl:text>Залоговое распоряжение</xsl:text>
          </xsl:when>
          <xsl:when test="$type/doc_type_code = 'UNPL'">
            <xsl:text>Распоряжение о прекращении залога</xsl:text>
          </xsl:when>
          <xsl:when test="$type/doc_type_code = 'PLMF'">
            <xsl:text>Распоряжение о внесении изменений в залог</xsl:text>
          </xsl:when>
          <xsl:when test="$type/doc_type_code = 'CREG'">
            <xsl:text>Свидетельство о регистрации</xsl:text>
          </xsl:when>
          <xsl:when test="$type/doc_type_code = 'COGR'">
            <xsl:text>Свидетельство о внесении в ЕГРЮЛ</xsl:text>
          </xsl:when>
          <xsl:when test="$type/doc_type_code = 'CHAR'">
            <xsl:text>Устав</xsl:text>
          </xsl:when>
          <xsl:when test="$type/doc_type_code = 'BANK'">
            <xsl:text>Банковская карточка</xsl:text>
          </xsl:when>
          <xsl:when test="$type/doc_type_code = 'MEET'">
            <xsl:text>Протокол собрания</xsl:text>
          </xsl:when>
          <xsl:when test="$type/doc_type_code = 'MNGR'">
            <xsl:text>Документ о назначении руководителя</xsl:text>
          </xsl:when>
          <xsl:when test="$type/doc_type_code = 'REQU'">
            <xsl:text>Запрос на выдачу информации из реестра</xsl:text>
          </xsl:when>
          <xsl:when test="$type/doc_type_code = 'APPL'">
            <xsl:text>Заявление об открытии счета/ изменении анкеты</xsl:text>
          </xsl:when>
          <xsl:when test="$type/doc_type_code = 'PASS'">
            <xsl:text>Копия документа, удостоверяющего личность</xsl:text>
          </xsl:when>
          <xsl:when test="$type/doc_type_code = 'PLDL'">
            <xsl:text>Распоряжение о передаче права залога</xsl:text>
          </xsl:when>
          <xsl:when test="$type/doc_type_code = 'PROX'">
            <xsl:text>Доверенность</xsl:text>
          </xsl:when>
        </xsl:choose>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

   <xsl:template name="block_reason_et">
       <xsl:param name="type"/>
        <xsl:choose>
            <xsl:when test="$type = 'ARST'">
                <xsl:text>Арест ЦБ</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'DEAT'">
                <xsl:text>В связи со смертью</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'BUY8'">
                <xsl:text>При выкупе ЦБ (ст.84.8)</xsl:text>
            </xsl:when>
            <xsl:when test="$type = 'THIR'">
                <xsl:text>Обременение правами третьих лиц</xsl:text>
            </xsl:when>  
            <xsl:when test="$type = 'OTHR'">
                <xsl:text>По иным основаниям</xsl:text>
            </xsl:when>
        </xsl:choose>
    </xsl:template>


    <xsl:template name="licence_t">
        <xsl:param name="root"/>
        <xsl:if test="$root/lic_name">
            <xsl:value-of select="$root/lic_name"/>
            <xsl:text> </xsl:text> 
        </xsl:if>
        <xsl:text>Номер </xsl:text>
        <xsl:value-of select="$root/lic_num"/>
        <xsl:text> от </xsl:text>
        <xsl:call-template name="convertDate">
            <xsl:with-param name="date" select="$root/lic_date" />
        </xsl:call-template>
        <xsl:if test="$root/lic_place">
            <xsl:text> </xsl:text>
            <xsl:value-of select="$root/lic_place"/>
        </xsl:if>
        <xsl:if test="$root/lic_end">
            <xsl:choose>
                <xsl:when test="$root/lic_end/unlimited='Yes'">
                    <xsl:text> бессрочно</xsl:text>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:text> Дата окончания действия: </xsl:text>
                    <xsl:call-template name="convertDate">
                        <xsl:with-param name="date" select="$root/lic_end/end_date" />
                    </xsl:call-template>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:if>
    </xsl:template>
    
    <xsl:template name="doc_notary">
        <xsl:param name="root"/>
        <xsl:text>Лицензия на право нотариальной деятельности: </xsl:text>
        <xsl:call-template name="licence_t">
            <xsl:with-param name="root" select="$root/licence" />
        </xsl:call-template>
        <xsl:text>; </xsl:text>
        <br/>
        <xsl:text>Приказ о назначении на должность: № </xsl:text>
        <xsl:value-of select="$root/order"/>
        <xsl:text> от </xsl:text>
        <xsl:call-template name="convertDate">
            <xsl:with-param name="date" select="$root/order_date" />
        </xsl:call-template>
    </xsl:template>
    
    
    <xsl:template name="repotForm">
        <xsl:param name="root"/>
	    <table class="identTable">
	       <tbody>
	           <tr>
	               <td class="headerIdentTable sizeData">
	                   <xsl:text>Форма предоставления отчета: </xsl:text>
	               </td>
	               <td class="sizeData rightColumnIdentTable">
	                  <xsl:call-template name="report_mode_et">
	                      <xsl:with-param name="type" select="$root/report_mode" />
	                  </xsl:call-template>
	                  <xsl:if test="$root/report_mode='PAPER' and $root/e_copy='Yes'">
	                      <xsl:text> (требуется электронная копия) </xsl:text>
	                  </xsl:if>
	               </td>
	           </tr>
	           <xsl:if test="$root/report_mode='PAPER'">
	             <tr>
	               <td class="leftColumnIdentTable">
	                   <xsl:text>Способ выдачи: </xsl:text>
	               </td>
	               <td class="sizeData rightColumnIdentTable">
	                   <xsl:call-template name="delivery_method_et">
	                       <xsl:with-param name="type"
	                           select="$root/report_delivery/delivery_method" />
	                   </xsl:call-template>
	               </td>
	               </tr>
	                  <xsl:if test="$root/report_delivery/delivery_place">
	                      <tr>
	                          <td class="leftColumnIdentTable">
	                              <xsl:text>Место выдачи: </xsl:text>
	                          </td>
	                          <td class="sizeData rightColumnIdentTable">
	                              <xsl:call-template name="delivery_place_et">
	                                  <xsl:with-param name="type"
	                                      select="$root/report_delivery/delivery_place" />
	                              </xsl:call-template>
	                          </td>
	                      </tr>
	                  </xsl:if>
	            </xsl:if>
	           <xsl:if test="not(normalize-space(./comment)='')">
	               <tr>
	                   <td class="leftColumnIdentTable">
	                       <xsl:text>Комментарий: </xsl:text>
	                   </td>
	                   <td class="sizeData rightColumnIdentTable">
	                       <xsl:value-of select="normalize-space(./comment)" />
	                   </td>
	               </tr>
	            </xsl:if>
	       </tbody>
	    </table>
    </xsl:template>  
</xsl:stylesheet>
