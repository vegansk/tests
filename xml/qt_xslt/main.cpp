#include <QXmlQuery>
#include <QDir>
#include <QFileInfo>
#include <QDesktopServices>
#include <QGuiApplication>
#include <QTemporaryFile>

QFileInfo generateHtml() {
  QTemporaryFile tf(QDir::temp().path() + "/testXXXXXXX.html");
  tf.setAutoRemove(false);
  tf.open();

  auto dataDir = QDir(QFileInfo(__FILE__).dir().path() + "/data/");

  QXmlQuery q(QXmlQuery::XSLT20);
  q.setFocus(QUrl::fromLocalFile(dataDir.filePath("data.xml")));
  q.setQuery(QUrl::fromLocalFile(dataDir.filePath("transform.xslt")));
  q.evaluateTo(&tf);

  return QFileInfo(tf.fileName());
}

int main(int argc, char *argv[]) {
  // Needed for QDesktopServices
  QGuiApplication app(argc, argv);

  auto fi = generateHtml();

  QDesktopServices::openUrl(QUrl::fromLocalFile(fi.filePath()));

  return 0;
}

