#include <iostream>
#include <string>
#include <tuple>
#include <functional>

using namespace std;

struct always_eq_t {};

template<typename T> bool operator == (const T &, always_eq_t) {
    return true;
}

template<typename T> bool operator == (always_eq_t, const T &) {
    return true;
}

int main(int argc, char *argv[]) {
    always_eq_t _;

    auto p = make_tuple(1, "two", 3.0);
    if(p ==  make_tuple(_, "two", _))
        cout << "1" << endl;
    else if(p == make_tuple(1, _, _) && get<2>(p) < 5.0)
        cout << "2" << endl;
    else
        cout << "Oops" << endl;

    return 0;
}

