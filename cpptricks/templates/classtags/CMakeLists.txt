set(APP_NAME classtags)

aux_source_directory(. SRC)

add_executable(${APP_NAME} ${SRC})
