#include <iostream>

struct built_in_type_tag {};
struct custom_type_tag {};

template<typename T> struct type_info {
    typedef custom_type_tag type_tag;
};

void _print_tag(built_in_type_tag) {
    std::cout << "This is a built-in type" << std::endl;
}

void _print_tag(custom_type_tag) {
    std::cout << "This is a custom type" << std::endl;
}

template<typename T> void print_tag() {
    _print_tag(typename type_info<T>::type_tag());
}

template<> struct type_info<int> {
    typedef built_in_type_tag type_tag;
};

template<> struct type_info<float> {
    typedef built_in_type_tag type_tag;
};

int main(int argc, char *argv[]) {

    std::cout << "Get type for int: ";
    print_tag<int>();

    std::cout << "Get type for float: ";
    print_tag<float>();

    std::cout << "Get type for ostream: ";
    print_tag<std::ostream>();

    return 0;
}
