#include <memory>
#include <map>
#include <typeinfo>
#include <typeindex>
#include <iostream>
#include <tuple>

using namespace std;

/*********************************************************
 * Library
 */

struct prototype{};
struct singleton{};

struct std_init{};
struct custom_init{};

class service_base {
public:
    virtual ~service_base() {}
};

template<typename service_cfg, typename service_base_class = service_base, template<typename X> class ptr = shared_ptr> class service_locator : private service_cfg {
public:
    template<class X> using service = typename service_cfg::template service<X>::type;
    template<class X> using scope = typename service_cfg::template service<X>::scope;
    template<class X> using init_type = typename service_cfg::template service<X>::init_type;

    template<typename svc> ptr<svc> get() {
        return _get<svc>(scope<svc>());
    }

private:

    template<typename svc> svc* _create(std_init) {
        return new service<svc>;
    }

    template<typename svc> svc* _create(custom_init) {
        return service_cfg::template service<svc>::create();
    }

    template<typename svc> ptr<svc> _get(prototype) {
        return ptr<svc>(_create<svc>(init_type<svc>()));
    }

    typedef map<type_index, ptr<service_base_class>> singletones;

    singletones _s;

    template<typename svc> ptr<svc> _get(singleton) {
        auto it = _s.find(type_index(typeid(svc)));
        if(_s.end() == it) {
            auto obj = _get<svc>(prototype());
            _s.insert(make_pair(type_index(typeid(svc)), dynamic_pointer_cast<service_base_class>(obj)));
            return obj;
        }

        return dynamic_pointer_cast<svc>((*it).second);
    }
};

class service_cfg_base {
public:
    template<typename svc> struct service {
        typedef void* type;
        typedef void scope;
        typedef void init_type;
    };
};

#define configure_service(cfg, svc, svcImpl, svcScope) template<> struct cfg::service<svc> { \
    typedef svcImpl type; \
    typedef svcScope scope; \
    typedef std_init init_type; \
    }

#define configure_service_with_params(cfg, svc, svcImpl, svcScope, ...) template<> struct cfg::service<svc> { \
    typedef svcImpl type; \
    typedef svcScope scope; \
    typedef custom_init init_type; \
    static svc* create() { \
        return new svcImpl{__VA_ARGS__}; \
    } \
    }

#define configure_service_with_func(cfg, svc, svcImpl, svcScope, func) template<> struct cfg::service<svc> { \
    typedef svcImpl type; \
    typedef svcScope scope; \
    typedef custom_init init_type; \
    static svc* create() { \
        return static_cast<svc*>((func())); \
    } \
    }

template<typename cfg> extern service_locator<cfg>& getServiceLocator();

template<typename svc, typename cfg> class service_ref {
private:
    shared_ptr<svc> _svc;
    service_locator<cfg> &_cfg;

public:
    service_ref(service_locator<cfg> &c = getServiceLocator<cfg>()) : _svc(nullptr), _cfg(c) {}
    ~service_ref() {}

    svc* get() {
        if(!_svc) {
            _svc = _cfg.template get<svc>();
        }
        return _svc.get();
    }

    svc* operator ->() {
        return get();
    }

    svc& operator *() {
        return *get();
    }
};

/*********************************************************
 * Example
 */

class Service : public service_base {
public:
    virtual ~Service() {}
    virtual void f() = 0;
};

class ServiceImpl : public Service {
private:
    string _msg;

public:
    ServiceImpl(const string &msg) : _msg(msg) {
        cout << "ServiceImpl constructor" << endl;
    }

    virtual ~ServiceImpl() {
        cout << "ServiceImpl destructor" << endl;
    }

public:
    virtual void f() {
        cout << "Hello from service: " << _msg << endl;
    }
};

class test_cfg : public service_cfg_base {};
configure_service_with_func(test_cfg, Service, ServiceImpl, singleton, []() {
    cout << "Initializing ServiceImpl" << endl;
    return new ServiceImpl("Hello!!!");
});

template<> service_locator<test_cfg>& getServiceLocator<test_cfg>() {
    static service_locator<test_cfg> cfg;
    return cfg;
}

template<typename Cfg>class ServiceUser {
private:
    service_ref<Service, Cfg> _service;

public:
    ServiceUser() {}

    void f() {
        _service->f();
    }
};

namespace app {
    using ServiceUser = ServiceUser<test_cfg>;
}

int main(int argc, char *argv[]) {

    app::ServiceUser u;

    u.f();

    return 0;
}

