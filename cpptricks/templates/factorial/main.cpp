#include <iostream>

using namespace std;

template<long x> struct Factorial {
    enum FactorialValue {
        value = x * Factorial<x-1>::value
    };
};

template<> struct Factorial<0> {
    enum FactorialValue {
        value = 1
    };
};

template<typename T, T x> struct Factorial2 {
    static const T value = x * Factorial2<T, x-1>::value;
};

template<> struct Factorial2<long, 0> {
    static const long value = 1;
};

template<> struct Factorial2<unsigned long long, 0> {
    static const unsigned long long value = 1;
};

int main(int argc, char *argv[]) {

    cout << "TMP based factorial" << endl;

    cout << "Factorial for 10 is " << Factorial<10>::value << endl;

    cout << "Factorial2 for 60 is " << Factorial2<unsigned long long, 60>::value << endl;

    return 0;
}
