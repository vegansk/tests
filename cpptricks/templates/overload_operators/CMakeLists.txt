set(APP_NAME overload_operators)

FILE(GLOB_RECURSE SRC  *.cpp *.h)

add_executable(${APP_NAME} ${SRC})
