#include "value.h"
#include "container.h"

#include <vector>
#include <typeinfo>

void testValue() {
    v::value<int> v{};
    v << 1 << 2 << 3;
}

void testContainer() {
    container<int,std::vector> v{std::vector<int>{1,2,3,4}};
    v << [](int x) { return x*2; };

    std::function<int(const int&)> f = [](const int& x) { return x*2; };
    v << f;
}

int main(int argc, char *argv[]) {
    testValue();
    testContainer();
    return 0;
}
