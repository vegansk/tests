#ifndef VALUE_H
#define VALUE_H

#include <iostream>

namespace v {

using std::cout;
using std::endl;

template<typename T> class value {
private:
    T v;

    void log() const {
        cout << "value(" << v << ")" << endl;
    }

public:
    value(): v{} {}
    value(const T& v): v{v} {
        log();
    }

    value(const value& v): v{v.v} {
        log();
    }

    ~value() {}

    const value& operator = (const value& _v) {
        *this = _v.v;
        return *this;
    }

    const value& operator = (const T& _v) {
        v = _v;
        log();
        return *this;
    }

    T& get() { return v; }
    const T& get() const { return v; }
};

template<typename T> value<T>& operator << (value<T>& v, T d) {
    v = d;
    return v;
}

}

#endif // VALUE_H
