#ifndef CONTAINER_H
#define CONTAINER_H

#include <memory>

using std::cout;
using std::endl;

template<typename T, template<typename,typename=std::allocator<T>> class C> class container {
public:
    using c_t = C<T>;

private:
    c_t c;

    void log() {
        cout << "container(";
        for(auto it = c.begin(); it != c.end(); ++it) {
            if(it != c.begin()) cout << ",";
            cout << *it;
        }
        cout << ")" << endl;
    }

public:
    container(): c{} {}
    container(const container& c): c{c.c} { log(); }
    container(container&& c): c{std::move(c.c)} { log(); }
    container(const c_t& c): c{c} { log(); }

    ~container() {}

    c_t& get() { return c; }
    const c_t& get() const { return c; }

    const container& operator = (const container& _c) {
        *this = _c.c;
        return *this;
    }

    const container& operator = (const c_t& _c) {
        c = _c;
        log();
        return *this;
    }
};

template<typename T,
         template<typename,typename=std::allocator<T>> class C,
         typename F>
auto operator << (container<T,C>& c1, F f) -> container<decltype(f(T{})),C> {
    C<decltype(f(T{}))> c2{};

    auto it = std::inserter(c2, c2.begin());

    for(const T& t: c1.get()) {
        (*it++) = f(t);
    }
    return container<decltype(f(T{})),C>{c2};
}

#endif // CONTAINER_H
